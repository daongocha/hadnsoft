program ProjectCWModel;

uses
  Vcl.Forms,
  MainForm in 'Forms\MainForm.pas' {FormCW},
  UnitModeleCalcul in '..\..\NonLinear_CW_Model\Units\UnitModeleCalcul.pas',
  UnitModeleUsureCasingHa in '..\..\NonLinear_CW_Model\Units\UnitModeleUsureCasingHa.pas',
  UnitNewCasingWearModelStudy in '..\..\NonLinear_CW_Model\Units\UnitNewCasingWearModelStudy.pas',
  UnitsMathTools in '..\..\NonLinear_CW_Model\Units\UnitsMathTools.pas',
  UnitModeleStructureBasic in '..\..\NonLinear_CW_Model\Units\UnitModeleStructureBasic.pas',
  UnitSimpleDialogs in '..\..\..\GUI_Utinity_Units\UnitSimpleDialogs.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFormCW, FormCW);
  Application.Run;
end.
