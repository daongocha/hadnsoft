unit MainForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls,
  VclTee.TeeGDIPlus, VCLTee.TeEngine, VCLTee.TeeProcs, VCLTee.Chart,
  VCLTee.Series, UnitModeleStructureBasic, System.Actions, Vcl.ActnList, Vcl.ExtDlgs;

type
  TFormCW = class(TForm)
    PanelInput: TPanel;
    LabelCasingData: TLabel;
    LabelCasingExternalDiameter: TLabel;
    LabelCasingInternalDiameter: TLabel;
    LabelToolJoinExternalDiameter: TLabel;
    LabelToolJoinData: TLabel;
    LabelWearData: TLabel;
    LabelASlope: TLabel;
    LabelBPClim: TLabel;

    EditCasingExternalDiameter: TEdit;
    EditCasingInternalDiameter: TEdit;
    EditToolJoinExternalDiameter: TEdit;
    EditForce: TEdit;
    EditA: TEdit;
    EditB: TEdit;
    EditC: TEdit;
    LabelCWFlim: TLabel;
    LabelTestData: TLabel;
    LabelForce: TLabel;

    ButtonCal: TButton;
    LabelUnitCsgExtDia: TLabel;
    LabelUnitCsgIntDia: TLabel;
    LabelUnitTjExtDia: TLabel;
    LabelUnitTestForce: TLabel;
    LabelUnitAWearLaw: TLabel;
    LabelUnitBWearLaw: TLabel;
    LabelUnitCWearLaw: TLabel;
    ButtonGetDataFile: TButton;
    OpenDialogGetDataFile: TOpenDialog;
    Panel1: TPanel;
    LabelXAXisTest: TLabel;
    ComboBoxXAXisTest: TComboBox;
    LabelYAxisTest: TLabel;
    ComboBoxYAxisTest: TComboBox;
    ButtonLoadTestResult: TButton;
    OpenDialogTestResult: TOpenDialog;
    ChartTestResult: TChart;
    LabelTestForceUSUnit: TLabel;
    LabelCsgExtDiaUSUnit: TLabel;
    LabelCsgIntDiaUSUnit: TLabel;
    LabelTjExtDiaUSUnit: TLabel;
    SeriesCurveFitTest: TFastLineSeries;
    Panel2: TPanel;
    ChartModel: TChart;
    ComboBoxXaxisChart2: TComboBox;
    ComboBoxYaxisChart2: TComboBox;
    LabelXaxisChart2: TLabel;
    LabelYaxisChart2: TLabel;
    EditRCurveFit: TEdit;
    EditQCurveFit: TEdit;
    EditPCurveFit: TEdit;
    LabelPCurveFit: TLabel;
    LabelRCurveFit: TLabel;
    LabelQCurveFit: TLabel;
    MemoCurveFir: TMemo;
    ButtonFitCurve: TButton;
    EditRsquare: TEdit;
    LabelRsquare: TLabel;
    SeriesTreat3Points: TPointSeries;
    SeriesTreatCurveFit: TPointSeries;
    SeriesWearTestResult: TLineSeries;
    LabelYAXisTestUnit: TLabel;
    LabelXAXisTestUnit: TLabel;
    LabelXaxisChart2Unit: TLabel;
    LabelYaxisChart2Unit: TLabel;
    SeriesProposedModel: TFastLineSeries;
    ButtonExportCWModel: TButton;
    SaveDialogModelParameters: TSaveDialog;
    CheckBoxPlateOrCurvedContactLength: TCheckBox;
    LabelThres: TLabel;
    EditThres: TEdit;
    LabelUnitThres: TLabel;
    ButtonInfo: TButton;

    procedure EditCasingExternalDiameterChange(Sender: TObject);
    procedure EditCasingInternalDiameterChange(Sender: TObject);
    procedure EditToolJoinExternalDiameterChange(Sender: TObject);
    procedure EditAChange(Sender: TObject);
    procedure EditBChange(Sender: TObject);
    procedure EditCChange(Sender: TObject);
    procedure ComboBoxTestChange(Sender: TObject);
    procedure ComboBoxTreatChange(Sender: TObject);
    procedure ButtonLoadTestResultClick(Sender: TObject);
    procedure EditForceChange(Sender: TObject);
    procedure ButtonCalClick(Sender: TObject);
    procedure EditLinearWFChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ButtonGetDataFileClick(Sender: TObject);
    procedure ButtonFitCurveClick(Sender: TObject);
    procedure ButtonExportCWModelClick(Sender: TObject);
    procedure ButtonInfoClick(Sender: TObject);
    // procedure ChartContextPopup(Sender: TObject; MousePos: TPoint; var Handled: Boolean);

  type
    {$SCOPEDENUMS ON}
    TData = (Work, WearVolume, WearHeight, WearLength);
    {$SCOPEDENUMS OFF}

  private
    FCasingExternalDiameter, FCasingInternalDiameter, FToolJoinExternalDiameter, FForce,
    FA, FB, FC, FThres : Double; // Parameters of New Casing Wear Model
    FLinWF : Double;
    //
    FWorkArray, FWearVolumeArray, FWearHeightArray, FWearLengthArray : t_vdd;
    // Test Result
    FTestWorkArray, FTestWearVolumeArray, FTestWearHeightArray, FTestWearLengthArray : t_vdd;
    FDerivativeWearVolumeArray, FPressureContactArray : t_vdd;
    FTestWearVolumeArrayFit, FDerivativeWearVolumeArrayFit : t_vdd;
    // Curve Fitting Test
    FACurveFit, FBCurveFit, FCCurveFit : Double;
    FCurveFitWorkArray, FCurveFitWearVolumeArray, FCurveFitWearHeightArray, FCurveFitWearLengthArray : t_vdd;
    FCurveFitDerivativeWearVolumeArray, FCurveFitPressureContactArray : t_vdd;
    // Proposed Model DrillScan
    FWFproposed, FPCproposed : t_vdd;
    //
    procedure RenderChartTestResult();
    procedure RenderChartCurveFit();
    procedure RenderChartTreatResult();
    procedure FitCurveTest();
    procedure TreatTestResults();
    procedure LabelChartUnits();
    procedure RefreshResultArrays();
    procedure WriteResults3PointsMethod(FilePath : string);
    procedure WriteResultsCurveFitMethod(FilePath : string);
    function TestInputParameters() : Boolean;

  public
    { D�clarations publiques }
    procedure ReadCorFactor(Var LisWearPercent, LisCorFactor : t_vdd);
  end;
  // Procedures
  procedure RecontructVolWorkCurveNewModel (RextCsg, RintCsg, Rtjt : Double;
                                            F : Double;
                                            A, B, PClim : Double;
                                            WFlin : Double;
                                            LarMax : Double;
                                            nb_points : Integer;
                                            NomFic: string);

var
  FormCW: TFormCW;

implementation

uses
  Math,
  UnitNewCasingWearModelStudy,
  UnitModeleUsureCasingHa,
  UnitsMathTools,
  UnitSimpleDialogs;

{$R *.dfm}

//----------------------------------------------------------------------------//
procedure TFormCW.RenderChartTestResult();
var
  XData, YData : t_vdd_p;
  i : Integer;
begin
  SeriesWearTestResult.Clear();
 
  if(ComboBoxXAXisTest.ItemIndex >= 0) and
    (ComboBoxYAxisTest.ItemIndex >= 0)
  then
  begin
    // Initialisation
    XData := nil;
    YData := nil;
    //
    case ComboBoxXAXisTest.ItemIndex of
      0:
      begin
        XData := @FTestWorkArray;
      end;
      1:
      begin
        XData := @FTestWearVolumeArray;
      end;
      2:
      begin
        XData := @FTestWearHeightArray;
      end;
      3:
      begin
        XData := @FTestWearLengthArray;
      end;
    end;
    //
    case ComboBoxYAXisTest.ItemIndex of
      0:
      begin
        YData := @FTestWorkArray;
      end;
      1:
      begin
        YData := @FTestWearVolumeArray;
      end;
      2:
      begin
        YData := @FTestWearHeightArray;
      end;
      3:
      begin
        YData := @FTestWearLengthArray;
      end;
    end;
    //
    if (Assigned(XData) and
        Assigned(YData) and
        (Length(XData^)>1) and
        (Length(YData^)>1)) then
    begin
      for i := 0 to Min(Length(XData^)-1, Length(YData^)-1) do
        SeriesWearTestResult.AddXY(XData^[i], YData^[i]);
    end;
  end;
end;

//----------------------------------------------------------------------------//
procedure TFormCW.RenderChartCurveFit();
var
  XData1, YData1 : t_vdd_p;
  i : Integer;
begin
  SeriesCurveFitTest.Clear();

  if(ComboBoxXAXisTest.ItemIndex >= 0) and
    (ComboBoxYAxisTest.ItemIndex >= 0)
  then
  begin
    // Initialisation
    XData1 := nil;
    YData1 := nil;
    //
    case ComboBoxXAXisTest.ItemIndex of
      0:
      begin
        XData1 := @FCurveFitWorkArray;
      end;
      1:
      begin
        // Nothing
      end;
      2:
      begin
        // Nothing
      end;
      3:
      begin
        // Nothing
      end;
    end;
    //
    case ComboBoxYAXisTest.ItemIndex of
      0:
      begin
        // Nothing
      end;
      1:
      begin
        YData1 := @FCurveFitWearVolumeArray;
      end;
      2:
      begin
        // Nothing
      end;
      3:
      begin
        // Nothing
      end;
    end;
    //
    if (Assigned(XData1) and
        Assigned(YData1) and
        (Length(XData1^)>1) and
        (Length(YData1^)>1)) then
    begin
      for i := 0 to Min(Length(XData1^)-1, Length(XData1^)-1) do
        SeriesCurveFitTest.AddXY(XData1^[i], YData1^[i]);
    end;
  end;
end;

//----------------------------------------------------------------------------//
procedure TFormCW.RenderChartTreatResult();
var
  XData1, YData1 : t_vdd_p;
  XData2, YData2 : t_vdd_p;
  XData3, YData3 : t_vdd_p;
  i : Integer;
begin
  // Initialisation Series
  SeriesTreat3Points.Clear();
  SeriesTreatCurveFit.Clear();
  SeriesProposedModel.Clear();
  //
  if(ComboBoxXaxisChart2.ItemIndex >= 0) and
    (ComboBoxYaxisChart2.ItemIndex >= 0)
  then
  begin
    // Initialisation
    XData1 := nil;
    YData1 := nil;
    XData2 := nil;
    YData2 := nil;
    XData3 := nil;
    YData3 := nil;
    //FWFproposed, FPCproposed
    case ComboBoxXaxisChart2.ItemIndex of
      0:  // Contact Pressure
      begin
        XData1 := @FPressureContactArray;
        XData2 := @FCurveFitPressureContactArray;
        XData3 := @FPCproposed;
      end;
      1:
      begin
        XData1 := @FTestWorkArray;
        XData2 := @FCurveFitWorkArray;
        // XData3 := nil;
      end;
      2:
      begin
        XData1 := @FTestWearVolumeArray;
        XData2 := @FCurveFitWearVolumeArray;
        // XData3 := nil;
      end;
      3:
      begin
        XData1 := @FTestWearHeightArray;
        XData2 := @FCurveFitWearHeightArray;
        // XData3 := nil;
      end;
      4:
      begin
        XData1 := @FTestWearLengthArray;
        XData2 := @FCurveFitWearLengthArray;
        // XData3 := nil;
      end;
    end;
    //
    case ComboBoxYaxisChart2.ItemIndex of
      0:  // Differential Wear Factor
      begin
        YData1 := @FDerivativeWearVolumeArray;
        YData2 := @FCurveFitDerivativeWearVolumeArray;
        YData3 := @FWFproposed
      end;
      1:  // Contact Pressure
      begin
        YData1 := @FPressureContactArray;
        YData2 := @FCurveFitPressureContactArray;
        // YData3 := nil;
      end;
    end;
    //
    if (Assigned(XData1) and
        Assigned(YData1) and
        (Length(XData1^)>1) and
        (Length(YData1^)>1)) then
    begin
      for i := 0 to min(Length(XData1^)-1,Length(YData1^)-1) do begin
        if ((abs(XData1^[i]+999)>1E-6) and (abs(YData1^[i]+999)>1E-6)) then
        SeriesTreat3Points.AddXY(XData1^[i], YData1^[i]);
      end;
    end;
    //
    if (Assigned(XData2) and
        Assigned(YData2) and
        (Length(XData2^)>1) and
        (Length(YData2^)>1)) then
    begin
      for i := 0 to min(Length(XData2^)-1,Length(YData2^)-1) do begin
        if ((abs(XData2^[i]+999)>1E-6) and (abs(YData2^[i]+999)>1E-6)) then
        SeriesTreatCurveFit.AddXY(XData2^[i], YData2^[i]);
      end;
    end;
    //
    if (Assigned(XData3) and
        Assigned(YData3) and
        (Length(XData3^)>1) and
        (Length(YData3^)>1)) then
    begin
      for i := 0 to min(Length(XData3^)-1,Length(YData3^)-1) do begin
        if ((abs(XData3^[i]+999)>1E-6) and (abs(YData3^[i]+999)>1E-6)) then
        SeriesProposedModel.AddXY(XData3^[i], YData3^[i]);
      end;
    end;
  end;
end;

//----------------------------------------------------------------------------//
procedure TFormCW.ButtonCalClick(Sender: TObject);
begin
  if TestInputParameters()=True then
  FitCurveTest();
  TreatTestResults();
  // Graph - Chart
  RenderChartTestResult();
  // Defaut Value ComboBox
  ComboBoxXaxisChart2.ItemIndex :=0;
  ComboBoxYaxisChart2.ItemIndex :=0;
  RenderChartTreatResult();
end;


//----------------------------------------------------------------------------//
procedure TFormCW.ButtonExportCWModelClick(Sender: TObject);
var
  fic : TextFile;
  Path : string;
  i, n : Integer;
  LarMax, WFlin : Double;
begin
  if SaveDialogModelParameters.Execute() then
  begin
    // ODcsg(in)	IDcsg(in)	ODtj(in)	WF(E-10/psi)	TestFor(lb/in)	A(1/psi2)	B(psi)	C(1/psi)
    // FCasingExternalDiameter, FCasingInternalDiameter, FToolJoinExternalDiameter, FForce,
    assignFile(fic, SaveDialogModelParameters.Filename);
    rewrite(fic);
    writeln(fic, '  ODcsg(m)    IDcsg(m)   ODtj(m)	   TestFor(N/m)             A(1/Pa2)                   B(Pa)                    C(1/Pa)                      WF(1/Pa)');
    writeln(fic, ' ', FCasingExternalDiameter:10:8,
                 ' ', FCasingInternalDiameter:10:8,
                 ' ', FToolJoinExternalDiameter:10:8,
                 ' ', FForce:20,
                 ' ', FA:25,
                 ' ', FB:25,
                 ' ', FC:25,
                 ' --- ');
    writeln(fic, '  ODcsg(in)	  IDcsg(in)	ODtj(in)		TestFor(lb/in)            A(1/psi2)		              B(psi)                    C(1/psi)                    WF(1/psi)');
    writeln(fic, ' ', FCasingExternalDiameter/0.0254:10:8,
                 ' ', FCasingInternalDiameter/0.0254:10:8,
                 ' ', FToolJoinExternalDiameter/0.0254:10:8,
                 ' ', FForce*0.005710147:20,
                 ' ', FA*sqr(6894.75729):25,
                 ' ', FB*0.000145038:25,
                 ' ', FC*6894.75729:25,
                 ' --- ');
    closeFile(fic);

    Path  := ExtractFilePath(SaveDialogModelParameters.Filename);
    assignFile(fic, Path+'CWmodel_TestResult.txt');
    rewrite(fic);
    n := length(FTestWorkArray);
    writeln(fic, '  Work[N.m/m] Volume[m3/m] Height[m] Length[m] DerivWearVol[1/Pa] PressureContact[Pa] VolumeFit[m3/m] DerivWearVolFit[1/Pa] ');
    for i  := 1 to n do
    begin
    writeln(fic, ' ', FTestWorkArray[i-1]:15,
                 ' ', FTestWearVolumeArray[i-1]:15,
                 ' ', FTestWearHeightArray[i-1]:15,
                 ' ', FTestWearLengthArray[i-1]:15,
                 ' ', FDerivativeWearVolumeArray[i-1]:15,
                 ' ', FPressureContactArray[i-1]:15,
                 ' ', FTestWearVolumeArrayFit[i-1]:15,
                 ' ', FDerivativeWearVolumeArrayFit[i-1]:15);
    end;
    closeFile(fic);
    LarMax := FTestWearLengthArray[n-1]*1.1;
    WFlin :=  FTestWearVolumeArray[n-1]/FTestWorkArray[n-1];
    //
    assignFile(fic, Path+'CWmodel_DSDetailedResult.txt');
    rewrite(fic);
    n := length(FCurveFitWorkArray);
    writeln(fic, '  WorkFit[N.m/m] VolumeFit[m3/m] HeightFit[m] LengthFit[m] DerivWearVolFit[1/Pa] PressContactFit[Pa] ');
    for i  := 1 to n do
    begin
    writeln(fic, ' ', FCurveFitWorkArray[i-1]:15,
                 ' ', FCurveFitWearVolumeArray[i-1]:15,
                 ' ', FCurveFitWearHeightArray[i-1]:15,
                 ' ', FCurveFitWearLengthArray[i-1]:15,
                 ' ', FCurveFitDerivativeWearVolumeArray[i-1]:15,
                 ' ', FCurveFitPressureContactArray[i-1]:15);
    end;
    closeFile(fic);
    //
    assignFile(fic, Path+'CWmodel_DSWFdifVsPC.txt');
    rewrite(fic);
    n := length(FWFproposed);
    writeln(fic, '  WFproposed[1/Pa] FPCproposed[Pa] ');
    for i  := 1 to n do
    begin
    writeln(fic, ' ', FWFproposed[i-1]:15,
                 ' ', FPCproposed[i-1]:15);
    end;
    closeFile(fic);
    //
    RecontructVolWorkCurveNewModel (FCasingExternalDiameter/2.0, FCasingInternalDiameter/2.0, FToolJoinExternalDiameter/2.0,
                                    FForce, FA, FThres, FB, WFlin, LarMax, 100,
                                    Path+'CWmodel_ReconstrutedCurve.txt');
  end;

//      // Test
//    FTestWorkArray, FTestWearVolumeArray, FTestWearHeightArray, FTestWearLengthArray : t_vdd;
//    FDerivativeWearVolumeArray, FPressureContactArray : t_vdd;
//    FTestWearVolumeArrayFit, FDerivativeWearVolumeArrayFit : t_vdd;
//
//    // Curve Fitting Test
//    FACurveFit, FBCurveFit, FCCurveFit : Double;
//    FCurveFitWorkArray, FCurveFitWearVolumeArray, FCurveFitWearHeightArray, FCurveFitWearLengthArray : t_vdd;
//    FCurveFitDerivativeWearVolumeArray, FCurveFitPressureContactArray : t_vdd;
//
//    // Proposed Model DrillScan
//    FWFproposed, FPCproposed : t_vdd;



end;

procedure TFormCW.ButtonFitCurveClick(Sender: TObject);
begin
  FitCurveTest();
end;

//----------------------------------------------------------------------------//
procedure TFormCW.ButtonGetDataFileClick(Sender: TObject);
var
	fic : TextFile;
  i : Integer;
  DextCsg, DintCsg, DextTj, Force : Double;
begin
  if OpenDialogGetDataFile.Execute() then
  begin
    //  Load correction factor from path
    AssignFile(fic, OpenDialogGetDataFile.Filename);
    reset(fic);
    ReadLn(fic);
    ReadLn(fic, DextCsg, DintCsg, DextTj, Force);
    CloseFile(fic);
    if DextCsg>0 then
    begin
      FCasingExternalDiameter := DextCsg;
      EditCasingExternalDiameter.Text := Floattostr(FCasingExternalDiameter);
      LabelCsgExtDiaUSUnit.Caption := '('+FloatToStrF(FCasingExternalDiameter/0.0254, ffGeneral, 6, 3)+' in)';
      LabelCsgExtDiaUSUnit.Visible := true;
    end;
    if DintCsg>0 then
    begin
      FCasingInternalDiameter := DintCsg;
      EditCasingInternalDiameter.Text := Floattostr(FCasingInternalDiameter);
      LabelCsgIntDiaUSUnit.Caption := '('+FloatToStrF(FCasingInternalDiameter/0.0254, ffGeneral, 6, 3)+' in)';
      LabelCsgIntDiaUSUnit.Visible := true;
    end;
    if DextTj>0 then
    begin
      FToolJoinExternalDiameter := DextTj;
      EditToolJoinExternalDiameter.Text := Floattostr(FToolJoinExternalDiameter);
      LabelTjExtDiaUSUnit.Caption := '('+FloatToStrF(FToolJoinExternalDiameter/0.0254, ffGeneral, 6, 3)+' in)';
      LabelTjExtDiaUSUnit.Visible := true;
    end;
    //
    if Force>0 then
    begin
      FForce := Force;
      EditForce.Text := Floattostr(FForce);
      // lb/ft to N/m 14.59390289
      LabelTestForceUSUnit.Caption := '('+FloatToStrF(FForce/14.59390289, ffGeneral, 6, 1)+' lbf/ft)';
      LabelTestForceUSUnit.Visible := true;
    end;
  end;
  // ShowMessage('aaa ' + FloatToStr(FLisWearPercent[15]) + '  ' + FloatToStr(FLisCorFactor[15]));
end;


procedure TFormCW.ButtonInfoClick(Sender: TObject);
var
  LCaption: String;
  LTitle: String;
  LText: String;
begin
  LCaption := 'Infomation';
  LTitle := 'Casing Test Data Software';
  LText := 'Curve Fitting for Non-Linear Model' + #13#10 +
           'Developed by Ngoc Ha DAO' + #13#10 +
           '<ngoc-ha.dao@hpinc.com>' + #13#10 +
           'Copyright (c) 2022 H&P DrillScan' + #13#10 +
           'All rights reserved.';
           // '<daongocha@gmail.com>';
  TaskSimpleMessage(LCaption, LTitle, LText);
end;
//----------------------------------------------------------------------------//
procedure TFormCW.ButtonLoadTestResultClick(Sender: TObject);
var
  DextCsg, DintCsg, DextTj, Force : Double;
	fic : TextFile;
  i : Integer;
begin
  if OpenDialogTestResult.Execute() then
  begin
    AssignFile(fic, OpenDialogTestResult.Filename);
    reset(fic);
    ReadLn(fic);
    ReadLn(fic, DextCsg, DintCsg, DextTj, Force);
    if DextCsg>0 then
    begin
      FCasingExternalDiameter := DextCsg;
      EditCasingExternalDiameter.Text := Floattostr(FCasingExternalDiameter);
      LabelCsgExtDiaUSUnit.Caption := '('+FloatToStrF(FCasingExternalDiameter/0.0254, ffGeneral, 6, 3)+' in)';
      LabelCsgExtDiaUSUnit.Visible := true;
    end;
    if DintCsg>0 then
    begin
      FCasingInternalDiameter := DintCsg;
      EditCasingInternalDiameter.Text := Floattostr(FCasingInternalDiameter);
      LabelCsgIntDiaUSUnit.Caption := '('+FloatToStrF(FCasingInternalDiameter/0.0254, ffGeneral, 6, 3)+' in)';
      LabelCsgIntDiaUSUnit.Visible := true;
    end;
    if DextTj>0 then
    begin
      FToolJoinExternalDiameter := DextTj;
      EditToolJoinExternalDiameter.Text := Floattostr(FToolJoinExternalDiameter);
      LabelTjExtDiaUSUnit.Caption := '('+FloatToStrF(FToolJoinExternalDiameter/0.0254, ffGeneral, 6, 3)+' in)';
      LabelTjExtDiaUSUnit.Visible := true;
    end;
    //
    if Force>0 then
    begin
      FForce := Force;
      EditForce.Text := Floattostr(FForce);
      // lb/ft to N/m 14.59390289
      LabelTestForceUSUnit.Caption := '('+FloatToStrF(FForce/14.59390289, ffGeneral, 6, 1)+' lbf/ft)';
      LabelTestForceUSUnit.Visible := true;
    end;
    // Reset tables
    SetLength(FTestWorkArray, 0);
    SetLength(FTestWearVolumeArray, 0);
    SetLength(FTestWearHeightArray, 0);
    SetLength(FTestWearLengthArray, 0);
    // Lecture
    ReadLn(fic);
    i := 0;
    repeat
      setlength(FTestWorkArray, i+1);
      setlength(FTestWearVolumeArray, i+1);
      ReadLn(fic, FTestWorkArray[i], FTestWearVolumeArray[i]);
      i := i+1;
    until (Eof(fic));
    CloseFile(fic);
    RefreshResultArrays();
    RenderChartTestResult();
  end;
end;


procedure TFormCW.ComboBoxTestChange(Sender: TObject);
begin
  LabelChartUnits();
  RenderChartTestResult();
end;

procedure TFormCW.ComboBoxTreatChange(Sender: TObject);
begin
  LabelChartUnits();
  RenderChartTreatResult();
end;


procedure TFormCW.FitCurveTest();
var
  i, n, n1 : Integer;
  X, Y : t_vdd;
  XC, YC : Double;
  Ai, Bi, Ci : Double;
  Afit, Bfit, Cfit : Double;  // Y : E+9N.m/m  ;  X : E-6m3/m

  AmodelCW, BmodelCW, CmodelCW, ThresModelCW : Double;
  SST, SSR, SSE, Rsqua : Double;
  Y_Reg : t_vdd;
begin
  if ((length(FTestWorkArray)>1) and(length(FTestWearVolumeArray)>1)) then
  begin
    n := min(length(FTestWorkArray), length(FTestWearVolumeArray));
    setlength(X, n);
    setlength(Y, n);
    XC := FTestWorkArray[n-1]/10;
    YC := FTestWearVolumeArray[n-1]/10;
    // XC := 1E+9;
    // YC := 1E-6;
    for i := 0 to n-1 do
    begin
      X[i] := FTestWorkArray[i]/XC;
      Y[i] := FTestWearVolumeArray[i]/YC;
    end;
    Ai := Y[n-1];
    Bi := 1.0;
    Ci := 1.0;
    Curve_Fitting_CasingWear_LevenMarqua(n, X, Y, Ai, Bi, Ci, Afit, Bfit, Cfit,
                                         SST, SSR, SSE, Rsqua,
                                         Y_Reg);
    // Afit, Bfit, Cfit    -> // Y : YC  ;  X : XC
    FACurveFit := Afit*YC;
    FBCurveFit := Bfit/power(XC,Cfit);
    FCCurveFit := Cfit;
    // Visualisation
    EditPCurveFit.Text := FloatToStrF(FACurveFit, ffExponent, 11, 2);
    EditQCurveFit.Text := FloatToStrF(FBCurveFit, ffExponent, 11, 2);
    EditRCurveFit.Text := FloatToStrF(FCCurveFit, ffExponent, 11, 2);
    EditRsquare.Text := FloatToStrF(Rsqua, ffGeneral, 9, 6);
    //
    setlength(FTestWearVolumeArrayFit, n);
    setlength(FDerivativeWearVolumeArrayFit, n);
    for i := 0 to n-1 do
    begin
      FTestWearVolumeArrayFit[i] := FACurveFit*(1.0-exp(-FBCurveFit*power(FTestWorkArray[i],FCCurveFit)));
      if FTestWorkArray[i]<1E-15 then FDerivativeWearVolumeArrayFit[i] := -999
      else FDerivativeWearVolumeArrayFit[i] := FACurveFit*exp(-FBCurveFit*power(FTestWorkArray[i],FCCurveFit))*FBCurveFit*FCCurveFit*power(FTestWorkArray[i],FCCurveFit-1);
    end;
    // FCurveFitWorkArray || 0 --> FTestWorkArray[n-1] : (n1+1) points
    n1 := 100;
    setlength(FCurveFitWorkArray, n1+1);
    setlength(FCurveFitWearVolumeArray, n1+1);
    setlength(FCurveFitDerivativeWearVolumeArray, n1+1);
    for i := 0 to n1 do
    begin
      FCurveFitWorkArray[i] := i*FTestWorkArray[n-1]/n1;
      FCurveFitWearVolumeArray[i] := FACurveFit*(1.0-exp(-FBCurveFit*power(FCurveFitWorkArray[i],FCCurveFit)));
      if FCurveFitWorkArray[i]<1E-15 then FCurveFitDerivativeWearVolumeArray[i] := -999
      else FCurveFitDerivativeWearVolumeArray[i] := FACurveFit*exp(-FBCurveFit*power(FCurveFitWorkArray[i],FCCurveFit))*FBCurveFit*FCCurveFit*power(FCurveFitWorkArray[i],FCCurveFit-1);
    end;
    //
    if (not(Double.IsNan(FCasingInternalDiameter) or
            Double.IsNan(FToolJoinExternalDiameter) or
            Double.IsNan(FForce))) then
    begin
      If CheckBoxPlateOrCurvedContactLength.Checked then
      begin
        CalLarHauPCFromWearVol_PlateContactLength (FCurveFitWearVolumeArray,
                             FCasingInternalDiameter, FToolJoinExternalDiameter, FForce,
                             FCurveFitWearLengthArray, FCurveFitWearHeightArray, FCurveFitPressureContactArray);
      end else
      begin
        CalLarHauPCFromWearVol_CurvedContactLength (FCurveFitWearVolumeArray,
                             FCasingInternalDiameter, FToolJoinExternalDiameter, FForce,
                             FCurveFitWearLengthArray, FCurveFitWearHeightArray, FCurveFitPressureContactArray);
      end;
    end;
    // Proposed Parameters of New Casing Wear Model
    setlength(X, n1);
    setlength(Y, n1);
    for i := 1 to n1 do
    begin
      X[i-1] := FCurveFitPressureContactArray[i];
      Y[i-1] := FCurveFitDerivativeWearVolumeArray[i];
    end;
    // Y = AmodelCW * X + ThresModelCW;
    Regression_Linear(n1, X, Y, AmodelCW, ThresModelCW,
                      SST, SSR, SSE, Rsqua);

    CmodelCW := (FDerivativeWearVolumeArrayFit[n-1]+FDerivativeWearVolumeArrayFit[n-1])/2;  // WFlimite
    BmodelCW := CmodelCW/AmodelCW-ThresModelCW/AmodelCW;  // PClimite
    // Y = AmodelCW * (X - ThresModelCW);
    ThresModelCW := -ThresModelCW/AmodelCW;
    // FloatToStrF(AAA, ffExponent, 9, 2);
    EditA.Text := FloatToStrF(AmodelCW*1E19, ffGeneral, 9, 6);
    EditB.Text := FloatToStrF(BmodelCW/1E6, ffGeneral, 9, 6);
    EditC.Text := FloatToStrF(CmodelCW*1E14, ffGeneral, 9, 6);
    EditThres.Text := FloatToStrF(ThresModelCW/1E6, ffGeneral, 9, 6);
    FA := AmodelCW;
    FB := BmodelCW;
    FC := CmodelCW;
    FThres := ThresModelCW;
    // FWFproposed, FPCproposed
    setlength(FWFproposed, 10);
    setlength(FPCproposed, 10);
    FPCproposed[0] := 0.0;
    FWFproposed[0] := CmodelCW;
    for i := 1 to 10-1 do
    begin
      FPCproposed[i] := BmodelCW+(i-1)*(FCurveFitPressureContactArray[1]-BmodelCW)/(10-1);
      FWFproposed[i] := AmodelCW*(FPCproposed[i]-BmodelCW)+CmodelCW;
    end;
    WriteResultsCurveFitMethod('Outputs/CurveFitResults.txt');
    RenderChartCurveFit();
  end;
end;

//----------------------------------------------------------------------------//
procedure TFormCW.TreatTestResults();
begin
  // Casing Wear DrillScan Model
  if ((length(FTestWorkArray)>2) and
      (length(FTestWearVolumeArray)>2) and
       not(Double.IsNan(FCasingInternalDiameter) or
           Double.IsNan(FToolJoinExternalDiameter) or
           Double.IsNan(FForce)))
  then
  begin
    RegressionWorkWear(FTestWorkArray, FTestWearVolumeArray, 3, FDerivativeWearVolumeArray);
    //
    If CheckBoxPlateOrCurvedContactLength.Checked then
    begin
      CalLarHauPCFromWearVol_PlateContactLength(FTestWearVolumeArray,
                         FCasingInternalDiameter, FToolJoinExternalDiameter, FForce,
                         FTestWearLengthArray, FTestWearHeightArray, FPressureContactArray);
    end else
    begin
      CalLarHauPCFromWearVol_CurvedContactLength (FTestWearVolumeArray,
                         FCasingInternalDiameter, FToolJoinExternalDiameter, FForce,
                         FTestWearLengthArray, FTestWearHeightArray, FPressureContactArray);
    end;
    //
    WriteResults3PointsMethod('Outputs/DeriveeWearVol_PC.txt');
    end;
  // RenderChartTestResult();
end;

//----------------------------------------------------------------------------//
procedure TFormCW.LabelChartUnits();
begin
  if (ComboBoxXAXisTest.ItemIndex >= 0) then
  begin
    case ComboBoxXAXisTest.ItemIndex of
      0: LabelXAXisTestUnit.Caption := 'N.m/m';
      1: LabelXAXisTestUnit.Caption := 'm^3/m';
      2: LabelXAXisTestUnit.Caption := 'm';
      3: LabelXAXisTestUnit.Caption := 'm';
    end;
    LabelXAXisTestUnit.Visible := true;
  end else
  begin
    LabelXAXisTestUnit.Caption := 'Unit';
    LabelXAXisTestUnit.Visible := False;
  end;
  //
  if (ComboBoxYAXisTest.ItemIndex >= 0) then
  begin
    case ComboBoxYAXisTest.ItemIndex of
      0: LabelYAXisTestUnit.Caption := 'N.m/m';
      1: LabelYAXisTestUnit.Caption := 'm^3/m';
      2: LabelYAXisTestUnit.Caption := 'm';
      3: LabelYAXisTestUnit.Caption := 'm';
    end;
    LabelYAXisTestUnit.Visible := true;
  end else
  begin
    LabelYAXisTestUnit.Caption := 'Unit';
    LabelYAXisTestUnit.Visible := False;
  end;
  //
  if (ComboBoxXaxisChart2.ItemIndex >= 0) then
  begin
    case ComboBoxXaxisChart2.ItemIndex of
      0: LabelXaxisChart2Unit.Caption := 'Pa';
      1: LabelXaxisChart2Unit.Caption := 'N.m/m';
      2: LabelXaxisChart2Unit.Caption := 'm^3/m';
      3: LabelXaxisChart2Unit.Caption := 'm';
      4: LabelXaxisChart2Unit.Caption := 'm';
    end;
    LabelXaxisChart2Unit.Visible := true;
  end else
  begin
    LabelXaxisChart2Unit.Caption := 'Unit';
    LabelXaxisChart2Unit.Visible := False;
  end;
  //
  if (ComboBoxYaxisChart2.ItemIndex >= 0) then
  begin
    case ComboBoxYaxisChart2.ItemIndex of
      0: LabelYaxisChart2Unit.Caption := '1/Pa';
      1: LabelYaxisChart2Unit.Caption := 'Pa';
    end;
    LabelYaxisChart2Unit.Visible := true;
  end else
  begin
    LabelYaxisChart2Unit.Caption := 'Unit';
    LabelYaxisChart2Unit.Visible := False;
  end;
end;


//----------------------------------------------------------------------------//
procedure TFormCW.RefreshResultArrays();
begin
  // Regression Result
  EditPCurveFit.Text := '';
  EditQCurveFit.Text := '';
  EditRCurveFit.Text := '';
  EditRsquare.Text := '';
  EditA.Text := '';
  EditB.Text := '';
  EditC.Text := '';
  // Release Old Curve Fit Data
  SetLength(FCurveFitWorkArray, 0);
  SetLength(FCurveFitWearVolumeArray, 0);
  // Curve fit chart
  SeriesCurveFitTest.Clear();
  // Derivative & Contact Pressure
  SetLength(FDerivativeWearVolumeArray, 0);
  SetLength(FPressureContactArray, 0);
end;

//----------------------------------------------------------------------------//



//----------------------------------------------------------------------------//
procedure TFormCW.WriteResults3PointsMethod(FilePath : string);
var
  j : Integer;
  fic : TextFile;
begin
  //
    AssignFile(fic, FilePath);
    Rewrite(fic);
    writeln(fic, '    Dintcsg       Dexttj       FForce  ');
    writeln(fic,' ', FCasingInternalDiameter:12:9,
                ' ', FToolJoinExternalDiameter:12:9,
                  ' ', FForce:12:3);
    writeln(fic, '             Work                    WearVol               DeriveeWearVol            PressionContact     || ');
    for j := 1 to length(FTestWorkArray) do
    begin
      writeln(fic,' ', FTestWorkArray[j-1]:25,
                  ' ', FTestWearVolumeArray[j-1]:25,
                  ' ', FDerivativeWearVolumeArray[j-1]:25,
                  ' ', FPressureContactArray[j-1]:25,' ||',
                  ' ', FTestWearLengthArray[j-1]:25,
                  ' ', FTestWearHeightArray[j-1]:25);
    end;
    closefile(fic);
end;


//----------------------------------------------------------------------------//
procedure TFormCW.WriteResultsCurveFitMethod(FilePath : string);
var
  i : Integer;
  fic : TextFile;
begin
  AssignFile(fic, FilePath);
  Rewrite(fic);
  writeln(fic, '  Dintcsg[m]    Dexttj[m]    FForce[N/m] ');
  writeln(fic,' ', FCasingInternalDiameter:12:9,
              ' ', FToolJoinExternalDiameter:12:9,
              ' ', FForce:12:3);
  writeln(fic, '        Work[N-m/m]             WearVol[m3/m]            DeriveeWearVol[1/Pa]     PressionContact[Pa]    ||');
  for i := 1 to length(FCurveFitWorkArray) do
  begin
    writeln(fic,' ', FCurveFitWorkArray[i-1]:25,
                ' ', FCurveFitWearVolumeArray[i-1]:25,
                ' ', FCurveFitDerivativeWearVolumeArray[i-1]:25,
                ' ', FCurveFitPressureContactArray[i-1]:25,' ||',
                ' ', FCurveFitWearLengthArray[i-1]:25,
                ' ', FCurveFitWearHeightArray[i-1]:25);
  end;
  closefile(fic)
end;

//----------------------------------------------------------------------------//
function TFormCW.TestInputParameters() : Boolean;
begin
  result := True;
  //FCasingExternalDiameter, FCasingInternalDiameter, FToolJoinExternalDiameter, FForce,
  //FA, FB, FC : Double;
  //FLinWF : Double;
  if((Double.IsNan(FCasingExternalDiameter) or
      Double.IsNan(FCasingInternalDiameter) or
      Double.IsNan(FToolJoinExternalDiameter) or
      Double.IsNan(FForce)))
  then
  begin
    ShowMessage('Not enough parameters');
    result := False;
    Exit;
  end;

  if (FCasingExternalDiameter<=FCasingInternalDiameter) then
  begin
    ShowMessage('Casing internal diameter must be smaller than the casing external diameter');
    result := False;
    Exit;
  end;

  if (FToolJoinExternalDiameter>=FCasingInternalDiameter) then
  begin
    ShowMessage('Tool-joint diameter must be smaller than the casing internal diameter');
    result := False;
    Exit;
  end;
end;

//----------------------------------------------------------------------------//
procedure TFormCW.ReadCorFactor (Var LisWearPercent, LisCorFactor : t_vdd);
var
	fic : TextFile;
  i : Integer;
begin
  //  Load correction factor from path FilePath
  if FileExists('./Data/DataCorFac') then
  begin
    AssignFile(fic, './Data/DataCorFac');
    reset(fic);
    // Reset tables
    SetLength(LisWearPercent, 0);
    SetLength(LisCorFactor, 0);
    // Lecture
    i := 0;
    repeat
      setlength(LisWearPercent, i+1);
      setlength(LisCorFactor, i+1);
      ReadLn(fic, LisWearPercent[i], LisCorFactor[i]);
      i := i+1;
    until (Eof(fic));
    CloseFile(fic);
  end;
end;


{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Relation WF diff�rentiel Vs Pression de Contact
         --> Lin�aire avec seuil (Thres : Pressure Threshole)
         dS/dPsi=Pente*(Force/2LarUse-Thres)
         PClim et WFasym : Point � partir duquel PC<PClim --> WF=const=WFasym
-------------------------------------------------------------------------------}
procedure RecontructVolWorkCurveNewModel (RextCsg, RintCsg, Rtjt : Double;
                                          F : Double;
                                          A, B, PClim : Double;
                                          WFlin : Double;
                                          LarMax : Double;
                                          nb_points : Integer;
                                          NomFic: string);
var
  i : Integer;
  Work1, Work2, Work3, VolUse : Double;
  Lar, Hau, HauMax, Bmax, Amax : Double;
  fic : text;
begin
  Assign(fic, NomFic);
  Rewrite(fic);
  // Writeln(fic, '  RextCsg(m) RintCsg(m)   Rtjt(m)  Force(N/m)  A-Pente(1/Pa^2)  B-Thres(Pa)    WFAsym(1/Pa)     PClim(Pa)   WFlin(1/Pa)');
  // Writeln(fic, RextCsg:10:6,' ', RintCsg:10:6,' ', Rtjt:10:6,' ', F:15, ' ', A:15,' ', B:15,' ', A*(PClim-B):15,' ',PClim:15, ' ' ,WFlin:15);
  Writeln(fic, '  WorkLarPlate(N.m/m)    WorkLarCourbee(N-m/m)   WorkWFLin(N-m/m)      VolUse(m3/m)    HauUse(m)     LarUse(m)');

  // Hau := 0.5*(RextCsg-RintCsg);
  // LarMax := Min(CalculLarFromHau(Hau, RintCsg, Rtjt),Rtjt);

  for i  := 0 to nb_points do begin
    Lar := i*LarMax/nb_points;  // m
    Hau := CalculHauFromLar(Lar, RintCsg, Rtjt);
    VolUse := CalculSecFromLar(Lar, RintCsg, Rtjt);

    Work1 := CalculWorkFromLargeurModelComplet4aGauss(Lar, A, B, PClim, F, RintCsg, Rtjt);
    Work2 := CalculWorkFromLargeurModelComplet4bGauss(Lar, A, B, PClim, F, RintCsg, Rtjt);
    if WFlin>0 then Work3 := VolUse/WFlin else Work3 := -999;

    if ((Work1<-1E-9) or (Work1>1E50) or (Work2<-1E-9) or (Work2>1E50)) then break
    else Writeln(fic,' ', Work1:20,
                     ' ', Work2:20,
                     ' ', Work3:20,
                     ' ', VolUse:20,
                     ' ', Hau:20,
                     ' ', Lar:20,' ');
  end;
  Close(fic);
end;
//----------------------------------------------------------------------------//


//----------------------------------------------------------------------------//
procedure TFormCW.EditAChange(Sender: TObject);
var
  LOut : Double;
begin
  if Double.TryParse(TEdit(Sender).Text, LOut) then
    FA := LOut*1E-12
  else
    FA := Double.NaN;
end;

//----------------------------------------------------------------------------//
procedure TFormCW.EditBChange(Sender: TObject);
var
  LOut : Double;
begin
  if Double.TryParse(TEdit(Sender).Text, LOut) then
    FB := LOut
  else
    FB := Double.NaN;
end;

//----------------------------------------------------------------------------//
procedure TFormCW.EditCasingExternalDiameterChange(Sender: TObject);
var
  LOut : Double;
begin
  if Double.TryParse(TEdit(Sender).Text, LOut) then begin
    FCasingExternalDiameter := LOut;
    LabelCsgExtDiaUSUnit.Caption := '('+FloatToStrF(FCasingExternalDiameter/0.0254, ffGeneral, 6, 3)+' in)';
    LabelCsgExtDiaUSUnit.Visible := true
  end
  else begin
    FCasingExternalDiameter := Double.NaN;
    LabelCsgExtDiaUSUnit.Visible := false;
  end;
end;

//----------------------------------------------------------------------------//
procedure TFormCW.EditCasingInternalDiameterChange(Sender: TObject);
var
  LOut : Double;
begin
  if Double.TryParse(TEdit(Sender).Text, LOut) then begin
    FCasingInternalDiameter := LOut;
    LabelCsgIntDiaUSUnit.Caption := '('+FloatToStrF(FCasingInternalDiameter/0.0254, ffGeneral, 6, 3)+' in)';
    LabelCsgIntDiaUSUnit.Visible := true;
  end
  else begin
    FCasingInternalDiameter := Double.NaN;
    LabelCsgIntDiaUSUnit.Visible := false;
  end;
end;

//----------------------------------------------------------------------------//
procedure TFormCW.EditCChange(Sender: TObject);
var
  LOut : Double;
begin
  if Double.TryParse(TEdit(Sender).Text, LOut) then
    FC := LOut*1E-10
  else
    FC := Double.NaN;
end;

//----------------------------------------------------------------------------//
procedure TFormCW.EditForceChange(Sender: TObject);
var
  LOut : Double;
begin
  if Double.TryParse(TEdit(Sender).Text, LOut) then begin
    FForce := LOut;
    LabelTestForceUSUnit.Caption := '('+FloatToStrF(FForce/14.59390289, ffGeneral, 6, 1)+' lbf/ft)';
    LabelTestForceUSUnit.Visible := true;
  end
  else begin
    FForce := Double.NaN;
    LabelTestForceUSUnit.Visible := false;
  end;
end;

//----------------------------------------------------------------------------//
procedure TFormCW.EditLinearWFChange(Sender: TObject);
var
  LOut : Double;
begin
  if Double.TryParse(TEdit(Sender).Text, LOut) then
    FLinWF := LOut*1E-10
  else
    FLinWF := Double.NaN;
end;

//----------------------------------------------------------------------------//
procedure TFormCW.EditToolJoinExternalDiameterChange(Sender: TObject);
var
  LOut : Double;
begin
  if Double.TryParse(TEdit(Sender).Text, LOut) then begin
    FToolJoinExternalDiameter := LOut;
    LabelTjExtDiaUSUnit.Caption := '('+FloatToStrF(FToolJoinExternalDiameter/0.0254, ffGeneral, 6, 3)+' in)';
    LabelTjExtDiaUSUnit.Visible := true;
  end
  else begin
    FToolJoinExternalDiameter := Double.NaN;
    LabelTjExtDiaUSUnit.Visible := false;
  end;
end;

//----------------------------------------------------------------------------//
procedure TFormCW.FormCreate(Sender: TObject);
begin
  // TEST TEST
  CreateDir('Outputs');
  // Initialisation
  SetLength(FTestWorkArray, 0);
  SetLength(FTestWearVolumeArray, 0);
  SetLength(FTestWearHeightArray, 0);
  SetLength(FTestWearLengthArray, 0);
  SetLength(FDerivativeWearVolumeArray, 0);
  SetLength(FPressureContactArray, 0);
  SetLength(FCurveFitWorkArray, 0);
  SetLength(FCurveFitWearVolumeArray, 0);

  SetLength(FWorkArray, 0);
  SetLength(FWearVolumeArray, 0);
  SetLength(FWearHeightArray, 0);
  SetLength(FWearLengthArray, 0);

  FCasingExternalDiameter := Double.NaN;
  FCasingInternalDiameter := Double.NaN;
  FToolJoinExternalDiameter := Double.NaN;
  FForce := Double.NaN;
  FA := Double.NaN;
  FB := Double.NaN;
  FC := Double.NaN;
  FLinWF := Double.NaN;

  // Defaut Value ComboBox
  ComboBoxXAXisTest.ItemIndex :=0;
  ComboBoxYAxisTest.ItemIndex :=1;
  LabelChartUnits();

  //
  LabelTestForceUSUnit.Visible := False;
  LabelCsgExtDiaUSUnit.Visible := False;
  LabelCsgIntDiaUSUnit.Visible := False;
  LabelTjExtDiaUSUnit.Visible := False;
end;

end.
