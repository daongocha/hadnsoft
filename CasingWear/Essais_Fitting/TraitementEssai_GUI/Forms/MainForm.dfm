object FormCW: TFormCW
  Left = 0
  Top = 0
  Width = 1200
  Height = 750
  AutoScroll = True
  Caption = 'Casing Wear Parameters'
  Color = clBtnFace
  Constraints.MinHeight = 375
  Constraints.MinWidth = 665
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnCreate = FormCreate
  DesignSize = (
    1184
    711)
  PixelsPerInch = 96
  TextHeight = 13
  object PanelInput: TPanel
    Left = 834
    Top = 1
    Width = 350
    Height = 708
    Anchors = [akTop, akRight, akBottom]
    BevelOuter = bvLowered
    TabOrder = 0
    object LabelCasingData: TLabel
      Left = 8
      Top = 50
      Width = 66
      Height = 13
      Caption = 'Casing data'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LabelCasingExternalDiameter: TLabel
      Left = 16
      Top = 71
      Width = 62
      Height = 13
      Caption = 'External Dia.'
    end
    object LabelCasingInternalDiameter: TLabel
      Left = 16
      Top = 98
      Width = 60
      Height = 13
      Caption = 'Internal Dia.'
    end
    object LabelToolJoinExternalDiameter: TLabel
      Left = 16
      Top = 147
      Width = 62
      Height = 13
      Caption = 'External Dia.'
    end
    object LabelToolJoinData: TLabel
      Left = 8
      Top = 124
      Width = 77
      Height = 13
      Caption = 'Tool join data'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LabelWearData: TLabel
      Left = 8
      Top = 347
      Width = 255
      Height = 13
      Caption = 'Wear data (DrillScan) - Proposed Parameters'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LabelASlope: TLabel
      Left = 16
      Top = 368
      Width = 43
      Height = 13
      Caption = 'A - Slope'
    end
    object LabelBPClim: TLabel
      Left = 16
      Top = 395
      Width = 50
      Height = 13
      Caption = 'B - PC limit'
    end
    object LabelCWFlim: TLabel
      Left = 16
      Top = 422
      Width = 54
      Height = 13
      Caption = 'C - WF limit'
    end
    object LabelTestData: TLabel
      Left = 8
      Top = 171
      Width = 54
      Height = 13
      Caption = 'Test data'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LabelForce: TLabel
      Left = 16
      Top = 194
      Width = 27
      Height = 13
      Caption = 'Force'
    end
    object LabelUnitCsgExtDia: TLabel
      Left = 207
      Top = 71
      Width = 8
      Height = 13
      Caption = 'm'
    end
    object LabelUnitCsgIntDia: TLabel
      Left = 207
      Top = 98
      Width = 8
      Height = 13
      Caption = 'm'
    end
    object LabelUnitTjExtDia: TLabel
      Left = 207
      Top = 147
      Width = 8
      Height = 13
      Caption = 'm'
    end
    object LabelUnitTestForce: TLabel
      Left = 207
      Top = 194
      Width = 19
      Height = 13
      Caption = 'N/m'
    end
    object LabelUnitAWearLaw: TLabel
      Left = 207
      Top = 368
      Width = 43
      Height = 13
      Caption = 'E-19/Pa'#178
    end
    object LabelUnitBWearLaw: TLabel
      Left = 207
      Top = 395
      Width = 20
      Height = 13
      Caption = 'MPa'
    end
    object LabelUnitCWearLaw: TLabel
      Left = 207
      Top = 422
      Width = 38
      Height = 13
      Caption = 'E-14/Pa'
    end
    object LabelTestForceUSUnit: TLabel
      Left = 247
      Top = 194
      Width = 24
      Height = 13
      Caption = 'lbf/ft'
    end
    object LabelCsgExtDiaUSUnit: TLabel
      Left = 247
      Top = 71
      Width = 8
      Height = 13
      Caption = 'in'
    end
    object LabelCsgIntDiaUSUnit: TLabel
      Left = 247
      Top = 98
      Width = 8
      Height = 13
      Caption = 'in'
    end
    object LabelTjExtDiaUSUnit: TLabel
      Left = 247
      Top = 147
      Width = 8
      Height = 13
      Caption = 'in'
    end
    object LabelThres: TLabel
      Left = 16
      Top = 467
      Width = 47
      Height = 13
      Caption = 'Threshole'
    end
    object LabelUnitThres: TLabel
      Left = 207
      Top = 467
      Width = 20
      Height = 13
      Caption = 'MPa'
    end
    object EditCasingExternalDiameter: TEdit
      Left = 104
      Top = 68
      Width = 97
      Height = 21
      TabOrder = 0
      OnChange = EditCasingExternalDiameterChange
    end
    object EditCasingInternalDiameter: TEdit
      Left = 104
      Top = 95
      Width = 97
      Height = 21
      TabOrder = 1
      OnChange = EditCasingInternalDiameterChange
    end
    object EditToolJoinExternalDiameter: TEdit
      Left = 104
      Top = 144
      Width = 97
      Height = 21
      TabOrder = 2
      OnChange = EditToolJoinExternalDiameterChange
    end
    object EditA: TEdit
      Left = 104
      Top = 365
      Width = 97
      Height = 21
      ReadOnly = True
      TabOrder = 4
      OnChange = EditAChange
    end
    object EditB: TEdit
      Left = 104
      Top = 392
      Width = 97
      Height = 21
      ReadOnly = True
      TabOrder = 5
      OnChange = EditBChange
    end
    object EditC: TEdit
      Left = 104
      Top = 419
      Width = 97
      Height = 21
      ReadOnly = True
      TabOrder = 6
      OnChange = EditCChange
    end
    object EditForce: TEdit
      Left = 104
      Top = 191
      Width = 97
      Height = 21
      TabOrder = 3
      OnChange = EditForceChange
    end
    object ButtonCal: TButton
      Left = 104
      Top = 271
      Width = 97
      Height = 25
      Caption = 'Calculation'
      TabOrder = 7
      OnClick = ButtonCalClick
    end
    object ButtonGetDataFile: TButton
      Left = 104
      Top = 16
      Width = 95
      Height = 21
      Caption = 'Get Data File'
      TabOrder = 8
      Visible = False
      OnClick = ButtonGetDataFileClick
    end
    object ButtonExportCWModel: TButton
      Left = 280
      Top = 365
      Width = 54
      Height = 21
      Caption = 'Export'
      TabOrder = 9
      OnClick = ButtonExportCWModelClick
    end
    object CheckBoxPlateOrCurvedContactLength: TCheckBox
      Left = 104
      Top = 232
      Width = 145
      Height = 17
      Caption = '  Plate Contact Length'
      TabOrder = 10
    end
    object EditThres: TEdit
      Left = 104
      Top = 464
      Width = 97
      Height = 21
      ReadOnly = True
      TabOrder = 11
      OnChange = EditBChange
    end
    object ButtonInfo: TButton
      Left = 321
      Top = 678
      Width = 25
      Height = 25
      Caption = 'I'
      TabOrder = 12
      OnClick = ButtonInfoClick
    end
  end
  object Panel1: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 825
    Height = 341
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 1
    DesignSize = (
      825
      341)
    object LabelXAXisTest: TLabel
      Left = 24
      Top = 8
      Width = 29
      Height = 13
      Caption = 'X Axis'
    end
    object LabelYAxisTest: TLabel
      Left = 292
      Top = 8
      Width = 29
      Height = 13
      Caption = 'Y Axis'
    end
    object LabelPCurveFit: TLabel
      Left = 695
      Top = 118
      Width = 51
      Height = 13
      Anchors = [akTop, akRight]
      Caption = 'P (m^3/m)'
    end
    object LabelRCurveFit: TLabel
      Left = 695
      Top = 221
      Width = 77
      Height = 13
      Anchors = [akTop, akRight]
      Caption = 'R (dimesionless)'
    end
    object LabelQCurveFit: TLabel
      Left = 695
      Top = 169
      Width = 78
      Height = 13
      Anchors = [akTop, akRight]
      Caption = 'Q (dimesionless)'
    end
    object LabelRsquare: TLabel
      Left = 695
      Top = 280
      Width = 104
      Height = 13
      Anchors = [akTop, akRight]
      Caption = 'R-square (Fit Quality)'
    end
    object LabelYAXisTestUnit: TLabel
      Left = 453
      Top = 8
      Width = 19
      Height = 13
      Caption = 'Unit'
      Visible = False
    end
    object LabelXAXisTestUnit: TLabel
      Left = 185
      Top = 8
      Width = 19
      Height = 13
      Caption = 'Unit'
      Visible = False
    end
    object ComboBoxXAXisTest: TComboBox
      Left = 59
      Top = 5
      Width = 120
      Height = 21
      TabOrder = 0
      OnChange = ComboBoxTestChange
      Items.Strings = (
        'Work'
        'Wear volume'
        'Wear height'
        'Wear length')
    end
    object ComboBoxYAxisTest: TComboBox
      Left = 327
      Top = 5
      Width = 120
      Height = 21
      TabOrder = 1
      OnChange = ComboBoxTestChange
      Items.Strings = (
        'Work'
        'Wear volume'
        'Wear height'
        'Wear length')
    end
    object ButtonLoadTestResult: TButton
      Left = 576
      Top = 5
      Width = 97
      Height = 21
      Caption = 'Load Test Results'
      TabOrder = 2
      OnClick = ButtonLoadTestResultClick
    end
    object ChartTestResult: TChart
      Left = 24
      Top = 32
      Width = 665
      Height = 300
      BackWall.Brush.Gradient.Direction = gdBottomTop
      BackWall.Brush.Gradient.EndColor = clWhite
      BackWall.Brush.Gradient.StartColor = 15395562
      BackWall.Brush.Gradient.Visible = True
      BackWall.Transparent = False
      Foot.Font.Color = clBlue
      Foot.Font.Name = 'Verdana'
      Gradient.Direction = gdBottomTop
      Gradient.EndColor = clWhite
      Gradient.MidColor = 15395562
      Gradient.StartColor = 15395562
      Gradient.Visible = True
      LeftWall.Color = 14745599
      Legend.Alignment = laBottom
      Legend.Font.Name = 'Verdana'
      Legend.LegendStyle = lsSeries
      Legend.Shadow.Transparency = 0
      RightWall.Color = 14745599
      ScrollMouseButton = mbMiddle
      Title.Font.Name = 'Verdana'
      Title.Text.Strings = (
        'TChart')
      Title.Visible = False
      BottomAxis.Axis.Color = 4210752
      BottomAxis.AxisValuesFormat = '0.0E-00'
      BottomAxis.Grid.Color = 11119017
      BottomAxis.Grid.Style = psDash
      BottomAxis.LabelsFormat.Font.Name = 'Verdana'
      BottomAxis.TicksInner.Color = 11119017
      BottomAxis.Title.Font.Name = 'Verdana'
      DepthAxis.Axis.Color = 4210752
      DepthAxis.Grid.Color = 11119017
      DepthAxis.LabelsFormat.Font.Name = 'Verdana'
      DepthAxis.TicksInner.Color = 11119017
      DepthAxis.Title.Font.Name = 'Verdana'
      DepthTopAxis.Axis.Color = 4210752
      DepthTopAxis.Grid.Color = 11119017
      DepthTopAxis.LabelsFormat.Font.Name = 'Verdana'
      DepthTopAxis.TicksInner.Color = 11119017
      DepthTopAxis.Title.Font.Name = 'Verdana'
      LeftAxis.Axis.Color = 4210752
      LeftAxis.AxisValuesFormat = '0.0E-00'
      LeftAxis.Grid.Color = 11119017
      LeftAxis.Grid.Style = psDash
      LeftAxis.LabelsFormat.Font.Name = 'Verdana'
      LeftAxis.Ticks.Style = psDash
      LeftAxis.TicksInner.Color = 11119017
      LeftAxis.Title.Font.Name = 'Verdana'
      Panning.MouseWheel = pmwNone
      RightAxis.Automatic = False
      RightAxis.AutomaticMaximum = False
      RightAxis.AutomaticMinimum = False
      RightAxis.Axis.Color = 4210752
      RightAxis.Grid.Color = 11119017
      RightAxis.LabelsFormat.Font.Name = 'Verdana'
      RightAxis.Maximum = 0.048061757887150600
      RightAxis.Minimum = -0.048061756707150610
      RightAxis.TicksInner.Color = 11119017
      RightAxis.Title.Font.Name = 'Verdana'
      TopAxis.Automatic = False
      TopAxis.AutomaticMaximum = False
      TopAxis.AutomaticMinimum = False
      TopAxis.Axis.Color = 4210752
      TopAxis.Grid.Color = 11119017
      TopAxis.LabelsFormat.Font.Name = 'Verdana'
      TopAxis.Maximum = 0.048061757885598590
      TopAxis.Minimum = -0.048061756708702660
      TopAxis.TicksInner.Color = 11119017
      TopAxis.Title.Font.Name = 'Verdana'
      View3D = False
      Zoom.MouseWheel = pmwNormal
      BevelOuter = bvLowered
      TabOrder = 3
      Anchors = [akLeft, akTop, akRight]
      DefaultCanvas = 'TGDIPlusCanvas'
      PrintMargins = (
        15
        27
        15
        27)
      ColorPaletteIndex = 13
      object SeriesWearTestResult: TLineSeries
        HoverElement = [heCurrent]
        Marks.Style = smsValue
        Marks.AutoPosition = False
        Marks.Callout.Brush.Color = 10485760
        Marks.Callout.HorizSize = 3
        Marks.Callout.Pen.Color = 10485760
        Marks.Callout.VertSize = 3
        Marks.Callout.Visible = True
        Marks.Symbol.Visible = True
        SeriesColor = 10485760
        Title = 'Test Result'
        ValueFormat = '#.0 "x10" E+0'
        Brush.BackColor = clDefault
        LinePen.Color = 10485760
        LinePen.Width = 2
        Pointer.HorizSize = 2
        Pointer.InflateMargins = True
        Pointer.Style = psRectangle
        Pointer.VertSize = 2
        Pointer.Visible = True
        XValues.Name = 'X'
        XValues.Order = loAscending
        YValues.Name = 'Y'
        YValues.Order = loNone
        Data = {0000000000}
      end
      object SeriesCurveFitTest: TFastLineSeries
        HoverElement = []
        Selected.Hover.Visible = True
        SeriesColor = clRed
        Title = 'Curve Fit'
        IgnoreNulls = False
        LinePen.Color = clRed
        LinePen.Style = psDash
        TreatNulls = tnDontPaint
        XValues.Name = 'X'
        XValues.Order = loAscending
        YValues.Name = 'Y'
        YValues.Order = loNone
        Data = {0000000000}
      end
    end
    object EditRCurveFit: TEdit
      Left = 695
      Top = 240
      Width = 121
      Height = 21
      Anchors = [akTop, akRight]
      ReadOnly = True
      TabOrder = 4
    end
    object EditQCurveFit: TEdit
      Left = 695
      Top = 188
      Width = 121
      Height = 21
      Anchors = [akTop, akRight]
      ReadOnly = True
      TabOrder = 5
    end
    object EditPCurveFit: TEdit
      Left = 695
      Top = 137
      Width = 121
      Height = 21
      Anchors = [akTop, akRight]
      ReadOnly = True
      TabOrder = 6
    end
    object MemoCurveFir: TMemo
      Left = 695
      Top = 32
      Width = 121
      Height = 40
      Anchors = [akTop, akRight]
      Enabled = False
      Lines.Strings = (
        'Curve Fitting Function'
        'Y = P(1-exp(-Q.X^R))')
      TabOrder = 7
    end
    object ButtonFitCurve: TButton
      Left = 712
      Top = 78
      Width = 97
      Height = 21
      Anchors = [akTop, akRight]
      Caption = 'Fit Curve'
      TabOrder = 8
      Visible = False
      OnClick = ButtonFitCurveClick
    end
    object EditRsquare: TEdit
      Left = 695
      Top = 299
      Width = 121
      Height = 21
      Anchors = [akTop, akRight]
      ReadOnly = True
      TabOrder = 9
    end
  end
  object Panel2: TPanel
    Left = 3
    Top = 350
    Width = 825
    Height = 354
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 2
    DesignSize = (
      825
      354)
    object LabelXaxisChart2: TLabel
      Left = 24
      Top = 22
      Width = 29
      Height = 13
      Anchors = [akLeft, akBottom]
      Caption = 'X Axis'
    end
    object LabelYaxisChart2: TLabel
      Left = 292
      Top = 22
      Width = 29
      Height = 13
      Anchors = [akLeft, akBottom]
      Caption = 'Y Axis'
    end
    object LabelXaxisChart2Unit: TLabel
      Left = 185
      Top = 22
      Width = 19
      Height = 13
      Anchors = [akLeft, akBottom]
      Caption = 'Unit'
      Visible = False
    end
    object LabelYaxisChart2Unit: TLabel
      Left = 453
      Top = 22
      Width = 19
      Height = 13
      Anchors = [akLeft, akBottom]
      Caption = 'Unit'
      Visible = False
    end
    object ChartModel: TChart
      Left = 24
      Top = 46
      Width = 665
      Height = 300
      BackWall.Brush.Gradient.Direction = gdBottomTop
      BackWall.Brush.Gradient.EndColor = clWhite
      BackWall.Brush.Gradient.StartColor = 15395562
      BackWall.Brush.Gradient.Visible = True
      BackWall.Transparent = False
      Foot.Font.Color = clBlue
      Foot.Font.Name = 'Verdana'
      Gradient.Direction = gdBottomTop
      Gradient.EndColor = clWhite
      Gradient.MidColor = 15395562
      Gradient.StartColor = 15395562
      Gradient.Visible = True
      LeftWall.Color = 14745599
      Legend.Alignment = laBottom
      Legend.Font.Name = 'Verdana'
      Legend.LegendStyle = lsSeries
      Legend.Shadow.Transparency = 0
      Legend.Title.Visible = False
      RightWall.Color = 14745599
      ScrollMouseButton = mbMiddle
      Title.Font.Name = 'Verdana'
      Title.Text.Strings = (
        'TChart')
      Title.Visible = False
      BottomAxis.Axis.Color = 4210752
      BottomAxis.AxisValuesFormat = '0.0E-00'
      BottomAxis.Grid.Color = 11119017
      BottomAxis.Grid.Style = psDash
      BottomAxis.LabelsFormat.Font.Name = 'Verdana'
      BottomAxis.TicksInner.Color = 11119017
      BottomAxis.Title.Font.Name = 'Verdana'
      DepthAxis.Axis.Color = 4210752
      DepthAxis.Grid.Color = 11119017
      DepthAxis.LabelsFormat.Font.Name = 'Verdana'
      DepthAxis.TicksInner.Color = 11119017
      DepthAxis.Title.Font.Name = 'Verdana'
      DepthTopAxis.Axis.Color = 4210752
      DepthTopAxis.Grid.Color = 11119017
      DepthTopAxis.LabelsFormat.Font.Name = 'Verdana'
      DepthTopAxis.TicksInner.Color = 11119017
      DepthTopAxis.Title.Font.Name = 'Verdana'
      LeftAxis.Axis.Color = 4210752
      LeftAxis.AxisValuesFormat = '0.0E-00'
      LeftAxis.Grid.Color = 11119017
      LeftAxis.Grid.Style = psDash
      LeftAxis.LabelsFormat.Font.Name = 'Verdana'
      LeftAxis.Ticks.Style = psDash
      LeftAxis.TicksInner.Color = 11119017
      LeftAxis.Title.Font.Name = 'Verdana'
      Panning.MouseWheel = pmwNone
      RightAxis.Automatic = False
      RightAxis.AutomaticMaximum = False
      RightAxis.AutomaticMinimum = False
      RightAxis.Axis.Color = 4210752
      RightAxis.Grid.Color = 11119017
      RightAxis.LabelsFormat.Font.Name = 'Verdana'
      RightAxis.Maximum = 0.000099933037115409
      RightAxis.Minimum = -0.000099932937113625
      RightAxis.TicksInner.Color = 11119017
      RightAxis.Title.Font.Name = 'Verdana'
      TopAxis.Automatic = False
      TopAxis.AutomaticMaximum = False
      TopAxis.AutomaticMinimum = False
      TopAxis.Axis.Color = 4210752
      TopAxis.Grid.Color = 11119017
      TopAxis.LabelsFormat.Font.Name = 'Verdana'
      TopAxis.Maximum = 0.000099933037111857
      TopAxis.Minimum = -0.000099932937117177
      TopAxis.TicksInner.Color = 11119017
      TopAxis.Title.Font.Name = 'Verdana'
      View3D = False
      Zoom.MouseWheel = pmwNormal
      BevelOuter = bvLowered
      TabOrder = 0
      Anchors = [akLeft, akRight, akBottom]
      DefaultCanvas = 'TGDIPlusCanvas'
      PrintMargins = (
        15
        8
        15
        8)
      ColorPaletteIndex = 13
      object SeriesTreat3Points: TPointSeries
        HoverElement = [heCurrent]
        Marks.Callout.Length = 8
        SeriesColor = 10485760
        Title = '3 Points Reg.'
        ClickableLine = False
        Pointer.HorizSize = 2
        Pointer.InflateMargins = True
        Pointer.Style = psRectangle
        Pointer.VertSize = 2
        XValues.Name = 'X'
        XValues.Order = loAscending
        YValues.Name = 'Y'
        YValues.Order = loNone
        Data = {0000000000}
      end
      object SeriesTreatCurveFit: TPointSeries
        HoverElement = [heCurrent]
        Marks.Callout.Length = 8
        SeriesColor = 10485760
        Title = 'Curve Fitting'
        ClickableLine = False
        Pointer.Brush.Color = clRed
        Pointer.HorizSize = 2
        Pointer.InflateMargins = True
        Pointer.Pen.Color = clRed
        Pointer.Style = psRectangle
        Pointer.VertSize = 2
        XValues.Name = 'X'
        XValues.Order = loAscending
        YValues.Name = 'Y'
        YValues.Order = loNone
        Data = {0000000000}
      end
      object SeriesProposedModel: TFastLineSeries
        HoverElement = []
        Selected.Hover.Visible = True
        SeriesColor = clRed
        Title = 'Proposed (DrillScan)'
        LinePen.Color = clRed
        LinePen.Width = 3
        TreatNulls = tnDontPaint
        XValues.Name = 'X'
        XValues.Order = loAscending
        YValues.Name = 'Y'
        YValues.Order = loNone
        Data = {0000000000}
      end
    end
    object ComboBoxXaxisChart2: TComboBox
      Left = 59
      Top = 19
      Width = 120
      Height = 21
      Anchors = [akLeft, akBottom]
      TabOrder = 1
      OnChange = ComboBoxTreatChange
      Items.Strings = (
        'Contact Pressure'
        'Work'
        'Wear volume'
        'Wear height'
        'Wear length')
    end
    object ComboBoxYaxisChart2: TComboBox
      Left = 327
      Top = 19
      Width = 120
      Height = 21
      Anchors = [akLeft, akBottom]
      TabOrder = 2
      OnChange = ComboBoxTreatChange
      Items.Strings = (
        'Differential Wear Factor'
        'Contact Pressure')
    end
  end
  object OpenDialogGetDataFile: TOpenDialog
    DefaultExt = '.txt'
    Options = [ofHideReadOnly, ofFileMustExist, ofEnableSizing]
    Title = 'Load Data File'
    Left = 1072
    Top = 11
  end
  object OpenDialogTestResult: TOpenDialog
    DefaultExt = '.txt'
    Filter = 'Text file|*.txt'
    Options = [ofHideReadOnly, ofFileMustExist, ofEnableSizing]
    Title = 'Load Test Result File'
    Left = 528
    Top = 11
  end
  object SaveDialogModelParameters: TSaveDialog
    DefaultExt = '.txt'
    Left = 978
    Top = 537
  end
end
