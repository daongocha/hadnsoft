(**
Misc functions: Activation/Deactivation of ComboBox, Labels, ...
*)
unit UnitGestionTestBusiness;

interface

// {$I Foundation.inc}

uses
  { Standard units }
  System.Classes,
  System.DateUtils,

  Vcl.Controls,
  Vcl.StdCtrls,
  Vcl.ComCtrls,
  Vcl.Samples.Spin,

  { Component units }
  AdvSpin,
  AdvDateTimePicker,

  UnitColour
  ;

procedure SwitchOn(const ACtrl: TEdit); overload;
procedure SwitchOn(const ACtrl: TMemo); overload;
procedure SwitchOn(const ACtrl: TDateTimePicker); overload;
procedure SwitchOn(const ACtrl: TAdvSpinEdit); overload;

procedure SwitchOf(const ACtrl: TEdit); overload;
procedure SwitchOf(const ACtrl: TMemo); overload;
procedure SwitchOf(const ACtrl: TDateTimePicker); overload;
procedure SwitchOf(const ACtrl: TAdvSpinEdit); overload;

procedure SetEdit(const AEdit: TEdit; const AValeur: String);
procedure SetCheckBox(const ACheckBox: TCheckBox; AChecked: Boolean);
procedure SetRadioButton(const ARadioButton: TRadioButton; AChecked: Boolean);
procedure SetListViewCheck(const AListView: TListView; AItemIndex: Integer; AChecked: Boolean);
procedure EnabledAsParent(const container: TWinControl);

procedure ValidateComboBox(const AComboBox: TComboBox; isValid: Boolean; const Hint: String='');
procedure SwitchOnCbx(const AComboBox: TComboBox);
procedure SwitchOfCbx(const AComboBox: TComboBox);

implementation

uses
  { Standard units }
  System.Types,
  System.StrUtils,
  System.Math,
  System.SysUtils,
  Vcl.Graphics
  ;

  {*------------------------------------------------------------------------------
  Enable Editbox :
     - Tag set to 1
      - Background is set to default
      - Font color is set to black
      - Show Hint is disabled

   @param AEdit Editbox to activate
-------------------------------------------------------------------------------}
procedure SwitchOn(const ACtrl: TEdit);
begin
  ACtrl.Tag := 1;
  ACtrl.Color := clWindow;
  ACtrl.Font.Color := clBlack;
  ACtrl.ShowHint := False;
end;

procedure SwitchOn(const ACtrl: TMemo);
begin
  ACtrl.Tag := 1;
  ACtrl.Color := clWindow;
  ACtrl.Font.Color := clBlack;
  ACtrl.ShowHint := False;
end;

procedure SwitchOn(const ACtrl: TDateTimePicker);
begin
  ACtrl.Tag := 1;
  ACtrl.Color := clWindow;
  ACtrl.Font.Color := clBlack;
  ACtrl.ShowHint := False;
end;

procedure SwitchOn(const ACtrl: TAdvSpinEdit);
begin
  ACtrl.Tag := 1;
  ACtrl.Color := clWindow;
  ACtrl.Font.Color := clBlack;
  ACtrl.ShowHint := False;
end;

{*------------------------------------------------------------------------------
  Disable Editbox :
     - Tag set to -1
      - Background is red to indicate error
      - Font color is set to white
      - Show Hint is set to true (to display why this error occured)

   @param AEdit EditBox to report an error to
-------------------------------------------------------------------------------}
procedure SwitchOf(const ACtrl: TEdit);
begin
  ACtrl.Tag := -1;
  ACtrl.Color := HtmlToColor(COLOURERR);
  ACtrl.Font.Color := clWhite;
  ACtrl.ShowHint := True;
end;

procedure SwitchOf(const ACtrl: TMemo);
begin
  ACtrl.Tag := -1;
  ACtrl.Color := HtmlToColor(COLOURERR);
  ACtrl.Font.Color := clWhite;
  ACtrl.ShowHint := True;
end;

procedure SwitchOf(const ACtrl: TDateTimePicker);
begin
  ACtrl.Tag := -1;
  ACtrl.Color := HtmlToColor(COLOURERR);
  ACtrl.Font.Color := clWhite;
  ACtrl.ShowHint := True;
end;

procedure SwitchOf(const ACtrl: TAdvSpinEdit);
begin
  ACtrl.Tag := -1;
  ACtrl.Color := HtmlToColor(COLOURERR);
  ACtrl.Font.Color := clWhite;
  ACtrl.ShowHint := True;
end;



{*------------------------------------------------------------------------------
  Switch label and Combobox enabled or not depending on Etat.
       - 1: Everything enabled
       - 2: Label OK, Combobox disabled
       - 3: Everything disabled
       - 4: Everything invisible

   @param p_Label Label to switch on or off
   @param p_ComboBox Combobox to switch on or off
   @param Etat 1, 2 or 3,4. Base for value used for switching
-------------------------------------------------------------------------------}
procedure SwitchComboBox(const ALabel: TLabel; const AComboBox: TComboBox; Etat: Integer; CanIni: Boolean=True);
begin
  case Etat of
    1:
    begin
      if Assigned(ALabel) then
        ALabel.Enabled := True;
      AComboBox.Enabled := True;
      AComboBox.Color := clWindow;
      AComboBox.Tag := 0;
    end;
    2:
    begin
      if Assigned(ALabel) then
        ALabel.Enabled := True;
      AComboBox.Color := HtmlToColor(COLOURDIS);
      AComboBox.Enabled := False;
    end;
    3,4:
    begin
      if Assigned(ALabel) then
        ALabel.Enabled := False;
      if CanIni then
        AComboBox.ItemIndex := -1;
      AComboBox.Color := HtmlToColor(COLOURDIS);
      AComboBox.Enabled := False;
      AComboBox.Tag := -1;
      if Etat = 4 then
      begin
        ALabel.Visible := False;
        ACombobox.Visible := False;
      end;
    end;
    5:
    begin
      if Assigned(ALabel) then
        ALabel.Enabled := False;
      AComboBox.Color := HtmlToColor(COLOURDIS);
      AComboBox.Enabled := False;
    end;
  end;
end;

{*------------------------------------------------------------------------------
  Switch label and Combobox enabled or not depending on Etat.
       - 1: Everything enabled
      - 2: Label OK, Combobox disabled
      - 3: Everything disabled

   @param p_Label Label to switch on or off
   @param p_ComboBox Combobox to switch on or off
   @param p_LabelUni Unit label to switch on or off
   @param Etat 1, 2 or 3. Base for value used for switching
-------------------------------------------------------------------------------}
procedure SwitchComboBoxUni(
    const ALabel: TLabel;
    const AComboBox: TComboBox;
    const ALabelUni: TLabel;
    Etat: Integer);
begin
  case Etat of
    1:
    begin
      if Assigned(ALabel) then
        ALabel.Enabled := True;
      AComboBox.Enabled := True;
      AComboBox.Color := clWindow;
      AComboBox.Tag := 0;
      ALabelUni.Enabled := True;
    end;
    2:
    begin
      if Assigned(ALabel) then
        ALabel.Enabled := True;
      AComboBox.Color := HtmlToColor(COLOURDIS);
      AComboBox.Enabled := False;
      ALabelUni.Enabled := True;
    end;
    3:
    begin
      if Assigned(ALabel) then
        ALabel.Enabled := False;
      AComboBox.ItemIndex := -1;
      AComboBox.Color := HtmlToColor(COLOURDIS);
      AComboBox.Enabled := False;
      AComboBox.Tag := -1;
      ALabelUni.Enabled := False;
    end;
  end;
end;

{*------------------------------------------------------------------------------
  Switch label, Combobox and button enabled or not depending on Etat.
     - 1: Everything enabled
      - 2: Label OK, Combobox and button disabled
      - 3: Everything disabled

   @param p_Label Label to switch on or off
   @param p_ComboBox Combobox to switch on or off
   @param p_Button Button to switch on or off
   @param Etat 1, 2 or 3. Base for value used for switching
-------------------------------------------------------------------------------}
procedure SwitchComboBoxPlus(
      const ALabel: TLabel;
      const AComboBox: TComboBox;
      const AButton: TWinControl;
      Etat: Integer);
begin
  case Etat of
    1:
    begin
      if Assigned(ALabel) then
        ALabel.Enabled := True;
      AComboBox.Enabled := True;
      AComboBox.Color := clWindow;
      AButton.Enabled := True;
    end;
    2:
    begin
      if Assigned(ALabel) then
        ALabel.Enabled := True;
      AComboBox.Enabled := False;
      AComboBox.Color := HtmlToColor(COLOURDIS);
      AButton.Enabled := False;
    end;
    3:
    begin
      if Assigned(ALabel) then
        ALabel.Enabled := False;
      AComboBox.ItemIndex := -1;
      AComboBox.Enabled := False;
      AComboBox.Color := HtmlToColor(COLOURDIS);
      AButton.Enabled := False;
    end;
  end;
end;

{*------------------------------------------------------------------------------
  Switch label and Editbox enabled or not depending on Etat
  - 1: Everything enabled
  - 2: Label OK, Edit disabled, color disabled
  - 3: Everything disabled, color disabled, cleared if CanIni
  - 4: Everything invisible
  - 5: Label OK, Edit enable and  readonly, color readonly

   @param p_Label Label to switch on or off
   @param AEdit Editbox to switch on or off
   @param Etat 1 to 5, see description above
   @param CanAct False to bypass the function (no action)
   @param CanIni Used with Etat=3, True to clear edit, False to keep value
-------------------------------------------------------------------------------}
procedure SwitchEdit(
      const ALabel: TLabel;
      const AEdit: TEdit;
      Etat: Integer;
      CanAct: Boolean=True;
      CanIni: Boolean=True);
begin
  if not CanAct then
    Exit;
  case Etat of
    1:
    begin
      if Assigned(ALabel) then
        ALabel.Enabled := True;
      AEdit.TabStop := True;
      AEdit.Enabled := True;
      AEdit.Color := clWindow;
    end;
    2:
    begin
      if Assigned(ALabel) then
        ALabel.Enabled := True;
      AEdit.Enabled := False;
      AEdit.Color := HtmlToColor(COLOURDIS);
    end;
    3:
    begin
      if Assigned(ALabel) then
        ALabel.Enabled := False;
      if CanIni then
        AEdit.Clear;
      AEdit.Enabled := False;
      AEdit.Color := HtmlToColor(COLOURDIS);
    end;
    4:
    begin
      if Assigned(ALabel) then
        ALabel.Visible := False;
      AEdit.Visible := False;
    end;
    5:
    begin
      if Assigned(ALabel) then
        ALabel.Enabled := True;
      AEdit.TabStop := False;
      AEdit.Enabled := True;
      AEdit.ReadOnly := True;
      AEdit.Color := HtmlToColor(COLOURREADONLY);
    end;
  end;
end;

procedure SwitchMemo(
      const ALabel: TLabel;
      const AEdit: TMemo;
      Etat: Integer;
      CanAct: Boolean=True;
      CanIni: Boolean=True);
begin
  if not CanAct then
    Exit;
  case Etat of
    1:
    begin
      if Assigned(ALabel) then
        ALabel.Enabled := True;
      AEdit.TabStop := True;
      AEdit.Enabled := True;
      AEdit.Color := clWindow;
    end;
    2:
    begin
      if Assigned(ALabel) then
        ALabel.Enabled := True;
      AEdit.Enabled := False;
      AEdit.Color := HtmlToColor(COLOURDIS);
    end;
    3:
    begin
      if Assigned(ALabel) then
        ALabel.Enabled := False;
      if CanIni then
        AEdit.Clear;
      AEdit.Enabled := False;
      AEdit.Color := HtmlToColor(COLOURDIS);
    end;
    4:
    begin
      if Assigned(ALabel) then
        ALabel.Visible := False;
      AEdit.Visible := False;
    end;
    5:
    begin
      if Assigned(ALabel) then
        ALabel.Enabled := True;
      AEdit.TabStop := False;
      AEdit.Enabled := True;
      AEdit.ReadOnly := True;
      AEdit.Color := HtmlToColor(COLOURREADONLY);
    end;
  end;
end;

procedure SwitchSpinEdit(
      const ALabel: TLabel;
      const AEdit: TSpinEdit;
      Etat: Integer;
      CanAct: Boolean=True;
      CanIni: Boolean=True);
begin
  if not CanAct then
    Exit;
  case Etat of
    1:
    begin
      if Assigned(ALabel) then ALabel.Enabled := True;
      AEdit.TabStop := True;
      AEdit.Enabled := True;
      AEdit.Color := clWindow;
    end;
    2:
    begin
      if Assigned(ALabel) then ALabel.Enabled := True;
      AEdit.Enabled := False;
      AEdit.Color := HtmlToColor(COLOURDIS);
    end;
    3:
    begin
      if Assigned(ALabel) then ALabel.Enabled := False;
      if CanIni then AEdit.Clear;
      AEdit.Enabled := False;
      AEdit.Color := HtmlToColor(COLOURDIS);
    end;
    4:
    begin
      if Assigned(ALabel) then ALabel.Visible := False;
      AEdit.Visible := False;
    end;
    5:
    begin
      if Assigned(ALabel) then ALabel.Enabled := True;
      AEdit.TabStop := False;
      AEdit.Enabled := True;
      AEdit.ReadOnly := True;
      AEdit.Color := HtmlToColor(COLOURREADONLY);
    end;
  end;
end;

procedure SwitchDateTimePicker(
      const ALabel: TLabel;
      const APicker: TDateTimePicker;
      Etat: Integer;
      CanAct: Boolean=True;
      CanIni: Boolean=True);
begin
  if not CanAct then
    Exit;
  case Etat of
    1:
    begin
      if Assigned(ALabel) then ALabel.Enabled := True;
      APicker.TabStop := True;
      APicker.Enabled := True;
      APicker.Color := clWindow;
    end;
    2:
    begin
      if Assigned(ALabel) then ALabel.Enabled := True;
      APicker.Enabled := False;
      APicker.Color := HtmlToColor(COLOURDIS);
    end;
    3:
    begin
      if Assigned(ALabel) then ALabel.Enabled := False;
      //if CanIni then APicker.Clear;
      APicker.Enabled := False;
      APicker.Color := HtmlToColor(COLOURDIS);
    end;
    4:
    begin
      if Assigned(ALabel) then ALabel.Visible := False;
      APicker.Visible := False;
    end;
    5:
    begin
      if Assigned(ALabel) then ALabel.Enabled := True;
      APicker.TabStop := False;
      APicker.Enabled := True;
      //APicker.ReadOnly := True;
      APicker.Color := HtmlToColor(COLOURREADONLY);
    end;
  end;
end;

{*------------------------------------------------------------------------------
  Switch label and Editbox enabled or not depending on Etat
  - 1: Everything enabled
  - 2: Label OK, Edit disabled, color disabled
  - 3: Everything disabled, color disabled, cleared if CanIni
  - 4: Everything invisible
  - 5: Label OK, Edit enable and  readonly, color readonly

   @param p_Label Label to switch on or off
   @param AEdit Editbox to switch on or off
   @param p_LabelUni Label to switch on or off
   @param Etat 1 to 5, see description above
   @param CanIni Used with Etat=3, True to clear edit, False to keep value
-------------------------------------------------------------------------------}
procedure SwitchEditUni(
      const ALabel: TLabel;
      const AEdit: TEdit;
      const ALabelUni: TLabel;
      Etat: Integer;
      CanIni: Boolean=True);
begin
  case Etat of
    1:
    begin
      if Assigned(ALabel) then
        ALabel.Enabled := True;
      AEdit.TabStop := True;
      AEdit.Enabled := True;
      AEdit.Color := clWindow;
      ALabelUni.Enabled := True;
    end;
    2:
    begin
      if Assigned(ALabel) then
        ALabel.Enabled := True;
      AEdit.Enabled := False;
      AEdit.Color := HtmlToColor(COLOURDIS);
      ALabelUni.Enabled := True;
    end;
    3:
    begin
      if Assigned(ALabel) then
        ALabel.Enabled := False;
      if (CanIni) then AEdit.Clear;
      AEdit.Enabled := False;
      AEdit.Color := HtmlToColor(COLOURDIS);
      ALabelUni.Enabled := False;
    end;
    4:
    begin
      if Assigned(ALabel) then
        ALabel.Visible := False;
      AEdit.Visible := False;
      ALabelUni.Visible := False;
    end;
    5:
    begin
      if Assigned(ALabel) then
        ALabel.Enabled := True;
      AEdit.TabStop := False;
      AEdit.Enabled := True;
      AEdit.ReadOnly := True;
      AEdit.Color := HtmlToColor(COLOURREADONLY);
      ALabelUni.Enabled := True;
    end;
  end;
end;

procedure SwitchAdvDateTimePicker(
      const ALabel: TLabel;
      const AAdvDateTimePicker: TAdvDateTimePicker;
      Etat: Integer;
      CanAct: Boolean=True;
      CanIni: Boolean=True);
begin
  if not CanAct then
    Exit;
  case Etat of
    1:
    begin
      if Assigned(ALabel) then ALabel.Enabled := True;
      AAdvDateTimePicker.Enabled := True;
      AAdvDateTimePicker.Color := clWindow;
    end;
    2:
    begin
      if Assigned(ALabel) then ALabel.Enabled := True;
      AAdvDateTimePicker.Enabled := False;
      AAdvDateTimePicker.Color := HtmlToColor(COLOURDIS);
    end;
    3:
    begin
      if Assigned(ALabel) then ALabel.Enabled := False;
      if CanIni then AAdvDateTimePicker.DateTime := Now;
      AAdvDateTimePicker.Enabled := False;
      AAdvDateTimePicker.Color := HtmlToColor(COLOURDIS);
    end;
    4:
    begin
      if Assigned(ALabel) then ALabel.Visible := False;
      AAdvDateTimePicker.Visible := False;
    end;
    5:
    begin
      if Assigned(ALabel) then ALabel.Enabled := True;
      AAdvDateTimePicker.Enabled := True;
//        AAdvDateTimePicker.ReadOnly := True;
      AAdvDateTimePicker.Color := HtmlToColor(COLOURDIS);
    end;
  end;
end;

{*------------------------------------------------------------------------------
  Switch label and CheckBox enabled or not depending on Etat.
       - 1: Everything enabled
      - 2: Label OK, CheckBox disabled
      - 3: Everything disabled

   @param p_Label Label to switch on or off
   @param p_CheckBox CheckBox to switch on or off
   @param Etat 1, 2 or 3. Base for value used for switching
-------------------------------------------------------------------------------}
procedure SwitchCheckBox(
      const ALabel: TLabel;
      const ACheckBox: TCheckBox;
      Etat: Integer);
begin
  case Etat of
    1:
    begin
      if Assigned(ALabel) then ALabel.Enabled := True;
      ACheckBox.Color := clWindow;
      ACheckBox.Enabled := True;
      ACheckBox.Tag := 0;
    end;
    2:
    begin
      if Assigned(ALabel) then ALabel.Enabled := True;
      ACheckBox.Color := HtmlToColor(COLOURDIS);
      ACheckBox.Enabled := False;
    end;
    3:
    begin
      if Assigned(ALabel) then ALabel.Enabled := False;
      ACheckBox.Color := HtmlToColor(COLOURDIS);
      ACheckBox.Enabled := False;
      ACheckBox.Tag := -1;
    end;
  end;
end;

{*------------------------------------------------------------------------------
  Set given Edit content without trigerring OnChange event
  @author BHS <boris.seve@drillscan.com>

  @param AEdit TEdit to fill
  @param AValeur Value to set to
-------------------------------------------------------------------------------}
procedure SetEdit(const AEdit: TEdit; const AValeur: String);
var
  LOnChange: TNotifyEvent;
begin
  LOnChange := AEdit.OnChange;
  AEdit.OnChange := nil;
  AEdit.Text := AValeur;
  AEdit.OnChange := LOnChange;
end;

{*------------------------------------------------------------------------------
  Set given Checkbox content without trigerring OnClick event
  @author BHS <boris.seve@drillscan.com>

  @param ACheckBox PTCheckBox to fill
  @param AChecked Shall we set the checkbox to checked state
-------------------------------------------------------------------------------}
procedure SetCheckBox(const ACheckBox: TCheckBox; AChecked: Boolean);
var
  LOnClick: TNotifyEvent;
begin
  LOnClick := ACheckBox.OnClick;
  ACheckBox.OnClick := nil;
  ACheckBox.Checked := AChecked;
  ACheckBox.OnClick := LOnClick;
end;

{*------------------------------------------------------------------------------
  Set given RadioButton content without trigerring OnClick event
  @author VM <vincent.martignoni@drillscan.com>

  @param ARadioButton TRadioButton to fill
  @param AChecked Shall we set the RadioButton to checked state
-------------------------------------------------------------------------------}
procedure SetRadioButton(const ARadioButton: TRadioButton; AChecked: Boolean);
var
  LOnClick: TNotifyEvent;
begin
  LOnClick := ARadioButton.OnClick;
  ARadioButton.OnClick := nil;
  ARadioButton.Checked := AChecked;
  ARadioButton.OnClick := LOnClick;
end;

{*------------------------------------------------------------------------------
  Set given ListView.Item[].Checked content without trigerring OnItemChecked event
  @author VM <vincent.martignoni@drillscan.com>

  @param AListView TListView to fill
  @param AItemIndex Item index in list view to fill
  @param AChecked Shall we set the CheckBox to checked state
-------------------------------------------------------------------------------}
procedure SetListViewCheck(const AListView: TListView; AItemIndex: Integer; AChecked: Boolean);
var
  LOnItemChecked: TLVCheckedItemEvent;
begin
  LOnItemChecked := AListView.OnItemChecked;
  AListView.OnItemChecked := nil;
  AListView.Items[AItemIndex].Checked := AChecked;
  AListView.OnItemChecked := LOnItemChecked;
end;

{*------------------------------------------------------------------------------
  Switch Enabled flag for all children of given control to reflect their
  parent status (useful to globally enable or disable a TPanel and all its
  children)
  @author BHS <boris.seve@drillscan.com>

  @param Container TWinControl to parse
-------------------------------------------------------------------------------}
procedure EnabledAsParent(const Container: TWinControl);
var
  index: Integer;
  aControl: TControl;
  isContainer: Boolean;
begin
  for index := 0 to container.ControlCount - 1 do
  begin
    aControl := Container.Controls[index];
    if aControl is TListView then
      Continue;
    aControl.Enabled := Container.Enabled;
    isContainer := csAcceptsControls in container.Controls[index].ControlStyle;

    // Recursive for child controls
    if isContainer and (aControl is TWinControl) then
      EnabledAsParent(aControl as TWinControl);
  end;
end;

{*------------------------------------------------------------------------------
  Enable ComboBox :
    - Tag set to 1
    - Background reset
    - Show Hint is disabled

  @param p_ComboBox ComboBox to disable
-------------------------------------------------------------------------------}
procedure SwitchOnCbx(const AComboBox: TComboBox);
begin
  AComboBox.Tag := 1;
  AComboBox.Color := clWindow;
  AComboBox.ShowHint := False;
end;

{*------------------------------------------------------------------------------
  Disable ComboBox :
    - Tag set to -1
    - Background error
    - Show Hint is enabled

   @param p_ComboBox ComboBox to activate
   @param targetColor color info
-------------------------------------------------------------------------------}
procedure SwitchOfCbx(const AComboBox: TComboBox);
begin
  AComboBox.Tag := -1;
  AComboBox.Color := HtmlToColor(COLOURERR);
  AComboBox.ShowHint := True;
end;

{*------------------------------------------------------------------------------
  Set visual status of a target ComboBox based on validity parameters

  @author JB <jeremy.berge@drillscan.com>
  @param p_ComboBox: Target combobox
  @param isValid: validity parameters
  @param Hint: Hint to be displayed in case of error
-------------------------------------------------------------------------------}
procedure ValidateComboBox(const AComboBox: TComboBox; isValid: Boolean; const Hint: String='');
begin
  if isValid then
    SwitchOnCbx(AComboBox)
  else
  begin
    AComboBox.Hint := Hint;
    SwitchOfCbx(AComboBox);
  end;
end;


end.
