(**
Colours related helpers function : HTML<->TColor, Windows colour pickup
*)
unit UnitColour;

interface

// {$I Foundation.inc}

uses
 { Standard units }
  Vcl.Graphics
  ;

const
  COLOURERR =               '#FFCCCC';
  COLOURREQ =               '#C4DEFF';
  COLOURREADONLY =          '#d4d4d4';
  COLOURDIS =               '#EEEEEE';

  function ColorToHtml(Clr: Integer): string;
  function HtmlToColor(const Color: string): Integer;
  procedure ColorToRGB2(const Color: Integer; out R, G, B: Double);
  function ColorToRGBA(const Color: Integer; AlphaPct: Integer): Integer;


implementation

uses
  { Standard units }
  Winapi.Windows,
  System.SysUtils
  ;

procedure ColorToRGB2(const Color: Integer; out R, G, B: Double);
var
  LColor: Integer;
begin
  if Color < 0 then
    LColor := GetSysColor(Color and $000000FF) // needed to handle system color or palette color
  else
    LColor := Color;
  R := LColor and $FF;
  G := (LColor shr 8) and $FF;
  B := (LColor shr 16) and $FF;
end;

function ColorToRGBA(const Color: Integer; AlphaPct: Integer): Integer;
  // Local copy from TeCanvas.pas of TeeChart so we don't have to include all
  // of TeeChart with DrillScan.Foundation.System runtime package
  function RGBA(const r,g,b,a:Integer) : Integer;
  begin
    Result := (r or (g shl 8) or (b shl 16) or (a shl 24));
  end;
var
  R,G,B : Double;
begin
  ColorToRGB2(Color, R, G, B);
  Result := RGBA(Round(R), Round(G), Round(B), Round((AlphaPct/100)*255));
end;

function ColorToHtml(Clr: Integer): string;
var
  R, G, B: Double;
begin
  ColorToRGB2(Clr, R, G, B);
  Result := Format('#%.2x%.2x%.2x', [Byte(Round(R)),Byte(Round(G)), Byte(Round(B))]);
end;

function HtmlToColor(const Color: string): Integer;
begin
  try
    if (Length(Color) >= 6) then
      Result := StringToColor('$' + Copy(Color, 6, 2) + Copy(Color, 4, 2) + Copy(Color, 2, 2))
    else
      Result := StringToColor('$FFFFFF');
  except
    Result := StringToColor('$FFFFFF');
  end;
end;

end.
