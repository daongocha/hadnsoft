unit MainForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TFormTreatCaliper = class(TForm)
    PanelMain: TPanel;

    OpenDialogGetDataFile: TOpenDialog;
    OpenDialogDeleteFingersFile: TOpenDialog;

    ButtonHelp: TButton;
    ButtonInfo: TButton;
    PanelFile: TPanel;
    CheckBoxDataFile: TCheckBox;
    ButtonGetDataFile: TButton;
    ButtonDeleteFingersFile: TButton;
    CheckBoxDeleteFingersFile: TCheckBox;
    PanelColumn: TPanel;
    EditMdColumn: TEdit;
    LabelMdColumn: TLabel;
    LabelIniFingerColumn: TLabel;
    EditIniFingerColumn: TEdit;
    LabelEndFingerColumn: TLabel;
    EditEndFingerColumn: TEdit;
    PanelWear: TPanel;
    LabelToolJoinData: TLabel;
    LabelIniFingerWear: TLabel;
    EditIniFingerWear: TEdit;
    EditEndFingerWear: TEdit;
    LabelEndFingerWear: TLabel;

    ButtonCalculate: TButton;
    LabellProgress: TLabel;
    EditProgress: TEdit;
    ButtonCancel: TButton;

    procedure ButtonGetDataFileClick(Sender: TObject);
    procedure EditMdColumnChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EditIniFingerColumnChange(Sender: TObject);
    procedure EditEndFingerColumnChange(Sender: TObject);
    procedure ButtonCalculateClick(Sender: TObject);
    procedure ButtonHelpClick(Sender: TObject);
    procedure ButtonInfoClick(Sender: TObject);
    procedure ButtonDeleteFingersFileClick(Sender: TObject);

    procedure EditIniFingerWearChange(Sender: TObject);
    procedure EditEndFingerWearChange(Sender: TObject);
    procedure ButtonCancelClick(Sender: TObject);
  private
    { Private declarations }
    FDataFile: String;
    FDeleteFingersFile: String;

    FWorkFolder: String;
    FResultFolder: String;

    FMdColumn: Integer;
    FIniFingerColumn: Integer;
    FEndFingerColumn: Integer;

    FIniFingerWear: Integer;
    FEndFingerWear: Integer;

    FLineProgress: Integer;
    FCancel: Boolean;

    function CanCompute(): Integer;
    procedure DipslayErrorMessage(const AErrorCode: Integer);
    procedure ProgressUpdate();

    procedure LockFieldsBeforeCal();
    procedure UnLockFieldsAfterCal();
    procedure Calculate();

  private const
    ERRCODE_ALL_OK = 0;
    ERRCODE_NOPATHDATA = 1;
    ERRCODE_FILEDATA   = 2;
    ERRCODE_COLUMN     = 3;
    ERRCODE_FINGERSCOLUMN = 4;
    ERRCODE_MDCOLUMN   = 5;
  public
    { Public declarations }
  end;

var
  FormTreatCaliper: TFormTreatCaliper;

implementation

uses
  System.IOUtils,
  System.Threading,
  FormInfos,
  UnitSimpleDialogs,
  UnitGestionTestBusiness,
  UnitReadCaliperData
  ;

{$R *.dfm}

procedure TFormTreatCaliper.FormCreate(Sender: TObject);
begin
  FDataFile := '';
  FDeleteFingersFile := '';

  FWorkFolder := '';
  FMdColumn := -999;
  FIniFingerColumn := -999;
  FEndFingerColumn := -999;

  FIniFingerWear := -999;
  FEndFingerWear := -999;

  FLineProgress := -999;
  FCancel := False;
end;


procedure TFormTreatCaliper.ButtonGetDataFileClick(Sender: TObject);
begin
  if OpenDialogGetDataFile.Execute() then
  begin
    FDataFile := OpenDialogGetDataFile.Filename;
    ButtonGetDataFile.Hint := FDataFile;
    ButtonGetDataFile.ShowHint := True;
    FWorkFolder := ExtractFilePath(FDataFile);
    CheckBoxDataFile.Checked := True;
  end;
end;

procedure TFormTreatCaliper.ButtonCancelClick(Sender: TObject);
begin
  FCancel := True;
end;

procedure TFormTreatCaliper.ButtonDeleteFingersFileClick(Sender: TObject);
begin
  if OpenDialogDeleteFingersFile.Execute() then
  begin
    FDeleteFingersFile := OpenDialogDeleteFingersFile.Filename;
    ButtonDeleteFingersFile.Hint := FDeleteFingersFile;
    ButtonDeleteFingersFile.ShowHint := True;
    CheckBoxDeleteFingersFile.Checked := True;
  end;
end;

procedure TFormTreatCaliper.ButtonHelpClick(Sender: TObject);
begin
  TFormInfo.DisplayFormInfo();
end;

procedure TFormTreatCaliper.ButtonInfoClick(Sender: TObject);
var
  LCaption: String;
  LTitle: String;
  LText: String;
begin
  LCaption := 'Infomation';
  LTitle := 'Caliper Data Treatment Software';
  LText := 'Developed by Ngoc Ha DAO' + #13#10 +
           '<ngoc-ha.dao@hpinc.com>' + #13#10 +
           'Copyright (c) 2022 H&P DrillScan' + #13#10 +
           'All rights reserved.';
           // '<daongocha@gmail.com>';
  TaskSimpleMessage(LCaption, LTitle, LText);
end;

procedure TFormTreatCaliper.EditEndFingerColumnChange(Sender: TObject);
var
  LOut : Integer;
begin
  LOut := -999;
  if Integer.TryParse(TEdit(Sender).Text, LOut) and
     (LOut > 0)
  then
  begin
    FEndFingerColumn := LOut;
    SwitchOn(EditIniFingerColumn);
  end
  else
  begin
    FEndFingerColumn := -999;
    SwitchOf(EditIniFingerColumn);
  end;
end;

procedure TFormTreatCaliper.EditIniFingerColumnChange(Sender: TObject);
var
  LOut : Integer;
begin
  LOut := -999;
  if Integer.TryParse(TEdit(Sender).Text, LOut) and
     (LOut > 0)
  then
  begin
    FIniFingerColumn := LOut;
    SwitchOn(EditIniFingerColumn);
  end
  else
  begin
    FIniFingerColumn := -999;
    SwitchOf(EditIniFingerColumn);
  end;
end;

procedure TFormTreatCaliper.EditMdColumnChange(Sender: TObject);
var
  LOut : Integer;
begin
  LOut := -999;
  if Integer.TryParse(TEdit(Sender).Text, LOut) and
     (LOut > 0)
  then
  begin
    FMdColumn := LOut;
    SwitchOn(EditMdColumn);
  end
  else
  begin
    FMdColumn := -999;
    SwitchOf(EditMdColumn);
  end;
end;

procedure TFormTreatCaliper.EditIniFingerWearChange(Sender: TObject);
var
  LOut : Integer;
begin
  LOut := -999;
  if Integer.TryParse(TEdit(Sender).Text, LOut) and
     (LOut > 0)
  then
  begin
    FIniFingerWear := LOut;
    SwitchOn(EditIniFingerWear);
  end
  else
  begin
    FIniFingerWear := -999;
    SwitchOf(EditIniFingerWear);
  end;
end;

procedure TFormTreatCaliper.EditEndFingerWearChange(Sender: TObject);
var
  LOut : Integer;
begin
  LOut := -999;
  if Integer.TryParse(TEdit(Sender).Text, LOut) and
     (LOut > 0)
  then
  begin
    FEndFingerWear := LOut;
    SwitchOn(EditEndFingerWear);
  end
  else
  begin
    FEndFingerWear := -999;
    SwitchOf(EditEndFingerWear);
  end;
end;

procedure TFormTreatCaliper.ButtonCalculateClick(Sender: TObject);
var
  LErrorCode: Integer;
begin
  TTask.Run(
    procedure
    begin
      LErrorCode := CanCompute();
      // verify the parameter
      if LErrorCode <> ERRCODE_ALL_OK then
      begin
        DipslayErrorMessage(LErrorCode);
        Exit;
      end
      else
      begin
        Calculate();
      end;
    end);
end;

function TFormTreatCaliper.CanCompute(): Integer;
begin
  Result := ERRCODE_ALL_OK;
  if FDataFile = '' then
    Exit(ERRCODE_NOPATHDATA);

  if not FileExists(FDataFile) then
    Exit(ERRCODE_FILEDATA);

  // verify the parameters
  if (FMdColumn < 0) or
     (FIniFingerColumn < 0) or
     (FEndFingerColumn < 0)
  then
    Exit(ERRCODE_COLUMN);

  if (FIniFingerColumn >= FEndFingerColumn) then
    Exit(ERRCODE_FINGERSCOLUMN);

  if (FMdColumn >= FIniFingerColumn) and
     (FMdColumn <= FEndFingerColumn)
  then
    Exit(ERRCODE_MDCOLUMN);
end;

procedure TFormTreatCaliper.DipslayErrorMessage(const AErrorCode: Integer);
var
  LErrorString: String;
begin
  LErrorString := '';

  case AErrorCode of
    ERRCODE_NOPATHDATA:
      LErrorString := '';
    ERRCODE_FILEDATA:
      LErrorString := '';
    ERRCODE_COLUMN:
      LErrorString := '';
    ERRCODE_FINGERSCOLUMN:
      LErrorString := '';
    ERRCODE_MDCOLUMN:
      LErrorString := '';
  else

  end;
end;

procedure TFormTreatCaliper.ProgressUpdate();
begin
  if FLineProgress > 0 then
    EditProgress.Text := IntToStr(FLineProgress)
  else
    EditProgress.Text := '';
end;

procedure TFormTreatCaliper.LockFieldsBeforeCal();
begin
  PanelFile.Enabled := False;
  PanelColumn.Enabled := False;
  PanelWear.Enabled := False;

  ButtonCalculate.Enabled := False;
end;

procedure TFormTreatCaliper.UnLockFieldsAfterCal();
begin
  PanelFile.Enabled := True;
  PanelColumn.Enabled := True;
  PanelWear.Enabled := True;

  ButtonCalculate.Enabled := True;
end;

procedure TFormTreatCaliper.Calculate();
var
  LComputationDone : Boolean;
  FingerMinAll, FingerMaxAll : Double;
  jMinAll, jMaxAll : Integer;
begin
  FResultFolder := TPath.Combine(FWorkFolder, 'CaliperTreatEllipseResults');
  CreateDir(FResultFolder);

  // Start progress in parallel to computation
  LComputationDone := False;

//  TTask.Run(procedure
//  begin
//    repeat
//      Sleep(2000);
//      TThread.Synchronize(
//        nil,
//        procedure
//        begin
//          ProgressUpdate();
//        end
//      );
//    until LComputationDone;
//  end);

  TTask.Run(procedure
  begin
    repeat
      Sleep(1000);
      ProgressUpdate();
    until LComputationDone
  end);

  // Calcul
  try
    FCancel := False;
    ButtonCancel.Enabled := True;
    FLineProgress := -999;
    LockFieldsBeforeCal();

    ReadDataCaliperFittingEllipseDelSomeFingers3 (
      FDataFile, FDeleteFingersFile, FResultFolder,
      FMdColumn, FIniFingerColumn,FEndFingerColumn,
      FIniFingerWear, FEndFingerWear,
      FingerMinAll, FingerMaxAll, jMinAll, jMaxAll,
      @FCancel, @FLineProgress);
  finally
    LComputationDone := True;
    ButtonCancel.Enabled := False;
    UnLockFieldsAfterCal();
  end;

end;

end.
