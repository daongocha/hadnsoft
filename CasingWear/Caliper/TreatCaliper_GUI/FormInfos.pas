unit FormInfos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TFormInfo = class(TForm)
    PanelHelp: TPanel;
    LabelTextHelp: TLabel;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure PanelHelpMessage();
  public
    { Public declarations }

  class function DisplayFormInfo(): Boolean;

  end;

var
  FormInfo: TFormInfo;

implementation

{$R *.dfm}

procedure TFormInfo.FormShow(Sender: TObject);
begin
  PanelHelpMessage();
end;

procedure TFormInfo.PanelHelpMessage();
begin
  LabelTextHelp.Caption := 'Caliper Data Treatment Software' + #13 + #10 +
                           'Developed by Ngoc Ha DAO' + #13 + #10 +
                           '<ngoc-ha.dao@hpinc.com>' + #13 + #10 +
                           'Help to develop.'
end;

class function TFormInfo.DisplayFormInfo(): Boolean;
var
  Form: TFormInfo;
begin
  if Screen.ActiveForm <> nil then
  begin
    Form := TFormInfo.Create(Screen.ActiveForm);
  end
  else
    Form := TFormInfo.Create(Application.MainForm);

  try
    Form.ShowModal;
  finally
    Form.Free;
  end;
end;



end.
