program TreatCaliperGUI;



uses
  Vcl.Forms,
  MainForm in 'Forms\MainForm.pas' {FormTreatCaliper},
  UnitColour in '..\..\..\GUI_Utinity_Units\UnitColour.pas',
  UnitGestionTestBusiness in '..\..\..\GUI_Utinity_Units\UnitGestionTestBusiness.pas',
  FormInfos in 'Forms\FormInfos.pas' {FormInfo},
  UnitSimpleDialogs in '..\..\..\GUI_Utinity_Units\UnitSimpleDialogs.pas',
  UnitDifferentProgTreat in '..\TreatCaliperData\Units\UnitDifferentProgTreat.pas',
  UnitTreatCaliperData in '..\TreatCaliperData\Units\UnitTreatCaliperData.pas',
  UnitModeleStructureBasic in '..\ReadCaliperData\Units\UnitModeleStructureBasic.pas',
  UnitReadCaliperData in '..\ReadCaliperData\Units\UnitReadCaliperData.pas',
  UnitsMathTools in '..\ReadCaliperData\Units\UnitsMathTools.pas',
  UnitDebugCommon in '..\..\..\GeneralUnits\Debug\UnitDebugCommon.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFormTreatCaliper, FormTreatCaliper);
  Application.CreateForm(TFormInfo, FormInfo);
  Application.Run;
end.
