object FormTreatCaliper: TFormTreatCaliper
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Treat Caliper Data'
  ClientHeight = 520
  ClientWidth = 338
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PanelMain: TPanel
    Left = 8
    Top = 8
    Width = 321
    Height = 473
    TabOrder = 0
    object LabellProgress: TLabel
      Left = 37
      Top = 401
      Width = 60
      Height = 13
      Caption = 'Treated Line'
    end
    object ButtonCalculate: TButton
      Left = 141
      Top = 365
      Width = 97
      Height = 25
      Caption = 'Calculate'
      TabOrder = 0
      OnClick = ButtonCalculateClick
    end
    object EditProgress: TEdit
      Left = 141
      Top = 398
      Width = 97
      Height = 21
      ReadOnly = True
      TabOrder = 1
      OnChange = EditEndFingerColumnChange
    end
    object PanelFile: TPanel
      Left = 21
      Top = 18
      Width = 282
      Height = 89
      TabOrder = 2
      object CheckBoxDataFile: TCheckBox
        Left = 233
        Top = 20
        Width = 16
        Height = 17
        Enabled = False
        TabOrder = 0
      end
      object ButtonGetDataFile: TButton
        Left = 120
        Top = 16
        Width = 97
        Height = 25
        Caption = 'Select Data File'
        TabOrder = 1
        OnClick = ButtonGetDataFileClick
      end
      object ButtonDeleteFingersFile: TButton
        Left = 120
        Top = 47
        Width = 97
        Height = 25
        Caption = 'Delete Finger File'
        TabOrder = 2
        OnClick = ButtonDeleteFingersFileClick
      end
      object CheckBoxDeleteFingersFile: TCheckBox
        Left = 233
        Top = 51
        Width = 16
        Height = 17
        Caption = 'Delete Finger File'
        Enabled = False
        TabOrder = 3
      end
    end
    object PanelColumn: TPanel
      Left = 21
      Top = 118
      Width = 282
      Height = 117
      TabOrder = 3
      object LabelMdColumn: TLabel
        Left = 17
        Top = 16
        Width = 52
        Height = 13
        Caption = 'Md Column'
      end
      object LabelIniFingerColumn: TLabel
        Left = 17
        Top = 46
        Width = 97
        Height = 13
        Caption = 'Initial Finger Column'
      end
      object LabelEndFingerColumn: TLabel
        Left = 17
        Top = 81
        Width = 89
        Height = 13
        Caption = 'End Finger Column'
      end
      object EditMdColumn: TEdit
        Left = 120
        Top = 13
        Width = 97
        Height = 21
        TabOrder = 0
        OnChange = EditMdColumnChange
      end
      object EditIniFingerColumn: TEdit
        Left = 120
        Top = 45
        Width = 97
        Height = 21
        TabOrder = 1
        OnChange = EditIniFingerColumnChange
      end
      object EditEndFingerColumn: TEdit
        Left = 120
        Top = 77
        Width = 97
        Height = 21
        TabOrder = 2
        OnChange = EditEndFingerColumnChange
      end
    end
    object PanelWear: TPanel
      Left = 21
      Top = 248
      Width = 282
      Height = 106
      TabOrder = 4
      object LabelToolJoinData: TLabel
        Left = 17
        Top = 9
        Width = 133
        Height = 13
        Caption = 'Wear Groove (optional)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LabelIniFingerWear: TLabel
        Left = 17
        Top = 41
        Width = 88
        Height = 13
        Caption = 'Initial Finger Wear'
      end
      object LabelEndFingerWear: TLabel
        Left = 17
        Top = 76
        Width = 80
        Height = 13
        Caption = 'End Finger Wear'
      end
      object EditIniFingerWear: TEdit
        Left = 120
        Top = 39
        Width = 97
        Height = 21
        TabOrder = 0
        OnChange = EditIniFingerWearChange
      end
      object EditEndFingerWear: TEdit
        Left = 120
        Top = 72
        Width = 97
        Height = 21
        TabOrder = 1
        OnChange = EditEndFingerWearChange
      end
    end
    object ButtonCancel: TButton
      Left = 141
      Top = 427
      Width = 97
      Height = 25
      Caption = 'Cancel'
      Enabled = False
      TabOrder = 5
      OnClick = ButtonCancelClick
    end
  end
  object ButtonHelp: TButton
    Left = 274
    Top = 487
    Width = 25
    Height = 25
    Caption = '?'
    TabOrder = 1
    OnClick = ButtonHelpClick
  end
  object ButtonInfo: TButton
    Left = 305
    Top = 487
    Width = 25
    Height = 25
    Caption = 'I'
    TabOrder = 2
    OnClick = ButtonInfoClick
  end
  object OpenDialogGetDataFile: TOpenDialog
    DefaultExt = '.txt'
    Options = [ofHideReadOnly, ofFileMustExist, ofEnableSizing]
    Title = 'Load Data File'
    Left = 306
    Top = 3
  end
  object OpenDialogDeleteFingersFile: TOpenDialog
    Left = 296
    Top = 72
  end
end
