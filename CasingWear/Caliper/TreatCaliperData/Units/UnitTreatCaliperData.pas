unit UnitTreatCaliperData;

interface

uses
	Windows, SysUtils, Classes, Math,
  UnitModeleStructureBasic,
  UnitsMathTools;

function ReadDataCaliperExample (FilePathRead : String; var n : Integer; var Finger : t_vdd) : Boolean;
function ReadDataCaliperExample2 (FilePathRead : String; var n : Integer; var Finger : t_vdd) : Boolean;
function ReadXYExample (FilePathRead : String; var n : Integer; var X, Y : t_vdd) : Boolean;
Procedure ChangeAxisToEllipseAxis (Xc, Yc, Theta, X, Y : Double; Var Xnew, Ynew : Double);
Procedure ChangeAxisToGlobalAxis (Xc, Yc, Theta, Xelp, Yelp : Double; Var Xnew, Ynew : Double);
Procedure IntersectionLineEllipse (Aelp, Belp : Double; X0, Y0, Vx, Vy : Double;
                                   Var Xres, Yres : Double);
Function DistancePointToIntersectionEllipse (X, Y, A, B : Double) : Double;
function CalSectionCaliper (n : Integer; Finger : t_vdd) : Double;
Procedure CalXYfromFingers (n : Integer; Finger : t_vdd;
                            var X, Y : t_vdd);
Procedure CalXY_XciYciRi_fromFingers (n : Integer; Finger : t_vdd;
                                      var X, Y : t_vdd;
                                      var Xci, Yci, Ri : Double);
Procedure CalXY_XciYciAiBiThetai_fromFingers (n : Integer; Finger : t_vdd;
                                      var X, Y : t_vdd;
                                      var Xci, Yci, Ai, Bi, Thetai : Double);
Procedure CentralizationCaliperData (n : Integer; Finger : t_vdd;
                                     var Xc, Yc : Double;
                                     var NewFinger : t_vdd);
Procedure CalNewFingerXcYc (n : Integer; X, Y : t_vdd;
                            Xc, Yc : Double;
                            var NewFinger : t_vdd);
Procedure CalNewFingerOfEllipse (n : Integer; X, Y : t_vdd;
                                 A, B, Theta, Xc, Yc : Double;
                                 var NewFingerEllipse : t_vdd;
                                 var Xelp, Yelp : t_vdd);

function FittingEllipseLevenbergMarquardt (X, Y : t_vdd;
                                           var Xc, Yc : Double;
                                           var A, B : Double;
                                           var Theta : Double) : Boolean;
function FittingCircleLevenbergMarquardt (n : Integer; X, Y : t_vdd;
                                          Xci, Yci, Ri : Double;
                                          var Xc, Yc, R : Double;
                                          var SumSquaRes : Double) : Boolean;
function FittingEllipseKnowingCenterLevenbergMarquardt (n : Integer; X, Y : t_vdd;
                                                        Ai, Bi, Thetai : Double;
                                                        var A, B, Theta : Double;
                                                        var SumSquaRes : Double) : Boolean;
function FittingEllipseGeneralLevenbergMarquardt (n : Integer; X, Y : t_vdd;
                                                 Ai, Bi, Thetai, Xci, Yci : Double;
                                                 var A, B, Theta, Xc, Yc: Double;
                                                 var SumSquaRes : Double;
                                                 var IsOK : Boolean) : Boolean;

function FittingEllipseFourierTransform (n : Integer; Rayon, Theta : t_vdd;
    var SumSquaRes : Double;
    var IsOK : Boolean) : Boolean;

// HaDN 19/12/2016

function CalculLarFromHau(const Hauteur, Rcg, Rtj: Double): Double;
function WearHightOfFinger(const Rcsg, Rtj, hmax, Alpha : Double): Double;
procedure GenerateInitialWearFingers(const Rcsg, Rtj : Double;
    const nFingers : Integer;
    const hmax, TfoMax : Double;
    var WearHightFingers : t_vdd);

implementation

{-------------------------------------------------------------------------------
  Calculate the section from caliper data
  Parameters :
  + n : number of finger,
  + Finger (array) measured data at each finger.
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
function CalSectionCaliper (n : Integer; Finger : t_vdd) : Double;
var
  i : Integer;
  Sec : Double;
  Ang : Double;
  //
  function SectionTriangle (A, B, Angle : Double) : Double;
  begin
    result := A*B*sin(Angle)/2.0;
  end;
Begin
  Ang := 2*Pi/n;
  Sec := 0.0;
  for i := 0 to n-2 do Sec := Sec + SectionTriangle(Finger[i], Finger[i+1], Ang);
  Sec := Sec + SectionTriangle(Finger[n-1], Finger[0], Ang);
  Result := Sec;
End;


{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
function ReadDataCaliperExample (FilePathRead : String; var n : Integer; var Finger : t_vdd) : Boolean;
var
   ok : Boolean;
   i : integer;
   ReadValue : Double;
   FicRead : textfile;
begin
  ok := false;
  if FileExists(FilePathRead) then
  begin
    ok := True;
    AssignFile(FicRead, FilePathRead);
    Reset(FicRead);
    i := 0;
    repeat
      Read(FicRead, ReadValue);
      if EoLn(FicRead) then Readln(FicRead);
      if (ReadValue>0) then
      begin
        setlength(Finger, i+1);
        Finger[i] := ReadValue;
        i := i+1;
      end;
    until (EOF(FicRead));
    n := i;
    CloseFile(FicRead);
  end;
  Result := ok;
end;
// -----------------------------------------------------------------------------

{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
function ReadDataCaliperExample2 (FilePathRead : String; var n : Integer; var Finger : t_vdd) : Boolean;
var
   ok : Boolean;
   i : integer;
   FicRead : textfile;
begin
  ok := false;
  if FileExists(FilePathRead) then
  begin
    ok := True;
    AssignFile(FicRead, FilePathRead);
    Reset(FicRead);
    Readln(FicRead, n);
    setlength(Finger, n);
    for i := 1 to n do
    begin
      Read(FicRead, Finger[i-1]);
    end;
    CloseFile(FicRead);
  end;
  Result := ok;
end;
// -----------------------------------------------------------------------------

{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
function ReadXYExample (FilePathRead : String; var n : Integer; var X, Y : t_vdd) : Boolean;
var
   ok : Boolean;
   i : integer;
   ReadValue : Double;
   FicRead : textfile;
begin
  ok := false;
  if FileExists(FilePathRead) then
  begin
    ok := True;
    AssignFile(FicRead, FilePathRead);
    Reset(FicRead);
    i := 0;
    repeat
      setlength(X, i+1);
      setlength(Y, i+1);
      Read(FicRead, X[i], Y[i]);
      i := i+1;
    until (EOF(FicRead));
    n := i;
    CloseFile(FicRead);
  end;
  Result := ok;
end;
// -----------------------------------------------------------------------------

{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Change Axis un point (X, Y) to the axis of ellipse (Xnew, Ynew)
  Axis of ellipse : a translation (Xc, Yc) and a rotation (Theta)
-------------------------------------------------------------------------------}
Procedure ChangeAxisToEllipseAxis (Xc, Yc, Theta, X, Y : Double; Var Xnew, Ynew : Double);
begin
  Xnew := (X-Xc)*cos(Theta)+(Y-Yc)*sin(Theta);
  Ynew := -(X-Xc)*sin(Theta)+(Y-Yc)*cos(Theta);
end;

{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Change Axis un point (Xelp, Yelp) in ellipse axis to the global axis (Xnew, Ynew)
  Axis of ellipse : a translation (Xc, Yc) and a rotation (Theta)
-------------------------------------------------------------------------------}
Procedure ChangeAxisToGlobalAxis (Xc, Yc, Theta, Xelp, Yelp : Double; Var Xnew, Ynew : Double);
begin
  Xnew := Xelp*cos(Theta) - Yelp*sin(Theta) + Xc;
  Ynew := Xelp*sin(Theta) + Yelp*cos(Theta) + Yc;
end;
{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Intersection of a line of the origin (X0, Y0) to the direction (Vx, Vy) with
  the ellipse
-------------------------------------------------------------------------------}
Procedure IntersectionLineEllipse (Aelp, Belp : Double; X0, Y0, Vx, Vy : Double;
                                   Var Xres, Yres : Double);
var
  Alpha, Delta, A, B, C : Double;
begin
  A := Sqr(Vx/Aelp)+sqr(Vy/Belp);
  B := 2*((Vx*X0/sqr(Aelp))+(Vy*Y0/sqr(Belp)));
  C := Sqr(X0/Aelp)+sqr(Y0/Belp)-1;

  Delta := Sqr(B)-4*A*C;
  if Delta >= 0 then
  begin
    Alpha := (-B + Sqrt(Delta))/(2*A);
    Xres := X0 + (Alpha*Vx);
    Yres := Y0 + (Alpha*Vy)
  end else
  begin
    Xres := -999;
    Yres := -999;
  end;
end;

{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
Function DistancePointToIntersectionEllipse (X, Y, A, B : Double) : Double;
var
  DisToCenter : Double;
  Alpha, Radius : Double;
begin
  if (X = 0) and (Y = 0) then
  begin
    Result := -999;
    exit;
  end;
  DisToCenter := sqrt(sqr(X)+sqr(Y));
  Alpha := Sqrt(1/(sqr(X/A)+sqr(Y/B)));
  Radius := Alpha*DisToCenter;
  Result :=  DisToCenter-Radius;
end;
{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
Procedure CalXYfromFingers (n : Integer; Finger : t_vdd;
                            var X, Y : t_vdd);
var
  i : Integer;
  Tfo : Double;
begin
  setlength(X, n);
  setlength(Y, n);
  for i := 0 to n-1 do
  begin
    Tfo := (2*Pi/n)*i;
    X[i] := Finger[i]*cos(Tfo);
    Y[i] := Finger[i]*sin(Tfo);
  end;
end;
// -----------------------------------------------------------------------------

{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
Procedure CalXY_XciYciRi_fromFingers (n : Integer; Finger : t_vdd;
                                      var X, Y : t_vdd;
                                      var Xci, Yci, Ri : Double);
var
  i : Integer;
  Tfo : Double;
  Ecc : Double;  // Eccentricity
  NoMax : Integer;
begin
  setlength(X, n);
  setlength(Y, n);
  //
  Ecc := 0.0;
  NoMax :=0;
  for i := 0 to (round(n/2)-1) do
  begin
    if abs(Finger[i]-Finger[i+round(n/2)])>Ecc then
    begin
      Ecc := abs(Finger[i]-Finger[i+round(n/2)]);
      if (Finger[i]>Finger[i+round(n/2)]) then NoMax := i
      else NoMax := i+round(n/2);
    end;
  end;
  NoMax := NoMax+1; // Number of finger
  //
  Tfo := (2*Pi/n)*(NoMax-1);
  Xci := (Ecc/2)*cos(Tfo);
  Yci := (Ecc/2)*sin(Tfo);
  //
  Ri := 0.0;
  for i := 0 to n-1 do
  begin
    Tfo := (2*Pi/n)*i;
    X[i] := Finger[i]*cos(Tfo);
    Y[i] := Finger[i]*sin(Tfo);
    Ri := Ri + sqrt(sqr(X[i]-Xci)+sqr(Y[i]-Yci))/n;
  end;
  //
end;
// -----------------------------------------------------------------------------


{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
Procedure CalXY_XciYciAiBiThetai_fromFingers (n : Integer; Finger : t_vdd;
                                      var X, Y : t_vdd;
                                      var Xci, Yci, Ai, Bi, Thetai : Double);
var
  i : Integer;
  Tfo : Double;
  Ecc : Double;  // Eccentricity
  Ri : Double;
  NoMax : Integer;
  MaxNew, MinNew, FingerNew : Double;
begin
  // SetLength only one time
  // setlength(X, n);
  // setlength(Y, n);
  //
  Ecc := 0.0;
  NoMax :=0;
  for i := 0 to (round(n/2)-1) do
  begin
    if abs(Finger[i]-Finger[i+round(n/2)])>Ecc then
    begin
      Ecc := abs(Finger[i]-Finger[i+round(n/2)]);
      if (Finger[i]>Finger[i+round(n/2)]) then NoMax := i
      else NoMax := i+round(n/2);
    end;
  end;
  NoMax := NoMax+1; // Number of finger
  //
  Tfo := (2*Pi/n)*(NoMax-1);
  Xci := (Ecc/2)*cos(Tfo);
  Yci := (Ecc/2)*sin(Tfo);
  // Thetai := Tfo;
  //
  Ri := 0.0;
  for i := 0 to n-1 do
  begin
    Tfo := (2*Pi/n)*i;
    X[i] := Finger[i]*cos(Tfo);
    Y[i] := Finger[i]*sin(Tfo);
    Ri := Ri + sqrt(sqr(X[i]-Xci)+sqr(Y[i]-Yci))/n;
  end;
  //
  i := 0;
  FingerNew := Sqrt(sqr(X[i]-Xci)+sqr(Y[i]-Yci));
  MaxNew := FingerNew;
  MinNew := FingerNew;
  for i := 0 to (n-1) do
  begin
    FingerNew := Sqrt(sqr(X[i]-Xci)+sqr(Y[i]-Yci));
    if (FingerNew > MaxNew) then
    begin
      MaxNew := FingerNew;
      NoMax := i;
    end;
    if (FingerNew < MinNew) then MinNew := FingerNew;
  end;
  Thetai := (2*Pi/n)*(NoMax);
  if (MaxNew-MinNew)< (1E-3*MinNew) then MaxNew := MinNew*1.05;
  Ai := MaxNew;
  Bi := MinNew;
end;
// -----------------------------------------------------------------------------


{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
Procedure CentralizationCaliperData (n : Integer; Finger : t_vdd;
                                     var Xc, Yc : Double;
                                     var NewFinger : t_vdd);
var
  i, j : Integer;
  X, Y : t_vdd;
  Tfo : Double;
  FingerPro, TfoPro : t_vdd;
  // Dia : t_vdd;
  Ecc : Double;  // Eccentricity
  Oval : Double;
  NoMax : Integer;

  FicPath : String;
  Fic : textfile;
  //
  Function  CalAngle2Vectors (X1, Y1 : Double; X2, Y2 : Double) : Double;
  var
    L1, L2 : Double;
    ProSca : Double;
    Res : Double;
  begin
    L1 := sqrt(sqr(X1)+sqr(Y1));
    L2 := sqrt(sqr(X2)+sqr(Y2));
    if (L1>0) and (L2>0) then
    begin
      ProSca := X1*X2 + Y1*Y2;
      Res := arccos(ProSca/(L1*L2));
    end else Res := 0.0;
    Result := Res;
  end;
  //
  Function  CalNewFinger (Tfo : Double; n : Integer; ListFinger, ListTfo :  t_vdd) : Double;
  var
    i : Integer;
    Flag : Boolean;
    Res, a, b, alpha, beta : Double;
  begin
    Flag := False;
    for i := 1 to n-1 do
    begin
      if ((Tfo>=ListTfo[i-1]) and (Tfo<=ListTfo[i])) then
      begin
        Flag := True;
        a := ListFinger[i-1];
        b := ListFinger[i];
        alpha :=  ListTfo[i]-ListTfo[i-1];
        beta := Tfo-ListTfo[i-1];
        Break;
      end;
    end;
    if (Flag = False) then
    begin
      a := ListFinger[n-1];
      b := ListFinger[0];
      alpha :=  2*Pi-ListTfo[n-1];
      beta := Tfo-ListTfo[n-1];
    end;
    // Area of triangles
    // Res*b*sin(alpha-beta)+Res*a*sin(beta)=a*b*sin(alpha)
    Res := a*b*sin(alpha)/(b*sin(alpha-beta)+a*sin(beta));
    Result := Res;
  end;
begin
  setlength(NewFinger, n);
  setlength(FingerPro, n);
  setlength(X, n);
  setlength(Y, n);
  setlength(TfoPro, n);
  // setlength(Dia, round(n/2));
  //
  for i := 0 to n-1 do
  begin
    Tfo := (2*Pi/n)*i;
    X[i] := Finger[i]*cos(Tfo);
    Y[i] := Finger[i]*sin(Tfo);
  end;
  //
//  for i := 0 to (round(n/2)-1) do
//  begin
//    Dia[i] := Finger[i]+Finger[i+round(n/2)];
//  end;
  //
//  Oval := 0.0;
//  for i := 0 to (round(n/4)-1) do
//  begin
//    j := i+round(n/4);
//    if abs(Dia[i]-Dia[j])/2>Oval then Oval := abs(Dia[i]-Dia[j])/2;
//  end;
  //
  Ecc := 0.0;
  NoMax :=0;
  for i := 0 to (round(n/2)-1) do
  begin
    if abs(Finger[i]-Finger[i+round(n/2)])>Ecc then
    begin
      Ecc := abs(Finger[i]-Finger[i+round(n/2)]);
      if (Finger[i]>Finger[i+round(n/2)]) then NoMax := i
      else NoMax := i+round(n/2);
    end;
  end;
  NoMax := NoMax+1; // Number of finger
  //
  Tfo := (2*Pi/n)*(NoMax-1);
  Xc := (Ecc/2)*cos(Tfo);
  Yc := (Ecc/2)*sin(Tfo);
  //
  for i := 0 to n-1 do
  begin
    if (i<1) then TfoPro[0] := 0
    else TfoPro[i] := TfoPro[i-1]+ CalAngle2Vectors (X[i-1]-Xc, Y[i-1]-Yc, X[i]-Xc, Y[i]-Yc);
    FingerPro[i] := sqrt(sqr(X[i]-Xc)+sqr(Y[i]-Yc));
  end;
  //
  for i := 1 to n do
  begin
    if (i=1) then NewFinger[0] := FingerPro[0]
    else NewFinger[i-1] := CalNewFinger ((i-1)*2*Pi/n, n, FingerPro, TfoPro);
  end;
//  //
//  FicPath := 'C:\HaDN_Data\DrillScanWork\CasingWear\Treat_Caliper_Data\Test.txt';
//  AssignFile(Fic, FicPath);
//  Rewrite(Fic);
//  for i := 0 to n-1 do
//  begin
//    if i<1 then writeln(Fic,' ', X[i]:20,
//                            ' ', Y[i]:20,
//                            ' ', (X[i]+X[i+round(n/2)])/2:20,
//                            ' ', (Y[i]+Y[i+round(n/2)])/2:20,
//                            ' ', Xc:20,
//                            ' ', Yc:20)
//    else if i<n/2 then writeln(Fic,' ', X[i]:20,
//                                   ' ', Y[i]:20,
//                                   ' ', (X[i]+X[i+round(n/2)])/2:20,
//                                   ' ', (Y[i]+Y[i+round(n/2)])/2:20)
//    else writeln(Fic,' ', X[i]:20,' ', Y[i]:20);
//  end;
//  writeln(Fic,' ', X[0]:20,' ', Y[0]:20);
//  CloseFile(Fic);
//  //
//  FicPath := 'C:\HaDN_Data\DrillScanWork\CasingWear\Treat_Caliper_Data\Test1.txt';
//  AssignFile(Fic, FicPath);
//  Rewrite(Fic);
//  for i := 0 to n-1 do writeln(Fic,' ', Finger[i]:20,
//                                   ' ', FingerPro[i]:20,
//                                   ' ', TfoPro[i]*180/Pi:20:15,
//                                   ' ', NewFinger[i]:20 );
//  CloseFile(Fic);
//  //
//  FicPath := 'C:\HaDN_Data\DrillScanWork\CasingWear\Treat_Caliper_Data\Test2.txt';
//  AssignFile(Fic, FicPath);
//  Rewrite(Fic);
//  writeln(Fic,' ', Nomax:20,
//              ' ', Ecc:20);
//  for i := 0 to round(n/2-1) do
//  begin
//    writeln(Fic,' ', Finger[i]+Finger[i+round(n/2)]:20,
//                ' ', abs(Finger[i]-Finger[i+round(n/2)]):20);
//  end;
//  CloseFile(Fic);
//  //
//  FicPath := 'C:\HaDN_Data\DrillScanWork\CasingWear\Treat_Caliper_Data\Test3.txt';
//  AssignFile(Fic, FicPath);
//  Rewrite(Fic);
//  for i := 0 to n-1 do
//  begin
//    Tfo := (2*Pi/n)*i;
//    writeln(Fic,' ', NewFinger[i]*cos(Tfo):20,' ', NewFinger[i]*sin(Tfo):20,
//                ' ', FingerPro[i]*cos(TfoPro[i]):20,' ', FingerPro[i]*sin(TfoPro[i]):20);
//  end;
//  i := 0;
//  Tfo := (2*Pi/n)*i;
//  writeln(Fic,' ', NewFinger[i]*cos(Tfo):20,' ', NewFinger[i]*sin(Tfo):20,
//              ' ', FingerPro[i]*cos(TfoPro[i]):20,' ', FingerPro[i]*sin(TfoPro[i]):20);
//  CloseFile(Fic);
//  //

  setlength(FingerPro, 0);
  setlength(X, 0);
  setlength(Y, 0);
  setlength(TfoPro, 0);
  // setlength(Dia, 0);
end;
// -----------------------------------------------------------------------------

{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
Procedure CalNewFingerXcYc (n : Integer; X, Y : t_vdd;
                            Xc, Yc : Double;
                            var NewFinger : t_vdd);
var
  i, j : Integer;
  Tfo : Double;
  FingerPro, TfoPro : t_vdd;
  NoMax : Integer;
  //
  Function  CalAngle2Vectors (X1, Y1 : Double; X2, Y2 : Double) : Double;
  var
    L1, L2 : Double;
    ProSca : Double;
    Res : Double;
  begin
    L1 := sqrt(sqr(X1)+sqr(Y1));
    L2 := sqrt(sqr(X2)+sqr(Y2));
    if (L1>0) and (L2>0) then
    begin
      ProSca := X1*X2 + Y1*Y2;
      Res := arccos(ProSca/(L1*L2));
    end else Res := 0.0;
    Result := Res;
  end;
  //
  Function  CalNewFinger (Tfo : Double; n : Integer; ListFinger, ListTfo :  t_vdd) : Double;
  var
    i : Integer;
    Flag : Boolean;
    Res, a, b, alpha, beta : Double;
  begin
    Flag := False;
    for i := 1 to n-1 do
    begin
      if ((Tfo>=ListTfo[i-1]) and (Tfo<=ListTfo[i])) then
      begin
        Flag := True;
        a := ListFinger[i-1];
        b := ListFinger[i];
        alpha :=  ListTfo[i]-ListTfo[i-1];
        beta := Tfo-ListTfo[i-1];
        Break;
      end;
    end;
    if (Flag = False) then
    begin
      a := ListFinger[n-1];
      b := ListFinger[0];
      alpha :=  2*Pi-ListTfo[n-1];
      beta := Tfo-ListTfo[n-1];
    end;
    // Area of triangles
    // Res*b*sin(alpha-beta)+Res*a*sin(beta)=a*b*sin(alpha)
    Res := a*b*sin(alpha)/(b*sin(alpha-beta)+a*sin(beta));
    Result := Res;
  end;
begin
  // setlength(NewFinger, n);
  setlength(FingerPro, n);
  setlength(TfoPro, n);
  // setlength(Dia, round(n/2));
  //
  for i := 0 to n-1 do
  begin
    if (i<1) then TfoPro[0] := 0
    else TfoPro[i] := TfoPro[i-1]+ CalAngle2Vectors (X[i-1]-Xc, Y[i-1]-Yc, X[i]-Xc, Y[i]-Yc);
    FingerPro[i] := sqrt(sqr(X[i]-Xc)+sqr(Y[i]-Yc));
  end;
  //
  for i := 1 to n do
  begin
    if (i=1) then NewFinger[0] := FingerPro[0]
    else NewFinger[i-1] := CalNewFinger ((i-1)*2*Pi/n, n, FingerPro, TfoPro);
  end;
  setlength(FingerPro, 0);
  setlength(TfoPro, 0);
end;
// -----------------------------------------------------------------------------

{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  NewFinger = Distance of (Old Center to Intersection old finger with ellipse)
  For Florian to compare the ellipse with the data
-------------------------------------------------------------------------------}
Procedure CalNewFingerOfEllipse (n : Integer; X, Y : t_vdd;
                                 A, B, Theta, Xc, Yc : Double;
                                 var NewFingerEllipse : t_vdd;
                                 var Xelp, Yelp : t_vdd);
var
  i : Integer;
  X0, Y0 : Double;       // Old center
  X0elp, Y0elp : Double; // Old center of fingers in ellipe axis
  Xp, Yp : Double;       // Finger point
  Xpelp, Ypelp : Double;   // Finger point in ellipe axis
  XpRes, YpRes : Double;   // Intersection Finger - Ellipse
  // Intersection Finger - ellipse
  Alpha, Vx, Vy : Double;
begin
  X0 := 0.0;
  Y0 := 0.0;
  ChangeAxisToEllipseAxis (Xc, Yc, Theta, X0, Y0, X0elp, Y0elp);
  // SetLength(NewFingerEllipse, n);
  for i := 1 to n do
  begin
    Xp := X[i-1];
    Yp := Y[i-1];
    ChangeAxisToEllipseAxis (Xc, Yc, Theta, Xp, Yp, Xpelp, Ypelp);
    Vx := Xpelp - X0elp;
    Vy := Ypelp - Y0elp;
    IntersectionLineEllipse (A, B, X0elp, Y0elp, Vx, Vy, XpRes, YpRes);
    NewFingerEllipse[i-1] := Sqrt(Sqr(XpRes-X0elp)+Sqr(YpRes-Y0elp));

    ChangeAxisToGlobalAxis (Xc, Yc, Theta, XpRes, YpRes, Xelp[i-1], Yelp[i-1]);
  end;
end;



{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
function FittingEllipseLevenbergMarquardt (X, Y : t_vdd;
                                           var Xc, Yc : Double;
                                           var A, B : Double;
                                           var Theta : Double) : Boolean;
  //--------------------------------------------------------------------------//
  Procedure ChangeAxis (Xp, Yp : Double; Xc, Yc : Double; Theta : Double;
                        var XpNew, YpNew : Double);
  var
    Xtemp, Ytemp : Double;
  begin
    // translation
    Xtemp := Xp - Xc;
    Ytemp := Yp - Yc;
    // Rotation
    XpNew := Xtemp*cos(Theta) + Ytemp*sin(Theta);
    YpNew := Ytemp*cos(Theta) - Xtemp*sin(Theta);
  end;
  //--------------------------------------------------------------------------//

  //--------------------------------------------------------------------------//
  // Xp, Yp are the coordinates of point in the canonical axis of ellipse
  Procedure ProjectionPointToEllipse (Xp, Yp : Double; A, B : Double;
                                      var Xproj, Yproj : Double);
  var
    Eps, EpsB, EpsA2 : Double;
    XpA, YpA : Double;
    iter : Integer;
    T, Tini : Double;
    TA2, TB2, A2, B2, PP1, PP2 : Double;
    F, Fder, Ratio : Double;
  begin
    Eps := 1E-12;
    EpsA2 := Eps*A*A;

    XpA := abs(Xp);
    YpA := abs(Yp);
    Tini := max(A*(XpA-A), B*(YpA-B));
    A2 := A*A;
    B2 := B*B;
    if (A>B) then
    begin
      if XpA<Eps*A then
      begin
        Xproj := 0;
        if Yp>0 then Yproj := B
        else Yproj := - B;
      end else
      begin

      end;
    end;

    // Loop
    T := Tini;
    for iter := 1 to 100 do
    begin
      TA2 := T + A2;
      TB2 := T + B2;
      PP1 := sqr(XpA*A/TA2);
      PP2 := sqr(YpA*B/TB2);
      F  := PP1 + PP2 - 1;
      if F<0 then break;
      Fder := 2*(PP1/TA2 + PP2/TB2);
      Ratio := F/Fder;
      if (Ratio<EpsA2) then break;
      T := T + Ratio;
    end;

    Xproj := Xp*A2/TA2;
    if Yp>0 then Yproj := B*sqrt(1-sqr(Xproj/A))
    else Yproj := - B*sqrt(1-sqr(Xproj/A));

  end;
  //--------------------------------------------------------------------------//
  //--------------------------------------------------------------------------//
VAR
  XpNew, YpNew : Double;
BEGIN
  ChangeAxis (5, 8, 2, 1, 105*Pi/180,
              XpNew, YpNew);

  ProjectionPointToEllipse (1, -5 , 3, 7,
                            XpNew, YpNew);

END;
// -----------------------------------------------------------------------------


{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
function FittingCircleLevenbergMarquardt (n : Integer; X, Y : t_vdd;
                                          Xci, Yci, Ri : Double;
                                          var Xc, Yc, R : Double;
                                          var SumSquaRes : Double) : Boolean;
//  //--------------------------------------------------------------------------//
//  // Xp, Yp are the coordinates of point in the global axis
//  // Not Use Now in New Version
//  Procedure ProjectionPointToCircle (Xp, Yp : Double;
//                                     Xc, Yc : Double; R : Double;
//                                     var Xproj, Yproj : Double; var Res : Double);
//  var
//    Eps : Double;
//    Rp : Double;
//  begin
//    Eps := 1E-20;
//    Rp := sqrt(sqr(Xp-Xc)+sqr(Yp-Yc));
//    Res := Rp-R;
//    if (Rp<Eps*R) then
//    begin
//      if abs(Xp-Xc)<abs(Yp-Yc) then
//      begin
//        Xproj := Xc;
//        if (Yp-Yc>0) then Yproj := Yc+R
//        else Yproj := Yc-R
//      end else
//      begin
//        Yproj := Yc;
//        if (Xp-Xc>0) then Xproj := Xc+R
//        else Xproj := Xc-R;
//      end;
//    end else
//    begin
//      Xproj := ((Xp-Xc)/Rp)*R;
//      Yproj := ((Yp-Yc)/Rp)*R;
//    end;
//  end;
//  //--------------------------------------------------------------------------//
//  //--------------------------------------------------------------------------//
//  Procedure ResidualAndJmatrixLMG (n : Integer; X, Y : t_vdd;
//                                   Xc, Yc : Double;
//                                   R : Double;
//                                   var SumSquaRes : Double;
//                                   var Residual : t_vdd;
//                                   var Jacobin : t_mdr) ;
//  var
//    Eps, EpsR, EpsR2 : Double;
//    XpA, YpA : Double;
//    iter : Integer;
//    T, Tini : Double;
//    TA2, TB2, A2, B2, PP1, PP2 : Double;
//    F, Fder, Ratio : Double;
//  begin
//    Eps := 1E-12;
//    EpsR := Eps*R;
//    EpsR2 := Eps*R*R;
//  end;
//  //--------------------------------------------------------------------------//
  //*********************************************************//
  function CalFunction_fi(XX, YY, XXc, YYc, RR : Double) : Double;
  begin
    Result := RR - sqrt(sqr(XX-XXc)+sqr(YY-YYc));
  end;
  //*********************************************************//
  function CalGradient_fi_Xc(XX, YY, XXc, YYc, RR : Double) : Double;
  begin
    Result := (XX-XXc)/sqrt(sqr(XX-XXc)+sqr(YY-YYc));
  end;
  //*********************************************************//
  function CalGradient_fi_Yc(XX, YY, XXc, YYc, RR : Double) : Double;
  begin
    Result := (YY-YYc)/sqrt(sqr(XX-XXc)+sqr(YY-YYc));;
  end;
  //*********************************************************//
  function CalGradient_fi_R(XX, YY, XXc, YYc, RR : Double) : Double;
  begin
    Result := 1;
  end;
  //*********************************************************//
  //--------------------------------------------------------------------------//
VAR
  i, j : Integer;
  Ym : Double;
  ok : boolean;
  F : t_vdd;        // Vector n elements
  GradF : t_vdd;    // Matrix n*3 - Matrix Jacobien
  GradF_T : t_vdd;  // Transpose of GradF -> Matrix 3*n
  mat : t_vdd;      // mat = GradF_T.GradF + lamda*diag(GradF_T.GradF) -> 3*3
  matDiag : t_vdd;  // Diagonale of Mat
  matw1, matw2 : t_vdd;  // Work Matrix
  vecTem, vec : t_vdd;       // vec = -grad_f = -GradF_T*F -> 3
  P : t_vdd;
  NorP : Double;
  NorGradf : Double;           // NorGradf = |grad_f|
  mm, nn : Integer;          // Matrix Size
  iter, iterLim : Integer;

  lmLamda : Double;            // Levenberg�Marquardt Factor Lamda
  lmFactor : Double;           // Factor for correcting lmLamda after each iteration
  SumSquX, SumSquXplusP : Double;  // Previous and Actual Sum squares (for determining lmfactor of each iteration)

  Xcw, Ycw, Rw : Double;        // Work A, B, C
BEGIN
  //
  setlength(F, n);
  setlength(GradF_T, 3*n);
  iterLim := 1000;
  iter := 0;
  lmLamda := 0.001;
  lmFactor := 10;
  // Initial values of Xc, Yc, Ri
  Xc := Xci;
  Yc := Yci;
  R := Ri;
  repeat
    iter := iter+1;
    Xcw := Xc;
    Ycw := Yc;
    Rw := R;
    for j := 0 to n-1 do F[j] := CalFunction_fi(X[j], Y[j], Xcw, Ycw, Rw);
    SumSquX := enormvec(n, F);
    // Derivatives : dfi/dA, dfi/dB, dfi/dC
    // Transpose du gradient de F -> GradF_T
    for j := 0 to n-1 do GradF_T[0*n+j] := CalGradient_fi_Xc(X[j], Y[j], Xcw, Ycw, Rw);
    for j := 0 to n-1 do GradF_T[1*n+j] := CalGradient_fi_Yc(X[j], Y[j], Xcw, Ycw, Rw);
    for j := 0 to n-1 do GradF_T[2*n+j] := CalGradient_fi_R(X[j], Y[j], Xcw, Ycw, Rw);
    TranposeMatrix (GradF_T, 3, n, GradF, mm, nn);
    // GradF_T.GradF --> matw1
    ProductMatrix (GradF_T, 3, n, GradF, n, 3, matw1, mm, nn);
    // Diag(GradF_T.GradF) --> matDiag
    ExtractDiagMatrix (matw1, 3, 3, matDiag, mm, nn);
    MultNumberMatrix (lmLamda, matDiag, 3, 3, matw2, mm, nn);
    AddMatrix (matw1, 3, 3, matw2, 3, 3, mat, mm, nn);
    // vec = -grad_f = GradF_T*F
    ProductMatrix (GradF_T, 3, n, F, n, 1, vecTem, mm, nn);
    MultNumberMatrix (-1.0, vecTem, 3, 1, vec, mm, nn);
    NorGradf := enormvec(3, vec);
    // Inverse Mat --> Matwork
    CalInverseMatrix3x3New (mat, matw2);
    ProductMatrix (matw2, 3, 3, vec, 3, 1, P, mm, nn);
    NorP := enormvec(3, P);
    Xcw := Xc+P[0];
    Ycw := Yc+P[1];
    Rw := R+P[2];
    for j := 0 to n-1 do F[j] := CalFunction_fi(X[j], Y[j], Xcw, Ycw, Rw);
    SumSquXplusP := enormvec(n, F);
    // Writeln(' ', iter:3,' ', A:10:6,' ', B:10:6, ' ', C:10:6,' ||', SumSquX:10, SumSquXplusP:10);
    if (SumSquXplusP>=SumSquX)  then
    begin
      lmLamda := lmLamda*lmFactor;
    end else
    begin
      lmLamda := lmLamda/lmFactor;
      // Update A, B, C
      Xc := Xc + p[0];
      Yc := Yc + p[1];
      R := R + p[2];
    end;
  until ((NorGradf<1E-20) or (NorP<1E-20) or (iter>= iterLim));
  // Free Memory
  setlength(F, 0);
  setlength(GradF, 0);
  setlength(GradF_T, 0);
  setlength(mat, 0);
  setlength(matDiag, 0);
  setlength(matw1, 0);
  setlength(matw2, 0);
  setlength(vecTem, 0);
  setlength(vec, 0);
  setlength(P, 0);
  //
  SumSquaRes := 0.0;
  for i := 0 to n-1 do begin
    SumSquaRes := SumSquaRes + sqr(CalFunction_fi(X[j], Y[j], Xc, Yc, R));
  end;
  //
  Result := True;
END;
// -----------------------------------------------------------------------------


{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
function FittingEllipseKnowingCenterLevenbergMarquardt (n : Integer; X, Y : t_vdd;
                                                        Ai, Bi, Thetai : Double;
                                                        var A, B, Theta : Double;
                                                        var SumSquaRes : Double) : Boolean;

  //*********************************************************//
  function CalFunction_fi(XX, YY, AA, BB, TT : Double) : Double;
  begin
    Result :=   sqr((XX*cos(TT)+YY*sin(TT))/AA)
              + sqr((-XX*sin(TT)+YY*cos(TT))/BB) - 1;
  end;
  //*********************************************************//
  function CalGradient_fi_A(XX, YY, AA, BB, TT : Double) : Double;
  begin
    Result := -2*sqr(XX*cos(TT)+YY*sin(TT))/(AA*AA*AA);
  end;
  //*********************************************************//
  function CalGradient_fi_B(XX, YY, AA, BB, TT : Double) : Double;
  begin
    Result := -2*sqr(-XX*sin(TT)+YY*cos(TT))/(BB*BB*BB);
  end;
  //*********************************************************//
  function CalGradient_fi_Theta(XX, YY, AA, BB, TT : Double) : Double;
  begin
    Result := 2*(XX*cos(TT)+YY*sin(TT))*(-XX*sin(TT)+YY*cos(TT))*((1/sqr(AA))-(1/sqr(BB)));
  end;
  //*********************************************************//
  //--------------------------------------------------------------------------//
VAR
  i, j : Integer;
  Ym : Double;
  ok : boolean;
  F : t_vdd;        // Vector n elements
  GradF : t_vdd;    // Matrix n*3 - Matrix Jacobien
  GradF_T : t_vdd;  // Transpose of GradF -> Matrix 3*n
  mat : t_vdd;      // mat = GradF_T.GradF + lamda*diag(GradF_T.GradF) -> 3*3
  matDiag : t_vdd;  // Diagonale of Mat
  matw1, matw2 : t_vdd;  // Work Matrix
  vecTem, vec : t_vdd;       // vec = -grad_f = -GradF_T*F -> 3
  P : t_vdd;
  NorP : Double;
  NorGradf : Double;           // NorGradf = |grad_f|
  mm, nn : Integer;          // Matrix Size
  iter, iterLim : Integer;

  lmLamda : Double;            // Levenberg�Marquardt Factor Lamda
  lmFactor : Double;           // Factor for correcting lmLamda after each iteration
  SumSquX, SumSquXplusP : Double;  // Previous and Actual Sum squares (for determining lmfactor of each iteration)

  Aw, Bw, Thetaw : Double;        // Work parameters
BEGIN
  // To test : why it does not run when Ai=Bi
  setlength(F, n);
  setlength(GradF_T, 3*n);
  iterLim := 1000;
  iter := 0;
  lmLamda := 0.001;
  lmFactor := 10;
  // Initial values of A, B and Theta
  A := Ai;
  B := Bi;
  Theta := Thetai;
  repeat
    iter := iter+1;
    Aw := A;
    Bw := B;
    Thetaw := Theta;
    for j := 0 to n-1 do F[j] := CalFunction_fi(X[j], Y[j], Aw, Bw, Thetaw);
    SumSquX := enormvec(n, F);
    // Derivatives : dfi/dA, dfi/dB, dfi/dC
    // Transpose du gradient de F -> GradF_T
    for j := 0 to n-1 do GradF_T[0*n+j] := CalGradient_fi_A(X[j], Y[j], Aw, Bw, Thetaw);
    for j := 0 to n-1 do GradF_T[1*n+j] := CalGradient_fi_B(X[j], Y[j], Aw, Bw, Thetaw);
    for j := 0 to n-1 do GradF_T[2*n+j] := CalGradient_fi_Theta(X[j], Y[j], Aw, Bw, Thetaw);
    TranposeMatrix (GradF_T, 3, n, GradF, mm, nn);
    // GradF_T.GradF --> matw1
    ProductMatrix (GradF_T, 3, n, GradF, n, 3, matw1, mm, nn);
    // Diag(GradF_T.GradF) --> matDiag
    ExtractDiagMatrix (matw1, 3, 3, matDiag, mm, nn);
    MultNumberMatrix (lmLamda, matDiag, 3, 3, matw2, mm, nn);
    AddMatrix (matw1, 3, 3, matw2, 3, 3, mat, mm, nn);
    // vec = -grad_f = GradF_T*F
    ProductMatrix (GradF_T, 3, n, F, n, 1, vecTem, mm, nn);
    MultNumberMatrix (-1.0, vecTem, 3, 1, vec, mm, nn);
    NorGradf := enormvec(3, vec);
    // Inverse Mat --> Matwork
    CalInverseMatrix3x3New (mat, matw2);
    ProductMatrix (matw2, 3, 3, vec, 3, 1, P, mm, nn);
    NorP := enormvec(3, P);
    Aw := A+P[0];
    Bw := B+P[1];
    Thetaw := Theta+P[2];
    for j := 0 to n-1 do F[j] := CalFunction_fi(X[j], Y[j], Aw, Bw, Thetaw);
    SumSquXplusP := enormvec(n, F);
    // Writeln(' ', iter:3,' ', A:10:6,' ', B:10:6, ' ', C:10:6,' ||', SumSquX:10, SumSquXplusP:10);
    if (SumSquXplusP>=SumSquX)  then
    begin
      lmLamda := lmLamda*lmFactor;
    end else
    begin
      lmLamda := lmLamda/lmFactor;
      // Update A, B, C
      A := A + p[0];
      B := B + p[1];
      Theta := Theta + p[2];
    end;
  until ((NorGradf<1E-20) or (NorP<1E-20) or (iter>= iterLim));
  // Free Memory
  setlength(F, 0);
  setlength(GradF, 0);
  setlength(GradF_T, 0);
  setlength(mat, 0);
  setlength(matDiag, 0);
  setlength(matw1, 0);
  setlength(matw2, 0);
  setlength(vecTem, 0);
  setlength(vec, 0);
  setlength(P, 0);
  //
  SumSquaRes := 0.0;
  for i := 0 to n-1 do
  begin
    SumSquaRes := SumSquaRes + sqr(CalFunction_fi(X[j], Y[j], A, B, Theta));
  end;
  //
  Result := True;
END;
// -----------------------------------------------------------------------------


{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
function FittingEllipseGeneralLevenbergMarquardt (n : Integer; X, Y : t_vdd;
                                                 Ai, Bi, Thetai, Xci, Yci : Double;
                                                 var A, B, Theta, Xc, Yc: Double;
                                                 var SumSquaRes : Double;
                                                 var IsOK : Boolean) : Boolean;
  //*********************************************************//
  function CalFunction_fi(XX, YY, AA, BB, TT, XXc, YYc : Double) : Double;
  begin
    Result :=   sqr(((XX-XXc)*cos(TT)+(YY-YYc)*sin(TT))/AA)
              + sqr((-(XX-XXc)*sin(TT)+(YY-YYc)*cos(TT))/BB) - 1;
  end;
    //*********************************************************//
  function CalGradient_fi_A(XX, YY, AA, BB, TT, XXc, YYc : Double) : Double;
  begin
    Result := -2*sqr((XX-XXc)*cos(TT)+(YY-YYc)*sin(TT))/(AA*AA*AA);
  end;
  //*********************************************************//
  function CalGradient_fi_B(XX, YY, AA, BB, TT, XXc, YYc : Double) : Double;
  begin
    Result := -2*sqr(-(XX-XXc)*sin(TT)+(YY-YYc)*cos(TT))/(BB*BB*BB);
  end;
  //*********************************************************//
  function CalGradient_fi_Theta(XX, YY, AA, BB, TT, XXc, YYc : Double) : Double;
  begin
    Result := 2 * ((XX-XXc)*cos(TT)+(YY-YYc)*sin(TT))
                * (-(XX-XXc)*sin(TT)+(YY-YYc)*cos(TT))
                * ((1/sqr(AA))-(1/sqr(BB)));
  end;
  //*********************************************************//
  function CalGradient_fi_Xc(XX, YY, AA, BB, TT, XXc, YYc : Double) : Double;
  begin
    Result := -(2/sqr(AA))*((XX-XXc)*cos(TT)+(YY-YYc)*sin(TT))*cos(TT)
              +(2/sqr(BB))*(-(XX-XXc)*sin(TT)+(YY-YYc)*cos(TT))*sin(TT);
  end;
  //*********************************************************//
  function CalGradient_fi_Yc(XX, YY, AA, BB, TT, XXc, YYc : Double) : Double;
  begin
    Result := -(2/sqr(AA))*((XX-XXc)*cos(TT)+(YY-YYc)*sin(TT))*sin(TT)
              -(2/sqr(BB))*(-(XX-XXc)*sin(TT)+(YY-YYc)*cos(TT))*cos(TT);
  end;
  //*********************************************************//
var
  i, j : Integer;
  Ym : Double;
  ok : boolean;
  F : t_vdd;        // Vector n elements
  GradF : t_vdd;    // Matrix n*3 - Matrix Jacobien
  GradF_T : t_vdd;  // Transpose of GradF -> Matrix 3*n
  mat : t_vdd;      // mat = GradF_T.GradF + lamda*diag(GradF_T.GradF) -> 3*3
  matDiag : t_vdd;  // Diagonale of Mat
  matw1, matw2 : t_vdd;  // Work Matrix
  vecTem, vec : t_vdd;       // vec = -grad_f = -GradF_T*F -> 3
  P : t_vdd;
  NorP : Double;
  NorGradf : Double;           // NorGradf = |grad_f|
  mm, nn : Integer;          // Matrix Size
  iter, iterLim : Integer;
  lmLamda : Double;            // Levenberg�Marquardt Factor Lamda
  lmFactor : Double;           // Factor for correcting lmLamda after each iteration
  SumSquX, SumSquXplusP : Double;  // Previous and Actual Sum squares (for determining lmfactor of each iteration)
  nbPara : Integer;
  Aw, Bw, Thetaw, Xcw, Ycw : Double;        // Work A, B, C
  ier : Integer;
BEGIN
  IsOK := True;
  nbPara := 5;
  //
  setlength(F, n);
  setlength(GradF_T, nbPara*n);
  iterLim := 1000;
  iter := 0;
  lmLamda := 0.001;
  lmFactor := 10;
  // Initial values of Xc, Yc, Ri
  A := Ai;
  B := Bi;
  Theta := Thetai;
  Xc := Xci;
  Yc := Yci;
  repeat
    iter := iter+1;
    Aw := A;
    Bw := B;
    Thetaw := Theta;
    Xcw := Xc;
    Ycw := Yc;
    for j := 0 to n-1 do F[j] := CalFunction_fi(X[j], Y[j], Aw, Bw, Thetaw, Xcw, Ycw);
    SumSquX := enormvec(n, F);
    // Derivatives : dfi/dA, dfi/dB, dfi/dC
    // Transpose du gradient de F -> GradF_T
    for j := 0 to n-1 do GradF_T[0*n+j] := CalGradient_fi_A(X[j], Y[j], Aw, Bw, Thetaw, Xcw, Ycw);
    for j := 0 to n-1 do GradF_T[1*n+j] := CalGradient_fi_B(X[j], Y[j], Aw, Bw, Thetaw, Xcw, Ycw);
    for j := 0 to n-1 do GradF_T[2*n+j] := CalGradient_fi_Theta(X[j], Y[j], Aw, Bw, Thetaw, Xcw, Ycw);
    for j := 0 to n-1 do GradF_T[3*n+j] := CalGradient_fi_Xc(X[j], Y[j], Aw, Bw, Thetaw, Xcw, Ycw);
    for j := 0 to n-1 do GradF_T[4*n+j] := CalGradient_fi_Yc(X[j], Y[j], Aw, Bw, Thetaw, Xcw, Ycw);
    TranposeMatrix (GradF_T, nbPara, n, GradF, mm, nn);
    // GradF_T.GradF --> matw1
    ProductMatrix (GradF_T, nbPara, n, GradF, n, nbPara, matw1, mm, nn);
    // Diag(GradF_T.GradF) --> matDiag
    ExtractDiagMatrix (matw1, nbPara, nbPara, matDiag, mm, nn);
    MultNumberMatrix (lmLamda, matDiag, nbPara, nbPara, matw2, mm, nn);
    AddMatrix (matw1, nbPara, nbPara, matw2, nbPara, nbPara, mat, mm, nn);
    // vec = -grad_f = GradF_T*F
    ProductMatrix (GradF_T, nbPara, n, F, n, 1, vecTem, mm, nn);
    MultNumberMatrix (-1.0, vecTem, nbPara, 1, vec, mm, nn);
    NorGradf := enormvec(nbPara, vec);
    // Inverse Mat --> Matwork
    // CalInverseMatrix3x3New (mat, matw2);
    gaussj_InvertMat_t_vdd(mat, nbPara, matw2, ier);
    ProductMatrix (matw2, nbPara, nbPara, vec, nbPara, 1, P, mm, nn);
    NorP := enormvec(nbPara, P);
    Aw := A+p[0];
    Bw := B+p[1];
    Thetaw := Theta+p[2];
    Xcw := Xc+p[3];
    Ycw := Yc+p[4];

    for j := 0 to n-1 do F[j] := CalFunction_fi(X[j], Y[j], Aw, Bw, Thetaw, Xcw, Ycw);
    SumSquXplusP := enormvec(n, F);
    // Writeln(' ', iter:3,' ', A:10:6,' ', B:10:6, ' ', C:10:6,' ||', SumSquX:10, SumSquXplusP:10);
    if (SumSquXplusP>=SumSquX)  then
    begin
      lmLamda := lmLamda*lmFactor;
    end else
    begin
      lmLamda := lmLamda/lmFactor;
      // Update A, B, C
      A := A+p[0];
      B := B+p[1];
      Theta := Theta+p[2];
      Xc := Xc+p[3];
      Yc := Yc+p[4];
    end;
    if ((iter>= iterLim) and Not((NorGradf<1E-20) or (NorP<1E-20))) then
      IsOK := False;
  until ((NorGradf<1E-20) or (NorP<1E-20) or (iter>= iterLim));
  if ((Theta > 2*Pi) or (Theta < 0.0)) then Theta := Theta - (2*Pi)*Trunc(Theta/(2*Pi));
  if (Theta < 0.0) then Theta := Theta + (2*Pi);

  // Free Memory
  setlength(F, 0);
  setlength(GradF, 0);
  setlength(GradF_T, 0);
  setlength(mat, 0);
  setlength(matDiag, 0);
  setlength(matw1, 0);
  setlength(matw2, 0);
  setlength(vecTem, 0);
  setlength(vec, 0);
  setlength(P, 0);
  //
  SumSquaRes := 0.0;
  for i := 0 to n-1 do
  begin
    SumSquaRes := SumSquaRes + sqr(CalFunction_fi(X[j], Y[j], A, B, Theta, Xc, Yc));
  end;
  //
  Result := True;
END;
// -----------------------------------------------------------------------------

{$REGION 'INITIAL_WEAR'}
// ***** Initial wear for caliper log ***** Initial wear for caliper log *****//

function CalculLarFromHau(const Hauteur, Rcg, Rtj: Double): Double;
begin
  if (Hauteur < 0) or (Hauteur > Rtj) then
    Exit(-999);
  if Hauteur < 1E-50 then
    Exit(0.0);
  Exit(Sqrt(Sqr(Rcg) -
      Sqr((Sqr(Rcg)-Sqr(Rtj)+Sqr(Rcg-Rtj+Hauteur)) / (2*(Rcg-Rtj+Hauteur)))));
end;

{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Alpha : Angle between the finger and the direction of tool-joint displacement
-------------------------------------------------------------------------------}
function WearHightOfFinger(const Rcsg, Rtj, hmax, Alpha : Double): Double;
var
  a, b, c, delta, t : Double;
  Dep : Double;
begin
  // Considering the axis of casing center
  // Coordinates of tool-joint center : [0, -(Rcsg+hmax-Rtjt)] => [0, -Dep]
  Dep := (Rcsg+hmax-Rtj);
  // Coordinates Point of finger on casing cercle : [-Rcsg*Sin(Alpha), -Rcsg*Cos(Alpha)]
  if Sqrt(Sqr(-Rcsg*Sin(Alpha))+Sqr(-Rcsg*Cos(Alpha)+Dep)) > Rtj then
    Exit (0.0);

  a := 1;
  b := -2*Dep*Cos(Alpha);
  c := Sqr(Dep)-Sqr(Rtj);
  delta := sqr(b)-4*a*c;
  if delta >= 0.0 then
  begin
    t := max((-b-sqrt(delta))/2/a,
             (-b+sqrt(delta))/2/a);
    Result := max(0.0, t - Rcsg);
  end else
    Exit(0.0)
end;

procedure GenerateInitialWearFingers(const Rcsg, Rtj : Double;
    const nFingers : Integer;
    const hmax, TfoMax : Double;
    var WearHightFingers : t_vdd);
var
  i : Integer;
  TfoStep, TfoCur : Double;
  Alpha, AlphaMax : Double;

begin
  // Clear
  SetLength(WearHightFingers, 0);
  SetLength(WearHightFingers, nFingers+1);
  for i := 0 to nFingers do
    WearHightFingers[i] := 0.0;

  AlphaMax := Asin(CalculLarFromHau(hmax, Rcsg, Rtj)/Rcsg);
  TfoStep := 2*pi/nFingers;
  TfoCur := -TfoStep;
  for i := 0 to nFingers do
  begin
    TfoCur := TfoCur + TfoStep;
    Alpha := TfoCur - TfoMax;
    while Alpha < -Pi do
      Alpha := Alpha + 2*Pi;
    while Alpha > Pi do
      Alpha := Alpha - 2*Pi;
    if abs(Alpha) <= AlphaMax then
      WearHightFingers[i] := WearHightOfFinger(Rcsg, Rtj, hmax, Alpha);
  end;
end;

// ***** Initial wear for caliper log ***** Initial wear for caliper log *****//
{$ENDREGION}

{$REGION 'TREAT_CALIPER_Fourier_Transform'}

function Cal_Coef_e0 (const n : Integer;
    const Rayon : t_vdd): Double;
var
  i : Integer;
  Res : Double;
begin
  if n < 1 then
    Exit(-999);
  Res := 0.0;
  for i := 0 to n-1 do
    Res := Res + Rayon[i];
  Result := Res/n;
end;

procedure Cal_Coef_e (
    const n : Integer;
    const Rayon, Theta : t_vdd;
    var e : t_vdd);
var
  i, k : Integer;
  Res : Double;
begin
  SetLength(e, 0);
  if n < 1 then
    Exit;
  SetLength(e, 4);
  for k := 1 to 4 do
  begin
    Res := 0.0;
    for i := 0 to n-1 do
      Res := Res + Rayon[i]*cos(k*Theta[i]);
    e[k-1] := Res*2/n;
  end;
end;

procedure Cal_Coef_s (
    const n : Integer;
    const Rayon, Theta : t_vdd;
    var s : t_vdd);
var
  i, k : Integer;
  Res : Double;
begin
  SetLength(s, 0);
  if n < 1 then
    Exit;
  SetLength(s, 4);
  for k := 1 to 4 do
  begin
    Res := 0.0;
    for i := 0 to n-1 do
      Res := Res + Rayon[i]*sin(k*Theta[i]);
    s[k-1] := Res*2/n;
  end;
end;

function FittingEllipseFourierTransform (n : Integer; Rayon, Theta : t_vdd;
    var SumSquaRes : Double;
    var IsOK : Boolean) : Boolean;
var
  e, s: t_vdd;
  h, hh, I0p, I0m, I1p, I1m, P, Q, PP, QQ : Double;
  e0, e1, e2, e3, e4 : Double;
  s1, s2, s3, s4 : Double;
  DX, Dy : Double;
  b, lam, phi, db, dlam, dphi : Double;
  f0, f_db, f_dlam, f_dphi : array [0..2] of Double;

  procedure Cal_Equ_Terms();
  begin
    h  := (1-sqrt(1-lam))/(1+sqrt(1-lam));
    hh := sqr(h);
    I0p := (1+(hh/4)*(1+(hh/16)*(1+hh/4)))/(1+h);
    I1p := - (h/(1+h))*(1-(hh/8)*(1+hh/8));
    I0m := (1+h)*(1+(hh/4)*(1+(hh*9/16)*(1+hh*25/36)));
    I1m := h*(1+h)*(1+(hh*3/8)*(1+hh*5/8));
    P := -(2-lam)*e1 + (lam/2)*((e3+e1)*cos(2*Phi)+(s3+s1)*sin(2*Phi));
    Q := -(2-lam)*s1 + (lam/2)*((s3-s1)*cos(2*Phi)-(e3-e1)*sin(2*Phi));
    PP := sqr(P);
    QQ := sqr(Q);
  end;

  function Func_f() : Double;
  begin
    Result := + (2-lam)*e0
              - (lam/2)*(e2*cos(2*Phi)+s2*sin(2*Phi))
              - 2*I0p*(b-(PP+QQ)/(8*b))
              - I0m*(PP+QQ)/(8*b)
              - P*Q*I1m*sin(2*Phi)/(8*b)
              -(PP-QQ)*I1m*cos(2*Phi)/(16*b);
  end;

  function Func_g() : Double;
  begin
    Result := + (1-lam/2)*s2
              - (lam/4)*(s4*cos(2*Phi)+(2*e0-e4)*sin(2*Phi))
              - I1p*(b-(PP+QQ)/(8*b))*sin(2*Phi)
              - I1m*(PP+QQ)*sin(2*Phi)/(16*b)
              - P*Q*I0m/(8*b);
  end;

  function Func_h() : Double;
  begin
    Result := + (1-lam/2)*e2
              - (lam/4)*((2*e0+e4)*cos(2*Phi)+s4*sin(2*Phi))
              - I1p*(b-(PP+QQ)/(8*b))*cos(2*Phi)
              - I1m*(PP+QQ)*cos(2*Phi)/(16*b)
              - (PP-QQ)*I0m/(16*b);
  end;

begin


end;

{$ENDREGION}

end.
