(** Definition of types and constants *)
unit UnitModeleStructureBasic;

interface

{.$I ..\Science.inc}
uses
  Classes;

const
  g           = 10.00;
  Pi          = 3.141592653589793238462643383279;
type
  { pointeurs }
  t_int_p      = ^integer;
  t_real_p     = ^real;

  { vecteurs & tableaux fixes }
  t_v2r        = array [0..1] of real;
  t_v2r_p      = ^t_v2r;
  t_m2r        = array [0..1,0..1] of real;
  t_m2r_p      = ^t_m2r;
  t_vec        = array [0..2] of real;
  t_vec_p      = ^t_vec;
  t_mat        = array [0..2,0..2] of real;
  t_mat_p      = ^t_mat;
	tvr = array [0..1000-1] of real;
	pvr = ^tvr;

  TArrayString = TArray<string>;

  { vecteurs & tableaux dynamiques }
  t_vdi   = array of integer;
  t_vdi_p = ^t_vdi;
  t_vdr   = array of real;
  t_vdr_p = ^t_vdr;
  t_vds   = array of string;
  t_vds_p = ^t_vds;
  t_mdr = array of array of real;
  t_mdr_p = ^t_mdr;
  t_vdd   = array of double;
  t_vdd_p = ^t_vdd;
  t_vdv   = array of t_vec;
  t_vdv_p = ^t_vdv;
  t_mdvec = array of array of t_vec;
  t_mdvec_p = ^t_mdvec;
  T3Real = record
    a : Real;
    b : Real;
    c : Real;
  end;
  T4Real = record
    a : Real;
    b : Real;
    c : Real;
    d : Real;
  end;

implementation

end.
