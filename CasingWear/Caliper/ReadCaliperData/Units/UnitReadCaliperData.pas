unit UnitReadCaliperData;

interface

uses
  Windows, SysUtils, Classes, Math,
  UnitModeleStructureBasic;

type
  TDataCaliper = record
    Md : Double;
    n : Integer;
    Fingers : t_vdd;
    Max : Double;
    Min : Double;
    Mean : Double;
    Medi : Double;
    LSPD : Double;
    MITDEV : Double;
    MITROT : Double;
    MITTEMP : Double;
    GR : Double;
    ShiftedNumber : Integer;
  end;
  PDataCaliper = ^TDataCaliper;

  TDataCaliperHelper = record helper for TDataCaliper
    procedure FreeListsAndPointers(AKillGraph: Boolean=True);
    procedure Init();
    procedure CopyTo(const p_Out: PDataCaliper);
    function GetCopy(): PDataCaliper;
    procedure SetNumberFingers(An : Integer);
  end;

Function ArrayStrsToDataCaliper(
    ASplitStrs : TArray<string>;
    var ADataCaliper : TDataCaliper) : Boolean;

function FindNoSpaceStr(phrase : string; var iBeg : Integer) : string;
function CalSectionCaliper (n : Integer; Finger : t_vdd) : Double;
function ReadBrutDataCaliper (FilePathRead,
                              FilePathWrite1,
                              FilePathWrite2,
                              FilePathWrite3,
                              FilePathWrite4 : string) : Boolean;
function ReadDataCaliperTEPItalia (FilePathRead,
                                   FilePathWrite1,
                                   FilePathWrite2,
                                   FilePathWrite3,
                                   FilePathWrite4 : string) : Boolean;
function ReadDataCaliperTEPItaliaBeforeTreat (FilePathRead,
                                   FilePathWrite1 : string) : Boolean;

function ReadDataCaliperTEPItaliaFittingEllipse (FilePathRead,
                                   FilePathWrite1,
                                   FilePathWrite2,
                                   FilePathWrite3,
                                   FilePathWrite4,
                                   FilePathWrite5 : string) : Boolean;
function ReadDataCaliperFittingEllipse (FilePathRead,
                                   FilePathWrite1,
                                   FilePathWrite2,
                                   FilePathWrite3,
                                   FilePathWrite4,
                                   FilePathWrite5 : string) : Boolean;
procedure ExtraitFingerToTreat(n : Integer; X, Y : t_vdd;
                               DelFingerNumber : t_vdi;
                               var nNew : Integer;
                               var Xused, Yused : t_vdd);
function ReadDataCaliperFittingEllipseDelSomeFingers (
    FolderPathData, FolderPathRes : String) : Boolean;

function ReadDataCaliperFittingEllipseDelSomeFingers2 (
    FolderPathData, FolderPathRes : String) : Boolean;

function ReadDataCaliperFittingEllipseDelSomeFingers3 (
    const FilePathRead, FilePathDelFingers: String;
    const FolderPathRes : String;
    const ColMd, ColIniFinger, ColEndFinger : Integer;
    const WearGrooveBegin, WearGrooveEnd : Integer;
    var FingerMinAll, FingerMaxAll : Double;
    var jMinAll, jMaxAll : Integer;
    const pCancel: pBoolean = nil;
    const pLineProgress: pInteger = nil) : Boolean;

Procedure TestReadDeletedFinger();

function TreatLineData(
    const LineDataRead: String;
    const ColMd, ColIniFinger, ColEndFinger: Integer;
    Var AMd: Double;
    const Finger: t_vdd): Boolean;

Procedure TreatLineDeletedFinger(
    ALineStr : String;
    nFingers : Integer;
    var AMD : Double;
    var ADelFingerNumber : t_vdi);

function CalWearGrooveMesh(n : Integer; NoFinger : Integer;
    Finger, NewFingerEllipse : t_vdd;
    X, Y, Xelp, Yelp : t_vdd) : Double;

    procedure test();

function RecalWearHight (FilePathRead,
                         FilePathWrite1: string) : Boolean;

function ReadLasFileCaliperBaker (FilePathRead,
    FilePathWrite1 : string) : Boolean;

function TreatRotatedCaliper (FilePathRead,
    FilePathWrite1 : string) : Boolean;

function TreatRotatedCaliper2 (FilePathRead,
    FilePathWrite1 : string) : Boolean;

function TreatRotatedCaliper3 (FilePathRaw,
    FilePathRot,
    FilePathRes : string) : Boolean;

function TreatExtraitColumesCaliper (
    nCols : Integer;
    Cols : t_vdi;
    FilePathRaw,
    FilePathRes : string) : Boolean;

function FindShiftedRotationNumber(
    const ARaw, ARot : TDataCaliper) : Integer;

Procedure WriteTestTreatCaliper(
    Nom : String;
    n : Integer;
    A, B, Theta, Xc, Yc : Double;
    Finger, NewFinger, NewFingerEllipse : t_vdd;
    X, Y : t_vdd;
    Xelp, Yelp : t_vdd);

function ReadDataGenerateInitialWear (FilePathData,
    FilePathRes : string) : Boolean;

implementation

uses
  UnitDebugCommon,
  UnitTreatCaliperData;


procedure TDataCaliperHelper.FreeListsAndPointers(AKillGraph: Boolean=True);
begin
end;

procedure TDataCaliperHelper.Init();
begin
  Self.Md := -999;
  Self.n := 0;
  SetLength(Self.Fingers, 0);
  Self.Max := -999;
  Self.Min := -999;
  Self.Mean := -999;
  Self.Medi := -999;
  Self.LSPD := -999;
  Self.MITDEV := -999;
  Self.MITROT := -999;
  Self.MITTEMP := -999;
  Self.GR := -999;
  Self.ShiftedNumber := -999;
end;

procedure TDataCaliperHelper.CopyTo(const p_Out: PDataCaliper);
begin
  p_Out.Md := Self.Md;
  p_Out.n := Self.n;
  p_Out.Fingers := Self.Fingers;
  p_Out.Max := Self.Max;
  p_Out.Min := Self.Min;
  p_Out.Mean := Self.Mean;
  p_Out.Medi := Self.Medi;
  p_Out.LSPD := Self.LSPD;
  p_Out.MITDEV := Self.MITDEV;
  p_Out.MITROT := Self.MITROT;
  p_Out.MITTEMP := Self.MITTEMP;
  p_Out.GR := Self.GR;
  p_Out.ShiftedNumber := Self.ShiftedNumber;
end;

function TDataCaliperHelper.GetCopy(): PDataCaliper;
begin
  New(Result);
  CopyTo(Result);
end;

procedure TDataCaliperHelper.SetNumberFingers(An : Integer);
begin
  if An < 0 then
    An := 0;
  Self.n := An;
  SetLength(Self.Fingers, An);
end;

Function ArrayStrsToDataCaliper(
    ASplitStrs : TArray<string>;
    var ADataCaliper : TDataCaliper) : Boolean;
var
  i, No : Integer;
begin
   if Length(ASplitStrs) < ADataCaliper.n+1+9  then
   begin
     Result := False;
     Exit;
   end;
   ADataCaliper.Md := StrToFloat(ASplitStrs[0]);
   No := 1;
   for i := 0 to ADataCaliper.n-1 do
   begin
     ADataCaliper.Fingers[i] := StrToFloat(ASplitStrs[No+i]);
   end;
   No := ADataCaliper.n+1;
   ADataCaliper.Max  := StrToFloat(ASplitStrs[No+0]);
   ADataCaliper.Min  := StrToFloat(ASplitStrs[No+1]);
   ADataCaliper.Mean := StrToFloat(ASplitStrs[No+2]);
   ADataCaliper.Medi := StrToFloat(ASplitStrs[No+3]);
   ADataCaliper.LSPD := StrToFloat(ASplitStrs[No+4]);
   ADataCaliper.MITDEV := StrToFloat(ASplitStrs[No+5]);
   ADataCaliper.MITROT := StrToFloat(ASplitStrs[No+6]);
   ADataCaliper.MITTEMP := StrToFloat(ASplitStrs[No+7]);
   ADataCaliper.GR := StrToFloat(ASplitStrs[No+8]);
   Result := True;
end;

{-------------------------------------------------------------------------------
  Calculate the section from caliper data
  Parameters :
  + n : number of finger,
  + Finger (array) measured data at each finger.
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
function CalSectionCaliper (n : Integer; Finger : t_vdd) : Double;
var
  i : Integer;
  Sec : Double;
  Ang : Double;
  //
  function SectionTriangle (A, B, Angle : Double) : Double;
  begin
    result := A*B*sin(Angle)/2.0;
  end;
Begin
  Ang := 2*Pi/n;
  Sec := 0.0;
  for i := 0 to n-2 do Sec := Sec + SectionTriangle(Finger[i], Finger[i+1], Ang);
  Sec := Sec + SectionTriangle(Finger[n-1], Finger[0], Ang);
  Result := Sec;
End;

Procedure FindMaxMinFingers (n : Integer; Finger : t_vdd;
                             var iMax : Integer; var FingerMax : Double;
                             var iMin : Integer; var FingerMin : Double);
var
  i : Integer;
Begin
  // Initialisation
  iMax := 1;
  iMin := 1;
  FingerMax := Finger[0];
  FingerMin := Finger[0];
  for i := 2 to n do
  begin
    if (Finger[i-1]>FingerMax) then
    begin
      iMax :=i;
      FingerMax:= Finger[i-1];
    end;
    if (Finger[i-1]<FingerMin) then
    begin
      iMin :=i;
      FingerMin:= Finger[i-1];
    end;
  end;
End;

{-------------------------------------------------------------------------------
  Lire le block des caract�res sans espace du string "phrase" � partir du
  caract�re No "iBeg"
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
function FindNoSpaceStr(phrase : string; var iBeg : Integer) : string;
var
  strRes : string[50];
begin
  if iBeg > length(phrase) then FindNoSpaceStr := ''
  else begin
    strRes := '';
    while phrase[iBeg]=' ' do iBeg:=iBeg+1;
    while phrase[iBeg]<>' ' do
    begin
      strRes := strRes + COPY(phrase, iBeg, 1);
      iBeg:=iBeg+1;
    end;
  end;
  result := strRes;
end;
//----------------------------------------------------------------------------//


{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}

function ReadBrutDataCaliper (FilePathRead,
                              FilePathWrite1,
                              FilePathWrite2,
                              FilePathWrite3,
                              FilePathWrite4 : string) : Boolean;
var
  ok, OKCentralization : boolean;
  i, j, k : Integer;
  strRead : string[2];
  n : Integer;
  MD : Double;
  jMinAll, jMaxAll : Integer;
  FingerMaxAll, FingerMinAll : Double;
  iMax, iMin : Integer;
  FingerMax, FingerMin : Double;
  Finger : t_vdd;
  TabRead : t_vdd;
  NoCol, NoLigne : Integer;
  IsGoodData : Boolean;
  X, Y : t_vdd;
  Xci, Yci, Ri : Double;
  Xc, Yc, R, SumSquaRes : Double;
  NewFinger : t_vdd;
  FicRead, FicWrite1, FicWrite2, FicWrite3, FicWrite4 : textfile;
begin
  n := 60;
  setlength(Finger,n);
  NoCol := 112;
  setlength(TabRead,NoCol);
  ok := False;
  if FileExists(FilePathRead) then
  begin
    ok := True;
    AssignFile(FicRead, FilePathRead);
    Reset(FicRead);
    repeat
      readln(FicRead, strRead);
    until (strRead='~A');
    //
    AssignFile(FicWrite1, FilePathWrite1);
    Rewrite(FicWrite1);
    write(FicWrite1, '   No  MD      iMax     FingerMax    iMin  FingerMin ');
    for i := 1 to n do write(FicWrite1, 'Finger[',i,'] ');
    writeln(FicWrite1);
    //
    AssignFile(FicWrite2, FilePathWrite2);
    Rewrite(FicWrite2);
    writeln(FicWrite2, '   No      MD  WearSection ');
    //
    AssignFile(FicWrite3, FilePathWrite3);
    Rewrite(FicWrite3);
    write(FicWrite3, '   No  MD  iMax     FingerMax    iMin  FingerMin XcCentralisation YcCentralisation ');
    for i := 1 to n do write(FicWrite3, 'NewFinger[',i,'] ');
    writeln(FicWrite3);
    //
    AssignFile(FicWrite4, FilePathWrite4);
    Rewrite(FicWrite4);
    write(FicWrite4, '   No      MD  ');
    for i := 1 to n do write(FicWrite4, 'WearHau[',i,'] ');
    writeln(FicWrite4);
    //
    FingerMaxAll := 0.0;
    FingerMinAll := 1E99;
    NoLigne := 0;
    j := 0;
    k := 0;
    repeat
      NoLigne := NoLigne+1;
      //
      for i := 1 to NoCol do Read(FicRead,TabRead[i-1]);
      Readln(FicRead);
      //
      MD := TabRead[0];
      for i:=25 to 64 do Finger[i-25] := TabRead[i];
      for i:=82 to 101 do Finger[i-42] := TabRead[i];
      // Traitement
      IsGoodData := True;
      for i := 1 to 60 do
        if (Finger[i-1]<0.0) then
        begin
          IsGoodData := False;
          Break;
        end;
      writeln(NoLigne:10, IsGoodData:10);

      if (IsGoodData) then
      begin
        j:=j+1;
        // Max, Min Finger
        FindMaxMinFingers (n, Finger, iMax, FingerMax, iMin, FingerMin);
        if FingerMax>FingerMaxAll then
        begin
          FingerMaxAll := FingerMax;
          jMaxAll := j;
        end;
        if FingerMin<FingerMinAll then
        begin
          FingerMinAll := FingerMin;
          jMinAll := j;
        end;
        //
        write(FicWrite1, j:10, MD:10:4, iMax:5, FingerMax:10:4, iMin:5, FingerMin:10:4);
        for i := 1 to n do write(FicWrite1, Finger[i-1]:10:4);
        writeln(FicWrite1);
        //
        writeln(FicWrite2, j:10, MD:10:4, CalSectionCaliper (60, Finger):35:30);

        // Traitement Traitement Traitement Traitement Traitement Traitement
//        CentralizationCaliperData (n, Finger, Xc, Yc, NewFinger);
//        FindMaxMinFingers (n, NewFinger, iMax, FingerMax, iMin, FingerMin);
//        //
//        write(FicWrite3, j:10, MD:10:4, iMax:5, FingerMax:15, iMin:5, FingerMin:15, Xc:15, Yc:15);
//        for i := 1 to n do write(FicWrite3, NewFinger[i-1]:15);
//        writeln(FicWrite3);

        OKCentralization := True;
        Try
          CalXY_XciYciRi_fromFingers (n, Finger, X, Y, Xci, Yci, Ri);
          FittingCircleLevenbergMarquardt (n, X, Y, Xci, Yci, Ri, Xc, Yc, R, SumSquaRes);
        Except
          writeln('Centralization process is not is not successful at ', j);
          OKCentralization := False;
        End;

        if OKCentralization then
        begin
          // Attention Units of parameters in SI
          k := k+1;
          CalNewFingerXcYc (n, X, Y, Xc, Yc, NewFinger);
          FindMaxMinFingers (n, NewFinger, iMax, FingerMax, iMin, FingerMin);
          //
          write(FicWrite3,j:10,' ', MD:10:4,' ', iMax:5,' ', FingerMax*0.0254:15,
                                            ' ', iMin:5,' ', FingerMin*0.0254:15,
                                            ' ', Xc*0.0254:15,' ', Yc*0.0254:15);
          for i := 1 to n do write(FicWrite3,' ', (NewFinger[i-1]*0.0254):15);
          writeln(FicWrite3);
          //
          write(FicWrite4, j:10,' ', MD:10:4);
          for i := 1 to n do write(FicWrite4,' ', (NewFinger[i-1]-FingerMin)*0.0254:15);
          writeln(FicWrite4);
        end;
      end;
    until (EOF(FicRead));
    //
    writeln('FingerMaxAll : ', FingerMaxAll:10:4, '  at result number  : ', jMaxAll);
    writeln('FingerMinAll : ', FingerMinAll:10:4, '  at result number  : ', jMinAll);
    //
    CloseFile(FicWrite1);
    CloseFile(FicWrite2);
    CloseFile(FicWrite3);
    CloseFile(FicWrite4);
    CloseFile(FicRead);
  end;
  // Free Array
  setlength(Finger,0);
  setlength(TabRead,0);
  Result := ok;
end;
// -----------------------------------------------------------------------------


{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}

function ReadDataCaliperTEPItalia (FilePathRead,
                                   FilePathWrite1,
                                   FilePathWrite2,
                                   FilePathWrite3,
                                   FilePathWrite4 : string) : Boolean;
var
  ok, OKCentralization : boolean;
  i, j, k : Integer;
  strRead : string[2];
  n : Integer;
  MD : Double;
  jMinAll, jMaxAll : Integer;
  FingerMaxAll, FingerMinAll : Double;
  iMax, iMin : Integer;
  IsGoodData : Boolean;
  FingerMax, FingerMin : Double;
  Finger : t_vdd;
  TabRead : t_vdd;
  NewFinger : t_vdd;
  NoCol, NoLigne : Integer;
  X, Y : t_vdd;
  Xci, Yci, Ri : Double;
  Xc, Yc, R, SumSquaRes : Double;
  FicRead, FicWrite1, FicWrite2, FicWrite3, FicWrite4 : textfile;
begin
  n := 60;
  setlength(Finger,n);
  NoCol := 102;
  setlength(TabRead,NoCol);
  ok := False;
  if FileExists(FilePathRead) then
  begin
    ok := True;
    // Preparation for Output files
    AssignFile(FicWrite1, FilePathWrite1);
    Rewrite(FicWrite1);
    write(FicWrite1, '   No  MD      iMax     FingerMax    iMin  FingerMin ');
    for i := 1 to n do write(FicWrite1, 'Finger[',i,'] ');
    writeln(FicWrite1);
    //
    AssignFile(FicWrite2, FilePathWrite2);
    Rewrite(FicWrite2);
    writeln(FicWrite2, '   No      MD  WearSection ');
    //
    AssignFile(FicWrite3, FilePathWrite3);
    Rewrite(FicWrite3);
    write(FicWrite3, '   No  MD  iMax     FingerMax    iMin  FingerMin XcCentralisation YcCentralisation ');
    for i := 1 to n do write(FicWrite3, 'NewFinger[',i,'] ');
    writeln(FicWrite3);
    //
    AssignFile(FicWrite4, FilePathWrite4);
    Rewrite(FicWrite4);
    write(FicWrite4, '   No      MD  ');
    for i := 1 to n do write(FicWrite4, 'WearHau[',i,'] ');
    writeln(FicWrite4);
    //  AssignFile(FicRead, FilePathRead);
    //  Reset(FicRead);
    //  repeat
    //    readln(FicRead, strRead);
    //  until (strRead='~A');
    AssignFile(FicRead, FilePathRead);
    Reset(FicRead);
    readln(FicRead, strRead);
    // OK Let's GOOOOOO
    FingerMaxAll := 0.0;
    FingerMinAll := 1E99;
    NoLigne := 0;
    j := 0;
    k := 0;
    repeat
      NoLigne := NoLigne+1;
      //
      for i := 1 to NoCol do Read(FicRead,TabRead[i-1]);
      Readln(FicRead);
      //
      MD := TabRead[0];
      for i:=5 to 64 do Finger[i-5] := TabRead[i];
      // Traitement
      IsGoodData := True;
      for i := 1 to 60 do
        if (Finger[i-1]<0.0) then
        begin
          IsGoodData := False;
          Break;
        end;
      writeln(NoLigne:10, IsGoodData:10);

      if (IsGoodData) then
      begin
        j:=j+1;
        // Max, Min Finger
        FindMaxMinFingers (n, Finger, iMax, FingerMax, iMin, FingerMin);
        if FingerMax>FingerMaxAll then
        begin
          FingerMaxAll := FingerMax;
          jMaxAll := j;
        end;
        if FingerMin<FingerMinAll then
        begin
          FingerMinAll := FingerMin;
          jMinAll := j;
        end;
        //
        write(FicWrite1, j:10, MD:10:4, iMax:5, FingerMax:10:4, iMin:5, FingerMin:10:4);
        for i := 1 to n do write(FicWrite1, Finger[i-1]:10:4);
        writeln(FicWrite1);
        //
        writeln(FicWrite2, j:10, MD:10:4, CalSectionCaliper (60, Finger):35:30);

        // Traitement Traitement Traitement Traitement Traitement Traitement
        OKCentralization := True;
        Try
          CalXY_XciYciRi_fromFingers (n, Finger, X, Y, Xci, Yci, Ri);
          FittingCircleLevenbergMarquardt (n, X, Y, Xci, Yci, Ri, Xc, Yc, R, SumSquaRes);
        Except
          writeln('Centralization process is not is not successful at ', j);
          OKCentralization := False;
        End;

        if OKCentralization then
        begin
          // Attention Units of parameters in SI
          k := k+1;
          CalNewFingerXcYc (n, X, Y, Xc, Yc, NewFinger);
          FindMaxMinFingers (n, NewFinger, iMax, FingerMax, iMin, FingerMin);
          //
          write(FicWrite3,j:10,' ', MD:10:4,' ', iMax:5,' ', FingerMax*0.0254:15,
                                            ' ', iMin:5,' ', FingerMin*0.0254:15,
                                            ' ', Xc*0.0254:15,' ', Yc*0.0254:15);
          for i := 1 to n do write(FicWrite3,' ', (NewFinger[i-1]*0.0254):15);
          writeln(FicWrite3);
          //
          write(FicWrite4, j:10,' ', MD:10:4);
          for i := 1 to n do write(FicWrite4,' ', (NewFinger[i-1]-FingerMin)*0.0254:15);
          writeln(FicWrite4);
        end;
      end;
    until (EOF(FicRead));
    //
    writeln('FingerMaxAll : ', FingerMaxAll:10:4, '  at result number  : ', jMaxAll);
    writeln('FingerMinAll : ', FingerMinAll:10:4, '  at result number  : ', jMinAll);
    //
    CloseFile(FicWrite1);
    CloseFile(FicWrite2);
    CloseFile(FicWrite3);
    CloseFile(FicWrite4);
    CloseFile(FicRead);
  end;
  // Free Array
  setlength(NewFinger,0);
  setlength(X,0);
  setlength(Y,0);
  setlength(Finger,0);
  setlength(TabRead,0);
  Result := ok;
end;
// -----------------------------------------------------------------------------


{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
function ReadDataCaliperTEPItaliaBeforeTreat (FilePathRead,
                                   FilePathWrite1 : string) : Boolean;
var
  ok, OKCentralization : boolean;
  i, j, k : Integer;
  strRead : string[2];
  n : Integer;
  MD : Double;
  jMinAll, jMaxAll : Integer;
  FingerMaxAll, FingerMinAll : Double;
  iMax, iMin : Integer;
  FingerMax, FingerMin : Double;
  NoCol, NoLigne : Integer;
  IsGoodData : Boolean;
  Finger : t_vdd;
  TabRead : t_vdd;
  FicRead, FicWrite1 : textfile;
begin
  n := 60;
  setlength(Finger,n);
  NoCol := 102;
  setlength(TabRead,NoCol);
  ok := False;
  if FileExists(FilePathRead) then
  begin
    // OK Find FingerMaxAll and FingerMinAll
    ok := True;
    AssignFile(FicRead, FilePathRead);
    Reset(FicRead);
    readln(FicRead, strRead);
    FingerMaxAll := 0.0;
    FingerMinAll := 1E99;
    NoLigne := 0;
    j := 0;
    k := 0;
    repeat
      NoLigne := NoLigne+1;
      //
      for i := 1 to NoCol do Read(FicRead,TabRead[i-1]);
      Readln(FicRead);
      //
      MD := TabRead[0];
      for i:=5 to 64 do Finger[i-5] := TabRead[i];
      // Traitement
      IsGoodData := True;
      for i := 1 to 60 do
        if (Finger[i-1]<0.0) then
        begin
          IsGoodData := False;
          Break;
        end;
      writeln(NoLigne:10, IsGoodData:10);

      if (IsGoodData) then
      begin
        j:=j+1;
        // Max, Min Finger
        FindMaxMinFingers (n, Finger, iMax, FingerMax, iMin, FingerMin);
        if FingerMax>FingerMaxAll then
        begin
          FingerMaxAll := FingerMax;
          jMaxAll := j;
        end;
        if (FingerMin<FingerMinAll) and (FingerMin > 0.0) then
        begin
          FingerMinAll := FingerMin;
          jMinAll := j;
        end;
      end;
    until (EOF(FicRead));

    writeln('FingerMaxAll : ', FingerMaxAll:10:4, '  at result number  : ', jMaxAll);
    writeln('FingerMinAll : ', FingerMinAll:10:4, '  at result number  : ', jMinAll);
    Readln;
    CloseFile(FicRead);

    // Let's GO
    ok := True;
    AssignFile(FicRead, FilePathRead);
    Reset(FicRead);
    readln(FicRead, strRead);

    AssignFile(FicWrite1, FilePathWrite1);
    Rewrite(FicWrite1);
    write(FicWrite1, '   No  MD ');
    for i := 1 to n do write(FicWrite1, 'Hau[',i,'] ');
    writeln(FicWrite1);
    //
    NoLigne := 0;
    j := 0;
    k := 0;
    repeat
      NoLigne := NoLigne+1;
      //
      for i := 1 to NoCol do Read(FicRead,TabRead[i-1]);
      Readln(FicRead);
      //
      MD := TabRead[0];
      for i:=5 to 64 do Finger[i-5] := TabRead[i];
      // Traitement
      IsGoodData := True;
      for i := 1 to 60 do
        if (Finger[i-1]<0.0) then
        begin
          IsGoodData := False;
          Break;
        end;
      writeln(NoLigne:10, IsGoodData:10);

      if (IsGoodData) then
      begin
        j:=j+1;
        write(FicWrite1,' ', j:10,' ', MD:10:4);
        for i := 1 to n do write(FicWrite1,' ', (Finger[i-1]-FingerMinAll)*0.0254:15);
        writeln(FicWrite1);
      end;
    until (EOF(FicRead));

    writeln(' FINISH FINISH !! ');
    CloseFile(FicRead);
  end;
  // Free Array
  setlength(Finger,0);
  setlength(TabRead,0);
  Readln;
  Result := ok;
end;
// -----------------------------------------------------------------------------


{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
function ReadDataCaliperTEPItaliaFittingEllipse (FilePathRead,
                                   FilePathWrite1,
                                   FilePathWrite2,
                                   FilePathWrite3,
                                   FilePathWrite4,
                                   FilePathWrite5 : string) : Boolean;
var
  ok, OKCentralization : boolean;
  i, j, k : Integer;
  strRead : string[2];
  n : Integer;
  MD : Double;
  jMinAll, jMaxAll : Integer;
  FingerMaxAll, FingerMinAll : Double;
  iMax, iMin : Integer;
  FingerMax, FingerMin : Double;
  NoCol, NoLigne : Integer;
  IsGoodData : Boolean;
  Finger : t_vdd;
  TabRead : t_vdd;
  NewFinger : t_vdd;
  X, Y : t_vdd;
  Ai, Bi, Thetai, Xci, Yci, Ri : Double;
  A, B, Theta, Xc, Yc, SumSquaRes : Double;
  FicRead, FicWrite1, FicWrite2, FicWrite3, FicWrite4, FicWrite5 : textfile;
begin
  n := 60;
  setlength(Finger,n);
  NoCol := 102;
  setlength(TabRead,NoCol);
  ok := False;
  if FileExists(FilePathRead) then
  begin
    ok := True;
    // Preparation for OutPut files
    AssignFile(FicWrite1, FilePathWrite1);
    Rewrite(FicWrite1);
    write(FicWrite1, '   No  MD      iMax     FingerMax    iMin  FingerMin WearSection ');
    for i := 1 to n do write(FicWrite1, 'Finger[',i,'] ');
    writeln(FicWrite1);
    //
    AssignFile(FicWrite2, FilePathWrite2);
    Rewrite(FicWrite2);
    writeln(FicWrite2, '   No      MD  WearSection ');
    //
    AssignFile(FicWrite3, FilePathWrite3);
    Rewrite(FicWrite3);
    write(FicWrite3, '   No  MD  iMax     FingerMax    iMin  FingerMin    Xci    Yci    Ai    Bi    Thetai   OKCentralization  Xc    Yc    A    B    Theta ');
    // for i := 1 to n do write(FicWrite3, 'NewFinger[',i,'] ');
    writeln(FicWrite3);
    //
    AssignFile(FicWrite4, FilePathWrite4);
    Rewrite(FicWrite4);
    write(FicWrite4, '   No      MD  ');
    for i := 1 to n do write(FicWrite4, 'WearHau[',i,'] ');
    writeln(FicWrite4);
    //
    AssignFile(FicWrite5, FilePathWrite5);
    Rewrite(FicWrite5);
    write(FicWrite5, '   No  MD  ');
    for i := 1 to n do write(FicWrite5, 'NewFinger[',i,'] ');
    writeln(FicWrite5);
    // Read data file
    AssignFile(FicRead, FilePathRead);
    Reset(FicRead);
    readln(FicRead, strRead);
    // OK Let's GOOOOOO
    FingerMaxAll := 0.0;
    FingerMinAll := 1E99;
    NoLigne := 0;
    j := 0;
    k := 0;
    repeat
      NoLigne := NoLigne+1;
      //
      for i := 1 to NoCol do Read(FicRead,TabRead[i-1]);
      Readln(FicRead);
      //
      MD := TabRead[0];
      for i:=5 to 64 do Finger[i-5] := TabRead[i];
      // Traitement
      IsGoodData := True;
      for i := 1 to 60 do
        if (Finger[i-1]<0.0) then
        begin
          IsGoodData := False;
          Break;
        end;
      writeln(NoLigne:10, IsGoodData:10);

      if (IsGoodData) then
      begin
        j:=j+1;
        // Max, Min Finger
        FindMaxMinFingers (n, Finger, iMax, FingerMax, iMin, FingerMin);
        if FingerMax>FingerMaxAll then
        begin
          FingerMaxAll := FingerMax;
          jMaxAll := j;
        end;
        if FingerMin<FingerMinAll then
        begin
          FingerMinAll := FingerMin;
          jMinAll := j;
        end;
        //
        write(FicWrite1, j:10, MD:10:4, iMax:5, FingerMax:10:4, iMin:5, FingerMin:10:4);
        for i := 1 to n do write(FicWrite1, Finger[i-1]:10:4);
        writeln(FicWrite1);
        //
        writeln(FicWrite2, j:10, MD:10:4, CalSectionCaliper (60, Finger):35:30);

        // Traitement Traitement Traitement Traitement Traitement Traitement
        OKCentralization := True;
        Try
          CalXY_XciYciAiBiThetai_fromFingers (n, Finger, X, Y, Xci, Yci, Ai, Bi, Thetai);
          //  Xc := Xci;
          //  Yc := Yci;
          //  A := Ai;
          //  B := Bi;
          //  Theta := Thetai;
          FittingEllipseGeneralLevenbergMarquardt (n, X, Y, Ai, Bi, Thetai, Xci, Yci,
                                         A, B, Theta, Xc, Yc, SumSquaRes, OKCentralization);

        Except
          writeln('Centralization process is not is not successful at ', j);
          OKCentralization := False;
        End;
        //
        write(FicWrite3,' ', j:10,
                        ' ', MD:10:4,
                        ' ', iMax:5,
                        ' ', FingerMax:15,
                        ' ', iMin:5,
                        ' ', FingerMin:15,
                        ' ', Xci:15,
                        ' ', Yci:15,
                        ' ', Ai:15,
                        ' ', Bi:15,
                        ' ', Thetai*180/Pi:15,
                        ' ', OKCentralization:15,
                        ' ', Xc:15,
                        ' ', Yc:15,
                        ' ', A:15,
                        ' ', B:15,
                        ' ', Theta*180/Pi:15,
                        ' ', A/B:15,
                        ' ', Sqrt(Sqr(Xc)+sqr(Yc))/min(A, B):15);
        writeln(FicWrite3);
        //
        if OKCentralization then
        begin
          // Attention Units of parameters in SI
          k := k+1;
          CalNewFingerXcYc (n, X, Y, Xc, Yc, NewFinger);
          FindMaxMinFingers (n, NewFinger, iMax, FingerMax, iMin, FingerMin);
          //
          write(FicWrite5,' ', j:10, ' ', MD:10:4);
          for i := 1 to n do write(FicWrite5,' ', (NewFinger[i-1]*0.0254):15);
          writeln(FicWrite5);
          //
          write(FicWrite4, j:10,' ', MD:10:4);
          for i := 1 to n do write(FicWrite4,' ', (NewFinger[i-1]-FingerMin)*0.0254:15);
          writeln(FicWrite4);
        end;
      end;
    until (EOF(FicRead));
    //
    writeln('FingerMaxAll : ', FingerMaxAll:10:4, '  at result number  : ', jMaxAll);
    writeln('FingerMinAll : ', FingerMinAll:10:4, '  at result number  : ', jMinAll);
    //
    CloseFile(FicWrite1);
    CloseFile(FicWrite2);
    CloseFile(FicWrite3);
    CloseFile(FicWrite4);
    CloseFile(FicWrite5);
    CloseFile(FicRead);
  end;
  // Free Array
  setlength(NewFinger,0);
  setlength(X,0);
  setlength(Y,0);
  setlength(Finger,0);
  setlength(TabRead,0);
  Result := ok;
end;

{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
function ReadDataCaliperFittingEllipse (FilePathRead,
                                        FilePathWrite1,
                                        FilePathWrite2,
                                        FilePathWrite3,
                                        FilePathWrite4,
                                        FilePathWrite5 : string) : Boolean;
var
  ok, OKCentralization : boolean;
  i, j, k : Integer;
  strRead : string[2];
  n : Integer;
  MD : Double;
  jMinAll, jMaxAll : Integer;
  FingerMaxAll, FingerMinAll : Double;
  iMax, iMin : Integer;
  FingerMax, FingerMin : Double;
  NoCol, NoLigne : Integer;
  IsGoodData : Boolean;
  Finger : t_vdd;
  TabRead : t_vdd;
  NewFinger : t_vdd;
  X, Y : t_vdd;
  Ai, Bi, Thetai, Xci, Yci, Ri : Double;
  A, B, Theta, Xc, Yc, SumSquaRes : Double;
  FicRead, FicWrite1, FicWrite2, FicWrite3, FicWrite4, FicWrite5 : textfile;
begin
  n := 60;
  setlength(Finger,n);
  NoCol := 61;
  setlength(TabRead,NoCol);
  ok := False;
  if FileExists(FilePathRead) then
  begin
    ok := True;
    // Preparation for Output files
    AssignFile(FicWrite1, FilePathWrite1);
    Rewrite(FicWrite1);
    write(FicWrite1, '   No  MD      iMax     FingerMax    iMin  FingerMin WearSection ');
    for i := 1 to n do write(FicWrite1, 'Finger[',i,'] ');
    writeln(FicWrite1);
    //
    AssignFile(FicWrite2, FilePathWrite2);
    Rewrite(FicWrite2);
    writeln(FicWrite2, '   No      MD  WearSection ');
    //
    AssignFile(FicWrite3, FilePathWrite3);
    Rewrite(FicWrite3);
    write(FicWrite3, '   No  MD  iMax     FingerMax    iMin  FingerMin    Xci    Yci    Ai    Bi    Thetai   OKCentralization  Xc    Yc    A    B    Theta ');
    // for i := 1 to n do write(FicWrite3, 'NewFinger[',i,'] ');
    writeln(FicWrite3);
    //
    AssignFile(FicWrite4, FilePathWrite4);
    Rewrite(FicWrite4);
    write(FicWrite4, '   No      MD  ');
    for i := 1 to n do write(FicWrite4, 'WearHau[',i,'] ');
    writeln(FicWrite4);
    //
    AssignFile(FicWrite5, FilePathWrite5);
    Rewrite(FicWrite5);
    write(FicWrite5, '   No  MD  ');
    for i := 1 to n do write(FicWrite5, 'NewFinger[',i,'] ');
    writeln(FicWrite5);
    // Read data file
    AssignFile(FicRead, FilePathRead);
    Reset(FicRead);
    readln(FicRead, strRead);
    // OK Let's GOOOOOO
    FingerMaxAll := 0.0;
    FingerMinAll := 1E99;
    NoLigne := 0;
    j := 0;
    k := 0;
    repeat
      NoLigne := NoLigne+1;
      //
      for i := 1 to NoCol do Read(FicRead,TabRead[i-1]);
      Readln(FicRead);
      //
      MD := TabRead[0];
      for i := 1 to 60 do Finger[i-1] := TabRead[i];
      // Hack Hack
      // Finger[35-1] := (Finger[34-1]+Finger[36-1])/2.0;
      // Traitement
      IsGoodData := True;
      for i := 1 to 60 do
        if (Finger[i-1]<0.0) then
        begin
          IsGoodData := False;
          Break;
        end;
      writeln(NoLigne:10, IsGoodData:10);

      if (IsGoodData) then
      begin
        j:=j+1;
        // Max, Min Finger
        FindMaxMinFingers (n, Finger, iMax, FingerMax, iMin, FingerMin);
        if FingerMax>FingerMaxAll then
        begin
          FingerMaxAll := FingerMax;
          jMaxAll := j;
        end;
        if FingerMin<FingerMinAll then
        begin
          FingerMinAll := FingerMin;
          jMinAll := j;
        end;
        //
        write(FicWrite1, j:10, MD:10:4, iMax:5, FingerMax:10:4, iMin:5, FingerMin:10:4);
        for i := 1 to n do write(FicWrite1, Finger[i-1]:10:4);
        writeln(FicWrite1);
        //
        writeln(FicWrite2, j:10, MD:10:4, CalSectionCaliper (60, Finger):35:30);

        // Traitement Traitement Traitement Traitement Traitement Traitement
        OKCentralization := True;
        Try
          CalXY_XciYciAiBiThetai_fromFingers (n, Finger, X, Y, Xci, Yci, Ai, Bi, Thetai);
          //  Xc := Xci;
          //  Yc := Yci;
          //  A := Ai;
          //  B := Bi;
          //  Theta := Thetai;
          FittingEllipseGeneralLevenbergMarquardt (n, X, Y, Ai, Bi, Thetai, Xci, Yci,
                                         A, B, Theta, Xc, Yc, SumSquaRes, OKCentralization);

        Except
          writeln('Centralization process is not is not successful at ', j);
          OKCentralization := False;
        End;
        //
        write(FicWrite3,' ', j:10,
                        ' ', MD:10:4,
                        ' ', iMax:5,
                        ' ', FingerMax:15,
                        ' ', iMin:5,
                        ' ', FingerMin:15,
                        ' ', Xci:15,
                        ' ', Yci:15,
                        ' ', Ai:15,
                        ' ', Bi:15,
                        ' ', Thetai*180/Pi:15,
                        ' ', OKCentralization:15,
                        ' ', Xc:15,
                        ' ', Yc:15,
                        ' ', A:15,
                        ' ', B:15,
                        ' ', Theta*180/Pi:15,
                        ' ', A/B:15,
                        ' ', Sqrt(Sqr(Xc)+sqr(Yc))/min(A, B):15);
        writeln(FicWrite3);
        //
        if OKCentralization then
        begin
          // Attention Units of parameters in SI
          k := k+1;
          CalNewFingerXcYc (n, X, Y, Xc, Yc, NewFinger);
          FindMaxMinFingers (n, NewFinger, iMax, FingerMax, iMin, FingerMin);
          //
          write(FicWrite5,' ', j:10, ' ', MD:10:4);
          for i := 1 to n do write(FicWrite5,' ', (NewFinger[i-1]*0.0254):15);
          writeln(FicWrite5);
          //
          write(FicWrite4, j:10,' ', MD:10:4);
          for i := 1 to n do write(FicWrite4,' ', (NewFinger[i-1]-FingerMin)*0.0254:15);
          writeln(FicWrite4);
        end;
      end;
    until (EOF(FicRead));
    //
    writeln('FingerMaxAll : ', FingerMaxAll:10:4, '  at result number  : ', jMaxAll);
    writeln('FingerMinAll : ', FingerMinAll:10:4, '  at result number  : ', jMinAll);
    //
    CloseFile(FicWrite1);
    CloseFile(FicWrite2);
    CloseFile(FicWrite3);
    CloseFile(FicWrite4);
    CloseFile(FicWrite5);
    CloseFile(FicRead);
  end;
  // Free Array
  setlength(NewFinger,0);
  setlength(X,0);
  setlength(Y,0);
  setlength(Finger,0);
  setlength(TabRead,0);
  Result := ok;
end;
// -----------------------------------------------------------------------------

procedure ExtraitFingerToTreat(n : Integer; X, Y : t_vdd;
                               DelFingerNumber : t_vdi;
                               var nNew : Integer;
                               var Xused, Yused : t_vdd);
var
  i, j : Integer;
  Flag : Boolean;
  nbDelFin : Integer;
begin
  nbDelFin := length(DelFingerNumber);
  setlength(Xused, n-nbDelFin);
  setlength(Yused, n-nbDelFin);
  nNew := 0;
  for i := 1 to n do
  begin
    Flag := True;
    for j := 1 to nbDelFin do
    begin
      if (DelFingerNumber[j-1] = i) then
      begin
        Flag := False;
        break;
      end;
    end;
    if (Flag) then
    begin
      nNew := nNew+1;
      Xused[nNew-1] := X[i-1];
      Yused[nNew-1] := Y[i-1];
    end;
  end;
end;

procedure CaliperFittingEllipseSection(
    n : Integer;
    Finger : t_vdd;
    DelFingerNumber : t_vdi;
    WearGrooveBegin, WearGrooveEnd : Integer;
    var Ai, Bi, Thetai, Xci, Yci : Double;
    var A, B, Theta, Xc, Yc, SumSquaRes : Double;
    var OKCentralization : Boolean;
    var NewFinger, NewFingerEllipse : t_vdd;
    var WearGroove : Double);
var
  X, Y : t_vdd;
  Xelp, Yelp : t_vdd;   // Intersection points (Old center - X, Y) with ellipse
  XelpB, YelpB : t_vdd;
  nNew : Integer;
  Xused, Yused : t_vdd;
  i : Integer;
begin
  // Traitement Traitement Traitement Traitement Traitement Traitement
  OKCentralization := True;
  setlength(X, n);
  setlength(Y, n);

  setlength(Xelp, n);
  setlength(Yelp, n);

  setlength(XelpB, n);
  setlength(YelpB, n);
  Try
    CalXY_XciYciAiBiThetai_fromFingers (n, Finger, X, Y, Xci, Yci, Ai, Bi, Thetai);
    //  Xc := Xci;
    //  Yc := Yci;
    //  A := Ai;
    //  B := Bi;
    //  Theta := Thetai;
    ExtraitFingerToTreat(n, X, Y, DelFingerNumber, nNew, Xused, Yused);
    FittingEllipseGeneralLevenbergMarquardt (nNew, Xused, Yused, Ai, Bi, Thetai, Xci, Yci,
                                   A, B, Theta, Xc, Yc, SumSquaRes, OKCentralization);

  Except
    OKCentralization := False;
  End;
  if OKCentralization then
  begin
    // Attention Units of parameters in SI
    CalNewFingerXcYc (n, X, Y, Xc, Yc, NewFinger);

    CalNewFingerOfEllipse (n, X, Y, A, B, Theta, Xc, Yc, NewFingerEllipse, Xelp, Yelp);

    if (WearGrooveBegin >=0) and (WearGrooveEnd >= 0) then
    begin
      WearGroove := 0.0;
      i := WearGrooveBegin;
      while i <> WearGrooveEnd do
      begin
        WearGroove := WearGroove +
            CalWearGrooveMesh(n, i, Finger, NewFingerEllipse, X, Y, Xelp, Yelp);
        i := i+1;
        if i > n then
          i := 1;
      end;
    end
    else
      WearGroove := -999;
  end;


  // Free
  setlength(X, 0);
  setlength(Y, 0);

  setlength(Xelp, 0);
  setlength(Yelp, 0);

  setlength(XelpB, 0);
  setlength(YelpB, 0);
end;

{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
function ReadDataCaliperFittingEllipseDelSomeFingers (
    FolderPathData, FolderPathRes : String) : Boolean;
var
  ok, OKCentralization : boolean;
  i, j, k : Integer;
  strRead : string[2];
  n : Integer;
  MD : Double;
  jMinAll, jMaxAll : Integer;
  FingerMaxAll, FingerMinAll : Double;
  iMax, iMin : Integer;
  FingerMax, FingerMin : Double;
  NoCol, NoLigne : Integer;
  IsGoodData : Boolean;
  Finger, NewFinger, NewFingerEllipse : t_vdd;
  TabRead : t_vdd;
  X, Y : t_vdd;
  Xelp, Yelp : t_vdd;   // Intersection points (Old center - X, Y) with ellipse
  XelpB, YelpB : t_vdd;
  nNew : Integer;
  Xused, Yused : t_vdd;
  Ai, Bi, Thetai, Xci, Yci, Ri : Double;
  A, B, Theta, Xc, Yc, SumSquaRes : Double;
  FicRead, FicWrite1, FicWrite2, FicWrite3, FicWrite4, FicWrite5, FicWrite6 : textfile;
  DelFingerNumber : t_vdi;
  Dis, Xnew, Ynew : Double;

  WearGroove : Double;
  WearGrooveBegin, WearGrooveEnd : Integer;
  FilePathDelFingers, FilePathWearGroove, FilePathRead : String;
begin
  FilePathDelFingers := FolderPathData+'\DeletedFingers.txt';
  FilePathWearGroove := FolderPathData+'\WearGrooveFingers.txt';
  FilePathRead       := FolderPathData+'\CaliperData.las';

  writeln(' ***   If you do not want to take some fingers in the treatment ***');
  writeln(' ***      Prepare the DeletedFingers.txt in the .exe folder     ***');
  writeln(' ***  This file has the finger numbers which will be not taken  ***');
  writeln;
  writeln(' OK, Enter to continue !! ');
  writeln;
  readln;
  if FileExists(FilePathDelFingers) then
  begin
    AssignFile(FicRead, FilePathDelFingers);
    Reset(FicRead);
    i := 0;
    while not Eof(FicRead) do
    begin
      i := i+1;
      SetLength(DelFingerNumber, i);
      Read(FicRead, DelFingerNumber[i-1]);
      if Eoln(FicRead) then Readln(FicRead)
    end;
    CloseFile(FicRead);
    writeln(' You want to delete ', i, ' fingers in the treatment');
    write(' Finger numbers : ');
    for i := 1 to Length(DelFingerNumber) do
    begin
      write('  ', DelFingerNumber[i-1]);
    end;
  end
  else
  begin
    SetLength(DelFingerNumber, 0);
  end;

  writeln;
  writeln;
  writeln(' ***           If you want calcculate the wear groove           ***');
  writeln(' ***    Prepare the WearGrooveFingers.txt in the .exe folder    ***');
  writeln(' ***     This file has ONLY the begin and end finger numbers    ***');
  writeln(' ***         of zone where you calculate the wear groove        ***');
  writeln;
  writeln(' OK, Enter to continue !! ');
  writeln;
  readln;
  if FileExists(FilePathWearGroove) then
  begin
    AssignFile(FicRead, FilePathWearGroove);
    Reset(FicRead);
    Read(FicRead, WearGrooveBegin, WearGrooveEnd);

    CloseFile(FicRead);
    writeln(' You want calcculate the wear groove from ', WearGrooveBegin,
        ' to ', WearGrooveEnd, ' fingers in clockwise direction');
  end
  else
  begin
    WearGrooveBegin := -999;
    WearGrooveEnd := -999;
  end;

  writeln;
  writeln;
  writeln('Did you prepare the file CaliperData.las in the .exe folder to treat?');
  writeln('If yes, Enter to start the treatement !!');
  readln;

  n := 60;
  // Memory for arrays
  setlength(Finger, n);
  SetLength(NewFinger, n);
  SetLength(NewFingerEllipse, n);
  setlength(X, n);
  setlength(Y, n);

  setlength(Xelp, n);
  setlength(Yelp, n);

  setlength(XelpB, n);
  setlength(YelpB, n);
  //
  NoCol := 61;
  setlength(TabRead,NoCol);
  ok := False;
  if FileExists(FilePathRead) then
  begin
    ok := True;
    // Preparation Output files
    AssignFile(FicWrite1, FolderPathRes+'\01FingerMaxMin.txt');
    Rewrite(FicWrite1);
    write(FicWrite1, '   No  MD      iMax     FingerMax    iMin  FingerMin WearSection ');
    for i := 1 to n do write(FicWrite1, 'Finger[',i,'] ');
    writeln(FicWrite1);
    //
    AssignFile(FicWrite2, FolderPathRes+'\02WearSection.txt');
    Rewrite(FicWrite2);
    writeln(FicWrite2, '        No    MD        WearSection        WearGroove(LimitedByBeginAndEndFingers)');
    //
    AssignFile(FicWrite3, FolderPathRes+'\03Centralisation.txt');
    Rewrite(FicWrite3);
    write(FicWrite3, '   No  MD  iMax     FingerMax    iMin  FingerMin    Xci    Yci    Ai    Bi    Thetai   OKCentralization  Xc    Yc    A    B    Theta ');
    // for i := 1 to n do write(FicWrite3, 'NewFinger[',i,'] ');
    writeln(FicWrite3);
    //
    AssignFile(FicWrite4, FolderPathRes+'\04WearHau.txt');
    Rewrite(FicWrite4);
    write(FicWrite4, '   No      MD  ');
    for i := 1 to n do write(FicWrite4, 'WearHau[',i,'] ');
    writeln(FicWrite4);
    //
    AssignFile(FicWrite5, FolderPathRes+'\05NewFinger.txt');
    Rewrite(FicWrite5);
    write(FicWrite5, '   No  MD  ');
    for i := 1 to n do write(FicWrite5, 'NewFinger[',i,'] ');
    writeln(FicWrite5);
    //
    AssignFile(FicWrite6, FolderPathRes+'\06NewFingerEllipse.txt');
    Rewrite(FicWrite6);
    write(FicWrite6, '   No  MD  ');
    for i := 1 to n do write(FicWrite6, 'NewFingerEllipse[',i,'] ');
    writeln(FicWrite6);
    // Read Data File
    AssignFile(FicRead, FilePathRead);
    Reset(FicRead);
    readln(FicRead, strRead);

    // OK Let's GOOOOOO
    FingerMaxAll := 0.0;
    FingerMinAll := 1E99;
    NoLigne := 0;
    j := 0;
    k := 0;
    repeat
      NoLigne := NoLigne+1;
      //
      for i := 1 to NoCol do Read(FicRead, TabRead[i-1]);
      Readln(FicRead);
      //
      MD := TabRead[0];
      for i := 1 to 60 do Finger[i-1] := TabRead[i];
      // Hack Hack
      // Finger[35-1] := (Finger[34-1]+Finger[36-1])/2.0;
      // Traitement
      IsGoodData := True;
      for i := 1 to 60 do
        if (Finger[i-1]<0.0) then
        begin
          IsGoodData := False;
          Break;
        end;
      writeln(NoLigne:10, IsGoodData:10);

      if (IsGoodData) then
      begin
        j := j+1;
        // Max, Min Finger
        FindMaxMinFingers (n, Finger, iMax, FingerMax, iMin, FingerMin);
        if FingerMax>FingerMaxAll then
        begin
          FingerMaxAll := FingerMax;
          jMaxAll := j;
        end;
        if FingerMin<FingerMinAll then
        begin
          FingerMinAll := FingerMin;
          jMinAll := j;
        end;
        //
        write(FicWrite1, j:10, MD:10:4, iMax:5, FingerMax:10:4, iMin:5, FingerMin:10:4);
        for i := 1 to n do write(FicWrite1, Finger[i-1]:10:4);
        writeln(FicWrite1);

        //
        write(FicWrite2, j:10, MD:10:4, CalSectionCaliper (60, Finger):20);

        // Traitement Traitement Traitement Traitement Traitement Traitement
        OKCentralization := True;
        Try
          CalXY_XciYciAiBiThetai_fromFingers (n, Finger, X, Y, Xci, Yci, Ai, Bi, Thetai);
          //  Xc := Xci;
          //  Yc := Yci;
          //  A := Ai;
          //  B := Bi;
          //  Theta := Thetai;
          ExtraitFingerToTreat(n, X, Y, DelFingerNumber, nNew, Xused, Yused);
          FittingEllipseGeneralLevenbergMarquardt (nNew, Xused, Yused, Ai, Bi, Thetai, Xci, Yci,
                                         A, B, Theta, Xc, Yc, SumSquaRes, OKCentralization);

        Except
          writeln('Centralization process is not is not successful at ', j);
          OKCentralization := False;
        End;
        //
        write(FicWrite3,' ', j:10,
                        ' ', MD:10:4,
                        ' ', iMax:5,
                        ' ', FingerMax:15,
                        ' ', iMin:5,
                        ' ', FingerMin:15,
                        ' ', Xci:15,
                        ' ', Yci:15,
                        ' ', Ai:15,
                        ' ', Bi:15,
                        ' ', Thetai*180/Pi:15,
                        ' ', OKCentralization:15,
                        ' ', Xc:15,
                        ' ', Yc:15,
                        ' ', A:15,
                        ' ', B:15,
                        ' ', Theta*180/Pi:15,
                        ' ', A/B:15,
                        ' ', Sqrt(Sqr(Xc)+sqr(Yc))/min(A, B):15);
        writeln(FicWrite3);
        //
        if OKCentralization then
        begin
          // Attention Units of parameters in SI
          k := k+1;
          CalNewFingerXcYc (n, X, Y, Xc, Yc, NewFinger);
          FindMaxMinFingers (n, NewFinger, iMax, FingerMax, iMin, FingerMin);
          //
          write(FicWrite5,' ', j:10, ' ', MD:10:4);
          for i := 1 to n do write(FicWrite5,' ', (NewFinger[i-1]):15);
          writeln(FicWrite5);
          //
          write(FicWrite4, j:10,' ', MD:10:4);
          for i := 1 to n do
          begin
            ChangeAxisToEllipseAxis (Xc, Yc, Theta, X[i-1], Y[i-1], Xnew, Ynew);
            Dis := DistancePointToIntersectionEllipse (Xnew, Ynew, A, B);
            write(FicWrite4,' ', Dis:15);
          end;
          writeln(FicWrite4);
          //
          CalNewFingerOfEllipse (n, X, Y, A, B, Theta, Xc, Yc, NewFingerEllipse, Xelp, Yelp);
          write(FicWrite6,' ', j:10, ' ', MD:10:4);
          for i := 1 to n do write(FicWrite6,' ', (NewFingerEllipse[i-1]):15);
          writeln(FicWrite6);

          // TEST TEST TEST
          //  WriteTestTreatCaliper(FolderPathRes+'\TreatCaliperTest.txt',
          //    n, A, B, Theta, Xc, Yc, Finger, NewFinger, NewFingerEllipse,
          //    X, Y, Xelp, Yelp);

          //  CalXY_XciYciAiBiThetai_fromFingers (n, NewFingerEllipse,
          //    XelpB, YelpB, Xci, Yci, Ai, Bi, Thetai);
          //  WriteTestTreatCaliper(FolderPathRes+'\TreatCaliperTestB.txt',
          //    n, A, B, Theta, Xc, Yc, Finger, NewFinger, NewFingerEllipse,
          //    X, Y, XelpB, YelpB);

          if (WearGrooveBegin >=0) and (WearGrooveEnd >= 0) then
          begin
            WearGroove := 0.0;
            i := WearGrooveBegin;
            while i <> WearGrooveEnd do
            begin
              WearGroove := WearGroove +
                  CalWearGrooveMesh(n, i, Finger, NewFingerEllipse, X, Y, Xelp, Yelp);
              i := i+1;
              if i > n then
                i := 1;
            end;
          end else
            WearGroove := -999;
          writeln(FicWrite2, ' ', WearGroove:20);
        end;
      end;
    until (EOF(FicRead));
    //
    writeln('FingerMaxAll : ', FingerMaxAll:10:4, '  at result number  : ', jMaxAll);
    writeln('FingerMinAll : ', FingerMinAll:10:4, '  at result number  : ', jMinAll);
    //
    CloseFile(FicRead);
    CloseFile(FicWrite1);
    CloseFile(FicWrite2);
    CloseFile(FicWrite3);
    CloseFile(FicWrite4);
    CloseFile(FicWrite5);
    CloseFile(FicWrite6);
  end;
  // Free Array
  setlength(Finger, 0);
  SetLength(NewFinger, 0);
  SetLength(NewFingerEllipse, 0);
  setlength(X, 0);
  setlength(Y, 0);
  setlength(Xelp, 0);
  setlength(Yelp, 0);
  setlength(XelpB, 0);
  setlength(YelpB, 0);
  setlength(TabRead,0);
  Result := ok;
end;

{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Deleted fingers for the caliper treatement are inputed line by line
  3000,50 5,5 5,15 17
  MD then Deleted fingers min and max
-------------------------------------------------------------------------------}
function ReadDataCaliperFittingEllipseDelSomeFingers2 (
    FolderPathData, FolderPathRes : String) : Boolean;
var
  ok, OKCentralization : boolean;
  i, j, k : Integer;
  strRead : string[2];
  n : Integer;
  MD : Double;
  jMinAll, jMaxAll : Integer;
  FingerMaxAll, FingerMinAll : Double;
  iMax, iMin : Integer;
  FingerMax, FingerMin : Double;
  NoCol, NoLigne : Integer;
  IsGoodData : Boolean;
  Finger, NewFinger, NewFingerEllipse : t_vdd;
  TabRead : t_vdd;
  X, Y : t_vdd;
  Xelp, Yelp : t_vdd;   // Intersection points (Old center - X, Y) with ellipse
  XelpB, YelpB : t_vdd;
  nNew : Integer;
  Xused, Yused : t_vdd;
  Ai, Bi, Thetai, Xci, Yci : Double;
  A, B, Theta, Xc, Yc, SumSquaRes : Double;
  FicRead, FicReadDelFinger : TextFile;
  FicWrite1, FicWrite2, FicWrite3, FicWrite4, FicWrite5, FicWrite6 : TextFile;

  OkDelFinger : Boolean;
  MdDelFinger : Double;
  LineDelFingerNumber : String;
  DelFingerNumber : t_vdi;
  Dis, Xnew, Ynew : Double;

  WearGroove : Double;
  WearGrooveBegin, WearGrooveEnd : Integer;
  FilePathDelFingers, FilePathWearGroove, FilePathRead : String;
begin
  FilePathDelFingers := FolderPathData + '\DeletedFingers.txt';
  FilePathWearGroove := FolderPathData + '\WearGrooveFingers.txt';
  FilePathRead       := FolderPathData + '\CaliperData.las';

  writeln(' ***   If you do not want to take some fingers in the treatment    ***');
  writeln(' ***     Prepare the DeletedFingers.txt in the exe file folder     ***');
  writeln(' *** This file has the finger numbers which will not be considered ***');

  writeln;
  writeln(' ATTENTIONS: ');
  writeln(' + Deleted fingers for the caliper treatment are inputted line by line ');
  writeln(' + File DeletedFingers.txt has the same number of lines as CaliperData.las');
  writeln(' + Each line of DeletedFingers.txt corresponds to one in CaliperData.las');

  writeln;
  writeln(' OK, Enter to continue !! ');
  writeln;
  readln;

  if FileExists(FilePathDelFingers) then
  begin
    OkDelFinger := True;
    AssignFile(FicReadDelFinger, FilePathDelFingers);
    Reset(FicReadDelFinger);
  end
  else
  begin
    OkDelFinger := False;
    SetLength(DelFingerNumber, 0);
  end;

  writeln;
  writeln(' ***          If you want to calculate the wear groove          ***');
  writeln(' ***  Prepare the WearGrooveFingers.txt in the exe file folder  ***');
  writeln(' ***     This file has ONLY the begin and end finger numbers    ***');
  writeln(' ***         of zone where you calculate the wear groove        ***');
  writeln;
  writeln(' OK, Enter to continue !! ');
  writeln;
  readln;
  if FileExists(FilePathWearGroove) then
  begin
    AssignFile(FicRead, FilePathWearGroove);
    Reset(FicRead);
    Read(FicRead, WearGrooveBegin, WearGrooveEnd);

    CloseFile(FicRead);
    writeln(' You want calcculate the wear groove from ', WearGrooveBegin,
        ' to ', WearGrooveEnd, ' fingers in clockwise direction');
  end
  else
  begin
    WearGrooveBegin := -999;
    WearGrooveEnd := -999;
  end;

  writeln;
  writeln('Did you prepare the file CaliperData.las in the .exe folder to treat?');
  writeln('If yes, Enter to start the treatement !!');
  readln;

  n := 60;
  // Memory for arrays
  setlength(Finger, n);
  SetLength(NewFinger, n);
  SetLength(NewFingerEllipse, n);
  setlength(X, n);
  setlength(Y, n);

  setlength(Xelp, n);
  setlength(Yelp, n);

  setlength(XelpB, n);
  setlength(YelpB, n);
  //
  NoCol := 61;
  setlength(TabRead,NoCol);
  ok := False;
  if FileExists(FilePathRead) then
  begin
    ok := True;
    // Preparation Output files
    AssignFile(FicWrite1, FolderPathRes+'\01FingerMaxMin.txt');
    Rewrite(FicWrite1);
    write(FicWrite1, '   No  MD      iMax     FingerMax    iMin  FingerMin ');
    for i := 1 to n do write(FicWrite1, 'Finger[',i,'] ');
    writeln(FicWrite1);
    //
    AssignFile(FicWrite2, FolderPathRes+'\02WearSection.txt');
    Rewrite(FicWrite2);
    writeln(FicWrite2, '        No    MD        WearSection        WearGroove(LimitedByBeginAndEndFingers)');
    //
    AssignFile(FicWrite3, FolderPathRes+'\03Centralisation.txt');
    Rewrite(FicWrite3);
    write(FicWrite3, '   No  MD  iMax     FingerMax    iMin  FingerMin    Xci    Yci    Ai    Bi    Thetai   OKCentralization  Xc    Yc    A    B    Theta ');
    // for i := 1 to n do write(FicWrite3, 'NewFinger[',i,'] ');
    writeln(FicWrite3);
    //
    AssignFile(FicWrite4, FolderPathRes+'\04WearHau.txt');
    Rewrite(FicWrite4);
    write(FicWrite4, '   No      MD  ');
    for i := 1 to n do write(FicWrite4, 'WearHau[',i,'] ');
    writeln(FicWrite4);
    //
    AssignFile(FicWrite5, FolderPathRes+'\05NewFinger.txt');
    Rewrite(FicWrite5);
    write(FicWrite5, '   No  MD  ');
    for i := 1 to n do write(FicWrite5, 'NewFinger[',i,'] ');
    writeln(FicWrite5);
    //
    AssignFile(FicWrite6, FolderPathRes+'\06NewFingerEllipse.txt');
    Rewrite(FicWrite6);
    write(FicWrite6, '   No  MD  ');
    for i := 1 to n do write(FicWrite6, 'NewFingerEllipse[',i,'] ');
    writeln(FicWrite6);
    // Read Data File
    AssignFile(FicRead, FilePathRead);
    Reset(FicRead);
    readln(FicRead, strRead);
    readln(FicReadDelFinger, strRead);
    // OK Let's GOOOOOO
    FingerMaxAll := 0.0;
    FingerMinAll := 1E99;
    NoLigne := 0;
    j := 0;
    k := 0;
    repeat
      NoLigne := NoLigne+1;
      // Deleted Fingers
      if OkDelFinger then
      begin
        if not Eof(FicReadDelFinger) then
        begin
          Readln(FicReadDelFinger, LineDelFingerNumber);
          TreatLineDeletedFinger(LineDelFingerNumber, n, MdDelFinger, DelFingerNumber);
        end
        else
        begin
          OkDelFinger := False;
          SetLength(DelFingerNumber, 0);
        end;
      end;
      //
      for i := 1 to NoCol do Read(FicRead, TabRead[i-1]);
      Readln(FicRead);
      //
      MD := TabRead[0];
      for i := 1 to 60 do Finger[i-1] := TabRead[i];
      // Hack Hack
      // Finger[35-1] := (Finger[34-1]+Finger[36-1])/2.0;
      // Traitement
      IsGoodData := True;
      for i := 1 to 60 do
        if (Finger[i-1] < 0.0) then
        begin
          IsGoodData := False;
          Break;
        end;
      write(NoLigne:10, IsGoodData:10);

      if (IsGoodData) then
      begin
        if OkDelFinger then
        begin
          write(' Deleted Finger Numbers => ');
          for i := 1 to Length(DelFingerNumber) do
          begin
            write(DelFingerNumber[i-1]);
            if i < Length(DelFingerNumber) then
              write(', ');
          end;
        end;
        writeln;
        j := j+1;
        // Max, Min Finger
        FindMaxMinFingers (n, Finger, iMax, FingerMax, iMin, FingerMin);
        if FingerMax>FingerMaxAll then
        begin
          FingerMaxAll := FingerMax;
          jMaxAll := j;
        end;
        if FingerMin<FingerMinAll then
        begin
          FingerMinAll := FingerMin;
          jMinAll := j;
        end;
        //
        write(FicWrite1, j:10, MD:10:4, iMax:5, FingerMax:10:4, iMin:5, FingerMin:10:4);
        for i := 1 to n do write(FicWrite1, Finger[i-1]:10:4);
        writeln(FicWrite1);

        //
        write(FicWrite2, j:10, MD:10:4, CalSectionCaliper (60, Finger):20);

        // Traitement Traitement Traitement Traitement Traitement Traitement
        OKCentralization := True;
        Try
          CalXY_XciYciAiBiThetai_fromFingers (n, Finger, X, Y, Xci, Yci, Ai, Bi, Thetai);
          //  Xc := Xci;
          //  Yc := Yci;
          //  A := Ai;
          //  B := Bi;
          //  Theta := Thetai;
          ExtraitFingerToTreat(n, X, Y, DelFingerNumber, nNew, Xused, Yused);
          FittingEllipseGeneralLevenbergMarquardt (nNew, Xused, Yused, Ai, Bi, Thetai, Xci, Yci,
                                         A, B, Theta, Xc, Yc, SumSquaRes, OKCentralization);

        Except
          writeln('Centralization process is not is not successful at ', j);
          OKCentralization := False;
        End;
        //
        write(FicWrite3,' ', j:10,
                        ' ', MD:10:4,
                        ' ', iMax:5,
                        ' ', FingerMax:15,
                        ' ', iMin:5,
                        ' ', FingerMin:15,
                        ' ', Xci:15,
                        ' ', Yci:15,
                        ' ', Ai:15,
                        ' ', Bi:15,
                        ' ', Thetai*180/Pi:15,
                        ' ', OKCentralization:15,
                        ' ', Xc:15,
                        ' ', Yc:15,
                        ' ', A:15,
                        ' ', B:15,
                        ' ', Theta*180/Pi:15,
                        ' ', A/B:15,
                        ' ', Sqrt(Sqr(Xc)+sqr(Yc))/min(A, B):15);
        writeln(FicWrite3);
        //
        if OKCentralization then
        begin
          // Attention Units of parameters in SI
          k := k+1;
          CalNewFingerXcYc (n, X, Y, Xc, Yc, NewFinger);
          FindMaxMinFingers (n, NewFinger, iMax, FingerMax, iMin, FingerMin);
          //
          write(FicWrite5,' ', j:10, ' ', MD:10:4);
          for i := 1 to n do write(FicWrite5,' ', (NewFinger[i-1]):15);
          writeln(FicWrite5);
          //
          write(FicWrite4, j:10,' ', MD:10:4);
          for i := 1 to n do
          begin
            ChangeAxisToEllipseAxis (Xc, Yc, Theta, X[i-1], Y[i-1], Xnew, Ynew);
            Dis := DistancePointToIntersectionEllipse (Xnew, Ynew, A, B);
            write(FicWrite4,' ', Dis:15);
          end;
          writeln(FicWrite4);
          //
          CalNewFingerOfEllipse (n, X, Y, A, B, Theta, Xc, Yc, NewFingerEllipse, Xelp, Yelp);
          write(FicWrite6,' ', j:10, ' ', MD:10:4);
          for i := 1 to n do write(FicWrite6,' ', (NewFingerEllipse[i-1]):15);
          writeln(FicWrite6);

          // TEST TEST TEST
          //  WriteTestTreatCaliper(FolderPathRes+'\TreatCaliperTest.txt',
          //    n, A, B, Theta, Xc, Yc, Finger, NewFinger, NewFingerEllipse,
          //    X, Y, Xelp, Yelp);

          //  CalXY_XciYciAiBiThetai_fromFingers (n, NewFingerEllipse,
          //    XelpB, YelpB, Xci, Yci, Ai, Bi, Thetai);
          //  WriteTestTreatCaliper(FolderPathRes+'\TreatCaliperTestB.txt',
          //    n, A, B, Theta, Xc, Yc, Finger, NewFinger, NewFingerEllipse,
          //    X, Y, XelpB, YelpB);

          if (WearGrooveBegin >=0) and (WearGrooveEnd >= 0) then
          begin
            WearGroove := 0.0;
            i := WearGrooveBegin;
            while i <> WearGrooveEnd do
            begin
              WearGroove := WearGroove +
                  CalWearGrooveMesh(n, i, Finger, NewFingerEllipse, X, Y, Xelp, Yelp);
              i := i+1;
              if i > n then
                i := 1;
            end;
          end else
            WearGroove := -999;
          writeln(FicWrite2, ' ', WearGroove:20);
        end;
      end
      else
        writeln;
    until (EOF(FicRead));
    //
    writeln;
    writeln('FingerMaxAll : ', FingerMaxAll:10:4, '  at result number  : ', jMaxAll);
    writeln('FingerMinAll : ', FingerMinAll:10:4, '  at result number  : ', jMinAll);
    //
    CloseFile(FicRead);
    if FileExists(FilePathDelFingers) then
      CloseFile(FicReadDelFinger);
    CloseFile(FicWrite1);
    CloseFile(FicWrite2);
    CloseFile(FicWrite3);
    CloseFile(FicWrite4);
    CloseFile(FicWrite5);
    CloseFile(FicWrite6);
  end;
  // Free Array
  setlength(Finger, 0);
  SetLength(NewFinger, 0);
  SetLength(NewFingerEllipse, 0);
  setlength(X, 0);
  setlength(Y, 0);
  setlength(Xelp, 0);
  setlength(Yelp, 0);
  setlength(XelpB, 0);
  setlength(YelpB, 0);
  setlength(TabRead,0);
  Result := ok;
end;

{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Deleted fingers for the caliper treatement are inputed line by line
  3000,50 5,5 5,15 17
  MD then Deleted fingers min and max
-------------------------------------------------------------------------------}
function ReadDataCaliperFittingEllipseDelSomeFingers3 (
    const FilePathRead, FilePathDelFingers: String;
    const FolderPathRes : String;
    const ColMd, ColIniFinger, ColEndFinger : Integer;
    const WearGrooveBegin, WearGrooveEnd : Integer;
    var FingerMinAll, FingerMaxAll : Double;
    var jMinAll, jMaxAll : Integer;
    const pCancel: pBoolean = nil;
    const pLineProgress: pInteger = nil) : Boolean;
var
  IsOk, OKCentralization : boolean;
  i, j, k : Integer;
  strRead : string[2];
  n : Integer;
  MD : Double;

  iMax, iMin : Integer;
  FingerMax, FingerMin : Double;
  NoCol, NoLigne : Integer;
  IsGoodData : Boolean;
  Finger, NewFinger, NewFingerEllipse : t_vdd;

  X, Y : t_vdd;
  Xelp, Yelp : t_vdd;   // Intersection points (Old center - X, Y) with ellipse
  XelpB, YelpB : t_vdd;
  nNew : Integer;
  Xused, Yused : t_vdd;
  Ai, Bi, Thetai, Xci, Yci, Ri : Double;
  A, B, Theta, Xc, Yc, SumSquaRes : Double;
  FicRead, FicReadDelFinger : TextFile;

  Header : String;
  FicWrite1, FicWrite2, FicWrite3, FicWrite4, FicWrite5, FicWrite6 : TextFile;

  IsDelFingerFile, OkDelFinger : Boolean;

  MdDelFinger : Double;
  LineDelFingerNumber, LineDataRead: String;
  DelFingerNumber : t_vdi;
  Dis, Xnew, Ynew : Double;

  WearGroove : Double;
begin
  n := ColEndFinger - ColIniFinger + 1;

  // Memory for arrays
  setlength(Finger, n);
  SetLength(NewFinger, n);
  SetLength(NewFingerEllipse, n);
  setlength(X, n);
  setlength(Y, n);

  setlength(Xelp, n);
  setlength(Yelp, n);

  setlength(XelpB, n);
  setlength(YelpB, n);


  IsOk := False;
  if FileExists(FilePathRead) then
  begin
    IsOk := True;
    // Preparation Output files
    AssignFile(FicWrite1, FolderPathRes+'\01FingerMaxMin.txt');
    Rewrite(FicWrite1);
    Header := 'No MD iMax FingerMax iMin FingerMin';
    for i := 1 to n do Header := Header + ' ' + 'Finger[' + IntToStr(i) + ']';
    WriteHeader(FicWrite1, Header.Split([' ', #9], TStringSplitOptions.ExcludeEmpty), 20);

    //
    AssignFile(FicWrite2, FolderPathRes+'\02WearSection.txt');
    Rewrite(FicWrite2);
    Header := ' No MD CaliperSection WearGroove';
    WriteHeader(FicWrite2, Header.Split([' ', #9], TStringSplitOptions.ExcludeEmpty), 20);
    //
    AssignFile(FicWrite3, FolderPathRes+'\03Centralisation.txt');
    Rewrite(FicWrite3);
    Header := ' No MD iMax FingerMax iMin FingerMin Xci Yci Ai Bi Thetai IsOK Xc Yc A B Theta Ratio_A_B Level';
    WriteHeader(FicWrite3, Header.Split([' ', #9], TStringSplitOptions.ExcludeEmpty), 20);

    //
    AssignFile(FicWrite4, FolderPathRes+'\04WearHau.txt');
    Rewrite(FicWrite4);
    Header := 'No MD ';
    for i := 1 to n do Header := Header + ' ' + 'WearHau[' + IntToStr(i) + ']';
    WriteHeader(FicWrite4, Header.Split([' ', #9], TStringSplitOptions.ExcludeEmpty), 20);

    //
    AssignFile(FicWrite5, FolderPathRes+'\05NewFinger.txt');
    Rewrite(FicWrite5);
    Header := 'No MD ';
    for i := 1 to n do Header := Header + ' ' + 'NewFinger[' + IntToStr(i) + ']';
    WriteHeader(FicWrite5, Header.Split([' ', #9], TStringSplitOptions.ExcludeEmpty), 20);

    //
    AssignFile(FicWrite6, FolderPathRes+'\06NewFingerEllipse.txt');
    Rewrite(FicWrite6);
    Header := 'No MD ';
    for i := 1 to n do Header := Header + ' ' + 'NewFingerEllipse[' + IntToStr(i) + ']';
    WriteHeader(FicWrite6, Header.Split([' ', #9], TStringSplitOptions.ExcludeEmpty), 20);

    // Read Data File
    AssignFile(FicRead, FilePathRead);
    Reset(FicRead);
    readln(FicRead, strRead);

    SetLength(DelFingerNumber, 0);
    if (FilePathDelFingers <> '') and
        FileExists(FilePathDelFingers)
    then
    begin
      IsDelFingerFile := True;
      OkDelFinger := True;
      AssignFile(FicReadDelFinger, FilePathDelFingers);
      Reset(FicReadDelFinger);
      readln(FicReadDelFinger, strRead);
    end
    else
    begin
      IsDelFingerFile := False;
      OkDelFinger := False;
    end;
    if IsDelFingerFile then


    // OK Let's GOOOOOO
    FingerMaxAll := 0.0;
    FingerMinAll := 1E99;
    NoLigne := 0;
    j := 0;
    k := 0;
    repeat
      if Assigned(pCancel) and
         (pCancel^)
      then
        Break;

      NoLigne := NoLigne+1;
      if Assigned(pLineProgress) then
        pLineProgress^ := NoLigne;

      // Deleted Fingers
      if OkDelFinger then
      begin
        if not Eof(FicReadDelFinger) then
        begin
          Readln(FicReadDelFinger, LineDelFingerNumber);
          TreatLineDeletedFinger(LineDelFingerNumber, n, MdDelFinger, DelFingerNumber);
        end
        else
        begin
          OkDelFinger := False;
          SetLength(DelFingerNumber, 0);
        end;
      end;
      //
      // for i := 1 to NoCol do Read(FicRead, TabRead[i-1]);
      // MD := TabRead[0];
      // for i := 1 to 60 do Finger[i-1] := TabRead[i];

      Readln(FicRead, LineDataRead);
      IsGoodData := TreatLineData(LineDataRead, ColMd, ColIniFinger, ColEndFinger, Md, Finger);

      if (IsGoodData) then
      begin
        //  if OkDelFinger then
        //  begin
        //    write(' Deleted Finger Numbers => ');
        //    for i := 1 to Length(DelFingerNumber) do
        //    begin
        //      write(DelFingerNumber[i-1]);
        //      if i < Length(DelFingerNumber) then
        //        write(', ');
        //    end;
        //  end;

        j := j+1;
        // Max, Min Finger
        FindMaxMinFingers (n, Finger, iMax, FingerMax, iMin, FingerMin);
        if FingerMax>FingerMaxAll then
        begin
          FingerMaxAll := FingerMax;
          jMaxAll := j;
        end;
        if FingerMin<FingerMinAll then
        begin
          FingerMinAll := FingerMin;
          jMinAll := j;
        end;
        //
        write(FicWrite1, ' ', j:20, ' ', MD:20:6, ' ', iMax:20,
                         ' ', FingerMax:20:6, ' ', iMin:20, ' ', FingerMin:20:6);
        for i := 1 to n do write(FicWrite1, ' ', Finger[i-1]:20:6);
        writeln(FicWrite1);

        //
        write(FicWrite2, ' ', j:20, ' ', MD:20:6, ' ', CalSectionCaliper(n, Finger):20);

        // Traitement Traitement Traitement Traitement Traitement Traitement
        OKCentralization := True;
        Try
          CalXY_XciYciAiBiThetai_fromFingers (n, Finger, X, Y, Xci, Yci, Ai, Bi, Thetai);
          //  Xc := Xci;
          //  Yc := Yci;
          //  A := Ai;
          //  B := Bi;
          //  Theta := Thetai;
          ExtraitFingerToTreat(n, X, Y, DelFingerNumber, nNew, Xused, Yused);
          FittingEllipseGeneralLevenbergMarquardt (nNew, Xused, Yused, Ai, Bi, Thetai, Xci, Yci,
                                         A, B, Theta, Xc, Yc, SumSquaRes, OKCentralization);

        Except
          // writeln('Centralization process is not is not successful at ', j);
          OKCentralization := False;
        End;
        //
        writeln(FicWrite3,
            ' ', j:20,
            ' ', MD:20:6,
            ' ', iMax:20,
            ' ', FingerMax:20,
            ' ', iMin:20,
            ' ', FingerMin:20,
            ' ', Xci:20,
            ' ', Yci:20,
            ' ', Ai:20,
            ' ', Bi:20,
            ' ', Thetai*180/Pi:20,
            ' ', OKCentralization:20,
            ' ', Xc:20,
            ' ', Yc:20,
            ' ', A:20,
            ' ', B:20,
            ' ', Theta*180/Pi:20,
            ' ', A/B:20,
            ' ', Sqrt(Sqr(Xc)+sqr(Yc))/min(A, B):20);

        //
        if OKCentralization then
        begin
          // Attention Units of parameters in SI
          k := k+1;
          CalNewFingerXcYc (n, X, Y, Xc, Yc, NewFinger);
          FindMaxMinFingers (n, NewFinger, iMax, FingerMax, iMin, FingerMin);
          //
          write(FicWrite5, ' ', j:20, ' ', MD:20:6);
          for i := 1 to n do write(FicWrite5,' ', (NewFinger[i-1]):20);
          writeln(FicWrite5);
          //
          write(FicWrite4, ' ', j:20,' ', MD:20:6);
          for i := 1 to n do
          begin
            ChangeAxisToEllipseAxis (Xc, Yc, Theta, X[i-1], Y[i-1], Xnew, Ynew);
            Dis := DistancePointToIntersectionEllipse (Xnew, Ynew, A, B);
            write(FicWrite4, ' ', Dis:20);
          end;
          writeln(FicWrite4);
          //
          CalNewFingerOfEllipse (n, X, Y, A, B, Theta, Xc, Yc, NewFingerEllipse, Xelp, Yelp);
          write(FicWrite6, ' ', j:20, ' ', MD:20:4);
          for i := 1 to n do write(FicWrite6, ' ', (NewFingerEllipse[i-1]):20);
          writeln(FicWrite6);

          // TEST TEST TEST
          //  WriteTestTreatCaliper(FolderPathRes+'\TreatCaliperTest.txt',
          //    n, A, B, Theta, Xc, Yc, Finger, NewFinger, NewFingerEllipse,
          //    X, Y, Xelp, Yelp);

          //  CalXY_XciYciAiBiThetai_fromFingers (n, NewFingerEllipse,
          //    XelpB, YelpB, Xci, Yci, Ai, Bi, Thetai);
          //  WriteTestTreatCaliper(FolderPathRes+'\TreatCaliperTestB.txt',
          //    n, A, B, Theta, Xc, Yc, Finger, NewFinger, NewFingerEllipse,
          //    X, Y, XelpB, YelpB);

          if (WearGrooveBegin >=0) and (WearGrooveEnd >= 0) then
          begin
            WearGroove := 0.0;
            i := WearGrooveBegin;
            while i <> WearGrooveEnd do
            begin
              WearGroove := WearGroove +
                  CalWearGrooveMesh(n, i, Finger, NewFingerEllipse, X, Y, Xelp, Yelp);
              i := i+1;
              if i > n then
                i := 1;
            end;
          end
          else
            WearGroove := -999;
          writeln(FicWrite2, ' ', WearGroove:20);
        end;
      end;
    until (EOF(FicRead));

    //
    CloseFile(FicRead);
    if IsDelFingerFile then
      CloseFile(FicReadDelFinger);

    CloseFile(FicWrite1);
    CloseFile(FicWrite2);
    CloseFile(FicWrite3);
    CloseFile(FicWrite4);
    CloseFile(FicWrite5);
    CloseFile(FicWrite6);
  end;
  // Free Array
  setlength(Finger, 0);
  SetLength(NewFinger, 0);
  SetLength(NewFingerEllipse, 0);
  setlength(X, 0);
  setlength(Y, 0);
  setlength(Xelp, 0);
  setlength(Yelp, 0);
  setlength(XelpB, 0);
  setlength(YelpB, 0);

  Result := IsOk;
end;

{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Deleted fingers for the caliper treatement are inputed line by line
  3000,50 5,5 5,15 17
  MD then Deleted fingers min and max
-------------------------------------------------------------------------------}
Procedure TestReadDeletedFinger();
var
  FilePath : String;
  FilMou : TextFile;
  LineStr : String;
  Md : Double;
  DelFingerNumber : t_vdi;
begin
  FilePath := 'C:\HaDN_Data\DrillScanWork\CasingWear\Treat_Caliper_Data\Cases\Florian20171009_ Deleted Caliper Input\DeletedFingers.txt';
  if FileExists(FilePath) then
  begin
    AssignFile(FilMou, FilePath);
    Reset(FilMou);
    readln(FilMou);
    repeat
      readln(FilMou, LineStr);
      TreatLineDeletedFinger(LineStr, 60, Md, DelFingerNumber);
    until (Eof(FilMou));
    CloseFile(FilMou);
  end;
end;

function TreatLineData(
    const LineDataRead: String;
    const ColMd, ColIniFinger, ColEndFinger: Integer;
    Var AMd: Double;
    const Finger: t_vdd): Boolean;
var
  ColNo, index : Integer;
  LFormatSettings: TFormatSettings;
  LSplitStrs: TArray<string>;
  MyClass: TComponent;
begin
  Result := True;

  // Force FormatSettings.DecimalSeparator to '.'
  LFormatSettings := FormatSettings;
  LFormatSettings.DecimalSeparator := '.';

  LSplitStrs := LineDataRead.Split([' ', #9, ';'], TStringSplitOptions.ExcludeEmpty);

  if (Length(LSplitStrs) < 1) or
     (LSplitStrs[0].Chars[0] = '#')
  then
    Exit(False);

  try
    // StrToInt
    // StrToFloat(LSplitStrs[ColNo], LFormatSettings); Inc(ColNo);
    ColNo := ColMd;
    AMD   := StrToFloat(LSplitStrs[ColNo-1], LFormatSettings);

    for ColNo := ColIniFinger to ColEndFinger do
    begin
      index := ColNo-ColIniFinger;
      Finger[index] := StrToFloat(LSplitStrs[ColNo-1], LFormatSettings);
    end;

    for index := 1 to ColEndFinger-ColIniFinger+1 do
    begin
      if (Finger[index-1] < 0.0) then
        Exit(False);
    end;
  except
    Exit(False);
  end;


end;

Procedure TreatLineDeletedFinger(
    ALineStr : String;
    nFingers : Integer;
    var AMD : Double;
    var ADelFingerNumber : t_vdi);
var
  LValues : TArray<string>;
  nValues, i : Integer;
  NoMin, NoMax : Integer;
  MinMaxDelFinger : TArray<string>;
  NoDelFinger : Integer;
  nMinMax, nb, iIni, ii : Integer;
  Flag : Boolean;
  kk: Integer;
begin
  LValues := ALineStr.Split([',']);
  AMD := StrToFloat(LValues[0]);
  //
  SetLength(ADelFingerNumber, 0);
  nValues := Length(LValues);
  for i := 1 to nValues-1 do
  begin
    SetLength(MinMaxDelFinger, 0);
    MinMaxDelFinger := LValues[i].Split([' ', #9, ';'], TStringSplitOptions.ExcludeEmpty);
    nMinMax := Length(MinMaxDelFinger);
    if nMinMax >= 2 then
    begin
      NoMin := StrToInt(MinMaxDelFinger[0]);
      NoMax := StrToInt(MinMaxDelFinger[1]);
    end
    else
    if nMinMax = 1 then
    begin
      NoMin := StrToInt(MinMaxDelFinger[0]);
      NoMax := StrToInt(MinMaxDelFinger[0]);
    end
    else
    begin
      NoMin := -999;
      NoMax := -999;
    end;
    // Add Deleted Finger
    if (NoMin <> -999) and
       (NoMax <> -999)
    then
    begin
      if NoMin > NoMax then
         NoMin := NoMin - nFingers;
      nb := NoMax - NoMin + 1;
      iIni := Length(ADelFingerNumber);
      for ii := NoMin to NoMax do
      begin
        if ii <= 0 then
          NoDelFinger := nFingers+ii
        else
          NoDelFinger := ii;
        Flag := True;
        nb := Length(ADelFingerNumber);
        for kk := 1 to nb do
        begin
          if NoDelFinger = ADelFingerNumber[kk-1] then
            Flag := False;
        end;
        if Flag then
        begin
          nb := nb+1;
          SetLength(ADelFingerNumber, nb);
          ADelFingerNumber[nb-1] := NoDelFinger;
        end;
      end;
    end;
  end;
end;

{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Principe of NoFinger :
    In general : NoFinger in [1, n]
     0  -> n
    -1 -> n-1
    -2 -> n-2
-------------------------------------------------------------------------------}
function CalWearGrooveMesh(n : Integer; NoFinger : Integer;
    Finger, NewFingerEllipse : t_vdd;
    X, Y, Xelp, Yelp : t_vdd) : Double;
var
  i, j : Integer;
  l12, l23, l34, l41, l13 : Double;

  X1, Y1, X2, Y2, X4, Y4, X3, Y3 : Double;
  A1, B1, C1, A2, B2, C2 : Double;
  Xi, Yi : Double;
begin
  i := NoFinger;
  j := i+1;
  if j > n then
    j := 1;
  //
  if (NewFingerEllipse[i-1] >= Finger[i-1]) and
     (NewFingerEllipse[j-1] >= Finger[j-1])
  then
  begin
    Result := 0.0;
    Exit;
  end;

  // Point 1 (X[i-1], Y[i-1])       &  Point 2 (X[j-1], Y[j-1])
  // Point 3 (Xelp[j-1], Yelp[j-1]) &  Point 4 (Xelp[i-1], Yelp[i-1])
  X1 := X[i-1];
  Y1 := Y[i-1];
  X2 := X[j-1];
  Y2 := Y[j-1];
  X3 := Xelp[j-1];
  Y3 := Yelp[j-1];
  X4 := Xelp[i-1];
  Y4 := Yelp[i-1];

  if (NewFingerEllipse[i-1] <= Finger[i-1]) and
     (NewFingerEllipse[j-1] <= Finger[j-1])
  then
  begin
    l12 := sqrt(sqr(X1-X2) + sqr(Y1-Y2));
    l23 := sqrt(sqr(X2-X3) + sqr(Y2-Y3));
    l34 := sqrt(sqr(X3-X4) + sqr(Y3-Y4));
    l41 := sqrt(sqr(X4-X1) + sqr(Y4-Y1));
    l13 := sqrt(sqr(X1-X3) + sqr(Y1-Y3));

    Result := +sqrt((+l12+l23+l13)*(-l12+l23+l13)*(+l12-l23+l13)*(+l12+l23-l13))/4
            +sqrt((+l13+l34+l41)*(-l13+l34+l41)*(+l13-l34+l41)*(+l13+l34-l41))/4;
    Exit;
  end;

  // Cal intersection P1P2 & P3P4
  A1 := Y2 - Y1;
  B1 := X1 - X2;
  C1 := X1*(Y2-Y1) + Y1*(X1-X2);
  A2 := Y3 - Y4;
  B2 := X4 - X3;
  C2 := X4*(Y3-Y4)+Y4*(X4-X3);
  if Abs(A1*B2-A2*B1) > 1E-12  then
  begin
    Xi := (C1*B2-C2*B1)/(A1*B2-A2*B1);
    Yi := (C1*A2-C2*A1)/(B1*A2-B2*A1);
    X3 := Xi;
    Y3 := Yi;
    if (Finger[i-1]-NewFingerEllipse[i-1]) > (Finger[j-1]-NewFingerEllipse[j-1]) then
    begin
      X1 := X[i-1];
      Y1 := Y[i-1];
      X2 := Xelp[i-1];
      Y2 := Yelp[i-1];
    end else
    begin
      X1 := X[j-1];
      Y1 := Y[j-1];
      X2 := Xelp[j-1];
      Y2 := Yelp[j-1];
    end;
    l12 := sqrt(sqr(X1-X2) + sqr(Y1-Y2));
    l23 := sqrt(sqr(X2-X3) + sqr(Y2-Y3));
    l13 := sqrt(sqr(X1-X3) + sqr(Y1-Y3));
    // Triangle Area
    Result := +sqrt((+l12+l23+l13)*(-l12+l23+l13)*(+l12-l23+l13)*(+l12+l23-l13))/4
  end else
  begin
    result := 0;
    Exit;
  end;



end;

procedure test();
var
  X1, Y1, X2, Y2, X4, Y4, X3, Y3 : Double;
  A1, B1, C1, A2, B2, C2 : Double;
  Xi, Yi : Double;
begin
  X1 := 0;
  Y1 := 1;
  X2 := 2;
  Y2 := 0;
  X3 := 2;
  Y3 := 1;
  X4 := 0;
  Y4 := 0;
  // Cal intersection
  A1 := Y2 - Y1;
  B1 := X1 - X2;
  C1 := X1*(Y2-Y1) + Y1*(X1-X2);
  A2 := Y3 - Y4;
  B2 := X4 - X3;
  C2 := X4*(Y3-Y4)+Y4*(X4-X3);

  Xi := (C1*B2-C2*B1)/(A1*B2-A2*B1);
  Yi := (C1*A2-C2*A1)/(B1*A2-B2*A1);

end;
{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
function RecalWearHight (FilePathRead,
                         FilePathWrite1: string) : Boolean;
var
  ok : boolean;
  i, j : Integer;
  n : Integer;
  MD : Double;
  NoLigne : Integer;
  NewFinger : t_vdd;
  FicRead, FicWrite1 : textfile;
  NewFingerMin : Double;
  No : Integer;
begin
  n := 60;
  setlength(NewFinger,n);
  ok := False;
  if FileExists(FilePathRead) then
  begin
    NewFingerMin := 999999;
    ok := True;
    AssignFile(FicRead, FilePathRead);
    Reset(FicRead);
    Readln(FicRead);
    //
    NoLigne := 0;
    repeat
      NoLigne := NoLigne+1;
      writeln(' AA ', NoLigne:10);
      Read(FicRead, j);
      Read(FicRead, MD);
      for i := 1 to n do Read(FicRead, NewFinger[i-1]);
      Readln(FicRead);
      for i := 1 to n do
      begin
        if (NewFinger[i-1] > 0) and (NewFinger[i-1] < NewFingerMin) then
        begin
          NewFingerMin := NewFinger[i-1];
          No := j;
        end;
      end;
    until (EOF(FicRead));
    CloseFile(FicRead);

    //
    AssignFile(FicRead, FilePathRead);
    Reset(FicRead);
    Readln(FicRead);
    //
    AssignFile(FicWrite1, FilePathWrite1);
    Rewrite(FicWrite1);
    writeln(FicWrite1, '   Finger Min All: ', NewFingerMin:15);
    writeln(FicWrite1, '         At line : ', No:15);
    write(FicWrite1, '   No      MD  ');
    for i := 1 to n do write(FicWrite1, 'WearHau[',i,'] ');
    writeln(FicWrite1);
    NoLigne := 0;
    repeat
      NoLigne := NoLigne+1;
      writeln(' BB ', NoLigne:10);
      Read(FicRead, j);
      Read(FicRead, MD);
      for i := 1 to n do Read(FicRead, NewFinger[i-1]);
      Readln(FicRead);
      write(FicWrite1, j:10,' ', MD:10:4);
      for i := 1 to n do write(FicWrite1,' ', (NewFinger[i-1]-NewFingerMin):15);
      writeln(FicWrite1);
    until (EOF(FicRead));
    //
    CloseFile(FicWrite1);
    CloseFile(FicRead);
  end;
  // Free Array
  setlength(NewFinger,0);
  Result := ok;
end;
// -----------------------------------------------------------------------------


{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}

function ReadLasFileCaliperBaker (FilePathRead,
    FilePathWrite1 : string) : Boolean;
var
  ok : boolean;
  iIni, iFin, i, j, k, Line : Integer;
  strRead : string[2];
  n : Integer;
  MD : Double;
  iMax, iMin : Integer;
  Finger : t_vdd;
  TabRead : t_vdd;
  NoCol, NoLigne : Integer;
  FicRead, FicWrite1 : textfile;
begin
  n := 60;
  setlength(Finger,n);
  NoCol := 69;
  setlength(TabRead,NoCol);
  ok := False;
  if FileExists(FilePathRead) then
  begin
    ok := True;
    AssignFile(FicRead, FilePathRead);
    Reset(FicRead);
    repeat
      readln(FicRead, strRead);
    until (strRead='~A');
    //
    AssignFile(FicWrite1, FilePathWrite1);
    Rewrite(FicWrite1);
    // write(FicWrite1, '   No  MD      iMax     FingerMax    iMin  FingerMin ');
    // for i := 1 to n do write(FicWrite1, 'Finger[',i,'] ');
    Write(FicWrite1, ' DEPT[M]  AM01[MM]  AM02[MM]  AM03[MM]  AM04[MM]  AM05[MM]  AM06[MM]  AM07[MM]  AM08[MM]  AM09[MM]  AM10[MM]  AM11[MM]  AM12[MM]  AM13[MM]  AM14[MM]  AM15[MM]  AM16[MM]  AM17[MM]  AM18[MM]  AM19[MM] ');
    Write(FicWrite1, ' AM20[MM]  AM21[MM]  AM22[MM]  AM23[MM]  AM24[MM]  AM25[MM]  AM26[MM]  AM27[MM]  AM28[MM]  AM29[MM]  AM30[MM]  AM31[MM]  AM32[MM]  AM33[MM]  AM34[MM]  AM35[MM]  AM36[MM]  AM37[MM]  AM38[MM]  AM39[MM] ');
    Write(FicWrite1, ' AM40[MM]  AM41[MM]  AM42[MM]  AM43[MM]  AM44[MM]  AM45[MM]  AM46[MM]  AM47[MM]  AM48[MM]  AM49[MM]  AM50[MM]  AM51[MM]  AM52[MM]  AM53[MM]  AM54[MM]  AM55[MM] ');
    Write(FicWrite1, ' AM56[MM]  AM57[MM]  AM58[MM]  AM59[MM]  AM60[MM]  AVG[MM]  CSPEED[M/MN] DEV[DEG] LOSS[]  MAX[MM] MIN[MM]  ROT[DEG] WALLTH[] ');
    writeln(FicWrite1);
    //
    NoLigne := 0;
    j := 0;
    k := 0;
    repeat
      NoLigne := NoLigne+1;
      //
      Readln(FicRead, TabRead[0]);
      for Line := 1 to 11 do
      begin
        iIni := (Line-1)*6+2;
        iFin := iIni+5;
        for i:=iIni to iFin do Read(FicRead,TabRead[i-1]);
        Readln(FicRead);
      end;
      iIni := iFin+1;
      iFin := iIni+1;
      for i:=iIni to iFin do Read(FicRead,TabRead[i-1]);
      Readln(FicRead);

      for i := 1 to NoCol do write(FicWrite1, TabRead[i-1]:10:4);
      writeln(FicWrite1);

      writeln(NoLigne:10);
    until (EOF(FicRead));
    //
    CloseFile(FicWrite1);
    CloseFile(FicRead);
  end;
  // Free Array
  setlength(Finger,0);
  setlength(TabRead,0);
  Result := ok;
end;
// -----------------------------------------------------------------------------


{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Caliper Rotation
  Rotated Caliper
-------------------------------------------------------------------------------}
function TreatRotatedCaliper (FilePathRead,
    FilePathWrite1 : string) : Boolean;
var
  ok : boolean;
  iIni, iFin, i, j, k, Line : Integer;
  strRead : string[2];
  n : Integer;
  MD : Double;
  iMax, iMin : Integer;
  Finger, NewFinger : t_vdd;
  TabRead : t_vdd;
  NoCol, NoLigne : Integer;
  FicRead, FicWrite1 : textfile;
  iDecalage : Integer;
begin
  n := 60;
  setlength(Finger,n);
  setlength(NewFinger,n);
  NoCol := 62;
  setlength(TabRead,NoCol);
  ok := False;
  if FileExists(FilePathRead) then
  begin
    ok := True;
    AssignFile(FicRead, FilePathRead);
    Reset(FicRead);
    //
    AssignFile(FicWrite1, FilePathWrite1);
    Rewrite(FicWrite1);
    write(FicWrite1, '  MD  ');
    for i := 1 to n do write(FicWrite1, 'AM[',i,'] ');
    writeln(FicWrite1);

    NoLigne := 0;
    repeat
      NoLigne := NoLigne+1;
      writeln(NoLigne:10);
      //
      for i := 1 to NoCol do Read(FicRead,TabRead[i-1]);
      Readln(FicRead);
      //
      MD := TabRead[0];
      iDecalage := Round(TabRead[1]);

      for i := 1 to n do Finger[i-1] := TabRead[i+2-1];

      //  for i := 1 to n do
      //  begin
      //    if ((i + iDecalage) > n)  then j := (i + iDecalage) mod n
      //    else j := (i + iDecalage);
      //    NewFinger[j-1] := Finger[i-1];
      //  end;

      for i := 1 to n do
      begin
        if ((i - iDecalage) < 1)  then j := (i - iDecalage) + n
        else j := (i - iDecalage);
        NewFinger[j-1] := Finger[i-1];
      end;

      write(FicWrite1, MD:10:4);
      for i := 1 to n do write(FicWrite1, NewFinger[i-1]:10:4);
      writeln(FicWrite1);
    until (EOF(FicRead));
    //
    CloseFile(FicWrite1);
    CloseFile(FicRead);
  end;
  // Free Array
  setlength(Finger,0);
  setlength(NewFinger,0);
  setlength(TabRead,0);
  Result := ok;
end;
// -----------------------------------------------------------------------------

{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Caliper Rotation
  Rotated Caliper

  Read the number of caliper fingers in the first line of data file
-------------------------------------------------------------------------------}
function TreatRotatedCaliper2 (FilePathRead,
    FilePathWrite1 : string) : Boolean;
var
  ok : boolean;
  iIni, iFin, i, j, k, Line : Integer;
  strRead : string[2];
  n : Integer;
  MD : Double;
  iMax, iMin : Integer;
  Finger, NewFinger : t_vdd;
  TabRead : t_vdd;
  NoCol, NoLigne : Integer;
  FicRead, FicWrite1 : textfile;
  iDecalage : Integer;
begin
  ok := False;
  if FileExists(FilePathRead) then
  begin
    ok := True;
    AssignFile(FicRead, FilePathRead);
    Reset(FicRead);
    Readln(FicRead, n);
    NoCol := n+2;
    // Allocate memory for arrays
    setlength(Finger, n);
    setlength(NewFinger, n);
    setlength(TabRead, NoCol);
    //
    AssignFile(FicWrite1, FilePathWrite1);
    Rewrite(FicWrite1);
    write(FicWrite1, '     MD  ');
    for i := 1 to n do write(FicWrite1, '      AM[',i,'] ');
    writeln(FicWrite1);
    
    NoLigne := 0;
    repeat
      NoLigne := NoLigne+1;
      writeln(NoLigne:10);
      //
      for i := 1 to NoCol do Read(FicRead, TabRead[i-1]);
      Readln(FicRead);
      //
      MD := TabRead[0];
      iDecalage := Round(TabRead[1]);
      for i := 1 to n do Finger[i-1] := TabRead[i+2-1];
      // D�caler
      for i := 1 to n do
      begin
        if ((i - iDecalage) < 1)  then j := (i - iDecalage) + n
        else j := (i - iDecalage);
        NewFinger[j-1] := Finger[i-1];
      end;
      //
      write(FicWrite1, MD:10:4);
      for i := 1 to n do write(FicWrite1, NewFinger[i-1]:12:8);
      writeln(FicWrite1);
    until (EOF(FicRead));
    //
    CloseFile(FicWrite1);
    CloseFile(FicRead);
  end;
  // Free Array
  setlength(Finger,0);
  setlength(NewFinger,0);
  setlength(TabRead,0);
  Result := ok;
end;
// -----------------------------------------------------------------------------


{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>

  Find - Depth - Shifted Cell - MITDEV - MITROT

-------------------------------------------------------------------------------}
function TreatRotatedCaliper3 (FilePathRaw,
    FilePathRot,
    FilePathRes : string) : Boolean;
var
  ok : boolean;

  i, n : Integer;
  MD : Double;
  LRaw, LRot : TDataCaliper;

  ColNo, NoLigne : Integer;
  FicReadRaw, FicReadRot, FicWrite1 : textfile;
  LineStr : String;
  LSplitStrs : TArray<string>;

  iDecalage : Integer;
begin
  ok := False;
  if FileExists(FilePathRaw) and FileExists(FilePathRot) then
  begin
    ok := True;
    n := 60;
    LRaw.Init();
    LRaw.SetNumberFingers(n);
    LRot.Init();
    LRot.SetNumberFingers(n);

    AssignFile(FicReadRaw, FilePathRaw);
    Reset(FicReadRaw);
    repeat
      Readln(FicReadRaw, LineStr);
      SetLength(LSplitStrs, 0);
      LSplitStrs := LineStr.Split([' ', #9], TStringSplitOptions.ExcludeEmpty);
    until LSplitStrs[0] = '~A';

    AssignFile(FicReadRot, FilePathRot);
    Reset(FicReadRot);
    repeat
      Readln(FicReadRot, LineStr);
      SetLength(LSplitStrs, 0);
      LSplitStrs := LineStr.Split([' ', #9], TStringSplitOptions.ExcludeEmpty);
    until LSplitStrs[0] = '~A';
    //
    AssignFile(FicWrite1, FilePathRes);
    Rewrite(FicWrite1);
    ColNo := 0;
    Inc(ColNo); Write(FicWrite1, ' ['+IntToStr(ColNo)+']Raw_MD');
    Inc(ColNo); Write(FicWrite1, ' ['+IntToStr(ColNo)+']Raw_MITDEV');
    Inc(ColNo); Write(FicWrite1, ' ['+IntToStr(ColNo)+']Raw_MITROT');
    Inc(ColNo); Write(FicWrite1, ' ['+IntToStr(ColNo)+']ShiftedNumber');
    //  Inc(ColNo); Write(FicWrite1, ' ['+IntToStr(ColNo)+']Rot_MD');
    //  Inc(ColNo); Write(FicWrite1, ' ['+IntToStr(ColNo)+']Rot_MITDEV');
    //  Inc(ColNo); Write(FicWrite1, ' ['+IntToStr(ColNo)+']Rot_MITROT');
    Writeln(FicWrite1);

    NoLigne := 0;
    repeat
      NoLigne := NoLigne+1;
      writeln(NoLigne:10);

      Readln(FicReadRaw, LineStr);
      SetLength(LSplitStrs, 0);
      LSplitStrs := LineStr.Split([' ', #9], TStringSplitOptions.ExcludeEmpty);
      ArrayStrsToDataCaliper(LSplitStrs, LRaw);

      Readln(FicReadRot, LineStr);
      SetLength(LSplitStrs, 0);
      LSplitStrs := LineStr.Split([' ', #9], TStringSplitOptions.ExcludeEmpty);
      ArrayStrsToDataCaliper(LSplitStrs, LRot);

      LRaw.ShiftedNumber := FindShiftedRotationNumber(LRaw, LRot);

      Writeln(FicWrite1, ' ', LRaw.Md:12:6,
                         ' ', LRaw.MITDEV:10:3,
                         ' ', LRaw.MITROT:10:3,
                         ' ', LRaw.ShiftedNumber:4);
                         // ' ', LRot.Md:12:6,
                         // ' ', LRot.MITDEV:10:3,
                         // ' ', LRot.MITROT:10:3);

    until EOF(FicReadRaw) or EOF(FicReadRot);
    //
    CloseFile(FicWrite1);
    CloseFile(FicReadRaw);
    CloseFile(FicReadRot);
  end;
  // Free Array
  LRaw.SetNumberFingers(0);
  LRot.SetNumberFingers(0);
  Result := ok;
end;


{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>

  Find - Depth - Shifted Cell - MITDEV - MITROT

-------------------------------------------------------------------------------}
function TreatExtraitColumesCaliper (
    nCols : Integer;
    Cols : t_vdi;
    FilePathRaw,
    FilePathRes : string) : Boolean;
var
  ok : boolean;

  i, n : Integer;
  MD : Double;

  ColNo, NoLigne : Integer;
  FicReadRaw, FicReadNum, FicWrite1 : textfile;
  Entete : String;
  LineStr : String;
  LSplitStrs : TArray<string>;

  iDecalage : Integer;
begin
  ok := False;
  if FileExists(FilePathRaw) then
  begin
    ok := True;
    n := 60;
    AssignFile(FicReadRaw, FilePathRaw);
    Reset(FicReadRaw);
    repeat
      Readln(FicReadRaw, LineStr);
      SetLength(LSplitStrs, 0);
      LSplitStrs := LineStr.Split([' ', #9], TStringSplitOptions.ExcludeEmpty);
    until LSplitStrs[0] = '~A';
    //
    AssignFile(FicWrite1, FilePathRes);
    Rewrite(FicWrite1);
    ColNo := 0;
    Inc(ColNo); Write(FicWrite1, ' ['+IntToStr(ColNo)+']MD');
    for i := 1 to nCols do
    begin
      Inc(ColNo);
      Entete := ' [' + IntToStr(ColNo) + ']Col' + IntToStr(Cols[i-1]);
      Write(FicWrite1, Entete);
    end;
    Writeln(FicWrite1);

    NoLigne := 0;
    repeat
      NoLigne := NoLigne+1;
      writeln(NoLigne:10);

      Readln(FicReadRaw, LineStr);
      SetLength(LSplitStrs, 0);
      LSplitStrs := LineStr.Split([' ', #9], TStringSplitOptions.ExcludeEmpty);

      Write(FicWrite1, ' ', LSplitStrs[0]);
      for i := 1 to nCols do
      begin
        if Cols[i-1] < Length(LSplitStrs) then
          Write(FicWrite1, ' ', LSplitStrs[Cols[i-1]])
        else
          Write(FicWrite1, ' -999')
      end;
      Writeln(FicWrite1);
    until EOF(FicReadRaw);
    //
    CloseFile(FicWrite1);
    CloseFile(FicReadRaw);
  end;
  Result := ok;
end;

// -----------------------------------------------------------------------------

function VerifyShiftedRotation(
    const NoRot : Integer;
    const ARaw, ARot : TDataCaliper) : Boolean;
var
  i, n, iRaw, iRot : Integer;
begin
  if (ARaw.n <> ARot.n) or
     (ARaw.n <= 0) or
     (ARot.n <= 0)
  then
  begin
    Result := False;
    Exit;
  end;

  Result := True;
  n := ARaw.n;
  for i := 1 to n do
  begin
    iRaw := i;
    iRot := i+NoRot;
    if iRot > n then
    begin
      repeat
        iRot := iRot-n;
      until iRot <= n;
    end;

    if Abs(ARaw.Fingers[iRaw-1]-ARot.Fingers[iRot-1]) > 1E-12 then
    begin
      Result := False;
      Exit;
    end;
  end;

end;

function FindShiftedRotationNumber(
    const ARaw, ARot : TDataCaliper) : Integer;
var
  i, n : Integer;
  IsOK : Boolean;
begin
  Result := -999;
  if (ARaw.n <> ARot.n) or
     (ARaw.n <= 0) or
     (ARot.n <= 0)
  then
    Exit;
  if Abs(ARaw.Md - ARot.Md) > 1E-12 then
    Exit;
  n :=  ARaw.n;
  IsOK := False;
  for i := 0 to n-1 do
  begin
    if VerifyShiftedRotation(i, ARaw, ARot) then
    begin
      Result := i;
      Exit;
    end;
  end;
end;

procedure WriteTestTreatCaliper(
    Nom : String;
    n : Integer;
    A, B, Theta, Xc, Yc : Double;
    Finger, NewFinger, NewFingerEllipse : t_vdd;
    X, Y : t_vdd;
    Xelp, Yelp : t_vdd   // Intersection points (Old center - X, Y) with ellipse
    );
var
  FilMou : Textfile;
  i : Integer;
begin
  AssignFile(FilMou, Nom);
  Rewrite(FilMou);
  writeln(FilMou, '  A     B     Theta    Xc     Yc   ');
  writeln(FilMou, ' ', A:15,
                  ' ', B:15,
                  ' ', Theta:15,
                  ' ', Xc:15,
                  ' ', Yc:15);
  writeln(FilMou, '  Finger     NewFinger     NewFingerEllipse    X     Y     Xelp   Yelp  ');
  for i := 1 to n do
    writeln(FilMou, ' ', (Finger[i-1]*0.0254):15,
                    ' ', (NewFingerEllipse[i-1]*0.0254):15,
                    ' ', (NewFingerEllipse[i-1]*0.0254):15,
                    ' ', (X[i-1]*0.0254):15,
                    ' ', (Y[i-1]*0.0254):15,
                    ' ', (Xelp[i-1]*0.0254):15,
                    ' ', (Yelp[i-1]*0.0254):15
                    );
  CloseFile(FilMou);
end;

function TestDataGenerateInitialWear(const Rcsg, Rtj, hmax, TfoMax : Double;
   var msg : String) : Boolean;
begin
  Result := True;
  if (Rcsg <= 0.0) or (Rtj <= 0.0) or (hmax < 0.0) then
  begin
    msg := 'Verify the negative value of Rcsg or Rtj or hwearmax';
    Exit(False);
  end;
  if (Rcsg <= Rtj) then
  begin
    msg := 'Verify Rcsg and Rtj ==> Rcsg <= Rtj is not good';
    Exit(False);
  end;
    if (hmax >= Rtj) then
  begin
    msg := 'Verify hmax and Rtj ==> hmax >= Rtj is not good';
    Exit(False);
  end;
end;
{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
function ReadDataGenerateInitialWear (FilePathData,
    FilePathRes : string) : Boolean;
var
  ok : boolean;

  i, n : Integer;
  MD, Rcsg, Rtj : Double;
  hmax, TfoMax : Double;
  WearHightFingers : t_vdd;

  ColNo, NoLigne : Integer;
  FicReadData, FicWrite1 : textfile;
  LineStr : String;
  LSplitStrs : TArray<string>;

  msg : String;

  LFormatSettings: TFormatSettings;
begin
  // Force FormatSettings.DecimalSeparator to '.'
  LFormatSettings := FormatSettings;
  LFormatSettings.DecimalSeparator := '.';

  ok := False;
  if FileExists(FilePathData) then
  begin
    ok := True;

    AssignFile(FicReadData, FilePathData);
    Reset(FicReadData);

    Readln(FicReadData, n);

    //
    AssignFile(FicWrite1, FilePathRes);
    Rewrite(FicWrite1);
    ColNo := 0;
    Inc(ColNo); Write(FicWrite1, ' ['+IntToStr(ColNo)+']MD');
    Inc(ColNo); Write(FicWrite1, ' ['+IntToStr(ColNo)+']Rcsg');
    Inc(ColNo); Write(FicWrite1, ' ['+IntToStr(ColNo)+']Rtj');
    Inc(ColNo); Write(FicWrite1, ' ['+IntToStr(ColNo)+']hwearmax');
    Inc(ColNo); Write(FicWrite1, ' ['+IntToStr(ColNo)+']TfoMax');
    for i := 0 to n do
    begin
       Inc(ColNo); Write(FicWrite1, ' ['+IntToStr(ColNo)+']Wear@'+IntToStr(i));
    end;
    Writeln(FicWrite1);

    NoLigne := 0;
    repeat
      NoLigne := NoLigne+1;
      writeln(NoLigne:10);

      Readln(FicReadData, LineStr);
      SetLength(LSplitStrs, 0);
      LSplitStrs := LineStr.Split([' ', #9], TStringSplitOptions.ExcludeEmpty);

      ColNo:= 0;
      MD     := StrToFloat(LSplitStrs[ColNo], LFormatSettings); Inc(ColNo);
      Rcsg   := StrToFloat(LSplitStrs[ColNo], LFormatSettings); Inc(ColNo);
      Rtj    := StrToFloat(LSplitStrs[ColNo], LFormatSettings); Inc(ColNo);
      hmax   := StrToFloat(LSplitStrs[ColNo], LFormatSettings); Inc(ColNo);
      TfoMax := StrToFloat(LSplitStrs[ColNo], LFormatSettings); Inc(ColNo);
      TfoMax := TfoMax*Pi/180;

      if TestDataGenerateInitialWear(Rcsg, Rtj, hmax, TfoMax, msg) then
      begin
        GenerateInitialWearFingers(Rcsg, Rtj, n, hmax, TfoMax, WearHightFingers);

        // Write ti file
        Write(FicWrite1, ' ', Md:12:6,
                         ' ', Rcsg:12:9,
                         ' ', Rtj:12:9,
                         ' ', hmax:12:9,
                         ' ', TfoMax*180/Pi:12:6);
        for i := 0 to n do
          Write(FicWrite1, ' ', WearHightFingers[i]:12:9);
        Writeln(FicWrite1);
      end else
      begin
        writeln;
        writeln(' Data error *** Please verify the data at line ', NoLigne+1:10, ' ***');
        writeln(' ', msg);
        Writeln( '      MD          Rcsg          Rtj        hwearmax       TfoMax ');
        Writeln( ' ', Md:12:6,
                 ' ', Rcsg:12:9,
                 ' ', Rtj:12:9,
                 ' ', hmax:12:9,
                 ' ', TfoMax*180/Pi:12:6);
        writeln;
        Break;
      end;


    until EOF(FicReadData);

    //
    SetLength(WearHightFingers, 0);
    CloseFile(FicWrite1);
    CloseFile(FicReadData);
  end;
  // Free Array
  Result := ok;
end;


end.
