unit UnitNewCasingWearModelStudy;

interface

uses
	SysUtils, Classes, Math,
  UnitModeleStructureBasic,
  UnitModeleCalcul,
  UnitModeleUsureCasingHa;

// Procedures and Functions
procedure LectureData(NomFic: string; var dextcsg, dintcsg, dexttj, wearvol : t_vdd);
procedure EtudeTjOD_Section_Hauteur(NomFic: string);

procedure LectureDataSensibility(NomFic: string;
                                 var dextcsg, dintcsg, dexttj, force : t_vdd;
                                 var A, B, WFasym : t_vdd);
procedure LectureDataEtudeForceVarie(FilePath: string;
                                     var dextcsg, dintcsg, dexttj, A, B, WFasym : Double;
                                     var Work, force : t_vdd);
procedure ReconstrucVuseTravailLarPlate(RextCsg, RintCsg, Rtjt : Double;
                                        F : Double;
                                        A, B, WFasymp : Double;
                                        n : Integer; LarLim : Double;
                                        Var LisWork, LisSecUse, LisHauUse, LisLarUse : t_vdd);
procedure ReconstrucVuseTravailLarCourbee(RextCsg, RintCsg, Rtjt : Double;
                                          F : Double;
                                          A, B, WFasymp : Double;
                                          n : Integer; LarLim : Double;
                                          Var LisWork, LisSecUse, LisHauUse, LisLarUse : t_vdd);
procedure ReconstrucVuseTravail_LinWF (RextCsg,RintCsg, Rtjt : Double;
                                       WFlin : Double;
                                       LisWear, LisCorFactor : t_vdd;
                                       n : Integer; LarLim : Double;
                                       Var LisWork, LisSecUse, LisHauUse, LisLarUse : t_vdd);
procedure ReconstrucVuseTravail_LinWF_NoCorrect (RextCsg, RintCsg, Rtjt : Double;
                                                 WFlin : Double;
                                                 n : Integer; LarLim : Double;
                                                 Var LisWork, LisSecUse, LisHauUse, LisLarUse : t_vdd);
procedure RecontructCurvesNewModelForComparison (RextCsg, RintCsg, Rtjt : Double;
     F : Double; A, B, PClim : Double; WFlin : Double; HauMax : Double;
     nb_intervals : Integer;
     Var AHau, ASec, ALarPlate, ALarCurved, AHauCorrected, ASecCorrected: t_vdd;
     Var AWearPercent, ACorFac, AWearPercentCorrect : t_vdd;
     Var AWorkModLin, AWorkModNonLin : t_vdd);
Procedure WriteCurvesNewModelForComparison(FilePath : String;
     AHau, ASec, ALarPlate, ALarCurved, AHauCorrected, ASecCorrected: t_vdd;
     AWearPercent, ACorFac, AWearPercentCorrect : t_vdd;
     AWorkModLin, AWorkModNonLin : t_vdd);

procedure Test_WearModelNoThreshole(Rcsg, Rtjt : Double;
                                    WF8h, VolUse8h : Double;
                                    NomFic: string);
procedure Test_WearModelThreshole_SI(Rcsg, Rtjt : Double;
                                     F : Double;
                                     A, B : Double;
                                     NomFic: string);
procedure Test_WearModelThreshole_US(Rcsg, Rtjt : Double;
                                     F : Double;
                                     A, B : Double;
                                     NomFic: string);
procedure Test_WearModelThreshole (Rcsg, Rtjt : Double;
                                   F : Double;
                                   A, B : Double;
                                   NomFic: string);
procedure Test_WearModelSansSeuilAsymptote (Rcsg, Rtjt : Double;
                                            F : Double;
                                            A, B, CoefK : Double;
                                            NomFic: string);
procedure Test_WearModelComplet (SysUnits : string;
                                 RextCsg, RintCsg, Rtjt : Double;
                                 F : Double;
                                 A, B, PClim : Double;
                                 WFlin : Double;
                                 nb_points : Integer;
                                 NomFic: string);

procedure Test_WearModel_PassantPoint8h (Rcsg, Rtjt : Double;
                                         F : Double;
                                         Hau8h, WF8h : Double;
                                         PathFic: string);
procedure Test_NewestWearModel_VolParLog(Rcsg, Rtjt : Double;
                                         F : Double;
                                         A, B, PClim : Double;
                                         NomFic: string);


implementation

{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
procedure LectureData(NomFic: string; var dextcsg, dintcsg, dexttj, wearvol : t_vdd);
var
  i : Integer;
	fic : text;
begin
  assign(fic, NomFic);
  reset(fic);
  i := 0;
  repeat
    setlength(dextcsg, i+1);
    setlength(dintcsg, i+1);
    setlength(dexttj, i+1);
    setlength(wearvol, i+1);
    // Lecture
    ReadLn(fic, dextcsg[i], dintcsg[i], dexttj[i], wearvol[i]);
    i := i+1;
  until (Eof(fic));
  close(fic);
end;
//------------------------------------------------------------------------------

{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
procedure EtudeTjOD_Section_Hauteur(NomFic: string);
var
  i, j, npi, npj : Integer;
	fic : text;
  dextcsg, dintcsg, wearvol, largeur : Double;
  dexttj, dexttjmin, dexttjmax : Double;
  hauteur, hauteurmin, hauteurmax : Double;

begin

  dextcsg := 374.650;
  dintcsg := 355.600;

  // dexttj : 139.700 --> 190.5
  dexttjmin := 139.700;
  dexttjmax := 190.500;
  hauteurmin := 1;
  hauteurmax := 10;
  // hauteur : 1 --> 6
  npi := 10;
  npj := 5;

  assign(fic, NomFic);
  rewrite(fic);
  writeln(fic,'  dextcsg    dintcsg     dexttj        wearsec          hauteur            largeur ');

  for j := 0 to npj do
  begin
    for i := 0 to npi do
    begin
      dexttj := dexttjmin + i*(dexttjmax-dexttjmin)/npi;
      hauteur := hauteurmin + j*(hauteurmax-hauteurmin)/npj;
      wearvol := CalculSecFromHau(hauteur, dintcsg/2, dexttj/2);
      largeur := CalculLarFromHau(hauteur, dintcsg/2, dexttj/2);
      writeln(fic,' ', dextcsg:10:6,
                  ' ', dintcsg:10:6,
                  ' ', dexttj:10:6,
                  ' ', wearvol:15:9,
                  ' ', hauteur:18:15,
                  ' ', largeur:18:15);
    end;
  end;
  close(fic);
end;
//------------------------------------------------------------------------------


{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Lecture du fichier de donn�es pour l'�tude de sensibilit� du nouveau mod�le
  de l'usure :
  Param�tres d'entr�es :
     + du casing : dextcsg, dintcsg
     + du TJ : dexttj
     + de la force de contact (r�partie)
     + de la nouvelle loi de l'usure : A, B et WFasym
  A partir de ces param�tres, on reconstruit la courbe Vus�-Travail et on �tudie
  l'influence de ces param�tres sur la relation Vus�-Travail
-------------------------------------------------------------------------------}
procedure LectureDataSensibility(NomFic: string;
                                 var dextcsg, dintcsg, dexttj, force : t_vdd;
                                 var A, B, WFasym : t_vdd);
var
  i : Integer;
	fic : text;
begin
  assign(fic, NomFic);
  reset(fic);
  readln(fic);
  i := 0;
  repeat
    setlength(dextcsg, i+1);
    setlength(dintcsg, i+1);
    setlength(dexttj, i+1);
    setlength(force, i+1);
    setlength(A, i+1);
    setlength(B, i+1);
    setlength(WFasym, i+1);
    // Lecture
    ReadLn(fic, dextcsg[i], dintcsg[i], dexttj[i], force[i],
                A[i], B[i], WFasym[i]);
    i := i+1;
  until (Eof(fic));
  close(fic);
end;
//------------------------------------------------------------------------------

{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Lecture du fichier de donn�es pour l'�tude de sensibilit� du nouveau mod�le
  de l'usure :
  Param�tres d'entr�es :
     + du casing : dextcsg, dintcsg
     + du TJ : dexttj
     + de la force de contact (r�partie)
     + de la nouvelle loi de l'usure : A, B et WFasym
  A partir de ces param�tres, on reconstruit la courbe Vus�-Travail et on �tudie
  l'influence de ces param�tres sur la relation Vus�-Travail
-------------------------------------------------------------------------------}
procedure LectureDataEtudeForceVarie(FilePath: string;
                                     var dextcsg, dintcsg, dexttj, A, B, WFasym : Double;
                                     var Work, force : t_vdd);
var
  i : Integer;
	fic : text;
begin
  assign(fic, FilePath);
  reset(fic);
  readln(fic);
  ReadLn(fic, dextcsg, dintcsg, dexttj, A, B, WFasym);
  readln(fic);
  i := 0;
  repeat
    setlength(Work, i+1);
    setlength(force, i+1);
    // Lecture
    ReadLn(fic, Work[i], force[i]);
    i := i+1;
  until (Eof(fic));
  close(fic);
end;
//------------------------------------------------------------------------------


{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  // Reconstruction le tableau Vus�-Work � partir de la relation WFdiff-PC
  // Sauvegarder dans --> LisWork, LisSecUse
-------------------------------------------------------------------------------}
procedure ReconstrucVuseTravailLarPlate(RextCsg, RintCsg, Rtjt : Double;
                                        F : Double;
                                        A, B, WFasymp : Double;
                                        n : Integer; LarLim : Double;
                                        Var LisWork, LisSecUse, LisHauUse, LisLarUse : t_vdd);
var
  PClim : Double;
  LarUse, HauUse, SecUse, Work : Double; //Llimite
  i : Integer;
begin
  setlength(LisWork, n+1);
  setlength(LisSecUse, n+1);
  setlength(LisHauUse, n+1);
  setlength(LisLarUse, n+1);

  PClim := WFasymp/A + B;

  for i := 0 to n do begin
    LarUse := i*Larlim/n;
    SecUse := CalculSecFromLar(LarUse, RintCsg, Rtjt);
    HauUse := CalculHauFromLar(LarUse, RintCsg, Rtjt);
    // Work := CalculWorkFromLargeurPlateModelComplet1(LarUse, A, B, PClim, F, RintCsg, Rtjt);
    Work := CalculWorkFromLargeurModelComplet4aGauss(LarUse, A, B, PClim, F, RintCsg, Rtjt);
    LisWork[i] := Work;
    LisSecUse[i] := SecUse;
    LisHauUse[i] := HauUse;
    LisLarUse[i] := LarUse;
  end;
end;
//------------------------------------------------------------------------------

{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  // Reconstruction le tableau Vus�-Work � partir de la relation WFdiff-PC
  // Sauvegarder dans --> LisWork, LisSecUse
-------------------------------------------------------------------------------}
procedure ReconstrucVuseTravailLarCourbee(RextCsg, RintCsg, Rtjt : Double;
                                          F : Double;
                                          A, B, WFasymp : Double;
                                          n : Integer; LarLim : Double;
                                          Var LisWork, LisSecUse, LisHauUse, LisLarUse : t_vdd);
var
  PClim : Double;
  LarUse, HauUse, SecUse, Work : Double; //Llimite
  i : Integer;
begin
  setlength(LisWork, n+1);
  setlength(LisSecUse, n+1);
  setlength(LisHauUse, n+1);
  setlength(LisLarUse, n+1);

  PClim := WFasymp/A + B;

  for i := 0 to n do begin
    LarUse := i*Larlim/n;
    SecUse := CalculSecFromLar(LarUse, RintCsg, Rtjt);
    HauUse := CalculHauFromLar(LarUse, RintCsg, Rtjt);
    Work := CalculWorkFromLargeurModelComplet4bGauss(LarUse, A, B, PClim, F, RintCsg, Rtjt);
    LisWork[i] := Work;
    LisSecUse[i] := SecUse;
    LisHauUse[i] := HauUse;
    LisLarUse[i] := LarUse;
  end;
end;
//------------------------------------------------------------------------------

{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  // Reconstruction le tableau Vus�-Work en utilisant le WF lin�aire et le
  // correction factor
  // Sauvegarder dans --> LisWork, LisSecUse
-------------------------------------------------------------------------------}
procedure ReconstrucVuseTravail_LinWF (RextCsg, RintCsg, Rtjt : Double;
                                       WFlin : Double;
                                       LisWear, LisCorFactor : t_vdd;
                                       n : Integer; LarLim : Double;
                                       Var LisWork, LisSecUse, LisHauUse, LisLarUse : t_vdd);
var
  Thickness : Double;
  WorkMin, WorkMax : Double;
  i : Integer;

  HauUseLin, SecUseLin : Double; //Llimite
  CorFac : Double;
  LarUse, HauUse, SecUse, Work : Double; //Llimite
begin
  setlength(LisWork, n+1);
  setlength(LisSecUse, n+1);
  setlength(LisHauUse, n+1);
  setlength(LisLarUse, n+1);

  Thickness := RextCsg - RintCsg;
  SecUse := CalculSecFromLar (LarLim, RintCsg, Rtjt);
  WorkMax := SecUse/WFlin;
  WorkMin := 0;

  for i := 0 to n do begin
    Work := WorkMin+i*(WorkMax-WorkMin)/n;
    SecUseLin := Work*WFlin;
    HauUseLin := CalculHauFromSec(SecUseLin, RintCsg, Rtjt);
    CorFac := CalNonlinearCorrectionFactor(HauUseLin*100/Thickness, LisWear, LisCorFactor);
    SecUse := CorFac*SecUseLin;
    HauUse := CalculHauFromSec(SecUse, RintCsg, Rtjt);
    LarUse := CalculLarFromHau(HauUse, RintCsg, Rtjt);
    //Work := CalculWorkFromLargeurModelThresholeWithLimit(LarUse, A, B, PClim, F, Rcsg, Rtjt);
    LisWork[i] := Work;
    LisSecUse[i] := SecUse;
    LisHauUse[i] := HauUse;
    LisLarUse[i] := LarUse;
  end;
end;
//------------------------------------------------------------------------------

{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  // Reconstruction le tableau Vus�-Work en utilisant le WF lin�aire et le
  // correction factor
  // Sauvegarder dans --> LisWork, LisSecUse
-------------------------------------------------------------------------------}
procedure ReconstrucVuseTravail_LinWF_NoCorrect (RextCsg, RintCsg, Rtjt : Double;
                                                 WFlin : Double;
                                                 n : Integer; LarLim : Double;
                                                 Var LisWork, LisSecUse, LisHauUse, LisLarUse : t_vdd);
var
  Thickness : Double;
  WorkMin, WorkMax : Double;
  i : Integer;

  HauUseLin, SecUseLin : Double; //Llimite
  CorFac : Double;
  LarUse, HauUse, SecUse, Work : Double; //Llimite
begin
  setlength(LisWork, n+1);
  setlength(LisSecUse, n+1);
  setlength(LisHauUse, n+1);
  setlength(LisLarUse, n+1);

  Thickness := RextCsg - RintCsg;
  SecUse := CalculSecFromLar (LarLim, RintCsg, Rtjt);
  WorkMax := SecUse/WFlin;
  WorkMin := 0;

  for i := 0 to n do begin
    Work := WorkMin+i*(WorkMax-WorkMin)/n;
    SecUseLin := Work*WFlin;
    HauUseLin := CalculHauFromSec(SecUseLin, RintCsg, Rtjt);
    CorFac := 1.0;
    SecUse := CorFac*SecUseLin;
    HauUse := CalculHauFromSec(SecUse, RintCsg, Rtjt);
    LarUse := CalculLarFromHau(HauUse, RintCsg, Rtjt);
    //Work := CalculWorkFromLargeurModelThresholeWithLimit(LarUse, A, B, PClim, F, Rcsg, Rtjt);
    LisWork[i] := Work;
    LisSecUse[i] := SecUse;
    LisHauUse[i] := HauUse;
    LisLarUse[i] := LarUse;
  end;
end;
//------------------------------------------------------------------------------


{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Relation WF diff�rentiel Vs Pression de Contact
         --> Lin�aire avec seuil (Thres : Pressure Threshole)
         dS/dPsi=Pente*(Force/2LarUse-Thres)
         PClim et WFasym : Point � partir duquel PC<PClim --> WF=const=WFasym
-------------------------------------------------------------------------------}
procedure RecontructCurvesNewModelForComparison (RextCsg, RintCsg, Rtjt : Double;
     F : Double; A, B, PClim : Double; WFlin : Double; HauMax : Double;
     nb_intervals : Integer;
     Var AHau, ASec, ALarPlate, ALarCurved, AHauCorrected, ASecCorrected: t_vdd;
     Var AWearPercent, ACorFac, AWearPercentCorrect : t_vdd;
     Var AWorkModLin, AWorkModNonLin : t_vdd);
var
  i : Integer;
  // Work1, Work2, Work3, VolUse : Double;
  // Lar, Hau, Bmax, Amax : Double;


begin
  // Clear
  SetLength(AHau, 0);
  SetLength(ASec, 0);
  SetLength(ALarPlate, 0);
  SetLength(ALarCurved, 0);
  SetLength(AHauCorrected, 0);
  SetLength(ASecCorrected, 0);

  SetLength(AWearPercent, 0);
  SetLength(AWearPercentCorrect, 0);

  SetLength(AWorkModLin, 0);
  SetLength(AWorkModNonLin, 0);

  // Allocate
  SetLength(AHau, nb_intervals+1);
  SetLength(ASec, nb_intervals+1);
  SetLength(ALarPlate, nb_intervals+1);
  SetLength(ALarCurved, nb_intervals+1);
  SetLength(AHauCorrected, nb_intervals+1);
  SetLength(ASecCorrected, nb_intervals+1);

  SetLength(AWearPercent, nb_intervals+1);
  SetLength(ACorFac, nb_intervals+1);
  SetLength(AWearPercentCorrect, nb_intervals+1);

  SetLength(AWorkModLin, nb_intervals+1);
  SetLength(AWorkModNonLin, nb_intervals+1);

  for i  := 0 to nb_intervals do
  begin
    AHau[i]           := i*HauMax/nb_intervals;
    ALarPlate[i]      := CalculLarFromHau(AHau[i], RintCsg, Rtjt);
    ASec[i]           := CalculSecFromLar(ALarPlate[i], RintCsg, Rtjt);
    ALarCurved[i]     := CalculLarConFromLar(ALarPlate[i], RintCsg, Rtjt);

    SimpleResultWithCorFactor(AHau[i], 2*RintCsg, 2*RextCsg,
      AWearPercent[i], ACorFac[i], AHauCorrected[i], AWearPercentCorrect[i]);
    ASecCorrected[i] := CalculSecFromHau(AHauCorrected[i], RintCsg, Rtjt);

    AWorkModNonLin[i] := CalculWorkFromLargeurModelComplet4bGauss(ALarPlate[i], A, B, PClim, F, RintCsg, Rtjt);
    if WFlin > 0 then
      AWorkModLin[i] := ASec[i]/WFlin
    else
      AWorkModLin[i] := -999;

    if ((AWorkModNonLin[i] < -1E-9) or (AWorkModNonLin[i]>1E50)) then
      break
  end;
end;

Procedure WriteCurvesNewModelForComparison(FilePath : String;
     AHau, ASec, ALarPlate, ALarCurved, AHauCorrected, ASecCorrected: t_vdd;
     AWearPercent, ACorFac, AWearPercentCorrect : t_vdd;
     AWorkModLin, AWorkModNonLin : t_vdd);
var
  i : Integer;
  FilMou : Textfile;
begin
    AssignFile(FilMou,FilePath);
    Rewrite(FilMou);
    Writeln(FilMou, '     WearHeight(m)       WearSection(m2)     PlateWearWidth(m)   CurvedWearWidth(m)',
                    '    WearPercent(%)     CorrectionFactor(-)  HeightCorrected(m)   SectionCorrected(m2)',
                    ' WearPercentCorrect(%) WorkModLinear(N.m/m) WorkModNonLinear(N.m/m) ');
    for i := 0 to Length(AHau)-1 do
    begin
      Writeln(FilMou, ' ', AHau[i]:20,
                      ' ', ASec[i]:20,
                      ' ', 2*ALarPlate[i]:20,
                      ' ', 2*ALarCurved[i]:20,
                      ' ', AWearPercent[i]*100:20,
                      ' ', ACorFac[i]:20,
                      ' ', AHauCorrected[i]:20,
                      ' ', ASecCorrected[i]:20,
                      ' ', AWearPercentCorrect[i]*100:20,
                      ' ', AWorkModLin[i]:20,
                      ' ', AWorkModNonLin[i]:20
                      );
    end;
    CloseFile(FilMou);
end;

{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
procedure Test_WearModelNoThreshole(Rcsg, Rtjt : Double;
                                    WF8h, VolUse8h : Double;
                                    NomFic: string);
var
  i : Integer;
  // Work8h : Double;
  WorkIni, WorkFin, Work, VolUse, WorkInverse : Double;
  nb_points : Integer;
  CoefWF : Double;
  fic : text;
begin
  WorkIni := 0;
  WorkFin := VolUse8h/WF8h;
  nb_points := 1000;

  // No Contact Pressure Threshole
  CoefWF := LinearDifferentialWearFactor(WF8h, VolUse8h, Rcsg, Rtjt);

  Assign(fic, NomFic);
  Rewrite(fic);
  Writeln(fic, '  Rcsing(m)  Rtjt(m)  WF8h(m3/N-m)  VolUse8h(m3/m)');
  Writeln(fic, Rcsg:10:6,' ', Rtjt:10:6, ' ', WF8h:15, ' ', VolUse8h:15);
  Writeln(fic, '  Work(N-m/m) VolUse(m3/m) WorkInverse(N-m/m) ');

  for i := 0 to nb_points do begin
    Work := WorkIni+ (i/nb_points)*(WorkFin-WorkIni);
    VolUse := CalculSectionFromWork(Work, CoefWF, Rcsg, Rtjt);
    WorkInverse := CalculWorkFromSection(VolUse, CoefWF, Rcsg, Rtjt);
    if (Work < 1E50) then Writeln(fic, Work:20,' ', VolUse:20,' ', WorkInverse:20);
  end;
  CloseFile(fic);


end;
//----------------------------------------------------------------------------//


{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
procedure Test_WearModelThreshole_SI(Rcsg, Rtjt : Double;
                                     F : Double;
                                     A, B : Double;
                                     NomFic: string);
var
  i : Integer;
  nb_points : Integer;
  Lar, Hau, HauMax, LarMax : Double;
  Work, VolUse : Double;
  fic : TextFile;
begin
  HauMax := 0.02*0.0254; // m
  nb_points := 1000;

  AssignFile(fic, NomFic);
  Rewrite(fic);
  Writeln(fic, '  Rcsing(m)  Rtjt(m)   A-Pente(1/Pa^2)  B-Thres(Pa)   Force(N/m) ');
  Writeln(fic, Rcsg:10:6,' ', Rtjt:10:6,' ', A:15,' ', B:15,' ', F:15);
  Writeln(fic, '  Work(N-m/m) VolUse(m3/m) HauUse(m) LarUse(m)');

  LarMax := CalculLarFromHau(HauMax, Rcsg, Rtjt);
  for i  := 0 to nb_points do begin
    Lar := i*LarMax/nb_points;  // m
    Hau := CalculHauFromLar(Lar, Rcsg, Rtjt);
    Work := CalculWorkFromLargeurModelThreshole(Lar, A, B, F, Rcsg, Rtjt);
    VolUse := CalculSecFromLar(Lar, Rcsg, Rtjt);
    if (Work < 1E50) then Writeln(fic, ' ', Work:20,' ', VolUse:20,' ', Hau:20,' ', Lar:20,' ');
  end;
  Close(fic);
end;
//----------------------------------------------------------------------------//


{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
procedure Test_WearModelThreshole_US(Rcsg, Rtjt : Double;
                                     F : Double;
                                     A, B : Double;
                                     NomFic: string);
var
  i : Integer;
  nb_points : Integer;
  Lar, Hau, HauMax, LarMax : Double;
  Work, VolUse : Double;
  fic : text;
begin
  HauMax := 0.083;      // in
  nb_points := 1000;

  Assign(fic, NomFic);
  Rewrite(fic);
  Writeln(fic, '  Rcsing(in)  Rtjt(in)   A-Pente(1/Psi^2)  B-Thres(Psi)   Force(lb/in) ');
  Writeln(fic, Rcsg:10:6,' ', Rtjt:10:6,' ', A:15,' ', B:15,' ', F:15);
  Writeln(fic, '  Work(lb-in/in) VolUse(in3/in) HauUse(in) LarUse(in)');

  LarMax := CalculLarFromHau(HauMax, Rcsg, Rtjt);
  for i  := 0 to nb_points do begin
    Lar := i*LarMax/nb_points;  // m
    Hau := CalculHauFromLar(Lar, Rcsg, Rtjt);
    Work := CalculWorkFromLargeurModelThreshole(Lar, A, B, F, Rcsg, Rtjt);
    VolUse := CalculSecFromLar(Lar, Rcsg, Rtjt);
    if (Work < 1E50) then Writeln(fic, ' ', Work:20,' ', VolUse:20,' ', Hau:20,' ', Lar:20,' ');
  end;
  Close(fic);
end;
//----------------------------------------------------------------------------//


{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
procedure Test_WearModelThreshole (Rcsg, Rtjt : Double;
                                   F : Double;
                                   A, B : Double;
                                   NomFic: string);
var
  i : Integer;
  nb_points : Integer;
  Work, VolUse : Double;
  Lar, Hau, LarMax : Double;
  fic : text;
begin
  nb_points := 1000;

  Assign(fic, NomFic);
  Rewrite(fic);
  Writeln(fic, '  RintCsg(m_in)   Rtjt(m_in)   A-Pente(1/Pa^2_1/psi^2)  B-Thres(Pa_psi)     Force(N/m_lbf/in)');
  Writeln(fic, Rcsg:14:9,' ', Rtjt:14:9,' ', A:20,' ', B:20,' ',  F:15);
  Writeln(fic, '  Work(N-m/m_lb-in/in)  VolUse(m3/m_in3/in)     HauUse(m_in)          LarUse(m_in) ');

  LarMax := F/(2*B);
  if LarMax>=Rtjt-1E-10 then LarMax := Rtjt-1E-10;

  for i  := 0 to nb_points do begin
    Lar := i*LarMax/nb_points;  // m
    Hau := CalculHauFromLar(Lar, Rcsg, Rtjt);
    Work := CalculWorkFromLargeurModelThreshole(Lar, A, B, F, Rcsg, Rtjt);
    VolUse := CalculSecFromLar(Lar, Rcsg, Rtjt);
    if (Work < 1E50) then Writeln(fic, ' ', Work:20,' ', VolUse:20,' ', Hau:20,' ', Lar:20,' ');
  end;
  Close(fic);
end;
//------------------------------------------------------------------------------



{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
     --> Non-Lin�aire sans seuil :  y = [Ax^2 + (Ak - AB)x]/(x+k)
                                    WFdif = y ; PC = x = Force/2LarUse ;
                                    -> Asymptote oblique y = A(x-B)
-------------------------------------------------------------------------------}
procedure Test_WearModelSansSeuilAsymptote (Rcsg, Rtjt : Double;
                                            F : Double;
                                            A, B, CoefK : Double;
                                            NomFic: string);
var
  i : Integer;
  nb_points : Integer;
  Work, VolUse : Double;
  Lar, Hau, LarMax : Double;
  fic : text;
begin
  nb_points := 1000;

  Assign(fic, NomFic);
  Rewrite(fic);
  Writeln(fic, '  Rcsing(m)  Rtjt(m)  A-Pente(1/Pa^2)  B-Thres(Pa)   CoefK(Pa) Force(N/m) ');
  Writeln(fic, Rcsg:10:6,' ', Rtjt:10:6,' ', A:15,' ', B:15,' ', CoefK:15,' ', F:15);
  Writeln(fic, '  Work(N-m/m) VolUse(m3/m) HauUse(m) LarUse(m)');

  // HauMax := 0.472*0.0254; // Epaisseur du casing
  // LarMax :=  CalculLarFromHau(HauMax, Rcsg, Rtjt);
  LarMax := Rtjt-1E-6;
  for i  := 0 to nb_points do begin
    Lar := i*LarMax/nb_points;  // m
    Hau := CalculHauFromLar(Lar, Rcsg, Rtjt);
    Work := CalculWorkFromLargeurModelNonLinearWFdif_PC(Lar, A, B, CoefK, F, Rcsg, Rtjt);
    VolUse := CalculSecFromLar(Lar, Rcsg, Rtjt);
    if (Work < 1E50) then Writeln(fic, ' ', Work:20,' ', VolUse:20,' ', Hau:20,' ', Lar:20,' ');
  end;
  Close(fic);
end;
//----------------------------------------------------------------------------//



{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Relation WF diff�rentiel Vs Pression de Contact
         --> Lin�aire avec seuil (Thres : Pressure Threshole)
         dS/dPsi=Pente*(Force/2LarUse-Thres)
         PClim et WFasym : Point � partir duquel PC<PClim --> WF=const=WFasym
-------------------------------------------------------------------------------}
procedure Test_WearModelComplet (SysUnits : string;
                                 RextCsg, RintCsg, Rtjt : Double;
                                 F : Double;
                                 A, B, PClim : Double;
                                 WFlin : Double;
                                 nb_points : Integer;
                                 NomFic: string);
var
  i : Integer;
  Work1, Work2, Work3, VolUse : Double;
  Lar, Hau, LarMax : Double;
  fic : text;
begin
  Assign(fic, NomFic);
  Rewrite(fic);
  if SysUnits = 'SI' then
  begin
    // Writeln(fic, '  RextCsg(m) RintCsg(m)   Rtjt(m)  Force(N/m)  A-Pente(1/Pa^2)  B-Thres(Pa)    WFAsym(1/Pa)     PClim(Pa)   WFlin(1/Pa)');
    // Writeln(fic, RextCsg:10:6,' ', RintCsg:10:6,' ', Rtjt:10:6,' ', F:15, ' ', A:15,' ', B:15,' ', A*(PClim-B):15,' ',PClim:15, ' ' ,WFlin:15);
    Writeln(fic, '  WorkLarPlate(N.m/m)    WorkLarCourbee(N-m/m)   WorkWFLin(N-m/m)      VolUse(m3/m)    HauUse(m)     LarUse(m)');
  end else
  if SysUnits = 'US' then
  begin
    // Writeln(fic, '  RextCsg(in) RintCsg(in)   Rtjt(in)  Force(lbf/in)  A-Pente(1/Psi^2)  B-Thres(Psi)    WFAsym(1/Psi)     PClim(Psi)   WFlin(1/Psi)');
    // Writeln(fic, RextCsg:10:6,' ', RintCsg:10:6,' ', Rtjt:10:6,' ', F:15, ' ', A:15,' ', B:15,' ', A*(PClim-B):15,' ',PClim:15, ' ' ,WFlin:15);
    Writeln(fic, ' WorkLarPlate(lbf.in/in)   WorkLarCourbee(lbf.in/in) WorkWFLin(lbf.in/in)    VolUse(in3/in)  HauUse(in)   LarUse(in)');
  end;

  Hau := 0.5*(RextCsg-RintCsg);
  LarMax := Min(CalculLarFromHau(Hau, RintCsg, Rtjt),Rtjt);

  for i  := 0 to nb_points do
  begin
    write(' ', i);
    Lar := i*LarMax/nb_points;  // m
    Hau := CalculHauFromLar(Lar, RintCsg, Rtjt);
    VolUse := CalculSecFromLar(Lar, RintCsg, Rtjt);

    Work1 := CalculWorkFromLargeurModelComplet4aGauss(Lar, A, B, PClim, F, RintCsg, Rtjt);
    Work2 := CalculWorkFromLargeurModelComplet4bGauss(Lar, A, B, PClim, F, RintCsg, Rtjt);
    if WFlin > 0 then
      Work3 := VolUse/WFlin
    else
      Work3 := -999;


    if ((Work1<-1E-9) or (Work1>1E50) or (Work2<-1E-9) or (Work2>1E50)) then break
    else Writeln(fic,' ', Work1:20,
                     ' ', Work2:20,
                     ' ', Work3:20,
                     ' ', VolUse:20,
                     ' ', Hau:20,
                     ' ', Lar:20,' ');
  end;
  // writeln();
  Writeln;
  Close(fic);
end;
//----------------------------------------------------------------------------//




{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  // Trouver AF pour que la courbe doit passer par WF8h
  // SI Units ** WF diff = A*(ContactPressure-B)
-------------------------------------------------------------------------------}
procedure Test_WearModel_PassantPoint8h (Rcsg, Rtjt : Double;
                                         F : Double;
                                         Hau8h, WF8h : Double;
                                         PathFic: string);
var
  i, k : Integer;
  nb_points, nbCas : Integer;
  Work, VolUse : Double;
  Work8h, SecUse8h, Lar8h : Double;
  A, B : Double;
  Lar, Hau, LarMax, Bmax : Double;
  fic, fic1: text;
begin

  //**************************************************************************//
  // Rcsg := 8.681*0.0254/2;
  // Rtjt := 6.25*0.0254/2;
  // F := 43781.70866;       // N/m
  // Hau8h := 0.02*0.0254; // m
  // WF8h := 1.34885E-14;

  nb_points := 1000;
  nbCas := 14;

  SecUse8h := CalculSecFromHau(Hau8h, Rcsg, Rtjt);
  Work8h := SecUse8h/WF8h;
  Lar8h := CalculLarFromHau(Hau8h, Rcsg, Rtjt);

  Bmax := F/(2*Lar8h);
  Assign(fic1,PathFic+'/Ha_WearModThres_PassantPoint8h_Data.txt');
  Rewrite(fic1);
  Writeln(fic1, '  Rcsing(m)  Rtjt(m)  A-Pente(1/Pa^2)  B-Thres(Pa)   Force(N/m) WF8h Work8h SecUse8h Hau8h');
  for k := 1 to nbCas do
  begin
    B := Bmax*k/(nbCas+1);
    A := CalculPenteModelThreshole(Work8h, SecUse8h, B, F, Rcsg, Rtjt);
    if A>0 then begin
      Writeln(fic1, Rcsg:10:6,' ', Rtjt:10:6,' ', A:15,' ', B:15,' ', F:15,' ', WF8h:15,' ', Work8h:15,' ', SecUse8h:15,' ', Hau8h:15);
      //----------------------------------------------------------------------//
      AssignFile(fic,PathFic+'/Ha_WearModThres_PassantPoint8h_'+IntToStr(k)+'.txt');
      Rewrite(fic);
      Writeln(fic, '  Rcsing(m)  Rtjt(m)  A-Pente(1/Pa^2)  B-Thres(Pa)   Force(N/m) WF8h Work8h SecUse8h Hau8h');
      Writeln(fic, Rcsg:10:6,' ', Rtjt:10:6,' ', A:15,' ', B:15,' ', F:15,' ', WF8h:15,' ', Work8h:15,' ', SecUse8h:15,' ', Hau8h:15);
      Writeln(fic, '  Work(N-m/m) VolUse(m3/m) HauUse(m) LarUse(m)');
      LarMax := F/(2*B);
      if LarMax>=Rtjt-1E-10 then LarMax := Rtjt-1E-10;
      Lar8h := CalculLarFromHau(Hau8h, Rcsg, Rtjt);
      for i  := 0 to nb_points do begin
        Lar := i*LarMax/nb_points;  // m
        Hau := CalculHauFromLar(Lar, Rcsg, Rtjt);
        Work := CalculWorkFromLargeurModelThreshole(Lar, A, B, F, Rcsg, Rtjt);
        VolUse := CalculSecFromLar(Lar, Rcsg, Rtjt);
        if (Work < 1E50) then Writeln(fic, ' ', Work:20,' ', VolUse:20,' ', Hau:20,' ', Lar:20,' ');
      end;
      Close(fic);
      //----------------------------------------------------------------------//
    end;
  end;
  Close(fic1);
  //**************************************************************************//
end;
//----------------------------------------------------------------------------//


{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Calculer LOG Vuse-Work
  Test du calcul du volume us� � partir du LOG
-------------------------------------------------------------------------------}
procedure Test_NewestWearModel_VolParLog(Rcsg, Rtjt : Double;
                                         F : Double;
                                         A, B, PClim : Double;
                                         NomFic: string);
var
  i : Integer;
  nb_points : Integer;
  Work, VolUse : Double;

  fic : text;

  ListWork, ListSecUse : t_vdd;
begin


  CalLogSecUse_Work_ModThresWithLimit(ListSecUse, ListWork, A, B, PClim, F, Rcsg, Rtjt);

  Assign(fic, NomFic);
  Rewrite(fic);
  Writeln(fic, '  Rcsing(m)  Rtjt(m)  A-Pente(1/Pa^2)  B-Thres(Pa)   PClim(Pa) Force(N/m)');
  Writeln(fic, Rcsg:10:6,' ', Rtjt:10:6,' ', A:15,' ', B:15,' ', PClim:15,' ', F:15);
  Writeln(fic, '  Work(N-m/m) VolUse(m3/m)');

  nb_points := length(ListWork);
  for i  := 0 to nb_points-1 do
  Writeln(fic, ' ', ListWork[i]:20,' ', ListSecUse[i]:20);

  Writeln(fic);
  for i  := 0 to 12 do begin
    Work := i*400000000.0/10.0 ;
    VolUse := CalValeurFromLog(Work, ListWork, ListSecUse);
    Writeln(fic, ' ', Work:20,' ', VolUse:20);
  end;
  Close(fic);

  // Lib�rer la m�moire
  setlength(ListSecUse,0);
  setlength(ListWork,0);
  //**************************************************************************//
end;
//----------------------------------------------------------------------------//


end.
