{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
unit UnitModeleUsureCasingHa;

interface
uses
  SysUtils, Classes, Math,
  UnitModeleStructureBasic,
  UnitModeleCalcul,
  UnitsMathTools;

type
  // Pour le mod�le non-lin�aire de l'usure
  TCasingWearTest = record
    CasMat : ShortInt;
    TjMat : ShortInt;
    DrillFluid : ShortInt;
    TjOD : Real;
    CasID : Real;
    LateralLoad : Real;
    RPM : Real;             // Vitesse de rotation du TJ
    WF8h : Real;            // Conventional Wear Factor at 8h
    ContPresThres : Real;   // Contact Pressure Threshold
  end;
  PCasingWearTest = ^TCasingWearTest;

  // Mod�le simple de casing wear (sans orientation de la force de contact)
  TCalCasingWearResultSimple = record
    Id : Integer;
    CalCasingWearStepId : Integer;
    Md : Real;
    SecUse : Real;
    HauUse : Real;
    SecUseLinear : Real;
    HauUseLinear : Real;
    Work : Real;
    Imail : Integer;
  end;
  PCalCasingWearResultSimple = ^TCalCasingWearResultSimple;

  TEffetCasingWearIncrement = record
    CalCasingWearId : Integer;
    CasingWearStepId : Integer;
    Dtj : Real;
    Ltj : Double;
    Fx : Double;   // Normal Force
    Fy : Double;
    Fz : Double;
    Work : Double;
    Duree : Double;
  end;
  PEffetCasingWearIncrement = ^TEffetCasingWearIncrement;

  // Proc�dures et Fonctions g�n�rales
  function CalculHauFromSec(Section, Rcg, Rtj : Double) : Double;
  function CalculHauFromLar(Largeur, Rcg, Rtj : Double) : Double;
  function CalculLarFromSec(Section, Rcg, Rtj : Double) : Double;
  function CalculLarFromSecMetTangente(Section, Rcg, Rtj : Double) : Double;
  function CalculLarFromHau(Hauteur, Rcg, Rtj : Double) : Double;
  function CalculSecFromHau(Hauteur, Rcg, Rtj : Double) : Double;
  function CalculSecFromLar(Largeur, Rcg, Rtj : Double) : Double;

  // LarCon : Largeur Courb�e de Contact - Curved Contact Length
  function CalculLarConFromLar(Largeur, Rcg, Rtj : Double) : Double;
  function CalculLarConFromHau(Hauteur, Rcg, Rtj : Double) : Double;
  function CalculLarConFromSec(Section, Rcg, Rtj : Double) : Double;
  //
  function LinearDifferentialWearFactor(WF, SecUse8h, Rcg, Rtj : Double) : Double;
  function CalculWorkFromSection(SecUse, WFdif, Rcg, Rtj : Double) : Double;
  function CalculWorkFromLargeur(LarUse, WFdif, Rcg, Rtj : Double) : Double;
  //
  function CalculWorkFromLargeurModelThreshole(LarUse, Pente, Thres, Force, Rcg, Rtj : Double) : Double;
  function CalculWorkFromLargeurModelComplet1(LarUse, Pente, Thres, PCLim, Force, Rcg, Rtj : Double) : Double;
  function CalculWorkFromLargeurModelComplet2(LarUse, Pente, Thres, PCLim, Force, Rcg, Rtj : Double) : Double;
  function CalculWorkFromLargeurModelComplet3Gauss(LarUse, Pente, Thres, PCLim, Force, Rcg, Rtj : Double) : Double;
  function CalculWorkFromLargeurModelComplet4aGauss(LarUse, Pente, Thres, PCLim, Force, Rcg, Rtj : Double) : Double;
  function CalculWorkFromLargeurModelComplet4bGauss(LarUse, Pente, Thres, PCLim, Force, Rcg, Rtj : Double) : Double;
  //
  function CalculWorkFromSectionModelComplet3aGauss(SecUse, Pente, Thres, PCLim, Force, Rcg, Rtj : Double) : Double;
  function CalculWorkFromSectionModelComplet3bGauss(SecUse, Pente, Thres, PCLim, Force, Rcg, Rtj : Double) : Double;
  function CalculDeltaWorkModelCompletGaussLarPlate(SecDeb, SecFin, Pente, Thres, PCLim, Force, Rcg, Rtj : Double) : Double;
  function CalculDeltaWorkModelCompletGaussLarCourbee(SecDeb, SecFin, Pente, Thres, PCLim, Force, Rcg, Rtj : Double) : Double;
  function CalculDeltaSectionModelComplet(SecDeb, DelWork, Pente, Thres, PCLim, Force, Rcg, Rtj : Double) : Double;
  function CalculDeltaSectionModelCompletTangentMethod(SecDeb, DelWork, Pente, Thres, PCLim, Force, Rcg, Rtj : Double) : Double;
  //
  function CalculWorkFromLargeurModelNonLinearWFdif_PC(LarUse, Pente, Thres, CoefK, Force, Rcg, Rtj : Double) : Double;
  procedure CalLogSecUse_Work_ModThresWithLimit(Var LisSecUse, LisWork : t_vdd; Pente, Thres, PCLimit, Force, Rcg, Rtj : Double);
  function CalculPenteModelThreshole(Work8h, SecUse8h, Thres, Force, Rcg, Rtj : Double) : Double;
  function CalculSectionFromWork(Work, CoeWFdif, Rcg, Rtj : Double) : Double;

  // Autre m�thode de calcul Volume us� - Work
  // En supposant qu'on connait la relation WFdiff-PC (A-Pente, B-PCLimi, C-WFLimit)
  function CalDiffWearFactor(Pente, PClim, WFlim, Largeur, Force: Double) : Double;

  // New Casing Wear Model Adjustment
  function Cal_PClim_WithDicho_SI(Work8h, SecUse8h : Double;
                                  A, WFlim : Double;
                                  Force, Rcg, Rtj : Double) : Double;

  procedure LectureNonlinearCorrecFactor(NomFic: string; var LisWear, LisCorFac : t_vdd);
  function CalNonlinearCorrectionFactor(WearPercent : Double; LisWear, LisCorFactor : t_vdd) : Double;
  function CalNonLinearityCorFactor(WearPercent : Double) : Double;
  Procedure SimpleResultWithCorFactor(HauMax, DintCsg, DextCsg : Double;
     var WearPercent, CorFac, HauMaxCor, WearPercentCor : Double);

  function CalValeurFromLog(ValA : Double; LisA, LisB : t_vdd) : Double;

  procedure LectureGaussFormula(NomFic: string; var x, Weight : t_vdd);
  procedure GaussFormula96Points(var x, Weight : t_vdd);
  procedure GaussFormula20Points(var x, Weight : t_vdd);

  // Treat Test Results
  procedure RegressionWorkWear(Work, WearVol : t_vdd; n : integer;
                             var Y_Der_Moy : t_vdd);
  procedure CalLarHauPCFromWearVol_PlateContactLength(WearVol : t_vdd;
                                 dintcsg, dexttj, force : Double;
                                 var Lar, Hau, PC : t_vdd);
  procedure CalLarHauPCFromWearVol_CurvedContactLength(WearVol : t_vdd;
                                 dintcsg, dexttj, force : Double;
                                 var Lar, Hau, PC : t_vdd);
  procedure CalLarHauFromWearVol(WearVol : t_vdd;
                                 dintcsg, dexttj : Double;
                                 var Lar, Hau : t_vdd);


  // For Casing Wear Increment Calculation
  Procedure WriteTestSection(FilePath : String;
                             SecRes, LarRes : Double);
  procedure CalculSection(DiaCsg, DiaTj, Dep : Double;
                        Tfo : Double;
                        P : Integer;
                        HauUsuIni : t_vdd;
                        p_dHauUsu : t_vdd_p;
                        var SecRes, LarRes : Double);
  procedure CalCulIncrementUsure(WearModel : Integer;
                                 WearFactor, DsnModA, DsnModB, DsnModC : Double;
                                 DiaCsg : Double;
                                 DiaTj, LonTj : Double;
                                 NormalForce, Tfo, WorkTotal : Double;
                                 P : Integer;
                                 p_WearHau : t_vdd_p;
                                 var dSecUse, LarUse : Double);

implementation

//----------------------------------------------------------------------------//

function CalculHauFromSec(Section, Rcg, Rtj : Double) : Double;
var
  h1, h2, h, s : Double;
  function CalSec(Rc, Rt, h : Double) : Double;
  var
    Bc, Bt, Lg : Double;
    Sc, St : Double;
  begin
    Lg := Sqrt(Sqr(Rc)-Sqr((Sqr(Rc)-Sqr(Rt)+Sqr(Rc-Rt+h))/(2*(Rc-Rt+h))));
    Bc := arcsin(Lg/Rc);
    Bt := arcsin(Lg/Rt);
    Sc := Bc*Sqr(Rc)-(Lg*Sqrt(Sqr(Rc)-Sqr(Lg)));
    St := Bt*Sqr(Rt)-(Lg*Sqrt(Sqr(Rt)-Sqr(Lg)));
    CalSec := St-Sc;
  end;
begin
  if (Section>1E-12) then begin
    h1 := 0;
    h2 := Rtj/2;
    while (h2-h1>=1e-12) do begin
      h := (h1+h2)/2;
      s := CalSec(Rcg,Rtj,h);
      if (s<Section)
      then h1 := h
      else h2 := h;
    end;
    h := (h1+h2)/2;
  end else h := 0.0;
  result := h;
end;
//----------------------------------------------------------------------------//

{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Calculer la hauteur H � partir de la largeur us�e L
-------------------------------------------------------------------------------}
function CalculHauFromLar(Largeur, Rcg, Rtj : Double) : Double;
var
  Hau : Double;
begin
  Hau := Rtj - Rcg + sqrt(Sqr(Rcg)-Sqr(Largeur))-sqrt(Sqr(Rtj)-Sqr(Largeur));
  Result := Hau;
end;
//----------------------------------------------------------------------------//



{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Calculer la largeur L � partir de la section us�e S
-------------------------------------------------------------------------------}
function CalculLarFromSec(Section, Rcg, Rtj : Double) : Double;
var
  hauteur : Double;
begin
  hauteur := CalculHauFromSec(Section, Rcg, Rtj);
  if hauteur<1E-20 then Result := 0.0
  else Result := Sqrt(Sqr(Rcg)-Sqr((Sqr(Rcg)-Sqr(Rtj)+Sqr(Rcg-Rtj+Hauteur))/(2*(Rcg-Rtj+Hauteur))));
end;
//----------------------------------------------------------------------------//


{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Calculer la largeur L � partir de la section us�e S
  M�thode Tangente - Newton-Raphson
-------------------------------------------------------------------------------}
function CalculLarFromSecMetTangente(Section, Rcg, Rtj : Double) : Double;
var
  x, x0, y, ytarget, A, B : Double;
  i, nbIterLim : Integer;
  //--------------------------------------------------------------------------//
  function f(Lg : Double) : Double;
  var
    Bc, Bt : Double;
    Sc, St : Double;
  begin
    Bc := arcsin(Lg/Rcg);
    Bt := arcsin(Lg/Rtj);
    Sc := Bc*Sqr(Rcg)-(Lg*Sqrt(Sqr(Rcg)-Sqr(Lg)));
    St := Bt*Sqr(Rtj)-(Lg*Sqrt(Sqr(Rtj)-Sqr(Lg)));
    Result := St-Sc;
  end;
  //--------------------------------------------------------------------------//
  function fDerived(Lg : Double) : Double;
  begin
    Result := 2*((sqr(Rtj)/sqrt(sqr(Rtj)-sqr(Lg)))
            - (sqr(Rcg)/sqrt(sqr(Rcg)-sqr(Lg)))
            +  sqrt(sqr(Rcg)-sqr(Lg))
            -  sqrt(sqr(Rtj)-sqr(Lg)));
  end;
  //--------------------------------------------------------------------------//
begin
  if Section<1E-15 then Result := 0.0
  else begin
    nbIterLim := 100;
    i := 0;
    ytarget := Section;
    x := Rtj-1E-12;
    Repeat
      i := i+1;
      x0 := x;
      y := f(x);
      A := fDerived(x);
      B := y - A*x;
      x := (ytarget - B)/A;
      // writeln((x-x0):25);
    Until ((abs(x-x0)<=1e-12) or (i>nbIterLim));
    Result := x;
  end;
end;
//----------------------------------------------------------------------------//


{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Calculer la largeur L � partir de la hauteur us�e Hauteur
-------------------------------------------------------------------------------}
function CalculLarFromHau(Hauteur, Rcg, Rtj : Double) : Double;
var
  Lg : Double;
begin
  Lg := Sqrt(Sqr(Rcg)-Sqr((Sqr(Rcg)-Sqr(Rtj)+Sqr(Rcg-Rtj+Hauteur))/(2*(Rcg-Rtj+Hauteur))));
  result := Lg;
end;
//----------------------------------------------------------------------------//



{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Calculer la section us�e S � partir de la hauteur h
-------------------------------------------------------------------------------}
function CalculSecFromHau(Hauteur, Rcg, Rtj : Double) : Double;
var
  Bc, Bt, Lg : Double;
  Sc, St : Double;
begin
  Lg := Sqrt(Sqr(Rcg)-Sqr((Sqr(Rcg)-Sqr(Rtj)+Sqr(Rcg-Rtj+Hauteur))/(2*(Rcg-Rtj+Hauteur))));
  Bc := arcsin(Lg/Rcg);
  Bt := arcsin(Lg/Rtj);
  Sc := Bc*Sqr(Rcg)-(Lg*Sqrt(Sqr(Rcg)-Sqr(Lg)));
  St := Bt*Sqr(Rtj)-(Lg*Sqrt(Sqr(Rtj)-Sqr(Lg)));
  CalculSecFromHau := Max(St-Sc, 0.0);
end;
//----------------------------------------------------------------------------//

{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Calculer la section us�e S � partir de la largeur L
-------------------------------------------------------------------------------}
function CalculSecFromLar(Largeur, Rcg, Rtj : Double) : Double;
var
  Bc, Bt : Double;
  Sc, St : Double;
begin
  Bc := arcsin(Largeur/Rcg);
  Bt := arcsin(Largeur/Rtj);
  Sc := Bc*Sqr(Rcg)-(Largeur*Sqrt(Sqr(Rcg)-Sqr(Largeur)));
  St := Bt*Sqr(Rtj)-(Largeur*Sqrt(Sqr(Rtj)-Sqr(Largeur)));
  CalculSecFromLar := Max(St-Sc, 0.0);
end;
//----------------------------------------------------------------------------//


{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Calculer la largeur d'arc de contact (LarCon) � partir de la largeur us�e L
  LarCon : Longueur de l'arc de contact entre le TJ et le CS
-------------------------------------------------------------------------------}
function CalculLarConFromLar(Largeur, Rcg, Rtj : Double) : Double;
begin
  Result := arcsin(Largeur/Rtj)*Rtj;
end;
//----------------------------------------------------------------------------//


{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Calculer la largeur d'arc de contact (LarCon) � partir de la hauteur us�e H
  LarCon : Longueur de l'arc de contact entre le TJ et le CS
-------------------------------------------------------------------------------}
function CalculLarConFromHau(Hauteur, Rcg, Rtj : Double) : Double;
var
  Largeur, LarCon : Double;
begin
  Largeur := CalculLarFromHau(Hauteur, Rcg, Rtj);
  LarCon := arcsin(Largeur/Rtj)*Rtj;
  Result := LarCon;
end;
//----------------------------------------------------------------------------//


{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Calculer la largeur d'arc de contact (LarCon) � partir de la section us�e S
  LarCon : Longueur de l'arc de contact entre le TJ et le CS
-------------------------------------------------------------------------------}
function CalculLarConFromSec(Section, Rcg, Rtj : Double) : Double;
var
  Largeur, LarCon : Double;
begin
  Largeur := CalculLarFromSecMetTangente(Section, Rcg, Rtj);
  LarCon := arcsin(Largeur/Rtj)*Rtj;
  Result := LarCon;
end;
//----------------------------------------------------------------------------//


{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  D�terminer le terme A.F dans l'�quation de Wear Factor diff�rentiel
  --> WFdiff = A.F/2L (F/2L : Pression de Contact)
-------------------------------------------------------------------------------}
function LinearDifferentialWearFactor(WF, SecUse8h, Rcg, Rtj : Double) : Double;
var
  Psi8h : Double;
  Lg : Double;
  Terme1, Terme2, Terme3 : Double;
begin
  Psi8h := SecUse8h/WF;
  Lg := CalculLarFromSecMetTangente(SecUse8h, Rcg, Rtj);
  Terme1 := 2*sqr(Rcg)*sqrt(sqr(Rcg)-sqr(Lg))-2*sqr(Rtj)*sqrt(sqr(Rtj)-sqr(Lg));
  Terme2 := (2/3)*power(sqr(Rtj)-sqr(Lg),3/2)-(2/3)*power(sqr(Rcg)-sqr(Lg),3/2);
  Terme3 := (4/3)*power(Rtj,3) -(4/3)*power(Rcg,3);
  Result := 2*(Terme1+Terme2+Terme3)/Psi8h;
end;
//----------------------------------------------------------------------------//

{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Calculer le travail Psi � partir de la section us�e S
-------------------------------------------------------------------------------}
function CalculWorkFromSection(SecUse, WFdif, Rcg, Rtj : Double) : Double;
var
  Lg, Work : Double;
  Terme1, Terme2, Terme3 : Double;
begin
  Lg := CalculLarFromSecMetTangente(SecUse, Rcg, Rtj);
  Terme1 := 2*sqr(Rcg)*sqrt(sqr(Rcg)-sqr(Lg))-2*sqr(Rtj)*sqrt(sqr(Rtj)-sqr(Lg));
  Terme2 := (2/3)*power(sqr(Rtj)-sqr(Lg),3/2)-(2/3)*power(sqr(Rcg)-sqr(Lg),3/2);
  Terme3 := (4/3)*power(Rtj,3) -(4/3)*power(Rcg,3);
  Work := (2/WFdif)*(Terme1+Terme2+Terme3);
  if Work<0 then Work:=0;
  result := Work;
end;
//----------------------------------------------------------------------------//



{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Calculer le travail Psi � partir de la largeur us�e S
-------------------------------------------------------------------------------}
function CalculWorkFromLargeur(LarUse, WFdif, Rcg, Rtj : Double) : Double;
var
  Lg, Work : Double;
  Terme1, Terme2, Terme3 : Double;
begin
  Lg := LarUse;
  Terme1 := 2*sqr(Rcg)*sqrt(sqr(Rcg)-sqr(Lg))-2*sqr(Rtj)*sqrt(sqr(Rtj)-sqr(Lg));
  Terme2 := (2/3)*power(sqr(Rtj)-sqr(Lg),3/2)-(2/3)*power(sqr(Rcg)-sqr(Lg),3/2);
  Terme3 := (4/3)*power(Rtj,3) -(4/3)*power(Rcg,3);
  Work := (2/WFdif)*(Terme1+Terme2+Terme3);
  if Work<0 then Work:=0;
  result := Work;
end;
//----------------------------------------------------------------------------//


{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Calculer le travail Psi � partir de la largeur us�e L
  Relation WF diff�rentiel Vs Pression de Contact
  --> Lin�aire avec seuil (Thres : Pressure Threshole)
  dS/dPsi=Pente*(PC-Thres)

  ATTENTION : Pression de contact PC = Force/(2*LargeurPlate)

  Not Use Now !!! Only for Saving !!!
-------------------------------------------------------------------------------}
function CalculWorkFromLargeurModelThreshole(LarUse, Pente, Thres, Force, Rcg, Rtj : Double) : Double;
var
  Lg, fx1, fx2, Work, Increment, Llimite : Double;
  i, n : Integer;
begin
  // Largeur Limite
  Llimite := Force/(2*Thres);
  if (LarUse > Llimite-1E-6) then Work := 1.0E100
  else begin
  // Int�grale Num�rique *****************************************************//
  Work := 0;
  n := 1000;
  Increment := LarUse/n;
  Lg := 0;
  fx2 := ((sqr(Rtj)/sqrt(sqr(Rtj)-sqr(Lg)))
        - (sqr(Rcg)/sqrt(sqr(Rcg)-sqr(Lg)))
        +  sqrt(sqr(Rcg)-sqr(Lg))
        -  sqrt(sqr(Rtj)-sqr(Lg))) * (4*Lg/(Pente*Force-Pente*Thres*2*Lg));
  for i := 1 to n do begin
    fx1 := fx2;
    Lg := i*Increment;
    fx2 :=  ((sqr(Rtj)/sqrt(sqr(Rtj)-sqr(Lg)))
           - (sqr(Rcg)/sqrt(sqr(Rcg)-sqr(Lg)))
           +  sqrt(sqr(Rcg)-sqr(Lg))
           -  sqrt(sqr(Rtj)-sqr(Lg))) * (4*Lg/(Pente*Force-Pente*Thres*2*Lg));
    Work := Work + (fx1+fx2)*Increment/2;
  end;
  //************************************************ Int�grale Num�rique *****//
  end;
  Result := Work;
end;
//----------------------------------------------------------------------------//


{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Calculer le travail Psi � partir de la largeur us�e L
  Relation WF diff�rentiel Vs Pression de Contact
         --> Lin�aire avec seuil (Thres : Pressure Threshole)
         dS/dPsi=Pente*(PC-Thres)
         PClim et WFasym : Point � partir duquel PC<PClim --> WF=const=WFasym

  ATTENTION : Pression de contact PC = Force/(2*LargeurPlate)

  Not Use Now !!! Only for Saving !!!
-------------------------------------------------------------------------------}
function CalculWorkFromLargeurModelComplet1(LarUse, Pente, Thres, PCLim, Force, Rcg, Rtj : Double) : Double;
var
  Lg, fx1, fx2, Work, Increment, Llimite : Double;
  i, n : Integer;
begin
  // Largeur Limite
  Llimite := Force/(2*PCLim);
  // Int�grale Num�rique *****************************************************//
  Work := 0;
  n := 2000;
  Increment := LarUse/n;
  Lg := 0;
  fx2 := ((sqr(Rtj)/sqrt(sqr(Rtj)-sqr(Lg)))
        - (sqr(Rcg)/sqrt(sqr(Rcg)-sqr(Lg)))
        +  sqrt(sqr(Rcg)-sqr(Lg))
        -  sqrt(sqr(Rtj)-sqr(Lg))) * (4*Lg/(Pente*(Force-Thres*2*Lg)));
  for i := 1 to n do begin
    fx1 := fx2;
    Lg := i*Increment;
    if Lg<=Llimite then
    fx2 := ((sqr(Rtj)/sqrt(sqr(Rtj)-sqr(Lg)))
          - (sqr(Rcg)/sqrt(sqr(Rcg)-sqr(Lg)))
          +  sqrt(sqr(Rcg)-sqr(Lg))
          -  sqrt(sqr(Rtj)-sqr(Lg))) * (4*Lg/(Pente*(Force-Thres*2*Lg)))
    else
    fx2 := ((sqr(Rtj)/sqrt(sqr(Rtj)-sqr(Lg)))
          - (sqr(Rcg)/sqrt(sqr(Rcg)-sqr(Lg)))
          +  sqrt(sqr(Rcg)-sqr(Lg))
          -  sqrt(sqr(Rtj)-sqr(Lg))) * (4*Llimite/(Pente*(Force-Thres*2*Llimite)));
    Work := Work + (fx1+fx2)*Increment/2;
  end;
  //************************************************ Int�grale Num�rique *****//
  if Work<0.0 then Work:=0.0;
  Result := Work;
end;
//----------------------------------------------------------------------------//



{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Calculer le travail Psi � partir de la largeur us�e L
  Relation WF diff�rentiel Vs Pression de Contact
         --> Lin�aire avec seuil (Thres : Pressure Threshole)
         dS/dPsi=Pente*(PC-Thres)
         PClim et WFasym : Point � partir duquel PC<PClim --> WF=const=WFasym

  M�thode :
    * calcul par l'incr�ment de l'usure
    * hypoth�se : pendant un incr�ment de l'usure, le WF est constant.

  ATTENTION : Pression de contact PC = Force/(2*LargeurPlate)

  Not Use Now !!! Only for Saving !!!
-------------------------------------------------------------------------------}
function CalculWorkFromLargeurModelComplet2(LarUse, Pente, Thres, PCLim, Force, Rcg, Rtj : Double) : Double;
var
  WFlim : Double;
  HauUse, Hau, WFdif : Double;
  IncrementHau, SecDeb, SecFin : Double;
  Lar, Work : Double;
  i, n : Integer;
begin
  WFlim := (PCLim-Thres)*Pente;
  // Int�grale Num�rique *****************************************************//
  Work := 0;
  n := 2000;
  //
  HauUse := CalculHauFromLar(LarUse, Rcg, Rtj);
  IncrementHau := HauUse/n;
  SecDeb := 0;
  for i := 1 to n do begin
    Hau := i*IncrementHau;
    Lar := CalculLarFromHau(Hau, Rcg, Rtj);
    WFdif := CalDiffWearFactor(Pente, PCLim, WFlim, Lar, Force);
    SecFin := CalculSecFromHau(Hau, Rcg, Rtj);
    Work := Work + (SecFin-SecDeb)/WFdif;
    SecDeb := SecFin;
  end;
  if Work<0.0 then Work:=0.0;
  Result := Work;
end;
//----------------------------------------------------------------------------//


{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Calculer le travail Psi � partir de la largeur us�e L
  Relation WF diff�rentiel Vs Pression de Contact
         --> Lin�aire avec seuil (Thres : Pressure Threshole)
         dS/dPsi=Pente*(PC-Thres)
         PClim et WFasym : Point � partir duquel PC<PClim --> WF=const=WFasym

  M�thode :
    * calcul l'int�grale par la formule de Gauss
      Variable : Section us�e (y = S)
      (voir page 887 � Handbook of Mathematical Functions)

  ATTENTION : Pression de contact PC = Force/(2*LargeurPlate)
-------------------------------------------------------------------------------}
function CalculWorkFromLargeurModelComplet3Gauss(LarUse, Pente, Thres, PCLim, Force, Rcg, Rtj : Double) : Double;
var
  i, n : Integer;
  WFlim : Double;
  SecUse, Sec, Lar, WFdif : Double;
  Work : Double;
  GaussX, GaussW : t_vdd;
begin
  try
    WFlim := (PCLim-Thres)*Pente;
    SecUse := CalculSecFromLar(LarUse, Rcg, Rtj);
    // Int�grale Num�rique *****************************************************//
    // LectureGaussFormula('GaussFormula/GaussFormula.txt', GaussX, GaussW);
    GaussFormula96Points(GaussX, GaussW);
    Work := 0;
    n := length(GaussX);
    // x positif
    for i := 0 to (n-1) do begin
      Sec := (SecUse/2)*GaussX[i]+(SecUse/2);
      Lar := CalculLarFromSecMetTangente (Sec, Rcg, Rtj);
      WFdif := CalDiffWearFactor(Pente, PCLim, WFlim, Lar, Force);
      Work := Work+((SecUse/2)*GaussW[i]*(1/WFdif));
    end;
    // x n�gatif
    for i := 0 to (n-1) do begin
      Sec := (SecUse/2)*(0.0-GaussX[i])+(SecUse/2);
      Lar := CalculLarFromSecMetTangente (Sec, Rcg, Rtj);
      WFdif := CalDiffWearFactor(Pente, PCLim, WFlim, Lar, Force);
      Work := Work+((SecUse/2)*GaussW[i]*(1/WFdif));
    end;
  except
    Work := -999;
  end;
  Result := Work;
end;
//----------------------------------------------------------------------------//


{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Calculer le travail Psi � partir de la largeur us�e L
  Relation WF diff�rentiel Vs Pression de Contact
         --> Lin�aire avec seuil (Thres : Pressure Threshole)
         dS/dPsi=Pente*(PC-Thres)
         PClim et WFasym : Point � partir duquel PC<PClim --> WF=const=WFasym

  M�thode :
    * calcul l'int�grale par la formule de Gauss
      (voir page 887 � Handbook of Mathematical Functions)
      Variable : Largeur us�e (y = L)
                 -> Beaucoup plus rapide car pas de calcul (it�ratif)
                    de la largeur � partir de la Section

  ATTENTION : Pression de contact PC = Force/(2*LargeurPlate)
-------------------------------------------------------------------------------}
function CalculWorkFromLargeurModelComplet4aGauss(LarUse, Pente, Thres, PCLim, Force, Rcg, Rtj : Double) : Double;
var
  i, n : Integer;
  WFlim : Double;
  Lar, WFdif : Double;
  Work : Double;
  GaussX, GaussW : t_vdd;
  fx : Double;
begin
  try
    WFlim := (PCLim-Thres)*Pente;
    // SecUse := CalculSecFromLar(LarUse, Rcg, Rtj);
    // Int�grale Num�rique *****************************************************//
    // LectureGaussFormula('GaussFormula/GaussFormula.txt', GaussX, GaussW);
    GaussFormula96Points(GaussX, GaussW);
    Work := 0;
    n := length(GaussX);
    // x positif
    for i := 0 to (n-1) do begin
      Lar := (LarUse/2)*GaussX[i]+(LarUse/2);
      WFdif := CalDiffWearFactor(Pente, PCLim, WFlim, Lar, Force);
      fx := 2 * ((sqr(Rtj)/sqrt(sqr(Rtj)-sqr(Lar)))
               - (sqr(Rcg)/sqrt(sqr(Rcg)-sqr(Lar)))
               +  sqrt(sqr(Rcg)-sqr(Lar))
               -  sqrt(sqr(Rtj)-sqr(Lar)))*(1/WFdif);
      Work := Work+((LarUse/2)*GaussW[i]*fx);
    end;
    // x n�gatif
    for i := 0 to (n-1) do begin
      Lar := (LarUse/2)*(0.0-GaussX[i])+(LarUse/2);
      WFdif := CalDiffWearFactor(Pente, PCLim, WFlim, Lar, Force);
      fx := 2 * ((sqr(Rtj)/sqrt(sqr(Rtj)-sqr(Lar)))
               - (sqr(Rcg)/sqrt(sqr(Rcg)-sqr(Lar)))
               +  sqrt(sqr(Rcg)-sqr(Lar))
               -  sqrt(sqr(Rtj)-sqr(Lar)))*(1/WFdif);
      Work := Work+((LarUse/2)*GaussW[i]*fx);
    end;
  except
    Work := -999;
  end;
  Result := Work;
end;
//----------------------------------------------------------------------------//


{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Calculer le travail Psi � partir de la largeur us�e L
  Relation WF diff�rentiel Vs Pression de Contact
         --> Lin�aire avec seuil (Thres : Pressure Threshole)
         dS/dPsi=Pente*(PC-Thres)
         PClim et WFasym : Point � partir duquel PC<PClim --> WF=const=WFasym

  M�thode :
    * calcul l'int�grale par la formule de Gauss
      (voir page 887 � Handbook of Mathematical Functions)
      Variable : Largeur us�e (y = L)
                 -> Beaucoup plus rapide car pas de calcul (it�ratif)
                    de la largeur � partir de la Section

  ATTENTION : Pression de contact PC = Force/(2*LargeurContactCourbee)
-------------------------------------------------------------------------------}
function CalculWorkFromLargeurModelComplet4bGauss(LarUse, Pente, Thres, PCLim, Force, Rcg, Rtj : Double) : Double;
var
  i, n : Integer;
  WFlim : Double;
  Lar, WFdif : Double;
  LarCon : Double;  // LarCon : Largeur courb�e de contact
  Work : Double;
  GaussX, GaussW : t_vdd;
  fx : Double;
begin
  try
    WFlim := (PCLim-Thres)*Pente;
    // SecUse := CalculSecFromLar(LarUse, Rcg, Rtj);
    // Int�grale Num�rique *****************************************************//
    // LectureGaussFormula('GaussFormula/GaussFormula.txt', GaussX, GaussW);
    GaussFormula96Points(GaussX, GaussW);
    Work := 0;
    n := length(GaussX);
    // x positif
    for i := 0 to (n-1) do
    begin
      Lar := (LarUse/2)*GaussX[i]+(LarUse/2);
      LarCon := CalculLarConFromLar(Lar, Rcg, Rtj);
      WFdif := CalDiffWearFactor(Pente, PCLim, WFlim, LarCon, Force);
      fx := 2 * ((sqr(Rtj)/sqrt(sqr(Rtj)-sqr(Lar)))
               - (sqr(Rcg)/sqrt(sqr(Rcg)-sqr(Lar)))
               +  sqrt(sqr(Rcg)-sqr(Lar))
               -  sqrt(sqr(Rtj)-sqr(Lar)))*(1/WFdif);
      Work := Work+((LarUse/2)*GaussW[i]*fx);
    end;
    // x n�gatif
    for i := 0 to (n-1) do begin
      Lar := (LarUse/2)*(0.0-GaussX[i])+(LarUse/2);
      LarCon := CalculLarConFromLar(Lar, Rcg, Rtj);
      WFdif := CalDiffWearFactor(Pente, PCLim, WFlim, LarCon, Force);
      fx := 2 * ((sqr(Rtj)/sqrt(sqr(Rtj)-sqr(Lar)))
               - (sqr(Rcg)/sqrt(sqr(Rcg)-sqr(Lar)))
               +  sqrt(sqr(Rcg)-sqr(Lar))
               -  sqrt(sqr(Rtj)-sqr(Lar)))*(1/WFdif);
      Work := Work+((LarUse/2)*GaussW[i]*fx);
    end;
  except
    Work := -999;
  end;
  Result := Work;
end;
//----------------------------------------------------------------------------//

{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Calculer le travail Psi � partir de la Section us�e S
  Relation WF diff�rentiel Vs Pression de Contact
         --> Lin�aire avec seuil (Thres : Pressure Threshole)
         dS/dPsi=Pente*(Force/2LarUse-Thres)
         PClim et WFasym : Point � partir duquel PC<PClim --> WF=const=WFasym

  M�thode :
    * calcul l'int�grale par la formule de Gauss
      (voir page 887 � Handbook of Mathematical Functions)
-------------------------------------------------------------------------------}
function CalculWorkFromSectionModelComplet3aGauss(SecUse, Pente, Thres, PCLim, Force, Rcg, Rtj : Double) : Double;
var
  i, n : Integer;
  WFlim : Double;
  Sec, Lar, WFdif : Double;
  Work : Double;
  GaussX, GaussW : t_vdd;
begin
  try
    WFlim := (PCLim-Thres)*Pente;
    // Int�grale Num�rique *****************************************************//
    // LectureGaussFormula('GaussFormula/GaussFormula.txt', GaussX, GaussW);
    GaussFormula96Points(GaussX, GaussW);

    Work := 0;
    n := length(GaussX);
    // x positif
    for i := 0 to (n-1) do begin
      Sec := (SecUse/2)*GaussX[i]+(SecUse/2);
      Lar := CalculLarFromSecMetTangente (Sec, Rcg, Rtj);
      WFdif := CalDiffWearFactor(Pente, PCLim, WFlim, Lar, Force);
      Work := Work+((SecUse/2)*GaussW[i]*(1/WFdif));
    end;
    // x n�gatif
    for i := 0 to (n-1) do begin
      Sec := (SecUse/2)*(0.0-GaussX[i])+(SecUse/2);
      Lar := CalculLarFromSecMetTangente (Sec, Rcg, Rtj);
      WFdif := CalDiffWearFactor(Pente, PCLim, WFlim, Lar, Force);
      Work := Work+((SecUse/2)*GaussW[i]*(1/WFdif));
    end;
  except
    Work := -999;
  end;
  Result := Work;
end;
//----------------------------------------------------------------------------//


{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Calculer le travail Psi � partir de la Section us�e S
  Relation WF diff�rentiel Vs Pression de Contact
         --> Lin�aire avec seuil (Thres : Pressure Threshole)
         dS/dPsi=Pente*(Force/2LarUse-Thres)
         PClim et WFasym : Point � partir duquel PC<PClim --> WF=const=WFasym

  M�thode :
    * calcul l'int�grale par la formule de Gauss
      (voir page 887 � Handbook of Mathematical Functions)

  ATTENTION : Pression de contact PC = Force/(2*LargeurContactCourbee)
-------------------------------------------------------------------------------}
function CalculWorkFromSectionModelComplet3bGauss(SecUse, Pente, Thres, PCLim, Force, Rcg, Rtj : Double) : Double;
var
  i, n : Integer;
  WFlim : Double;
  Sec, Lar, LarCon, WFdif : Double;
  Work : Double;
  GaussX, GaussW : t_vdd;
begin
  try
    WFlim := (PCLim-Thres)*Pente;
    // Int�grale Num�rique *****************************************************//
    // LectureGaussFormula('GaussFormula/GaussFormula.txt', GaussX, GaussW);
    GaussFormula96Points(GaussX, GaussW);

    Work := 0;
    n := length(GaussX);
    // x positif
    for i := 0 to (n-1) do begin
      Sec := (SecUse/2)*GaussX[i]+(SecUse/2);
      Lar := CalculLarFromSecMetTangente (Sec, Rcg, Rtj);
      LarCon := CalculLarConFromLar(Lar, Rcg, Rtj);
      WFdif := CalDiffWearFactor(Pente, PCLim, WFlim, LarCon, Force);
      Work := Work+((SecUse/2)*GaussW[i]*(1/WFdif));
    end;
    // x n�gatif
    for i := 0 to (n-1) do begin
      Sec := (SecUse/2)*(0.0-GaussX[i])+(SecUse/2);
      Lar := CalculLarFromSecMetTangente (Sec, Rcg, Rtj);
      LarCon := CalculLarConFromLar(Lar, Rcg, Rtj);
      WFdif := CalDiffWearFactor(Pente, PCLim, WFlim, LarCon, Force);
      Work := Work+((SecUse/2)*GaussW[i]*(1/WFdif));
    end;
  except
    Work := -999;
  end;
  Result := Work;
end;
//----------------------------------------------------------------------------//


{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Calcul du delta travail pour que l'usure se d�veloppe de SecDeb � SecFin.

  Relation WF Vs Pression de Contact --> Lin�aire avec seuil (Threshole)
       dS/dPsi=Pente*(Force/2LarUse-Thres)
       PClim et WFasym : Point � partir duquel PC<PClim --> WF=const=WFasym

  M�thode :
    * calcul l'int�grale par la formule de Gauss
      (voir page 887 � Handbook of Mathematical Functions)
-------------------------------------------------------------------------------}
function CalculDeltaWorkModelCompletGaussLarPlate(SecDeb, SecFin, Pente, Thres, PCLim, Force, Rcg, Rtj : Double) : Double;
var
  i, n : Integer;
  WFlim : Double;
  Sec, Lar, WFdif : Double;
  Work : Double;
  GaussX, GaussW : t_vdd;
begin
  try
    WFlim := (PCLim-Thres)*Pente;

    // Int�grale Num�rique *****************************************************//
    // LectureGaussFormula('GaussFormula/GaussFormula.txt', GaussX, GaussW);
    GaussFormula96Points(GaussX, GaussW);

    Work := 0;
    n := length(GaussX);
    // x positif
    for i := 0 to (n-1) do begin
      Sec := ((SecFin-SecDeb)/2)*GaussX[i]+((SecFin+SecDeb)/2);
      Lar := CalculLarFromSecMetTangente (Sec, Rcg, Rtj);
      WFdif := CalDiffWearFactor(Pente, PCLim, WFlim, Lar, Force);
      Work := Work+((SecFin-SecDeb)/2*GaussW[i]*(1/WFdif));
    end;
    // x n�gatif
    for i := 0 to (n-1) do begin
      Sec := ((SecFin-SecDeb)/2)*(0.0-GaussX[i])+((SecFin+SecDeb)/2);
      Lar := CalculLarFromSecMetTangente (Sec, Rcg, Rtj);
      WFdif := CalDiffWearFactor(Pente, PCLim, WFlim, Lar, Force);
      Work := Work+((SecFin-SecDeb)/2*GaussW[i]*(1/WFdif));
    end;
  except
    Work := -999;
  end;
  Result := Work;
end;
//----------------------------------------------------------------------------//


{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Calcul du delta travail pour que l'usure se d�veloppe de SecDeb � SecFin.

  Relation WF Vs Pression de Contact --> Lin�aire avec seuil (Threshole)
       dS/dPsi=Pente*(Force/2LarUse-Thres)
       PClim et WFasym : Point � partir duquel PC<PClim --> WF=const=WFasym

  M�thode :
    * calcul l'int�grale par la formule de Gauss
      (voir page 887 � Handbook of Mathematical Functions)

  ATTENTION : Pression de contact PC = Force/(2*LargeurContactCourbee)
-------------------------------------------------------------------------------}
function CalculDeltaWorkModelCompletGaussLarCourbee(SecDeb, SecFin, Pente, Thres, PCLim, Force, Rcg, Rtj : Double) : Double;
var
  i, n : Integer;
  WFlim : Double;
  Sec, Lar, LarCon, WFdif : Double;
  Work : Double;
  GaussX, GaussW : t_vdd;
begin
  try
    WFlim := (PCLim-Thres)*Pente;

    // Int�grale Num�rique *****************************************************//
    // LectureGaussFormula('GaussFormula/GaussFormula.txt', GaussX, GaussW);
    GaussFormula96Points(GaussX, GaussW);

    Work := 0;
    n := length(GaussX);
    // x positif
    for i := 0 to (n-1) do begin
      Sec := ((SecFin-SecDeb)/2)*GaussX[i]+((SecFin+SecDeb)/2);
      Lar := CalculLarFromSecMetTangente (Sec, Rcg, Rtj);
      LarCon := CalculLarConFromLar(Lar, Rcg, Rtj);
      WFdif := CalDiffWearFactor(Pente, PCLim, WFlim, LarCon, Force);
      Work := Work+((SecFin-SecDeb)/2*GaussW[i]*(1/WFdif));
    end;
    // x n�gatif
    for i := 0 to (n-1) do begin
      Sec := ((SecFin-SecDeb)/2)*(0.0-GaussX[i])+((SecFin+SecDeb)/2);
      Lar := CalculLarFromSecMetTangente (Sec, Rcg, Rtj);
      LarCon := CalculLarConFromLar(Lar, Rcg, Rtj);
      WFdif := CalDiffWearFactor(Pente, PCLim, WFlim, LarCon, Force);
      Work := Work+((SecFin-SecDeb)/2*GaussW[i]*(1/WFdif));
    end;
  except
    Work := -999;
  end;

  Result := Work;
end;
//----------------------------------------------------------------------------//

{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Calcul de l'incr�ment de la section us�e correspondant � l'incr�ment du travail

  Relation WF Vs Pression de Contact --> Lin�aire avec seuil (Threshole)
       dS/dPsi=Pente*(Force/2LarUse-Thres)
       PClim et WFasym : Point � partir duquel PC<PClim --> WF=const=WFasym

  M�thode : * calcul l'int�grale par la formule de Gauss
            * Dichotomie
-------------------------------------------------------------------------------}
function CalculDeltaSectionModelComplet(SecDeb, DelWork, Pente, Thres, PCLim, Force, Rcg, Rtj : Double) : Double;
var
  dSec : Double;
  dWork : Double;
  SecFin, SecFin1, SecFin2 : Double;
begin
  dSec := 0.001;
  SecFin := SecDeb;
  repeat
    SecFin := SecFin+dSec;
    dWork := CalculDeltaWorkModelCompletGaussLarPlate(SecDeb, SecFin, Pente, Thres, PCLim, Force, Rcg, Rtj);
  until (dWork>=DelWork);
  SecFin1 := SecFin-dSec;
  SecFin2 := SecFin;

  repeat
    SecFin := (SecFin1+SecFin2)/2;
    dWork := CalculDeltaWorkModelCompletGaussLarPlate(SecDeb, SecFin, Pente, Thres, PCLim, Force, Rcg, Rtj);
    if dWork>DelWork then SecFin2 := SecFin else SecFin1 := SecFin;
  until (abs(dWork-DelWork)<0.001);

  Result := SecFin-SecDeb;
end;
//----------------------------------------------------------------------------//


{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Calcul de l'incr�ment de la section us�e correspondant � l'incr�ment du travail

  Relation WF Vs Pression de Contact --> Lin�aire avec seuil (Threshole)
       dS/dPsi=Pente*(Force/2LarUse-Thres)
       PClim et WFasym : Point � partir duquel PC<PClim --> WF=const=WFasym

  M�thode : * calcul l'int�grale par la formule de Gauss
            * Tangent method
-------------------------------------------------------------------------------}
function CalculDeltaSectionModelCompletTangentMethod(SecDeb, DelWork, Pente, Thres, PCLim, Force, Rcg, Rtj : Double) : Double;
var
  dSecTan : Double;
  dWork, dWork1, dWork2 : Double;
  SecFin, SecFin1, SecFin2 : Double;
  cpt : Integer;
  FlagConv : Boolean;
begin
  FlagConv := False;
  cpt := 1;
  dSecTan := 1E-6;
  //
  SecFin := SecDeb;
  dWork := CalculDeltaWorkModelCompletGaussLarCourbee(SecDeb, SecFin, Pente, Thres, PCLim, Force, Rcg, Rtj);
  SecFin1 := SecFin;
  dWork1 := dWork;
  repeat
    SecFin := SecFin1+dSecTan;
    dWork := CalculDeltaWorkModelCompletGaussLarCourbee(SecDeb, SecFin, Pente, Thres, PCLim, Force, Rcg, Rtj);
    SecFin2 := SecFin;
    dWork2 := dWork;

    SecFin := SecFin1+(DelWork-dWork1)*(SecFin2-SecFin1)/(dWork2-dWork1);
    dWork := CalculDeltaWorkModelCompletGaussLarCourbee(SecDeb, SecFin, Pente, Thres, PCLim, Force, Rcg, Rtj);
    if (abs(dWork-DelWork)<1E-6) then FlagConv := True
    else begin
      cpt := cpt+1;
      SecFin1 := SecFin;
      dWork1 := dWork;
      if dSecTan>1E-12 then dSecTan := dSecTan/2.0;
    end;
  until ((FlagConv) or (cpt>50));
  //
  Result := SecFin-SecDeb;
end;
//----------------------------------------------------------------------------//


{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
    Calculer le travail Psi � partir de la largeur us�e L
    Relation WF diff�rentiel Vs Pression de Contact -> Non-Lin�aire sans seuil :
                     y = [Ax^2 + (Ak - AB)x]/(x+k)
                     WFdiff = y ; PC = x = Force/2LarUse ;
                     -> Asymptote oblique y = A(x-B)
-------------------------------------------------------------------------------}
function CalculWorkFromLargeurModelNonLinearWFdif_PC(LarUse, Pente, Thres, CoefK, Force, Rcg, Rtj : Double) : Double;
var
  Lg, Pc, fx1, fx2, Work, Increment : Double; //Llimite
  i, n : Integer;
begin
  // Largeur Limite
  // Llimite := Force/(2*Thres);
  // if (LarUse > Llimite-1E-6) then Work := 1.0E100
  if (LarUse<1E-50) then Work := 0
  else
  BEGIN
    // Int�grale Num�rique ***************************************************//
    Work := 0;
    n := 1000;
    Increment := LarUse/n;
    Lg := 0;
    // Pc := Force/(2*Lg) --> Infini;
    fx2 := 0;
    for i := 1 to n do begin
      fx1 := fx2;
      Lg := i*Increment;
      Pc := Force/(2*Lg);
      fx2 :=  ((sqr(Rtj)/sqrt(sqr(Rtj)-sqr(Lg)))
             - (sqr(Rcg)/sqrt(sqr(Rcg)-sqr(Lg)))
             +  sqrt(sqr(Rcg)-sqr(Lg))
             -  sqrt(sqr(Rtj)-sqr(Lg))) * (2*(Pc+CoefK)/(Pente*(PC*PC+(CoefK-Thres)*PC)));
      Work := Work + (fx1+fx2)*Increment/2;
    end;
    //********************************************** Int�grale Num�rique *****//
  END;

  Result := Work;
end;
//----------------------------------------------------------------------------//


{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
procedure CalLogSecUse_Work_ModThresWithLimit(Var LisSecUse, LisWork : t_vdd; Pente, Thres, PCLimit, Force, Rcg, Rtj : Double);
var
  LarUse, SecUse, Llimite, Work : Double; //Llimite
  i, n : Integer;
begin
  // Free
  setlength(LisSecUse, 0);
  setlength(LisWork, 0);

  // Largeur � partir de laquelle le volume us� est lin�aire par rapport au travail
  Llimite := Force/(2*PCLimit);

  n:=1000;
  for i := 0 to n do begin
    LarUse := i*Llimite/(n-10);
    SecUse := CalculSecFromLar(LarUse, Rcg, Rtj);
    Work := CalculWorkFromLargeurModelComplet1(LarUse, Pente, Thres, PCLimit, Force, Rcg, Rtj);

    setlength(LisSecUse, i+1);
    setlength(LisWork, i+1);
    LisWork[i] := Work;
    LisSecUse[i] := SecUse;
  end;
end;
//----------------------------------------------------------------------------//

{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Calculer la pente
-------------------------------------------------------------------------------}
function CalculPenteModelThreshole(Work8h, SecUse8h, Thres, Force, Rcg, Rtj : Double) : Double;
var
  LarUse, Lg, fx1, fx2, Integrale, Increment, Llimite : Double;
  i, n : Integer;
begin
  LarUse := CalculLarFromSecMetTangente(SecUse8h, Rcg, Rtj);
  // Largeur Limite
  Llimite := Force/(2*Thres);
  if (LarUse > Llimite-1E-6) then Result := -999
  else begin
  // Int�grale Num�rique *****************************************************//
  Integrale := 0;
  n := 1000;
  Increment := LarUse/n;
  Lg := 0;
  fx2 := ((sqr(Rtj)/sqrt(sqr(Rtj)-sqr(Lg)))
        - (sqr(Rcg)/sqrt(sqr(Rcg)-sqr(Lg)))
        +  sqrt(sqr(Rcg)-sqr(Lg))
        -  sqrt(sqr(Rtj)-sqr(Lg))) * (4*Lg/(Force-Thres*2*Lg));
  for i := 1 to n do begin
    fx1 := fx2;
    Lg := i*Increment;
    fx2 :=  ((sqr(Rtj)/sqrt(sqr(Rtj)-sqr(Lg)))
           - (sqr(Rcg)/sqrt(sqr(Rcg)-sqr(Lg)))
           +  sqrt(sqr(Rcg)-sqr(Lg))
           -  sqrt(sqr(Rtj)-sqr(Lg))) * (4*Lg/(Force-Thres*2*Lg));
    Integrale := Integrale + (fx1+fx2)*Increment/2;
  end;
  //************************************************* Int�grale Num�rique *****//
  end;
  Result := Integrale/Work8h;
end;
//----------------------------------------------------------------------------//


{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Calculer la section us�e S � partir du travail Psi
-------------------------------------------------------------------------------}
function CalculSectionFromWork(Work, CoeWFdif, Rcg, Rtj : Double) : Double;
var
  PsiWork, Lg, Lg1, Lg2 : Double;
  // Terme1, Terme2, Terme3 : Double;
begin
  Lg1 := 0;
  Lg2 := Rtj;

  // M�thode de dichotomie --> chercher la Largeur de la section us�e
  while (Lg2-Lg1>=1e-9) do begin
    Lg := (Lg1+Lg2)/2;
    PsiWork := CalculWorkFromLargeur(Lg, CoeWFdif, Rcg, Rtj);
    if (PsiWork<Work)
    then Lg1 := Lg
    else Lg2 := Lg;
  end;
  Lg := (Lg1+Lg2)/2;

  result := CalculSecFromLar(Lg, Rcg, Rtj);
end;
//----------------------------------------------------------------------------//


{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
function CalDiffWearFactor(Pente, PClim, WFlim, Largeur, Force: Double) : Double;
var
  P : Double;
begin
  if (Largeur>0) then begin
    P := Force/(2*Largeur);
    if (P<=PClim)
    then Result := WFlim
    else Result := WFlim+(P-PClim)*Pente;
  end else Result := -999;
end;
//----------------------------------------------------------------------------//

function Cal_PClim_WithDicho_SI(Work8h, SecUse8h : Double;
                                A, WFlim : Double;
                                Force, Rcg, Rtj : Double) : Double;
var
  Work : Double;
  Thres, B1, B2 : Double; // WFdif := A*(PC-B);
  iter : Integer;
  PClim : Double;
begin
  // recherche des bornes optimales pour minimiser les it�rations
  B1 := 0;
  B2 := 0;
  repeat
    B2 := B1;
    B1 := B1 + 1E5;
    PClim := (WFlim/A) + B1;
    // Largeur contact = Largeur courb�e
    Work := CalculWorkFromSectionModelComplet3bGauss(SecUse8h, A, B1, PCLim, Force, Rcg, Rtj);
  until Work > Work8h;
  //
  iter := 0;
  repeat
    iter := iter+1;
    Thres := (B1+B2)/2;
    PClim := (WFlim/A) + Thres;
    Work := CalculWorkFromSectionModelComplet3bGauss(SecUse8h, A, Thres, PCLim, Force, Rcg, Rtj);
    if Work > Work8h then B1 := Thres
    else B2 := Thres;
  until (abs(B2-B1)<1E-3);
  Thres := (B1+B2)/2;
  Result := (WFlim/A) + Thres;
end;

{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
procedure LectureNonlinearCorrecFactor(NomFic: string; var LisWear, LisCorFac : t_vdd);
var
  i : Integer;
	fic : text;
begin
  assign(fic, NomFic);
  reset(fic);
  i := 0;
  repeat
    setlength(LisWear, i+1);
    setlength(LisCorFac, i+1);
    // Lecture
    ReadLn(fic,LisWear[i],LisCorFac[i]);
    i := i+1;
  until (Eof(fic));
  close(fic);
end;
//------------------------------------------------------------------------------


{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
function CalNonlinearCorrectionFactor(WearPercent : Double; LisWear, LisCorFactor : t_vdd) : Double;
var
  i, n : Integer;
begin
  n := min(length(LisWear), length(LisCorFactor)) ;
  if n<1 then Result := 1 else
  begin
    if (WearPercent<=LisWear[0]) then Result := LisCorFactor[0]
    else if (WearPercent>=LisWear[n-1]) then Result := LisCorFactor[n-1]
    else begin
      i := 0;
      while (WearPercent>LisWear[i]) do i := i+1;
      Result := LisCorFactor[i-1]+(WearPercent-LisWear[i-1])*(LisCorFactor[i]-LisCorFactor[i-1])/(LisWear[i]-LisWear[i-1]);
    end;
  end;
end;
//----------------------------------------------------------------------------//

{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  With Formula
-------------------------------------------------------------------------------}
function CalNonLinearityCorFactor(WearPercent : Double) : Double;
var
  Res : Double;
begin
  if (WearPercent <= 0.01) then
    Res := 7.0
  else
  if (WearPercent < 0.5) then
    Res := 0.878 + (6.122/(100*WearPercent))
  else Res := 1.0;
  Result := Res;
end;

{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
Procedure SimpleResultWithCorFactor(HauMax, DintCsg, DextCsg : Double;
     var WearPercent, CorFac, HauMaxCor, WearPercentCor : Double);
begin
  WearPercent := HauMax/((DextCsg-DintCsg)/2.0);
  CorFac := CalNonLinearityCorFactor(WearPercent);
  HauMaxCor := HauMax*CorFac;
  WearPercentCor := WearPercent*CorFac;
end;
{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
function CalValeurFromLog(ValA : Double; LisA, LisB : t_vdd) : Double;
var
  i, n, k : Integer;
begin
  n := length(LisA);

  if (ValA<=LisA[0]) then k := 1
  else if (ValA>=LisA[n-1]) then k := n-1
  else begin
    i := 0;
    while (ValA>LisA[i]) do i := i+1;
    k := i;
  end;
  Result := LisB[k-1]+(ValA-LisA[k-1])*(LisB[k]-LisB[k-1])/(LisA[k]-LisA[k-1]);
end;
//----------------------------------------------------------------------------//



{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>

  Pour la formule de Gauss de l'int�gration
  --> voir page 887 � Handbook of Mathematical Functions
-------------------------------------------------------------------------------}
procedure LectureGaussFormula(NomFic: string; var x, Weight : t_vdd);
var
  i : Integer;
	fic : text;
begin
  assign(fic, NomFic);
  if FileExists(NomFic) then
  begin
    reset(fic);
    i := 0;
    repeat
      setlength(x, i+1);
      setlength(Weight, i+1);
      // Lecture
      ReadLn(fic, x[i], Weight[i]);
      i := i+1;
    until (Eof(fic));
  end else
  begin
    // D�faut : 20 points d'int�gration de Gauss
    setlength(x, 10);
    setlength(Weight, 10);
    x[0] := 0.076526521133497333755;
    x[1] := 0.227785851141645078080;
    x[2] := 0.373706088715419560673;
    x[3] := 0.510867001950827098004;
    x[4] := 0.636053680726515025453;
    x[5] := 0.746331906460150792614;
    x[6] := 0.839116971822218823395;
    x[7] := 0.912234428251325905868;
    x[8] := 0.963971927277913791268;
    x[9] := 0.993128599185094924786;

    Weight[0] := 0.152753387130725850698;
    Weight[1] := 0.149172986472603746788;
    Weight[2] := 0.142096109318382051329;
    Weight[3] := 0.131688638449176626898;
    Weight[4] := 0.118194531961518417312;
    Weight[5] := 0.101930119817240435037;
    Weight[6] := 0.083276741576704748725;
    Weight[7] := 0.062672048334109063570;
    Weight[8] := 0.040601429800386941331;
    Weight[9] := 0.017614007139152118312;
  end;
  close(fic);
end;
//------------------------------------------------------------------------------



{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>

  Pour la formule de Gauss de l'int�gration
  --> voir page 887 � Handbook of Mathematical Functions
-------------------------------------------------------------------------------}
procedure GaussFormula96Points(var x, Weight : t_vdd);
begin
    // 96 points d'int�gration de Gauss
    setlength(x, 48);
    setlength(Weight, 48);
    x[0] := 0.016276744849602969579;
    x[1] := 0.048812985136049731112;
    x[2] := 0.081297495464425558994;
    x[3] := 0.113695850110665920911;
    x[4] := 0.145973714654896941989;
    x[5] := 0.178096882367618602759;
    x[6] := 0.210031310460567203603;
    x[7] := 0.241743156163840012328;
    x[8] := 0.273198812591049141487;
    x[9] := 0.304364944354496353024;
    x[10] := 0.335208522892625422616;
    x[11] := 0.365696861472313635031;
    x[12] := 0.395797649828908603285;
    x[13] := 0.425478988407300545365;
    x[14] := 0.454709422167743008636;
    x[15] := 0.483457973920596359768;
    x[16] := 0.511694177154667673586;
    x[17] := 0.539388108324357436227;
    x[18] := 0.566510418561397168404;
    x[19] := 0.593032364777572080684;
    x[20] := 0.618925840125468570386;
    x[21] := 0.644163403784967106798;
    x[22] := 0.668718310043916153953;
    x[23] := 0.692564536642171561344;
    x[24] := 0.715676812348967626225;
    x[25] := 0.738030643744400132851;
    x[26] := 0.759602341176647498703;
    x[27] := 0.780369043867433217604;
    x[28] := 0.800308744139140817229;
    x[29] := 0.819400310737931675539;
    x[30] := 0.837623511228187121494;
    x[31] := 0.854959033434601455463;
    x[32] := 0.871388505909296502874;
    x[33] := 0.886894517402420416057;
    x[34] := 0.901460635315852341319;
    x[35] := 0.915071423120898074206;
    x[36] := 0.927712456722308690965;
    x[37] := 0.939370339752755216932;
    x[38] := 0.950032717784437635756;
    x[39] := 0.959688291448742539300;
    x[40] := 0.968326828463264212174;
    x[41] := 0.975939174585136466453;
    x[42] := 0.982517263563014677447;
    x[43] := 0.988054126329623799481;
    x[44] := 0.992543900323762624572;
    x[45] := 0.995981842987209290650;
    x[46] := 0.998364375863181677724;
    x[47] := 0.999689503883230766828;

    Weight[0] := 0.032550614492363166242;
    Weight[1] := 0.032516118713868835987;
    Weight[2] := 0.032447163714064269364;
    Weight[3] := 0.032343822568575928429;
    Weight[4] := 0.032206204794030250669;
    Weight[5] := 0.032034456231992663218;
    Weight[6] := 0.031828758894411006535;
    Weight[7] := 0.031589330770727168558;
    Weight[8] := 0.031316425596861355813;
    Weight[9] := 0.031010332586313837423;
    Weight[10] := 0.030671376123669149014;
    Weight[11] := 0.030299915420827593794;
    Weight[12] := 0.029896344136328385984;
    Weight[13] := 0.029461089958167905970;
    Weight[14] := 0.028994614150555236543;
    Weight[15] := 0.028497411065085385646;
    Weight[16] := 0.027970007616848334440;
    Weight[17] := 0.027412962726029242823;
    Weight[18] := 0.026826866725591762198;
    Weight[19] := 0.026212340735672413913;
    Weight[20] := 0.025570036005349361499;
    Weight[21] := 0.024900633222483610288;
    Weight[22] := 0.024204841792364691282;
    Weight[23] := 0.023483399085926219842;
    Weight[24] := 0.022737069658329374001;
    Weight[25] := 0.021966644438744349195;
    Weight[26] := 0.021172939892191298988;
    Weight[27] := 0.020356797154333324595;
    Weight[28] := 0.019519081140145022410;
    Weight[29] := 0.018660679627411467385;
    Weight[30] := 0.017782502316045260838;
    Weight[31] := 0.016885479864245172450;
    Weight[32] := 0.015970562902562291381;
    Weight[33] := 0.015038721026994938006;
    Weight[34] := 0.014090941772314860916;
    Weight[35] := 0.013128229566961572637;
    Weight[36] := 0.012151604671088319635;
    Weight[37] := 0.011162102099838498591;
    Weight[38] := 0.010160770535008415758;
    Weight[39] := 0.009148671230783386633;
    Weight[40] := 0.008126876925698759217;
    Weight[41] := 0.007096470791153865269;
    Weight[42] := 0.006058545504235961683;
    Weight[43] := 0.005014202742927517693;
    Weight[44] := 0.003964554338444686674;
    Weight[45] := 0.002910731817934946408;
    Weight[46] := 0.001853960788946921732;
    Weight[47] := 0.000796792065552012429;
end;
//------------------------------------------------------------------------------

{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>

  Pour la formule de Gauss de l'int�gration
-------------------------------------------------------------------------------}
procedure GaussFormula20Points(var x, Weight : t_vdd);
begin
    // D�faut : 20 points d'int�gration de Gauss
    setlength(x, 10);
    setlength(Weight, 10);
    x[0] := 0.076526521133497333755;
    x[1] := 0.227785851141645078080;
    x[2] := 0.373706088715419560673;
    x[3] := 0.510867001950827098004;
    x[4] := 0.636053680726515025453;
    x[5] := 0.746331906460150792614;
    x[6] := 0.839116971822218823395;
    x[7] := 0.912234428251325905868;
    x[8] := 0.963971927277913791268;
    x[9] := 0.993128599185094924786;

    Weight[0] := 0.152753387130725850698;
    Weight[1] := 0.149172986472603746788;
    Weight[2] := 0.142096109318382051329;
    Weight[3] := 0.131688638449176626898;
    Weight[4] := 0.118194531961518417312;
    Weight[5] := 0.101930119817240435037;
    Weight[6] := 0.083276741576704748725;
    Weight[7] := 0.062672048334109063570;
    Weight[8] := 0.040601429800386941331;
    Weight[9] := 0.017614007139152118312;
end;
//------------------------------------------------------------------------------


{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
procedure RegressionWorkWear(Work, WearVol : t_vdd; n : integer;
                             var Y_Der_Moy : t_vdd);
var
  i, j, k : Integer;

  ListWearVolReg : Tlist;
  ListWearVolDerivative: Tlist;
  ListRegQualite : Tlist;

  nPoint, nReg : integer;
  X, Y : t_vdd;
  Y_Reg, Y_Der : t_vdd;
  p_Y_Reg, p_Y_Der : t_vdd_p;
  p_RegRes : PRegOrdre2Res;
  OkReg : Boolean;
begin
  nPoint := length(Work);
  if (n<nPoint) then nReg := n else nReg := nPoint;

  // Initialisation List
  ListWearVolReg := TList.Create;
  ListWearVolDerivative := TList.Create;
  ListRegQualite := TList.Create;

  setlength(X, nReg);
  setlength(Y, nReg);
  for i := 0 to (nPoint-nReg) do
  begin
    for j := i to (i+nReg-1) do
    begin
      X[j-i] := Work[j];
      Y[j-i] := WearVol[j];
    end;
    New(p_RegRes);
    New(p_Y_Reg);
    New(p_Y_Der);
    OkReg := Regression_Polynomiale_Ordre2(nReg, X, Y, p_RegRes^.b0, p_RegRes^.b1, p_RegRes^.b2,
                                                       p_RegRes^.SST, p_RegRes^.SSR, p_RegRes^.SSE, p_RegRes^.Rsqua,
                                                       Y_Reg, Y_Der);
    if (OkReg) then
    begin
      setlength(p_Y_Reg^, nPoint);
      setlength(p_Y_Der^, nPoint);
      for j := 0 to (nPoint-1) do
      begin
        p_Y_Reg^[j] := 0.0;
        p_Y_Der^[j] := 0.0;
      end;
      for j := i to (i+nReg-1) do
      begin
        p_Y_Reg^[j] := Y_Reg[j-i];
        p_Y_Der^[j] := Y_Der[j-i];
      end;
      ListWearVolReg.Add(p_Y_Reg);
      ListWearVolDerivative.Add(p_Y_Der);
      ListRegQualite.Add(p_RegRes);
    end;
  end;
  // D�riv�e Moyenne
  setlength(Y_Der_Moy, nPoint);
  for j := 1 to nPoint do
  begin
    k := 0;
    Y_Der_Moy[j-1] := 0.0;
    for i := 1 to ListWearVolDerivative.Count do
    begin
      p_Y_Der := ListWearVolDerivative.Items[i-1];
      if (p_Y_Der^[j-1]>1E-30) then begin
        k := k+1;
        Y_Der_Moy[j-1] := Y_Der_Moy[j-1]+p_Y_Der^[j-1];
      end;
    end;
    Y_Der_Moy[j-1] := Y_Der_Moy[j-1]/k;
  end;

  // Free List
  ListWearVolReg.Clear;
  ListWearVolDerivative.Clear;
  ListRegQualite.Clear;
end;
//------------------------------------------------------------------------------

{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
procedure CalLarHauPCFromWearVol_PlateContactLength(WearVol : t_vdd;
                                 dintcsg, dexttj, force : Double;
                                 var Lar, Hau, PC : t_vdd);
var
  j, nPoint : Integer;
  LarCon : t_vdd;
begin
  nPoint := length(WearVol);
  // Initialisation
  setlength(Lar,nPoint);
  setlength(Hau,nPoint);
  setlength(PC,nPoint);
  setlength(LarCon,nPoint);
  for j := 1 to nPoint do
  begin
    Lar[j-1] := CalculLarFromSecMetTangente(WearVol[j-1], dintcsg/2, dexttj/2);
    Hau[j-1] := CalculHauFromSec(WearVol[j-1], dintcsg/2, dexttj/2);
    if (Lar[j-1]<1E-12) then PC[j-1] := -999
    else PC[j-1] := force/(2*Lar[j-1]);
  end;
end;
//------------------------------------------------------------------------------


{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
procedure CalLarHauPCFromWearVol_CurvedContactLength(WearVol : t_vdd;
                                 dintcsg, dexttj, force : Double;
                                 var Lar, Hau, PC : t_vdd);
var
  j, nPoint : Integer;
  LarCon : t_vdd;
begin
  nPoint := length(WearVol);
  // Initialisation
  setlength(Lar,nPoint);
  setlength(Hau,nPoint);
  setlength(PC,nPoint);
  setlength(LarCon,nPoint);
  for j := 1 to nPoint do
  begin
    Lar[j-1] := CalculLarFromSecMetTangente(WearVol[j-1], dintcsg/2, dexttj/2);
    Hau[j-1] := CalculHauFromSec(WearVol[j-1], dintcsg/2, dexttj/2);
    if (Lar[j-1]<1E-12) then PC[j-1] := -999
    else PC[j-1] := force/(2*Lar[j-1]);
    LarCon[j-1] := CalculLarConFromSec(WearVol[j-1], dintcsg/2, dexttj/2);
    if (LarCon[j-1]<1E-12) then PC[j-1] := -999
    else PC[j-1] := force/(2*LarCon[j-1]);
  end;
  setlength(LarCon,0);
end;
//------------------------------------------------------------------------------

{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
procedure CalLarHauFromWearVol(WearVol : t_vdd;
                               dintcsg, dexttj : Double;
                               var Lar, Hau : t_vdd);
var
  j, nPoint : Integer;
begin
  nPoint := length(WearVol);
  // Initialisation
  setlength(Lar,nPoint);
  setlength(Hau,nPoint);
  for j := 1 to nPoint do
  begin
    Lar[j-1] := CalculLarFromSecMetTangente(WearVol[j-1], dintcsg/2, dexttj/2);
    Hau[j-1] := CalculHauFromSec(WearVol[j-1], dintcsg/2, dexttj/2);
    // LarCon[j-1] := CalculLarConFromSec(WearVol[j-1], dintcsg/2, dexttj/2);
  end;
end;
//------------------------------------------------------------------------------


Procedure WriteTestSection(FilePath : String;
                             SecRes, LarRes : Double);
var
  i : Integer;
  FilMou : Textfile;
begin
    AssignFile(FilMou,FilePath);
    if FileExists(FilePath) then Append(FilMou)
    else begin
      Rewrite(FilMou);
      Writeln(FilMou, ' SecRes LarRes');
    end;
    Writeln(FilMou, ' ', SecRes:15:12,
                    ' ', LarRes:15:12);
    CloseFile(FilMou);
end;


procedure CalculSection(DiaCsg, DiaTj, Dep : Double;
                        Tfo : Double;
                        P : Integer;
                        HauUsuIni : t_vdd;
                        p_dHauUsu : t_vdd_p;
                        var SecRes, LarRes : Double);
  //--------------------------------------------------------------------------//
  function CalculLarFromHau(Hauteur, Rcg, Rtj : Double) : Double;
  var
    Lg : Double;
  begin
    if ((Hauteur<0) or (Hauteur>Rtj)) then Lg := -999
    else if (Hauteur<1E-50) then Lg := 0.0
    else Lg := Sqrt(Sqr(Rcg)-Sqr((Sqr(Rcg)-Sqr(Rtj)+Sqr(Rcg-Rtj+Hauteur))/(2*(Rcg-Rtj+Hauteur))));
    Result := Lg;
  end;
  {-----------------------------------------------------------------------------
    Determine the coordinates of intersection point between line P1P2 and circle
    P1 (X1,Y1) : Point inside the circle
    P2 (X2,Y2) : Point outside the circle
    Solution:
    X = X1+Lamda*(X2-X1);
    Y = Y1+Lamda*(Y2-Y1);
    Lamda is the root of following equation:
    sqr(X-Xc)+sqr(Y-Yc)=sqr(R)
  -----------------------------------------------------------------------------}
  Procedure LineCircleIntersection(Xc, Yc, R, X1, Y1, X2, Y2 : Double; var X, Y : Double);
  var
    Lamda : Double;
    a, b, c, delta, t1, t2 : Double;
  begin
    a := sqr(X2-X1)+sqr(Y2-Y1);
    b := (2*(X1-Xc)*(X2-X1))+(2*(Y1-Yc)*(Y2-Y1));
    c := sqr(X1-Xc)+sqr(Y1-Yc)-sqr(R);
    delta := sqr(b)-4*a*c;
    t1 := (-b-sqrt(delta))/2/a;
    t2 := (-b+sqrt(delta))/2/a;
    Lamda := Max(t1,t2);
    X := X1+Lamda*(X2-X1);
    Y := Y1+Lamda*(Y2-Y1);
  end;
  {-----------------------------------------------------------------------------
    Give mesh number in the interval [0, P-1]
  -----------------------------------------------------------------------------}
  Function MeshNumber(P : Integer; jcur : Integer) : Integer;
  var
    j : Integer;
  Begin
    if      (jcur<0) then j := jcur+P
    else if (jcur>P) then j := jcur-P
    else                  j := jcur;
    Result := j;
  End;
  //--------------------------------------------------------------------------//
  Procedure SectionUsePointCoordinate(Rcsg : Double;
                                      P : Integer; HauUsu : t_vdd;
                                      jcur : Integer;
                                      var HauCur, TfoCur : Double;
                                      var Un, Ub : Double);
  var
    j : Integer;
  begin
    j := MeshNumber(P, jcur);
    HauCur := HauUsuIni[j];
    TfoCur := j*(2*Pi/P);
    Un := (Rcsg+HauCur)*Cos(TfoCur);
    Ub := (Rcsg+HauCur)*Sin(TfoCur);
  end;
  //--------------------------------------------------------------------------//
  Function TriangleArea(a, b, c : Double) : Double;
  begin
    if ((a+b-c)*(b+c-a)*(c+a-b)>=0.0) then
      Result := sqrt((a+b+c)*(a+b-c)*(b+c-a)*(c+a-b))/4.0
    else Result := 0.0;
  end;
  //--------------------------------------------------------------------------//
var
  Rcsg, Rtj : Double;
  Hauteur, Largeur : Double;
  Ang : Double;
  j, jdeb, jfin, jcur, jcurTmp : Integer;
  dTfo : Double;
  HauCur, TfoCur : Double;
  dHauMax : Double;
  a, b, c, delta, t1, t2 : Double;
  // Tool-joint centre
  UnTj, UbTj : Double;
  // Coordinates of extremities
  Un1, Ub1 : Double;
  Un, Ub, UnUse, UbUse, RayIni, RayUse : t_vdd;
begin
  // Initalisation
  if (length(p_dHauUsu^)<P) then setlength(p_dHauUsu^, P);
  SecRes := 0.0;
  LarRes := 0.0;
  for j := 0 to P-1 do p_dHauUsu^[j] := 0.0;
  //
  Rcsg := DiaCsg/2.0;
  Rtj := DiaTj/2.0;
  dTfo := (2*Pi)/P;
  // Limitation
  Hauteur := Max((Dep+Rtj)-Rcsg, 0.0);
  Largeur := CalculLarFromHau(Hauteur, Rcsg, Rtj);
  Ang := Asin(Largeur/Rcsg);
  jdeb := Round((Tfo-Ang)/dTfo)-1;
  jfin := Round((Tfo+Ang)/dTfo)+1;

  setlength(Un, jfin-jdeb+1);
  setlength(Ub, jfin-jdeb+1);
  setlength(UnUse, jfin-jdeb+1);
  setlength(UbUse, jfin-jdeb+1);
  setlength(RayIni, jfin-jdeb+1);
  setlength(RayUse, jfin-jdeb+1);

  // Pertes en epaisseur & volume
  UnTj := Dep*Cos(Tfo);
  UbTj := Dep*Sin(Tfo);
  for jcur := jdeb to jfin do begin
    SectionUsePointCoordinate(Rcsg, P, HauUsuIni, jcur, HauCur, TfoCur, Un[jcur-jdeb], Ub[jcur-jdeb]);
    RayIni[jcur-jdeb] := Rcsg+HauCur;
    j := MeshNumber(P, jcur);
    if (Sqrt(Sqr(Un[jcur-jdeb]-UnTj)+Sqr(Ub[jcur-jdeb]-UbTj))<Rtj) then begin
      a := 1;
      b := -2*Dep*Cos(Tfo-TfoCur);
      c := Sqr(Dep)-Sqr(Rtj);
      delta := sqr(b)-4*a*c;
      t1 := (-b-sqrt(delta))/2/a;
      t2 := (-b+sqrt(delta))/2/a;
      p_dHauUsu^[j] := Max(t1,t2)-Rcsg-HauCur;
    end else p_dHauUsu^[j] := 0.0;
    RayUse[jcur-jdeb] := Rcsg+HauCur+p_dHauUsu^[j];
    UnUse[jcur-jdeb] := RayUse[jcur-jdeb]*Cos(TfoCur);
    UbUse[jcur-jdeb] := RayUse[jcur-jdeb]*Sin(TfoCur);
  end;
  dHauMax := 0.0;
  for jcur := jdeb to jfin do begin
    j := MeshNumber(P, jcur);
    if (p_dHauUsu^[j]>dHauMax) then dHauMax:= p_dHauUsu^[j];
  end;
  if dHauMax<1E-50 then Exit;
  // Cal SecRes, LarRes
  for jcur := jdeb to jfin-1 do begin
    if ((Sqrt(Sqr(Un[jcur-jdeb]-UnTj)+Sqr(Ub[jcur-jdeb]-UbTj))<=Rtj) and
        (Sqrt(Sqr(Un[jcur+1-jdeb]-UnTj)+Sqr(Ub[jcur+1-jdeb]-UbTj))<=Rtj)) then
    begin
      LarRes := LarRes+sqrt(sqr(UnUse[jcur-jdeb]-UnUse[jcur+1-jdeb])+sqr(UbUse[jcur-jdeb]-UbUse[jcur+1-jdeb]));
      SecRes := SecRes+((RayUse[jcur-jdeb]*RayUse[jcur+1-jdeb]-RayIni[jcur-jdeb]*RayIni[jcur+1-jdeb])*sin(dTfo)/2.0);
    end else
    if ((Sqrt(Sqr(Un[jcur-jdeb]-UnTj)+Sqr(Ub[jcur-jdeb]-UbTj))<=Rtj) and
        (Sqrt(Sqr(Un[jcur+1-jdeb]-UnTj)+Sqr(Ub[jcur+1-jdeb]-UbTj))>Rtj)) then
    begin
      // jcur inside and (jcur+1) outside the Tooljoint
      LineCircleIntersection(UnTj, UbTj, Rtj, Un[jcur-jdeb], Ub[jcur-jdeb],
                                              Un[jcur+1-jdeb], Ub[jcur+1-jdeb], Un1, Ub1);
      a := RayUse[jcur-jdeb]-RayIni[jcur-jdeb];
      b := sqrt(sqr(Un[jcur-jdeb]-Un1)+sqr(Ub[jcur-jdeb]-Ub1));
      c := sqrt(sqr(Un1-UnUse[jcur-jdeb])+sqr(Ub1-UbUse[jcur-jdeb]));
      SecRes := SecRes + TriangleArea(a, b, c);
      LarRes := LarRes+c;
    end else
    if ((Sqrt(Sqr(Un[jcur-jdeb]-UnTj)+Sqr(Ub[jcur-jdeb]-UbTj))>Rtj) and
        (Sqrt(Sqr(Un[jcur+1-jdeb]-UnTj)+Sqr(Ub[jcur+1-jdeb]-UbTj))<=Rtj)) then
    begin
      // jcur outside and (jcur+1) inside the Tooljoint
      LineCircleIntersection(UnTj, UbTj, Rtj, Un[jcur+1-jdeb], Ub[jcur+1-jdeb],
                                              Un[jcur-jdeb], Ub[jcur-jdeb], Un1, Ub1);
      a := RayUse[jcur+1-jdeb]-RayIni[jcur+1-jdeb];
      b := sqrt(sqr(Un[jcur+1-jdeb]-Un1)+sqr(Ub[jcur+1-jdeb]-Ub1));
      c := sqrt(sqr(Un1-UnUse[jcur+1-jdeb])+sqr(Ub1-UbUse[jcur+1-jdeb]));
      SecRes := SecRes + TriangleArea(a, b, c);
      LarRes := LarRes+c;
    end;
    // WriteTestSection('C:\HaDN_Data\DrillScanWork\Wellscan\Debug\CasingWearTest\TestCalSection\TEST.txt',
    //                     SecRes, LarRes);
  end;
  setlength(Un, 0);
  setlength(Ub, 0);
  setlength(UnUse, 0);
  setlength(UbUse, 0);
  setlength(RayIni, 0);
  setlength(RayUse, 0);
end;

procedure CalCulIncrementUsure(WearModel : Integer;
                               WearFactor, DsnModA, DsnModB, DsnModC : Double;
                               DiaCsg : Double;
                               DiaTj, LonTj : Double;
                               NormalForce, Tfo, WorkTotal : Double;
                               P : Integer;
                               p_WearHau : t_vdd_p;
                               var dSecUse, LarUse : Double);
var
  j, Cpt : Integer;
  dHauUsu : t_vdd;
  SecCur, LarCur : Double;
  DepCur, Dep1, Dep2, dDep : Double;
  dSecCur : Double;
  Work, dWork : Double;
  iWork, nWork : Integer;
  FlagConv, FlagSort : Boolean;
  dSec1, dSec2 : Double;
  //--------------------------------------------------------------------------//
  function CalWearFactor(L, F: Double) : Double;
  var
    P : Double;
  begin
    if L<1E-6 then L := 1E-6;
    case WearModel of
      0: // Hall
      begin
        Result := WearFactor
      end;
      2: // DrillScan
      begin
        if (L>0) then begin
          P := F/L;
          if (P<=DsnModB)
          then Result := DsnModC
          else Result := DsnModC+(P-DsnModB)*DsnModA;
        end else Result := -999;
      end;
    end;
  end;
  //--------------------------------------------------------------------------//
begin
  SetLength(dHauUsu,P);

  // dWork : incr�ment du travail pour d�terminer l'incr�ment de l'usure (Mod�le DrillScan)
  // Tested 50000 --> Pas bcp d'effet || A choisir la meilleure valeur
  dWork := 200000;
  dDep := 1E-4;
  dSecUse := 0.0;

  case WearModel of
    0: nWork := 1; // Hall
    2: nWork := max(1,round(WorkTotal/dWork)); // DrillScan
  end;
  Work := WorkTotal/nWork;

  // Calcul section
  DepCur := (DiaCsg-DiaTj)/2.0;

  for iWork := 1 to nWork do // Incr�ment de l'usure
  begin
    // Point de depart
    dSecCur := 0.0;
    repeat
      Dep1 := DepCur;
      dSec1 := dSecCur;
      DepCur := DepCur+dDep;
      CalculSection(DiaCsg, DiaTj, DepCur, Tfo, P, p_WearHau^, @dHauUsu, SecCur, LarCur);
      if (SecCur>0) then dSecCur := SecCur-CalWearFactor(LarCur,NormalForce/LonTj)*Work;
    until (dSecCur>0);
    Dep2 := DepCur;
    dSec2 := dSecCur;
    // Methode des tangentes
    FlagConv := False;
    FlagSort := False;
    Cpt := 0;
    repeat
      DepCur := Dep1-dSec1/(dSec2-dSec1)*(Dep2-Dep1);
      CalculSection(DiaCsg, DiaTj, DepCur, Tfo, P, p_WearHau^, @dHauUsu, SecCur, LarCur);
      dSecCur := SecCur-CalWearFactor(LarCur,NormalForce/LonTj)*Work;
      if (Abs(dSecCur)<=1E-15) then FlagConv := True
      else begin
        Cpt := Cpt+1;
        if (Cpt>10) then FlagSort := True;
        if dSecCur>0 then begin
          Dep2 := DepCur;
          dSec2 := dSecCur;
        end else begin
          Dep1 := DepCur;
          dSec1 := dSecCur;
        end;
      end;
    until ((FlagConv)or(FlagSort));
    // Methode dichotomie
    if (not(FlagConv)) then begin
     repeat
        DepCur := (Dep1+Dep2)/2;
        CalculSection(DiaCsg, DiaTj, DepCur, Tfo, P, p_WearHau^, @dHauUsu, SecCur, LarCur);
        dSecCur := SecCur-CalWearFactor(LarCur,NormalForce/LonTj)*Work;
        if (Abs(dSecCur)<=1E-12) then FlagConv := True
        else begin
          if (dSecCur<0) then Dep1 := DepCur;
          if (dSecCur>0) then Dep2 := DepCur;
        end;
      until (FlagConv);
    end;
    // Transfert
    for j := 0 to (P-1) do p_WearHau^[j] := p_WearHau^[j]+dHauUsu[j];
    dSecUse := dSecUse+SecCur;
    LarUse := LarCur;
  end;
  // Free Memory
  SetLength(dHauUsu,0);
end;



end.
