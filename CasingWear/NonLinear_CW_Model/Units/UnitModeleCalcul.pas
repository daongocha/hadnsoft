﻿(**
Basic calculus functions (power, tan, acos, ...)
*)
unit UnitModeleCalcul;

interface

uses
  { Linked units }
  UnitModeleStructureBasic
  ;

  { liste des fonctions }
function pow(x : Double; n : integer) : Double;
function acos(x : Double) : Double;
function asin(x : Double) : Double;
function norm(x_p : t_vec_p) : Double;
function inclin(x_p : t_vec_p) : Double;
function azimut(x_p : t_vec_p) : Double;
function prosca(const x, y: t_vec): Double;
function polair(x, y : Double) : Double;
function crevec(x,y,z : Double):t_vec;
function elivec(const x, t: t_vec): t_vec;
function a2vecs(x_p, y_p : t_vec_p) : Double;
{ liste des procedures }
procedure normev(x_p : t_vec_p);
procedure normev_v2r(x_p: t_v2r_p);
procedure provec(const x, y: t_vec; var z: t_vec);
procedure difvec(x_p, y_p, z_p : t_vec_p);
procedure ajvec(x_p, y_p, z_p : t_vec_p);
procedure amultvec(a : Double ; x_p, z_p : t_vec_p);
procedure moyvec(x_p, y_p, z_p : t_vec_p);
procedure affvec(x_p, y_p : t_vec_p);
procedure cremat(x_p, y_p, z_p : t_vec_p;
                 a_p : t_mat_p);
procedure matvec(a_p : t_mat_p;
                 x_p, y_p : t_vec_p);
procedure tramat(a_p, b_p : t_mat_p);
procedure promat(a_p, b_p, c_p : t_mat_p);
procedure tangen(inc, azi, gradinc, gradazi, s : Double;
                 x_p : t_vec_p);
procedure tancal(inc, azi, gradinc, gradazi, s : Double;
                 x_p : t_vec_p);
procedure matrot(axe : integer;
                 angle : Double;
                 m_p : t_mat_p);
procedure rotmat(t_p : t_vec_p;
                 angle : Double;
                 m_p : t_mat_p);
procedure angvec(tet, phi : Double;
                 x_p : t_vec_p);

implementation

uses
  { Standards units }
  System.Classes,
  System.Math
  ;

{*------------------------------------------------------------------------------
	Returns x^n

  @param x Double
  @param n integer
-------------------------------------------------------------------------------}
function pow(x : Double; n : integer) : Double;
var
  i : integer;
  y : Double;
begin
  //  pow := IntPower(x,n);
  y := 1.0;
  for i := 1 to n do
  begin
    y := y*x;
  end;
  result := y;
end;

{*------------------------------------------------------------------------------
	Returns acos(x)

  @param x Double
-------------------------------------------------------------------------------}
function acos(x : Double) : Double;
begin
  if (x>+1) then x := +1;
  if (x<-1) then x := -1;

  if ((x>+0) and (x<+1)) then acos := pi/2-arctan(sqrt(sqr(x)/(1-sqr(x))));
  if ((x>-1) and (x<+0)) then acos := pi/2+arctan(sqrt(sqr(x)/(1-sqr(x))));
  if (x=+1) then acos := +0.0;
  if (x=+0) then acos := +pi/2;
  if (x=-1) then acos := +pi;
end;

{*------------------------------------------------------------------------------
	Returns asin(x)

  @param x Double
-------------------------------------------------------------------------------}
function asin(x : Double) : Double;
begin
//    asin := arcsin(x);
    if (x>+0) and (x<+1) then asin := +arctan(sqrt(sqr(x)/(1-sqr(x))));
    if (x>-1) and (x<+0) then asin := -arctan(sqrt(sqr(x)/(1-sqr(x))));
    if (x>=+1) then asin := +pi/2;
//    if (x=+1) then asin := +pi/2;
    if (x=+0) then asin := +0.0;
    if (x<=-1) then asin := -pi/2;
//    if (x<-1) then asin := -pi/2;
end;

{*------------------------------------------------------------------------------
	Returns norm(x)

  @param x vector
-------------------------------------------------------------------------------}
function norm(x_p : t_vec_p) : Double;
begin
  norm := sqrt(x_p^[0]*x_p^[0]+x_p^[1]*x_p^[1]+x_p^[2]*x_p^[2]);
end;

{*------------------------------------------------------------------------------
	Returns inclin(x)

  @param x vector
-------------------------------------------------------------------------------}
function inclin(x_p : t_vec_p) : Double;
begin
  inclin := acos(x_p^[0]/(norm(x_p)+1.0e-200));
end;

{*------------------------------------------------------------------------------
	Returns azimut(x)

  @param x vector
-------------------------------------------------------------------------------}
function azimut(x_p : t_vec_p) : Double;
var
  nm : Double;
begin
  nm := sqrt(x_p^[1]*x_p^[1]+x_p^[2]*x_p^[2]);
  if (nm<>0.0)
  then begin
    if (x_p^[2]>=0.0)
    then azimut := +acos(x_p^[1]/nm)
    else azimut := -acos(x_p^[1]/nm);
  end
  else azimut := 0.0;
end;

{*------------------------------------------------------------------------------
	Returns x.y

  @param x vector
  @param y vector
-------------------------------------------------------------------------------}
function prosca(const x, y: t_vec): Double;
begin
  Exit(x[0] * y[0] + x[1] * y[1] + x[2] * y[2]);
end;

{*------------------------------------------------------------------------------
	Returns polar angle of (x,y)

  @param x Double
  @param y Double
-------------------------------------------------------------------------------}
function polair(x, y : Double) : Double;
var
  nm : Double;
begin
  nm := sqrt(x*x+y*y);
  if (y>=0)
  then polair := acos(x/nm)
  else polair := 2*pi-acos(x/nm);
end;

{*------------------------------------------------------------------------------
	Returns vector with coordinates (x,y,z)

  @param x Double
  @param y Double
  @param z Double
-------------------------------------------------------------------------------}
function crevec(x,y,z : Double):t_vec;
var
  t : t_vec;
begin
  t[0]:=x;
  t[1]:=y;
  t[2]:=z;
  Result := t;
end;

{*------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
	Returns vector en éliminant le terme d'une direction d'un vecteur
  Attention : le vecteur de direction t_p doit etre norme avant |t_p| = 1
  Result := x_p - (x_p.t_p).t_p
-------------------------------------------------------------------------------}
function elivec(const x, t: t_vec): t_vec;
var
  ret: t_vec;
  sca: Double;
begin
  sca := prosca(x, t);
  ret[0] := x[0] - sca * t[0];
  ret[1] := x[1] - sca * t[1];
  ret[2] := x[2] - sca * t[2];
  Exit(ret);
end;

{*------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
	Returns angle entre deux vecteurs (en radial)
  Result := acos(x_p.y_p)
-------------------------------------------------------------------------------}
function a2vecs(x_p, y_p : t_vec_p) : Double;
var
  x, y: t_vec;
begin
  x := x_p^;
  normev(@x);
  y := y_p^;
  normev(@y);
  Exit(acos(prosca(x, y)));
end;

{ liste des procedures }
{*------------------------------------------------------------------------------
	Produces x = x/norm(x)

  @param x vector
-------------------------------------------------------------------------------}
procedure normev(x_p: t_vec_p);
var
  nm : Double;
begin
  try
    nm := norm(x_p);
    if (nm = 0) then begin
      x_p[0] := nm;
      x_p[1] := nm;
      x_p[2] := nm;
    end else begin
      x_p[0] := x_p[0]/nm;
      x_p[1] := x_p[1]/nm;
      x_p[2] := x_p[2]/nm;
    end;
  except
    raise;
  end;
end;

procedure normev_v2r(x_p: t_v2r_p);
var
  nm: Double;
begin
  try
    nm := sqrt(x_p^[0]*x_p^[0]+x_p^[1]*x_p^[1]);
    if (nm = 0) then begin
      x_p[0] := nm;
      x_p[1] := nm;
    end else begin
      x_p[0] := x_p[0]/nm;
      x_p[1] := x_p[1]/nm;
    end;
  except
    raise;
  end;
end;

{*------------------------------------------------------------------------------
	Produces z = x ^ y

  @param x vector
  @param y vector
  @param z vector
-------------------------------------------------------------------------------}
procedure provec(const x, y: t_vec; var z: t_vec);
begin
  z[0] := x[1] * y[2] - x[2] * y[1];
  z[1] := x[2] * y[0] - x[0] * y[2];
  z[2] := x[0] * y[1] - x[1] * y[0];
end;

{*------------------------------------------------------------------------------
	Produces Z = x - y

  @param x vector
  @param y vector
-------------------------------------------------------------------------------}
procedure difvec(x_p, y_p, z_p : t_vec_p);
  begin
  z_p^[0] := x_p^[0]-y_p^[0];
  z_p^[1] := x_p^[1]-y_p^[1];
  z_p^[2] := x_p^[2]-y_p^[2];
end;

{*------------------------------------------------------------------------------
	Produces Z = x + y

  @param x vector
  @param y vector
-------------------------------------------------------------------------------}
procedure ajvec(x_p, y_p, z_p : t_vec_p);
  begin
  z_p^[0] := x_p^[0]+y_p^[0];
  z_p^[1] := x_p^[1]+y_p^[1];
  z_p^[2] := x_p^[2]+y_p^[2];
end;

{*------------------------------------------------------------------------------
	Produces Z = a * x
  @param x vector
  @param a scalar
-------------------------------------------------------------------------------}
procedure amultvec(a : Double ; x_p, z_p : t_vec_p);
  begin
  z_p^[0] := a * x_p^[0];
  z_p^[1] := a * x_p^[1];
  z_p^[2] := a * x_p^[2];
end;

{*------------------------------------------------------------------------------
	Produces Z = (x + y)/2

  @param x vector
  @param y vector
-------------------------------------------------------------------------------}
procedure moyvec(x_p, y_p, z_p : t_vec_p);
  begin
  ajvec(x_p, y_p, z_p);
  amultvec(0.5, z_p, z_p);
end;

{*------------------------------------------------------------------------------
	Produces y = x

  @param x vector
  @param y vector
-------------------------------------------------------------------------------}
procedure affvec(x_p, y_p : t_vec_p);
begin
  y_p^[0] := x_p^[0];
  y_p^[1] := x_p^[1];
  y_p^[2] := x_p^[2];
end;

{*------------------------------------------------------------------------------
	Produces a = | x y z |

  @param x vector
  @param y vector
  @param z vector
  @param a matrix
-------------------------------------------------------------------------------}
procedure cremat(x_p, y_p, z_p : t_vec_p;
                 a_p : t_mat_p);
var
  i : integer;
begin
  for i := 0 to 3-1 do
  begin
    a_p^[i][0] := x_p^[i];
    a_p^[i][1] := y_p^[i];
    a_p^[i][2] := z_p^[i];
  end;
end;

{*------------------------------------------------------------------------------
	Produces y = a.x

  @param a matrix
  @param x vector
  @param y vector
-------------------------------------------------------------------------------}
procedure matvec(a_p : t_mat_p;
                 x_p, y_p : t_vec_p);
var
  i, j : integer;
begin
  for i := 0 to 3-1 do
  begin
    y_p^[i] := 0.0;
    for j := 0 to 3-1 do
    begin
      y_p^[i] := y_p^[i]+a_p^[i][j]*x_p^[j];
    end;
  end;
end;

{*------------------------------------------------------------------------------
	Produces b = |a|T

  @param a matrix
  @param b matrix
-------------------------------------------------------------------------------}
procedure tramat(a_p, b_p : t_mat_p);
var
  i, j : integer;
begin
  for i := 0 to 3-1 do
  begin
    for j := 0 to 3-1 do
    begin
      b_p^[i][j] := a_p^[j][i];
    end;
  end;
end;

{*------------------------------------------------------------------------------
	Produces c = a.b

  @param a matrix
  @param b matrix
  @param c matrix
-------------------------------------------------------------------------------}
procedure promat(a_p, b_p, c_p : t_mat_p);
var
  i, j, k : integer;
begin
  for i := 0 to 3-1 do
  begin
    for j := 0 to 3-1 do
    begin
      c_p^[i][j] := 0.0;
      for k := 0 to 3-1 do
      begin
        c_p^[i][j] := c_p^[i][j]+a_p^[i][k]*b_p^[k][j];
      end;
    end;
  end;
end;

{*------------------------------------------------------------------------------
	Produces x tangent vector at inc+gradinc.s of inclination &
           azi+gradazi.s of azimuth following linear regresion

  @param inc Double
  @param azi Double
  @param gradinc Double
  @param gradazi Double
  @param s Double
  @param x vector
-------------------------------------------------------------------------------}
procedure tangen(inc, azi, gradinc, gradazi, s : Double;
                 x_p : t_vec_p);
begin
  x_p^[0] := cos(inc+s*gradinc);
  x_p^[1] := sin(inc+s*gradinc)*cos(azi+s*gradazi);
  x_p^[2] := sin(inc+s*gradinc)*sin(azi+s*gradazi);
end;

{*------------------------------------------------------------------------------
	Produces x tangent vector at inc+gradinc.s of inclination &
           azi+gradazi.s of azimuth following arc defined at 10 m

  @param inc Double
  @param azi Double
  @param gradinc Double
  @param gradazi Double
  @param s Double
  @param x vector
-------------------------------------------------------------------------------}
procedure tancal(inc, azi, gradinc, gradazi, s : Double;
                 x_p : t_vec_p);
var
  ang : Double;
  ti, tf, tn : t_vec;
  m_rot : t_mat;
begin
  tangen(inc,azi,gradinc,gradazi,-10,@ti);
  tangen(inc,azi,gradinc,gradazi,0.0,@tf);
  ang := acos(prosca(ti, tf));
  if (ang>0.0)
  then begin
    provec(ti, tf, tn);
    rotmat(@tn,ang/10*s,@m_rot);
    matvec(@m_rot,@tf,x_p);
  end
  else begin
    x_p[0] := tf[0];
    x_p[1] := tf[1];
    x_p[2] := tf[2];
  end;
end;

{*------------------------------------------------------------------------------
	Produces m rotation matrix around reference axis of angle

  @param axe integer (1 for x axis; 2 for Y axis; 3 for Z axis)
  @param angle Double
  @param m matrix
-------------------------------------------------------------------------------}
procedure matrot(axe : integer;
                 angle : Double;
                 m_p : t_mat_p);
begin
  case axe of
    1:
    begin
      m_p^[0][0] := 1.0;
      m_p^[1][0] := 0.0;
      m_p^[2][0] := 0.0;
      m_p^[0][1] := 0.0;
      m_p^[1][1] := +cos(angle);
      m_p^[2][1] := +sin(angle);
      m_p^[0][2] := 0.0;
      m_p^[1][2] := -sin(angle);
      m_p^[2][2] := +cos(angle);
    end;
    2:
    begin
      m_p^[0][0] := +cos(angle);
      m_p^[1][0] := 0.0;
      m_p^[2][0] := -sin(angle);
      m_p^[0][1] := 0.0;
      m_p^[1][1] := 1.0;
      m_p^[2][1] := 0.0;
      m_p^[0][2] := +sin(angle);
      m_p^[1][2] := 0.0;
      m_p^[2][2] := +cos(angle);
    end;
    3:
    begin
      m_p^[0][0] := +cos(angle);
      m_p^[1][0] := +sin(angle);
      m_p^[2][0] := 0.0;
      m_p^[0][1] := -sin(angle);
      m_p^[1][1] := +cos(angle);
      m_p^[2][1] := 0.0;
      m_p^[0][2] := 0.0;
      m_p^[1][2] := 0.0;
      m_p^[2][2] := 1.0;
    end;
  end;
end;

{*------------------------------------------------------------------------------
	Produces m rotation matrix around t vector of angle

  @param t vector
  @param angle Double
  @param m matrix
-------------------------------------------------------------------------------}
procedure rotmat(t_p : t_vec_p;
                 angle : Double;
                 m_p : t_mat_p);
var
  k, n, b : t_vec;
  rep_rg, rep_gr : t_mat;
  m_rot, m_tpm : t_mat;
begin
  normev(t_p);
  k[0] := 1.0;
  k[1] := 0.0;
  k[2] := 0.0;
  provec(k,t_p^,n);
  if (norm(@n)<=1.0e-15) then begin
    n[0] := 0.0;
    n[1] := 1.0;
    n[2] := 0.0;
  end;
  normev(@n);
  provec(t_p^, n, b);
  normev(@b);
  cremat(t_p,@n,@b,@rep_rg);
  tramat(@rep_rg,@rep_gr);
  m_rot[0][0] := 1.0;
  m_rot[1][0] := 0.0;
  m_rot[2][0] := 0.0;
  m_rot[0][1] := 0.0;
  m_rot[1][1] := +cos(angle);
  m_rot[2][1] := +sin(angle);
  m_rot[0][2] := 0.0;
  m_rot[1][2] := -sin(angle);
  m_rot[2][2] := +cos(angle);
  promat(@m_rot,@rep_gr,@m_tpm);
  promat(@rep_rg,@m_tpm,m_p);
end;

{*------------------------------------------------------------------------------
	Produces x vector tet inclination and phi azimuth in global system of axis

  @param tet Double
  @param azi Double
  @param x_p vector
-------------------------------------------------------------------------------}
procedure angvec(tet, phi : Double;
                 x_p : t_vec_p);
begin
  x_p^[0] := cos(tet);
  x_p^[1] := sin(tet)*cos(phi);
  x_p^[2] := sin(tet)*sin(phi);
end;

end.
