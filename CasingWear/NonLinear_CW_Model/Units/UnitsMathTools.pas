{-------------------------------------------------------------------------------
  Computation Math Tools
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Create: 01/01/2015
-------------------------------------------------------------------------------}
unit UnitsMathTools;

interface

uses
	Windows, SysUtils, Classes, Math,
  UnitModeleStructureBasic;

type
  TRegOrdre2Res = record
    b0 : Double;
    b1 : Double;
    b2 : Double;
    SST : Double;
    SSR : Double;
    SSE : Double;
    Rsqua : Double;
  end;
  PRegOrdre2Res = ^TRegOrdre2Res;

//----------------------------------------------------------------------------//
//----------------------------------------------------------------------------//

{ liste des procedures }
function pow(x : Double; n : integer) : Double;
function acos(x : Double) : Double;
function asin(x : Double) : Double;
function norm(x_p : t_vec_p) : Double;
function inclin(x_p : t_vec_p) : Double;
function azimut(x_p : t_vec_p) : Double;
function prosca(x_p, y_p : t_vec_p) : Double;
function polair(x, y : Double) : Double;
function crevec(x,y,z : Double):t_vec;
function elivec(x_p, t_p : t_vec_p):t_vec;
function a2vecs(x_p, y_p : t_vec_p) : Double;
procedure normev(x_p : t_vec_p);
procedure provec(x_p, y_p, z_p : t_vec_p);
procedure difvec(x_p, y_p, z_p : t_vec_p);
procedure ajvec(x_p, y_p, z_p : t_vec_p);
procedure amultvec(a : Double ; x_p, z_p : t_vec_p);
procedure moyvec(x_p, y_p, z_p : t_vec_p);
procedure affvec(x_p, y_p : t_vec_p);
procedure cremat(x_p, y_p, z_p : t_vec_p;
                 a_p : t_mat_p);
procedure matvec(a_p : t_mat_p;
                 x_p, y_p : t_vec_p);
procedure tramat(a_p, b_p : t_mat_p);
procedure promat(a_p, b_p, c_p : t_mat_p);
procedure rotmat(t_p : t_vec_p; angle : Double; m_p : t_mat_p);
procedure rotmatnew(t : t_vec; angle : Double; var m_rot : t_mat);
// Matrix 3x3
function CalDeterminantMatrix3x3 (a_p : t_mat_p): Double;
function CalDeterminantMatrix3x3New (a : t_vdd): Double;
function CalInverseMatrix3x3 (a_p, b_p : t_mat_p): Boolean;
function CalInverseMatrix3x3New (a : t_vdd; var b : t_vdd): Boolean;
// Matrix
procedure TranposeMatrix (a : t_vdd; am, an : Integer;
                          var aT : t_vdd; var aTm, aTn : Integer);
procedure ProductMatrix (a : t_vdd; am, an : Integer;
                         b : t_vdd; bm, bn : Integer;
                         var ab : t_vdd; var abm, abn : Integer);
procedure AddMatrix (a : t_vdd; am, an : Integer;
                     b : t_vdd; bm, bn : Integer;
                     var aab : t_vdd; var aabm, aabn : Integer);
procedure MultNumberMatrix (Factor : Double;
                            a : t_vdd; am, an : Integer;
                            var b : t_vdd; var bm, bn : Integer);
procedure ExtractDiagMatrix (a : t_vdd; am, an : Integer;
                             var b : t_vdd; var bm, bn : Integer);
PROCEDURE gaussj_t_vdd(VAR a: t_vdd; n: integer;
                       VAR b: t_vdd; m: integer);
PROCEDURE gaussj_InvertMat_t_vdd(aini: t_vdd; n: integer;
                                 VAR a: t_vdd;
                                 var ier : Integer);
function MatrixABackSlashB (a : t_vdd; am, an : Integer;
                            b : t_vdd; bm : integer;
                            var x : t_vdd): Boolean;
function MaxValueMatrixArray (a : t_vdd): Double;


// Vector n elements
function enormvec(n : integer; x : t_vdd): Double;

// Regression
function Regression_Polynomiale_Ordre2(n : Integer;
                                       x, y : t_vdd;
                                       var b0, b1, b2 : Double;
                                       var SST, SSR, SSE, Rsqua : Double;
                                       var y_Reg, y_derivee : t_vdd) : boolean;
function Regression_Linear(n : Integer;
                           X, Y : t_vdd;
                           var A, B : Double;
                           var SST, SSR, SSE, Rsqua : Double) : boolean;
function Curve_Fitting_CasingWear_GaussNewton(n : Integer;
                                              X, Y : t_vdd;
                                              Ai, Bi, Ci : Double;
                                              var A, B, C : Double;
                                              var SST, SSR, SSE, Rsqua : Double;
                                              var Y_Reg : t_vdd) : boolean;
function Curve_Fitting_CasingWear_LevenMarqua(n : Integer;
                                               X, Y : t_vdd;
                                               Ai, Bi, Ci : Double;
                                               var A, B, C : Double;
                                               var SST, SSR, SSE, Rsqua : Double;
                                               var Y_Reg : t_vdd) : boolean;

// Numerical Integration
procedure LectureGaussFormula(NomFic: string; var x, Weight : t_vdd);
procedure GaussFormula96Points(var x, Weight : t_vdd);

// TEST TEST TEST
procedure WriteMatrix(NomFic: string; MatA : t_vdd; am, an : Integer);


implementation

{*------------------------------------------------------------------------------
	Returns x^n

  @param x Double
  @param n integer
-------------------------------------------------------------------------------}
function pow(x : Double; n : integer) : Double;
var
  i : integer;
  y : Double;
begin
  y := 1.0;
  for i := 1 to n do
  begin
    y := y*x;
  end;
  result := y;
end;


{*------------------------------------------------------------------------------
	Returns acos(x)

  @param x Double
-------------------------------------------------------------------------------}
function acos(x : Double) : Double;
begin
  if (x>+1) then x := +1;
  if (x<-1) then x := -1;

  if ((x>+0) and (x<+1)) then acos := pi/2-arctan(sqrt(sqr(x)/(1-sqr(x))));
  if ((x>-1) and (x<+0)) then acos := pi/2+arctan(sqrt(sqr(x)/(1-sqr(x))));
  if (x=+1) then acos := +0.0;
  if (x=+0) then acos := +pi/2;
  if (x=-1) then acos := +pi;
end;

{*------------------------------------------------------------------------------
	Returns asin(x)

  @param x Double
-------------------------------------------------------------------------------}
function asin(x : Double) : Double;
begin
//    asin := arcsin(x);
    if (x>+0) and (x<+1) then asin := +arctan(sqrt(sqr(x)/(1-sqr(x))));
    if (x>-1) and (x<+0) then asin := -arctan(sqrt(sqr(x)/(1-sqr(x))));
    // christophe 2010.11.05
    if (x>=+1) then asin := +pi/2;
//    if (x=+1) then asin := +pi/2;
    if (x=+0) then asin := +0.0;
    // christophe 2010.11.05
    if (x<=-1) then asin := -pi/2;
//    if (x<-1) then asin := -pi/2;
end;

{*------------------------------------------------------------------------------
	Returns norm(x)

  @param x vector
-------------------------------------------------------------------------------}
function norm(x_p : t_vec_p) : Double;
begin
  norm := sqrt(x_p^[0]*x_p^[0]+x_p^[1]*x_p^[1]+x_p^[2]*x_p^[2]);
end;

{*------------------------------------------------------------------------------
	Returns inclin(x)

  @param x vector
-------------------------------------------------------------------------------}
function inclin(x_p : t_vec_p) : Double;
begin
  inclin := acos(x_p^[0]/(norm(x_p)+1.0e-200));
end;

{*------------------------------------------------------------------------------
	Returns azimut(x)

  @param x vector
-------------------------------------------------------------------------------}
function azimut(x_p : t_vec_p) : Double;
var
  nm : Double;
begin
  nm := sqrt(x_p^[1]*x_p^[1]+x_p^[2]*x_p^[2]);
  if (nm<>0.0)
  then begin
    if (x_p^[2]>=0.0)
    then azimut := +acos(x_p^[1]/nm)
    else azimut := -acos(x_p^[1]/nm);
  end
  else azimut := 0.0;
end;

{*------------------------------------------------------------------------------
	Returns x.y

  @param x vector
  @param y vector
-------------------------------------------------------------------------------}
function prosca(x_p, y_p : t_vec_p) : Double;
begin
  prosca := x_p^[0]*y_p^[0]+x_p^[1]*y_p^[1]+x_p^[2]*y_p^[2];
end;

{*------------------------------------------------------------------------------
	Returns polar angle of (x,y)

  @param x Double
  @param y Double
-------------------------------------------------------------------------------}
function polair(x, y : Double) : Double;
var
  nm : Double;
begin
  nm := sqrt(x*x+y*y);
  if (y>=0)
  then polair := acos(x/nm)
  else polair := 2*pi-acos(x/nm);
end;

{*------------------------------------------------------------------------------
	Returns vector with coordinates (x,y,z)

  @param x Double
  @param y Double
  @param z Double
-------------------------------------------------------------------------------}
function crevec(x,y,z : Double):t_vec;
var
  t : t_vec;
begin
  t[0]:=x;
  t[1]:=y;
  t[2]:=z;
  Result := t;
end;

{*------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
	Returns vector en �liminant le terme d'une direction d'un vecteur
  Result := x_p - (x_p.t_p).t_p
-------------------------------------------------------------------------------}
function elivec(x_p, t_p : t_vec_p):t_vec;
var
  t : t_vec;
  sca : Double;
begin
  sca := prosca(x_p, t_p);
  t[0]:=x_p^[0]-sca*t_p^[0];
  t[1]:=x_p^[1]-sca*t_p^[1];
  t[2]:=x_p^[2]-sca*t_p^[2];
  Result := t;
end;

{*------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
	Returns angle entre deux vecteurs (en radial)
  Result := acos(x_p.y_p)
-------------------------------------------------------------------------------}
function a2vecs(x_p, y_p : t_vec_p) : Double;
var
  x, y : t_vec;
  Ang : Double;
begin
  x := x_p^; normev(@x);
  y := y_p^; normev(@y);
  Ang := acos(prosca(@x,@y));
  Result := Ang;
end;

{ liste des procedures }
{*------------------------------------------------------------------------------
	Produces x = x/norm(x)

  @param x vector
-------------------------------------------------------------------------------}
procedure normev(x_p : t_vec_p);
var
  nm : Double;
begin
  try
    nm := norm(x_p);
    if (nm = 0) then begin
      x_p^[0] := nm;
      x_p^[1] := nm;
      x_p^[2] := nm;
    end else begin
      x_p^[0] := x_p^[0]/nm;
      x_p^[1] := x_p^[1]/nm;
      x_p^[2] := x_p^[2]/nm;
    end;
  except
    Raise;
  end;
end;

{*------------------------------------------------------------------------------
	Produces z = x ^ y

  @param x vector
  @param y vector
  @param z vector
-------------------------------------------------------------------------------}
procedure provec(x_p, y_p, z_p : t_vec_p);
begin
  z_p^[0] := x_p^[1]*y_p^[2]-x_p^[2]*y_p^[1];
  z_p^[1] := x_p^[2]*y_p^[0]-x_p^[0]*y_p^[2];
  z_p^[2] := x_p^[0]*y_p^[1]-x_p^[1]*y_p^[0];
end;

{*------------------------------------------------------------------------------
	Produces Z = x - y

  @param x vector
  @param y vector
-------------------------------------------------------------------------------}
procedure difvec(x_p, y_p, z_p : t_vec_p);
  begin
  z_p^[0] := x_p^[0]-y_p^[0];
  z_p^[1] := x_p^[1]-y_p^[1];
  z_p^[2] := x_p^[2]-y_p^[2];
end;

{*------------------------------------------------------------------------------
	Produces Z = x + y

  @param x vector
  @param y vector
-------------------------------------------------------------------------------}
procedure ajvec(x_p, y_p, z_p : t_vec_p);
  begin
  z_p^[0] := x_p^[0]+y_p^[0];
  z_p^[1] := x_p^[1]+y_p^[1];
  z_p^[2] := x_p^[2]+y_p^[2];
end;

{*------------------------------------------------------------------------------
	Produces Z = a * x
  @param x vector
  @param a scalar
-------------------------------------------------------------------------------}
procedure amultvec(a : Double ; x_p, z_p : t_vec_p);
  begin
  z_p^[0] := a * x_p^[0];
  z_p^[1] := a * x_p^[1];
  z_p^[2] := a * x_p^[2];
end;

{*------------------------------------------------------------------------------
	Produces Z = (x + y)/2

  @param x vector
  @param y vector
-------------------------------------------------------------------------------}
procedure moyvec(x_p, y_p, z_p : t_vec_p);
  begin
  ajvec(x_p, y_p, z_p);
  amultvec(0.5, z_p, z_p);
end;

{*------------------------------------------------------------------------------
	Produces y = x

  @param x vector
  @param y vector
-------------------------------------------------------------------------------}
procedure affvec(x_p, y_p : t_vec_p);
begin
  y_p^[0] := x_p^[0];
  y_p^[1] := x_p^[1];
  y_p^[2] := x_p^[2];
end;

{*------------------------------------------------------------------------------
	Produces a = | x y z |

  @param x vector
  @param y vector
  @param z vector
  @param a matrix
-------------------------------------------------------------------------------}
procedure cremat(x_p, y_p, z_p : t_vec_p;
                 a_p : t_mat_p);
var
  i : integer;
begin
  for i := 0 to 3-1 do
  begin
    a_p^[i][0] := x_p^[i];
    a_p^[i][1] := y_p^[i];
    a_p^[i][2] := z_p^[i];
  end;
end;

{*------------------------------------------------------------------------------
	Produces y = a.x

  @param a matrix
  @param x vector
  @param y vector
-------------------------------------------------------------------------------}
procedure matvec(a_p : t_mat_p;
                 x_p, y_p : t_vec_p);
var
  i, j : integer;
begin
  for i := 0 to 3-1 do
  begin
    y_p^[i] := 0.0;
    for j := 0 to 3-1 do
    begin
      y_p^[i] := y_p^[i]+a_p^[i][j]*x_p^[j];
    end;
  end;
end;

{*------------------------------------------------------------------------------
	Produces b = |a|T

  @param a matrix
  @param b matrix
-------------------------------------------------------------------------------}
procedure tramat(a_p, b_p : t_mat_p);
var
  i, j : integer;
begin
  for i := 0 to 3-1 do
  begin
    for j := 0 to 3-1 do
    begin
      b_p^[i][j] := a_p^[j][i];
    end;
  end;
end;

{*------------------------------------------------------------------------------
	Produces c = a.b

  @param a matrix
  @param b matrix
  @param c matrix
-------------------------------------------------------------------------------}
procedure promat(a_p, b_p, c_p : t_mat_p);
var
  i, j, k : integer;
begin
  for i := 0 to 3-1 do
  begin
    for j := 0 to 3-1 do
    begin
      c_p^[i][j] := 0.0;
      for k := 0 to 3-1 do
      begin
        c_p^[i][j] := c_p^[i][j]+a_p^[i][k]*b_p^[k][j];
      end;
    end;
  end;
end;

{*------------------------------------------------------------------------------
	Produces m rotation matrix around t vector of angle

  @param t vector
  @param angle Double
  @param m matrix
-------------------------------------------------------------------------------}
procedure rotmat(t_p : t_vec_p; angle : Double; m_p : t_mat_p);
var
  k, n, b : t_vec;
  rep_rg, rep_gr : t_mat;
  m_rot, m_tpm : t_mat;
begin
  normev(t_p);
  k[0] := 1.0;
  k[1] := 0.0;
  k[2] := 0.0;
  provec(@k,t_p,@n);
  if (norm(@n)<=1.0e-15) then begin
    n[0] := 0.0;
    n[1] := 1.0;
    n[2] := 0.0;
  end;
  normev(@n);
  provec(t_p,@n,@b);
  normev(@b);
  cremat(t_p,@n,@b,@rep_rg);
  tramat(@rep_rg,@rep_gr);
  m_rot[0][0] := 1.0;
  m_rot[1][0] := 0.0;
  m_rot[2][0] := 0.0;
  m_rot[0][1] := 0.0;
  m_rot[1][1] := +cos(angle);
  m_rot[2][1] := +sin(angle);
  m_rot[0][2] := 0.0;
  m_rot[1][2] := -sin(angle);
  m_rot[2][2] := +cos(angle);
  promat(@m_rot,@rep_gr,@m_tpm);
  promat(@rep_rg,@m_tpm,m_p);
end;

{*------------------------------------------------------------------------------
	Produces m rotation matrix around t vector of angle

  Source : Wikipedia

  @param t vector
  @param angle Double
  @param m_rot rotation matrix
-------------------------------------------------------------------------------}
procedure rotmatnew(t : t_vec; angle : Double; var m_rot : t_mat);
var
  tu : t_vec;
begin
  tu := t;
  normev(@tu);
  m_rot[0][0] := cos(angle) + sqr(tu[0])*(1-cos(angle));
  m_rot[1][0] := tu[0]*tu[1]*(1-cos(angle)) + tu[2]*sin(angle);
  m_rot[2][0] := tu[0]*tu[2]*(1-cos(angle)) - tu[1]*sin(angle);
  m_rot[0][1] := tu[0]*tu[1]*(1-cos(angle)) - tu[2]*sin(angle);
  m_rot[1][1] := cos(angle) + sqr(tu[1])*(1-cos(angle));
  m_rot[2][1] := tu[2]*tu[1]*(1-cos(angle)) + tu[0]*sin(angle);
  m_rot[0][2] := tu[0]*tu[2]*(1-cos(angle)) + tu[1]*sin(angle);
  m_rot[1][2] := tu[2]*tu[1]*(1-cos(angle)) - tu[0]*sin(angle);
  m_rot[2][2] := cos(angle) + sqr(tu[2])*(1-cos(angle));
end;
{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
function CalDeterminantMatrix3x3 (a_p : t_mat_p): Double;
begin
  Result :=   a_p^[0,0]*a_p^[1,1]*a_p^[2,2] - a_p^[0,0]*a_p^[1,2]*a_p^[2,1]
            - a_p^[0,1]*a_p^[1,0]*a_p^[2,2] + a_p^[0,1]*a_p^[1,2]*a_p^[2,0]
            + a_p^[0,2]*a_p^[1,0]*a_p^[2,1] - a_p^[0,2]*a_p^[1,1]*a_p^[2,0];

end;
//------------------------------------------------------------------------------


{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Matrix by t_vdd
-------------------------------------------------------------------------------}
function CalDeterminantMatrix3x3New (a : t_vdd): Double;
begin
  Result :=   a[0*3+0]*a[1*3+1]*a[2*3+2] - a[0*3+0]*a[1*3+2]*a[2*3+1]
            - a[0*3+1]*a[1*3+0]*a[2*3+2] + a[0*3+1]*a[1*3+2]*a[2*3+0]
            + a[0*3+2]*a[1*3+0]*a[2*3+1] - a[0*3+2]*a[1*3+1]*a[2*3+0];
end;
//------------------------------------------------------------------------------

{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
function CalInverseMatrix3x3 (a_p, b_p : t_mat_p): Boolean;
var
  i, j : ShortInt;
  DetA : Double;
begin
  DetA := CalDeterminantMatrix3x3 (a_p);
  if (DetA = 0) then begin
    for i := 0 to 2 do
      for j := 0 to 2 do b_p^[i,j] := 0.0;
    Result := False;
  end else
  begin
    b_p^[0,0] :=  (a_p^[1,1]*a_p^[2,2]-a_p^[1,2]*a_p^[2,1])/DetA;
    b_p^[0,1] := (-a_p^[0,1]*a_p^[2,2]+a_p^[0,2]*a_p^[2,1])/DetA;
    b_p^[0,2] :=  (a_p^[0,1]*a_p^[1,2]-a_p^[0,2]*a_p^[1,1])/DetA;
    b_p^[1,0] := (-a_p^[1,0]*a_p^[2,2]+a_p^[1,2]*a_p^[2,0])/DetA;
    b_p^[1,1] :=  (a_p^[0,0]*a_p^[2,2]-a_p^[0,2]*a_p^[2,0])/DetA;
    b_p^[1,2] := (-a_p^[0,0]*a_p^[1,2]+a_p^[0,2]*a_p^[1,0])/DetA;
    b_p^[2,0] :=  (a_p^[1,0]*a_p^[2,1]-a_p^[1,1]*a_p^[2,0])/DetA;
    b_p^[2,1] := (-a_p^[0,0]*a_p^[2,1]+a_p^[0,1]*a_p^[2,0])/DetA;
    b_p^[2,2] :=  (a_p^[0,0]*a_p^[1,1]-a_p^[0,1]*a_p^[1,0])/DetA;
    Result := True;
  end;
end;
//------------------------------------------------------------------------------


{-------------------------------------------------------------------------------
  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Matrix by t_vdd
-------------------------------------------------------------------------------}
function CalInverseMatrix3x3New (a : t_vdd; var b : t_vdd): Boolean;
var
  i, j : ShortInt;
  DetA : Double;
begin
  setlength(b, 3*3);
  DetA := CalDeterminantMatrix3x3New (a);
  if (DetA = 0) then begin
    for i := 0 to 2 do
      for j := 0 to 2 do b[i*3+j] := 0.0;
    Result := False;
  end else
  begin
    b[0*3+0] :=  (a[1*3+1]*a[2*3+2]-a[1*3+2]*a[2*3+1])/DetA;
    b[0*3+1] := (-a[0*3+1]*a[2*3+2]+a[0*3+2]*a[2*3+1])/DetA;
    b[0*3+2] :=  (a[0*3+1]*a[1*3+2]-a[0*3+2]*a[1*3+1])/DetA;
    b[1*3+0] := (-a[1*3+0]*a[2*3+2]+a[1*3+2]*a[2*3+0])/DetA;
    b[1*3+1] :=  (a[0*3+0]*a[2*3+2]-a[0*3+2]*a[2*3+0])/DetA;
    b[1*3+2] := (-a[0*3+0]*a[1*3+2]+a[0*3+2]*a[1*3+0])/DetA;
    b[2*3+0] :=  (a[1*3+0]*a[2*3+1]-a[1*3+1]*a[2*3+0])/DetA;
    b[2*3+1] := (-a[0*3+0]*a[2*3+1]+a[0*3+1]*a[2*3+0])/DetA;
    b[2*3+2] :=  (a[0*3+0]*a[1*3+1]-a[0*3+1]*a[1*3+0])/DetA;
    Result := True;
  end;
end;
//------------------------------------------------------------------------------


{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
procedure TranposeMatrix (a : t_vdd; am, an : Integer;
                          var aT : t_vdd; var aTm, aTn : Integer);
var
  i, j : Integer;
begin
  aTm := an;
  aTn := am;
  setlength(aT, am*an);
  for i := 0 to am-1 do
    for j := 0 to an-1 do aT[j*am+i] := a[i*an+j];
end;
//------------------------------------------------------------------------------



{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  [aab] = [a]+[b]
-------------------------------------------------------------------------------}
procedure AddMatrix (a : t_vdd; am, an : Integer;
                     b : t_vdd; bm, bn : Integer;
                     var aab : t_vdd; var aabm, aabn : Integer);
var
  i, j : Integer;
begin
  if ((am<>bm) or (an<>bn)) then
  begin
    setlength(aab, 0);
    aabm := -999;
    aabn := -999;
    exit;
  end;
  aabm := am;
  aabn := an;
  setlength(aab, am*an);
  // Adding
  for i := 0 to am-1 do
    for j := 0 to an-1 do aab[i*an+j] := a[i*an+j]+b[i*an+j];
end;
//------------------------------------------------------------------------------


{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  [b] = Factor*[a]
-------------------------------------------------------------------------------}
procedure MultNumberMatrix (Factor : Double;
                            a : t_vdd; am, an : Integer;
                            var b : t_vdd; var bm, bn : Integer);
var
  i, j, k : Integer;
begin
  bm := am;
  bn := an;
  setlength(b, am*an);
  for i := 0 to am-1 do
    for j := 0 to an-1 do b[i*an+j] := Factor*a[i*an+j];
end;
//------------------------------------------------------------------------------


{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
procedure ExtractDiagMatrix (a : t_vdd; am, an : Integer;
                             var b : t_vdd; var bm, bn : Integer);
var
  i, j, k : Integer;
begin
  if (am<>an) then
  begin
    setlength(b, 0);
    bm := -999;
    bn := -999;
    exit;
  end;

  bm := am;
  bn := an;
  setlength(b, am*an);
  for i := 0 to am-1 do
    for j := 0 to an-1 do
    begin
      if (i=j) then b[i*an+j] := a[i*an+j]
      else b[i*an+j] := 0.0;
    end;
end;
//------------------------------------------------------------------------------

{*******************************************************************************
*  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>                            *
*  Source : Numerical Recipes CD - Modification by Ngoc Ha DAO                 *
*                                                                              *
*  Gauss-Jordan method usingfull pivoting at each step. During the process,    *
*  original a and b matrices are destroyed to spare storage location.          *
* ---------------------------------------------------------------------------- *
*  Inputs:                                                                     *
*            a   : matrix n*n                                                  *
*            b   : matrix n*m                                                  *
* ---------------------------------------------------------------------------- *
*  Outputs:                                                                    *
*            aa   : inverse of aa                                              *
* ---------------------------------------------------------------------------- *
*  Note:     if m=0 inversion of aa matrix only (bb is not used here).         *
*******************************************************************************}
PROCEDURE gaussj_t_vdd(VAR a: t_vdd; n: integer;
                       VAR b: t_vdd; m: integer);
VAR
   big, dum, pivinv: Double;
   i, icol, irow, j, k, l, ll: integer;
   indxc, indxr, ipiv: t_vdi_p;
BEGIN
   new(indxc);
   new(indxr);
   new(ipiv);
   setlength(indxc^, n);
   setlength(indxr^, n);
   setlength(ipiv^, n);
   FOR j := 1 TO n DO ipiv^[j-1] := 0;
   FOR i := 1 TO n DO BEGIN
      big := 0.0;
      FOR j := 1 TO n DO
         IF ipiv^[j-1] <> 1 THEN
            FOR k := 1 TO n DO
               IF ipiv^[k-1] = 0 THEN
                  IF abs(a[(j-1)*n+(k-1)]) >= big THEN BEGIN
                     big := abs(a[(j-1)*n+(k-1)]);
                     irow := j;
                     icol := k
                  END
               ELSE IF ipiv^[k-1] > 1 THEN BEGIN
                  // writeln('pause 1 in GAUSSJ - singular matrix');
                  // readln
               END;
      ipiv^[icol-1] := ipiv^[icol-1]+1;
      IF irow <> icol THEN BEGIN
         FOR l := 1 TO n DO BEGIN
            dum := a[(irow-1)*n+(l-1)];
            a[(irow-1)*n+(l-1)] := a[(icol-1)*n+(l-1)];
            a[(icol-1)*n+(l-1)] := dum
         END;
         FOR l := 1 TO m DO BEGIN
            dum := b[(irow-1)*m+(l-1)];
            b[(irow-1)*m+(l-1)] := b[(icol-1)*m+(l-1)];
            b[(icol-1)*m+(l-1)] := dum
         END
      END;
      indxr^[i-1] := irow;
      indxc^[i-1] := icol;
      IF a[(icol-1)*n+(icol-1)] = 0.0 THEN BEGIN
         // writeln('pause 2 in GAUSSJ - singular matrix');
         // readln
      END;
      pivinv := 1.0/a[(icol-1)*n+(icol-1)];
      a[(icol-1)*n+(icol-1)] := 1.0;
      FOR l := 1 TO n DO
         a[(icol-1)*n+(l-1)] := a[(icol-1)*n+(l-1)]*pivinv;
      FOR l := 1 TO m DO
         b[(icol-1)*m+(l-1)] := b[(icol-1)*m+(l-1)]*pivinv;
      FOR ll := 1 TO n DO
         IF ll <> icol THEN BEGIN
            dum := a[(ll-1)*n+(icol-1)];
            a[(ll-1)*n+(icol-1)] := 0.0;
            FOR l := 1 TO n DO
               a[(ll-1)*n+(l-1)] := a[(ll-1)*n+(l-1)]-a[(icol-1)*n+(l-1)]*dum;
            FOR l := 1 TO m DO
               b[(ll-1)*m+(l-1)] := b[(ll-1)*m+(l-1)]-b[(icol-1)*m+(l-1)]*dum
         END
   END;
   FOR l := n DOWNTO 1 DO
      IF indxr^[l-1] <> indxc^[l-1] THEN
         FOR k := 1 TO n DO BEGIN
            dum := a[(k-1)*n+(indxr^[l-1]-1)];
            a[(k-1)*n+(indxr^[l-1]-1)] := a[(k-1)*n+(indxc^[l-1]-1)];
            a[(k-1)*n+(indxc^[l-1]-1)] := dum
         END;
   setlength(indxc^, 0);
   setlength(indxr^, 0);
   setlength(ipiv^, 0);
   dispose(ipiv);
   dispose(indxr);
   dispose(indxc)
END;


{*******************************************************************************
*  @contact Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>                            *
*  Source : Numerical Recipes CD - Modification by Ngoc Ha DAO                 *
*                                                                              *
*  Inversion of a Double square matrix by Gauss-Jordan method usingfull          *
*  pivoting at each step.                                                      *
* ---------------------------------------------------------------------------- *
*  Inputs:                                                                     *
*            ai   : matrix n*n to invert                                       *
* ---------------------------------------------------------------------------- *
*  Outputs:                                                                    *
*            a   : inverse of aa                                               *
*            ier : 1 - pause 1 in GAUSSJ - singular matrix                     *
*                  2 - pause 2 in GAUSSJ - singular matrix                     *
*******************************************************************************}
PROCEDURE gaussj_InvertMat_t_vdd(aini: t_vdd; n: integer;
                                 VAR a: t_vdd;
                                 var ier : Integer);
VAR
   big, dum, pivinv: Double;
   i, icol, irow, j, k, l, ll: integer;
   indxc, indxr, ipiv: t_vdi_p;
BEGIN
   ier := 0;
   // Initialisation
   setlength(a, n*n);
   for i := 1 to n do
     for j := 1 to n do a[(i-1)*n+(j-1)] := aini[(i-1)*n+(j-1)];
   //
   new(indxc);
   new(indxr);
   new(ipiv);
   setlength(indxc^, n);
   setlength(indxr^, n);
   setlength(ipiv^, n);
   //
   FOR j := 1 TO n DO ipiv^[j-1] := 0;
   FOR i := 1 TO n DO BEGIN
      big := 0.0;
      FOR j := 1 TO n DO
         IF ipiv^[j-1] <> 1 THEN
            FOR k := 1 TO n DO
               IF ipiv^[k-1] = 0 THEN
                  IF abs(a[(j-1)*n+(k-1)]) >= big THEN BEGIN
                     big := abs(a[(j-1)*n+(k-1)]);
                     irow := j;
                     icol := k
                  END
               ELSE IF ipiv^[k-1] > 1 THEN BEGIN
                  // writeln('pause 1 in GAUSSJ - singular matrix');
                  // readln
                  ier := 1;
               END;
      ipiv^[icol-1] := ipiv^[icol-1]+1;
      IF irow <> icol THEN BEGIN
         FOR l := 1 TO n DO BEGIN
            dum := a[(irow-1)*n+(l-1)];
            a[(irow-1)*n+(l-1)] := a[(icol-1)*n+(l-1)];
            a[(icol-1)*n+(l-1)] := dum
         END;
      END;
      indxr^[i-1] := irow;
      indxc^[i-1] := icol;
      IF a[(icol-1)*n+(icol-1)] = 0.0 THEN BEGIN
         // writeln('pause 2 in GAUSSJ - singular matrix');
         // readln
         ier := 2;
      END;
      pivinv := 1.0/a[(icol-1)*n+(icol-1)];
      a[(icol-1)*n+(icol-1)] := 1.0;
      FOR l := 1 TO n DO
         a[(icol-1)*n+(l-1)] := a[(icol-1)*n+(l-1)]*pivinv;
      FOR ll := 1 TO n DO
         IF ll <> icol THEN BEGIN
            dum := a[(ll-1)*n+(icol-1)];
            a[(ll-1)*n+(icol-1)] := 0.0;
            FOR l := 1 TO n DO
               a[(ll-1)*n+(l-1)] := a[(ll-1)*n+(l-1)]-a[(icol-1)*n+(l-1)]*dum;
         END
   END;
   FOR l := n DOWNTO 1 DO
      IF indxr^[l-1] <> indxc^[l-1] THEN
         FOR k := 1 TO n DO BEGIN
            dum := a[(k-1)*n+(indxr^[l-1]-1)];
            a[(k-1)*n+(indxr^[l-1]-1)] := a[(k-1)*n+(indxc^[l-1]-1)];
            a[(k-1)*n+(indxc^[l-1]-1)] := dum
         END;
   setlength(indxc^, 0);
   setlength(indxr^, 0);
   setlength(ipiv^, 0);
   dispose(ipiv);
   dispose(indxr);
   dispose(indxc)
END;


{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Matrix \ Matrix (backslash)
  x=A\b
  Le cas A n'est pas carr�e
  Si A n'est pas carr�e, x est une solution au sens des moindres carr�s,
  c'est � dire que norm(A*x-b) est minimale (norme euclidienne).
  Si A est de rang maximal (colonnes lin�airement ind�pendantes),
  la solution au sens des moindres carr�s, x=A\b, est unique (le vecteur x
  minimisant norm(A*x-b) est unique).
  Si A n'est pas de rang maximal, cette solution n'est pas unique,
  et x=A\b, en g�n�ral, n'est pas la solution de norme minimale
  (la solution de norme minimale est x=pinv(A)*b).
-------------------------------------------------------------------------------}
function MatrixABackSlashB (a : t_vdd; am, an : Integer;
                            b : t_vdd; bm : integer;
                            var x : t_vdd): Boolean;
var
  i, j, k : Integer;
  Anew, Bnew : t_vdd;
  Res : Boolean;
begin
  if (bm<am) then
  begin
    Result := False;
    Exit;
  end;
  try
    setlength(x, an);
    setlength(Anew, an*an);
    setlength(Bnew, an);
    // Initialisation
    for k := 1 to an do
    begin
      Bnew[k-1] := 0.0;
      for j := 1 to an do Anew[(k-1)*an+(j-1)] := 0.0;
    end;
    //
    for k := 1 to an do
    begin
      for i := 1 to am do Bnew[k-1] := Bnew[k-1]+(a[(i-1)*am+(k-1)]*b[i-1]);
      for j := 1 to an do
      begin
        for i := 1 to am do
          Anew[(k-1)*an+(j-1)] := Anew[(k-1)*an+(j-1)] + (a[(i-1)*am+(k-1)]*a[(i-1)*am+(j-1)]);
      end;
    end;
    gaussj_t_vdd(Anew, an, Bnew, 1);
    for k := 1 to an do x[k-1] := Bnew[k-1];
    setlength(Anew, 0);
    setlength(Bnew, 0);
    Res := True;
  except
    setlength(x, 0);
    setlength(Anew, 0);
    setlength(Bnew, 0);
    Res := False;
  end;

  Result := Res;
end;

{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
function MaxValueMatrixArray (a : t_vdd): Double;
var
  i : Integer;
  MaxVal : Double;
begin
  MaxVal := a[0];
  for i := 1 to (length(a)-1) do
    if (a[i]>MaxVal) then MaxVal := a[i];
  Result := MaxVal;
end;

{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
function enormvec(n : integer; x : t_vdd): Double;
Var
  i : Integer;
  temp : Double;
Begin
  temp:= 0.0 ;
  For i := 0 to n-1 do temp := temp + Sqr(x[i]);
  Result := sqrt(temp);
End;


{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
procedure ProductMatrix (a : t_vdd; am, an : Integer;
                         b : t_vdd; bm, bn : Integer;
                         var ab : t_vdd; var abm, abn : Integer);
var
  i, j, k : Integer;
begin
  if an<>bm then
  begin
    setlength(ab, 0);
    abm := -999;
    abn := -999;
    exit;
  end;

  abm := am;
  abn := bn;
  setlength(ab, am*bn);
  // Initialisation
  for i := 0 to abm-1 do
    for j := 0 to abn-1 do ab[i*abn+j] := 0.0;

  for i := 0 to abm-1 do
    for j := 0 to abn-1 do
      // Produit
      for k := 0 to an-1 do ab[i*abn+j] := ab[i*abn+j] + a[i*an+k]*b[k*bn+j];
end;
//------------------------------------------------------------------------------

{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Y = A*X + B
-------------------------------------------------------------------------------}
function Regression_Linear(n : Integer;
                           X, Y : t_vdd;
                           var A, B : Double;
                           var SST, SSR, SSE, Rsqua : Double) : boolean;
var
  i : Integer;
  Ym : Double;
  P1, P2, Q1, Q2, R1, R2 : Double;
  Y_Reg : t_vdd;
begin
  Try
    ym := 0.0;
    P1 := 0.0;
    P2 := 0.0;
    Q1 := 0.0;
    Q2 := 0.0;
    R1 := 0.0;
    R2 := 0.0;
    for i := 0 to n-1 do begin
      Ym := Ym + Y[i]/n;
      P1 := P1+sqr(X[i]);
      P2 := P2+X[i];
      Q1 := Q1+X[i];
      Q2 := Q2+1.0;
      R1 := R1+(X[i]*Y[i]);
      R2 := R2+Y[i];
    end;
    // if (Q1*P2-Q2*P1)<>0 then
    B := (R1*P2-R2*P1)/(Q1*P2-Q2*P1);
    A := (R2*Q1-R1*Q2)/(Q1*P2-Q2*P1);
    setlength(y_Reg, n);
    SST := 0.0;
    SSR := 0.0;
    SSE := 0.0;
    Rsqua := 0.0;
    for i := 0 to n-1 do begin
      y_Reg[i] := B + A*X[i];
      SST := SST + sqr(Y[i]-Ym);
      SSR := SSR + sqr(Y_Reg[i]-Ym);
      SSE := SSE + sqr(Y[i]-Y_Reg[i]);
    end;
    Rsqua := SSR/SST;
    Result := True;
  Except
    Result := False;
  end;
end;
//------------------------------------------------------------------------------


{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Y = b0 + b1*X + b2*X^2
-------------------------------------------------------------------------------}
function Regression_Polynomiale_Ordre2(n : Integer;
                                       x, y : t_vdd;
                                       var b0, b1, b2 : Double;
                                       var SST, SSR, SSE, Rsqua : Double;
                                       var y_Reg, y_derivee : t_vdd) : boolean;
var
  i, j : Integer;
  ym : Double;
  A, InverseA : t_mat;
  b, YY : t_vec;
  ok : boolean;
begin
  try
    for i := 0 to 2 do
    for j := 0 to 2 do A[i,j] := 0.0;
    for j := 0 to 2 do YY[j] := 0.0;
    ym := 0.0;
    for i := 0 to n-1 do begin
      ym := ym + y[i]/n;
      YY[0] := YY[0]+ y[i];
      YY[1] := YY[1]+ x[i]*y[i];
      YY[2] := YY[2]+ x[i]*x[i]*y[i];
      A[0,0] := A[0,0] + 1;
      A[0,1] := A[0,1] + x[i];
      A[0,2] := A[0,2] + pow(x[i],2);
      A[1,2] := A[1,2] + pow(x[i],3);
      A[2,2] := A[2,2] + pow(x[i],4);
    end;
    A[1,1] := A[0,2];
    A[2,0] := A[0,2];
    A[1,0] := A[0,1];
    A[2,1] := A[1,2];
    //
    ok := CalInverseMatrix3x3 (@A, @InverseA);
    if not(ok) then begin
      b0 := 0.0;
      b1 := 0.0;
      b2 := 0.0;
      SST := -999;
      SSR := -999;
      SSE := -999;
      setlength(y_Reg, 0);
      setlength(y_derivee, 0);
      Result := False;
    end else
    begin
      matvec(@InverseA, @YY, @b);
      b0 := b[0];
      b1 := b[1];
      b2 := b[2];
      setlength(y_Reg, n);
      setlength(y_derivee, n);
      SST := 0.0;
      SSR := 0.0;
      SSE := 0.0;
      Rsqua := 0.0;
      for i := 0 to n-1 do begin
        y_Reg[i] := b0 + b1*x[i] + b2*sqr(x[i]);
        y_derivee[i] := b1 + 2*b2*x[i];
        SST := SST + sqr(y[i]-ym);
        SSR := SSR + sqr(y_Reg[i]-ym);
        SSE := SSE + sqr(y[i]-y_Reg[i]);
      end;
      Rsqua := SSR/SST;
      Result := True;
    end;
  Except
    Result := False;
  end;
end;
//------------------------------------------------------------------------------


{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Regression Curve Fitting
  Model Function : Y = A*(1-exp(-B*X^C))
     * X, Y : data points
     * Ai, Bi, Ci : Initial value of A, B, C

  Using Gauss-Newton algorithm
  Yi := A*(1-exp(-B*Xi^C)) + ei
  -> ei := Yi - A*(1-exp(-B*Xi^C)) = fi(x) with x = (A, B, C)

  Principle: Find x = (A, B, C) for minimizing Sum(fi(x)^2)
  F(x) -> vector of n elements to minimize the sum of squares
       -> F(x) = (f1(x), f2(x),..., fn(x))T
-------------------------------------------------------------------------------}
function Curve_Fitting_CasingWear_GaussNewton(n : Integer;
                                              X, Y : t_vdd;
                                              Ai, Bi, Ci : Double;
                                              var A, B, C : Double;
                                              var SST, SSR, SSE, Rsqua : Double;
                                              var Y_Reg : t_vdd) : boolean;

var
  i, j : Integer;
  Ym : Double;
  F : t_vdd;        // Vector n elements
  GradF : t_vdd;    // Matrix n*3 - Matrix Jacobien
  GradF_T : t_vdd;  // Transpose of GradF -> Matrix 3*n
  matInverse, mat : t_vdd;   // mat = GradF_T.GradF -> 3*3
  vecTem, vec : t_vdd;       // vec = -grad_f = -GradF_T*F -> 3
  p : t_vdd;
  NorGradf : Double;           // NorGradf = |grad_f|
  mm, nn : Integer;          // Matrix Size
  iter, iterLim : Integer;
  //*********************************************************//
  function CalFunction_fi(XX, YY, AA, BB, CC : Double) : Double;
  begin
    Result := YY - AA*(1.0-exp(-BB*power(XX,CC)));
  end;
  //*********************************************************//
  function CalGradient_fi_A(XX, YY, AA, BB, CC : Double) : Double;
  begin
    Result := exp(-BB*power(XX,CC))-1.0;
  end;
  //*********************************************************//
  function CalGradient_fi_B(XX, YY, AA, BB, CC : Double) : Double;
  begin
    Result := -AA*exp(-BB*power(XX,CC))*power(XX,CC);
  end;
  //*********************************************************//
  function CalGradient_fi_C(XX, YY, AA, BB, CC : Double) : Double;
  begin
    if XX=0 then Result := 0.0
    else
    Result := -AA*exp(-BB*power(XX,CC))*BB*power(XX,CC)*ln(XX);
  end;
  //*********************************************************//
begin
  // Initial values of A, B, C
  A := Ai;
  B := Bi;
  C := Ci;
  //
  setlength(F, n);
  setlength(GradF_T, 3*n);
  iterLim := 100;
  iter := 0;
  repeat
    iter := iter+1;
    for j := 0 to n-1 do F[j] := CalFunction_fi(X[j], Y[j], A, B, C);
    // Derivatives : dfi/dA, dfi/dB, dfi/dC
    for j := 0 to n-1 do GradF_T[0*n+j] := CalGradient_fi_A(X[j], Y[j], A, B, C);
    for j := 0 to n-1 do GradF_T[1*n+j] := CalGradient_fi_B(X[j], Y[j], A, B, C);
    for j := 0 to n-1 do GradF_T[2*n+j] := CalGradient_fi_C(X[j], Y[j], A, B, C);
    // Transpose du gradient de F -> GradF_T
    TranposeMatrix (GradF_T, 3, n, GradF, mm, nn);
    // mat = GradF_T.GradF
    ProductMatrix (GradF_T, 3, n, GradF, n, 3, mat, mm, nn);
    // vec = -grad_f = GradF_T*F
    ProductMatrix (GradF_T, 3, n, F, n, 1, vecTem, mm, nn);
    MultNumberMatrix (-1.0, vecTem, 3, 1, vec, mm, nn);
    CalInverseMatrix3x3New (mat, matInverse);
    ProductMatrix (matInverse, 3, 3, vec, 3, 1, p, mm, nn);
    NorGradf := enormvec(3, vec);
    // Update A, B, C
    A := A + p[0];
    B := B + p[1];
    C := C + p[2];
    WriteMatrix('Outputs/TestMatrix.txt',  F, n, 1);
    // WriteMatrix('Outputs/TestMatrix.txt',  GradF, n, 3);
    // WriteMatrix('Outputs/TestMatrix.txt',  GradF_T, 3, n);
    // WriteMatrix('Outputs/TestMatrix.txt',  mat, 3, 3);
    // WriteMatrix('Outputs/TestMatrix.txt',  vec, 3, 1);
    // WriteMatrix('Outputs/TestMatrix.txt',  matInverse, 3, 3);
    // WriteMatrix('Outputs/TestMatrix.txt',  p, 3, 1);
    Writeln(' ', A:10:6,' ', B:10:6, ' ', C:10:6,' ||', NorGradf:10);
  until ((NorGradf<1E-12) or (iter>= iterLim));
  // Regression Results
  Ym := 0.0;
  for i := 0 to n-1 do Ym := Ym + Y[i]/n;
  SST := 0.0;
  SSR := 0.0;
  SSE := 0.0;
  Rsqua := 0.0;
  setlength(Y_Reg, n);
  for i := 0 to n-1 do begin
    Y_Reg[i] := A*(1-exp(-B*power(X[i],C)));
    SST := SST + pow((Y[i]-Ym),2);
    SSR := SSR + pow((Y_Reg[i]-Ym),2);
    SSE := SSE + pow((Y[i]-y_Reg[i]),2);
  end;
  Rsqua := SSR/SST;
  Result := True;
end;
//------------------------------------------------------------------------------


{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Regression Curve Fitting
  Model Function : Y = A*(1-exp(-B*X^C))
     * X, Y : data points
     * Ai, Bi, Ci : Initial value of A, B, C

  Using Levenberg�Marquardt algorithm
  Yi := A*(1-exp(-B*Xi^C)) + ei
  -> ei := Yi - A*(1-exp(-B*Xi^C)) = fi(x) with x = (A, B, C)

  Principle: Find x = (A, B, C) for minimizing Sum(fi(x)^2)
  F(x) -> vector of n elements to minimize the sum of squares
       -> F(x) = (f1(x), f2(x),..., fn(x))T
-------------------------------------------------------------------------------}
function Curve_Fitting_CasingWear_LevenMarqua(n : Integer;
                                               X, Y : t_vdd;
                                               Ai, Bi, Ci : Double;
                                               var A, B, C : Double;
                                               var SST, SSR, SSE, Rsqua : Double;
                                               var Y_Reg : t_vdd) : boolean;

var
  i, j : Integer;
  Ym : Double;
  ok : boolean;
  F : t_vdd;        // Vector n elements
  GradF : t_vdd;    // Matrix n*3 - Matrix Jacobien
  GradF_T : t_vdd;  // Transpose of GradF -> Matrix 3*n
  mat : t_vdd;      // mat = GradF_T.GradF + lamda*diag(GradF_T.GradF) -> 3*3
  matDiag : t_vdd;  // Diagonale of Mat
  matw1, matw2 : t_vdd;  // Work Matrix
  vecTem, vec : t_vdd;       // vec = -grad_f = -GradF_T*F -> 3
  P : t_vdd;
  NorP : Double;
  NorGradf : Double;           // NorGradf = |grad_f|
  mm, nn : Integer;          // Matrix Size
  iter, iterLim : Integer;

  lmLamda : Double;            // Levenberg�Marquardt Factor Lamda
  lmFactor : Double;           // Factor for correcting lmLamda after each iteration
  SumSquX, SumSquXplusP : Double;  // Previous and Actual Sum squares (for determining lmfactor of each iteration)

  Aw, Bw, Cw : Double;        // Work A, B, C

  //*********************************************************//
  function CalFunction_fi(XX, YY, AA, BB, CC : Double) : Double;
  begin
    Result := YY - AA*(1.0-exp(-BB*power(XX,CC)));
  end;
  //*********************************************************//
  function CalGradient_fi_A(XX, YY, AA, BB, CC : Double) : Double;
  begin
    Result := exp(-BB*power(XX,CC))-1.0;
  end;
  //*********************************************************//
  function CalGradient_fi_B(XX, YY, AA, BB, CC : Double) : Double;
  begin
    Result := -AA*exp(-BB*power(XX,CC))*power(XX,CC);
  end;
  //*********************************************************//
  function CalGradient_fi_C(XX, YY, AA, BB, CC : Double) : Double;
  begin
    if XX=0 then Result := 0.0
    else
    Result := -AA*exp(-BB*power(XX,CC))*BB*power(XX,CC)*ln(XX);
  end;
  //*********************************************************//
begin
  //
  setlength(F, n);
  setlength(GradF_T, 3*n);
  iterLim := 1000;
  iter := 0;
  lmLamda := 0.001;
  lmFactor := 10;
  // Initial values of A, B, C
  A := Ai;
  B := Bi;
  C := Ci;
  repeat
    iter := iter+1;
    Aw := A;
    Bw := B;
    Cw := C;
    for j := 0 to n-1 do F[j] := CalFunction_fi(X[j], Y[j], Aw, Bw, Cw);
    SumSquX := enormvec(n, F);
    // Derivatives : dfi/dA, dfi/dB, dfi/dC
    // Transpose du gradient de F -> GradF_T
    for j := 0 to n-1 do GradF_T[0*n+j] := CalGradient_fi_A(X[j], Y[j], Aw, Bw, Cw);
    for j := 0 to n-1 do GradF_T[1*n+j] := CalGradient_fi_B(X[j], Y[j], Aw, Bw, Cw);
    for j := 0 to n-1 do GradF_T[2*n+j] := CalGradient_fi_C(X[j], Y[j], Aw, Bw, Cw);
    TranposeMatrix (GradF_T, 3, n, GradF, mm, nn);
    // GradF_T.GradF --> matw1
    ProductMatrix (GradF_T, 3, n, GradF, n, 3, matw1, mm, nn);
    // Diag(GradF_T.GradF) --> matDiag
    ExtractDiagMatrix (matw1, 3, 3, matDiag, mm, nn);
    MultNumberMatrix (lmLamda, matDiag, 3, 3, matw2, mm, nn);
    AddMatrix (matw1, 3, 3, matw2, 3, 3, mat, mm, nn);
    // vec = -grad_f = GradF_T*F
    ProductMatrix (GradF_T, 3, n, F, n, 1, vecTem, mm, nn);
    MultNumberMatrix (-1.0, vecTem, 3, 1, vec, mm, nn);
    NorGradf := enormvec(3, vec);
    // Inverse Mat --> Matwork
    CalInverseMatrix3x3New (mat, matw2);
    ProductMatrix (matw2, 3, 3, vec, 3, 1, P, mm, nn);
    NorP := enormvec(3, P);
    Aw := A+P[0];
    Bw := B+P[1];
    Cw := C+P[2];
    for j := 0 to n-1 do F[j] := CalFunction_fi(X[j], Y[j], Aw, Bw, Cw);
    SumSquXplusP := enormvec(n, F);
    // Writeln(' ', iter:3,' ', A:10:6,' ', B:10:6, ' ', C:10:6,' ||', SumSquX:10, SumSquXplusP:10);
    if (SumSquXplusP>=SumSquX)  then
    begin
      lmLamda := lmLamda*lmFactor;
    end else
    begin
      lmLamda := lmLamda/lmFactor;
      // Update A, B, C
      A := A + p[0];
      B := B + p[1];
      C := C + p[2];
    end;
  until ((NorGradf<1E-20) or (NorP<1E-20) or (iter>= iterLim));
  // Free Memory
  setlength(F, 0);
  setlength(GradF, 0);
  setlength(GradF_T, 0);
  setlength(mat, 0);
  setlength(matDiag, 0);
  setlength(matw1, 0);
  setlength(matw2, 0);
  setlength(vecTem, 0);
  setlength(vec, 0);
  setlength(P, 0);
  // Regression Results
  Ym := 0.0;
  for i := 0 to n-1 do Ym := Ym + Y[i]/n;
  SST := 0.0;
  SSR := 0.0;
  SSE := 0.0;
  Rsqua := 0.0;
  setlength(Y_Reg, n);
  for i := 0 to n-1 do begin
    Y_Reg[i] := A*(1-exp(-B*power(X[i],C)));
    SST := SST + pow((Y[i]-Ym),2);
    SSR := SSR + pow((Y_Reg[i]-Ym),2);
    SSE := SSE + pow((Y[i]-y_Reg[i]),2);
  end;
  Rsqua := SSR/SST;
  Result := True;
end;
//------------------------------------------------------------------------------


{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Pour la formule de Gauss de l'int�gration
  --> voir page 887 � Handbook of Mathematical Functions
-------------------------------------------------------------------------------}
procedure LectureGaussFormula(NomFic: string; var x, Weight : t_vdd);
var
  i : Integer;
	fic : text;
begin
  assign(fic, NomFic);
  if FileExists(NomFic) then
  begin
    reset(fic);
    i := 0;
    repeat
      setlength(x, i+1);
      setlength(Weight, i+1);
      // Lecture
      ReadLn(fic, x[i], Weight[i]);
      i := i+1;
    until (Eof(fic));
  end else
  begin
    // D�faut : 20 points d'int�gration de Gauss
    setlength(x, 10);
    setlength(Weight, 10);
    x[0] := 0.076526521133497333755;
    x[1] := 0.227785851141645078080;
    x[2] := 0.373706088715419560673;
    x[3] := 0.510867001950827098004;
    x[4] := 0.636053680726515025453;
    x[5] := 0.746331906460150792614;
    x[6] := 0.839116971822218823395;
    x[7] := 0.912234428251325905868;
    x[8] := 0.963971927277913791268;
    x[9] := 0.993128599185094924786;

    Weight[0] := 0.152753387130725850698;
    Weight[1] := 0.149172986472603746788;
    Weight[2] := 0.142096109318382051329;
    Weight[3] := 0.131688638449176626898;
    Weight[4] := 0.118194531961518417312;
    Weight[5] := 0.101930119817240435037;
    Weight[6] := 0.083276741576704748725;
    Weight[7] := 0.062672048334109063570;
    Weight[8] := 0.040601429800386941331;
    Weight[9] := 0.017614007139152118312;
  end;
  close(fic);
end;
//------------------------------------------------------------------------------


{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>

  Pour la formule de Gauss de l'int�gration
  --> voir page 887 � Handbook of Mathematical Functions
-------------------------------------------------------------------------------}
procedure GaussFormula96Points(var x, Weight : t_vdd);
begin
    // 96 points d'int�gration de Gauss
    setlength(x, 48);
    setlength(Weight, 48);
    x[0] := 0.016276744849602969579;
    x[1] := 0.048812985136049731112;
    x[2] := 0.081297495464425558994;
    x[3] := 0.113695850110665920911;
    x[4] := 0.145973714654896941989;
    x[5] := 0.178096882367618602759;
    x[6] := 0.210031310460567203603;
    x[7] := 0.241743156163840012328;
    x[8] := 0.273198812591049141487;
    x[9] := 0.304364944354496353024;
    x[10] := 0.335208522892625422616;
    x[11] := 0.365696861472313635031;
    x[12] := 0.395797649828908603285;
    x[13] := 0.425478988407300545365;
    x[14] := 0.454709422167743008636;
    x[15] := 0.483457973920596359768;
    x[16] := 0.511694177154667673586;
    x[17] := 0.539388108324357436227;
    x[18] := 0.566510418561397168404;
    x[19] := 0.593032364777572080684;
    x[20] := 0.618925840125468570386;
    x[21] := 0.644163403784967106798;
    x[22] := 0.668718310043916153953;
    x[23] := 0.692564536642171561344;
    x[24] := 0.715676812348967626225;
    x[25] := 0.738030643744400132851;
    x[26] := 0.759602341176647498703;
    x[27] := 0.780369043867433217604;
    x[28] := 0.800308744139140817229;
    x[29] := 0.819400310737931675539;
    x[30] := 0.837623511228187121494;
    x[31] := 0.854959033434601455463;
    x[32] := 0.871388505909296502874;
    x[33] := 0.886894517402420416057;
    x[34] := 0.901460635315852341319;
    x[35] := 0.915071423120898074206;
    x[36] := 0.927712456722308690965;
    x[37] := 0.939370339752755216932;
    x[38] := 0.950032717784437635756;
    x[39] := 0.959688291448742539300;
    x[40] := 0.968326828463264212174;
    x[41] := 0.975939174585136466453;
    x[42] := 0.982517263563014677447;
    x[43] := 0.988054126329623799481;
    x[44] := 0.992543900323762624572;
    x[45] := 0.995981842987209290650;
    x[46] := 0.998364375863181677724;
    x[47] := 0.999689503883230766828;

    Weight[0] := 0.032550614492363166242;
    Weight[1] := 0.032516118713868835987;
    Weight[2] := 0.032447163714064269364;
    Weight[3] := 0.032343822568575928429;
    Weight[4] := 0.032206204794030250669;
    Weight[5] := 0.032034456231992663218;
    Weight[6] := 0.031828758894411006535;
    Weight[7] := 0.031589330770727168558;
    Weight[8] := 0.031316425596861355813;
    Weight[9] := 0.031010332586313837423;
    Weight[10] := 0.030671376123669149014;
    Weight[11] := 0.030299915420827593794;
    Weight[12] := 0.029896344136328385984;
    Weight[13] := 0.029461089958167905970;
    Weight[14] := 0.028994614150555236543;
    Weight[15] := 0.028497411065085385646;
    Weight[16] := 0.027970007616848334440;
    Weight[17] := 0.027412962726029242823;
    Weight[18] := 0.026826866725591762198;
    Weight[19] := 0.026212340735672413913;
    Weight[20] := 0.025570036005349361499;
    Weight[21] := 0.024900633222483610288;
    Weight[22] := 0.024204841792364691282;
    Weight[23] := 0.023483399085926219842;
    Weight[24] := 0.022737069658329374001;
    Weight[25] := 0.021966644438744349195;
    Weight[26] := 0.021172939892191298988;
    Weight[27] := 0.020356797154333324595;
    Weight[28] := 0.019519081140145022410;
    Weight[29] := 0.018660679627411467385;
    Weight[30] := 0.017782502316045260838;
    Weight[31] := 0.016885479864245172450;
    Weight[32] := 0.015970562902562291381;
    Weight[33] := 0.015038721026994938006;
    Weight[34] := 0.014090941772314860916;
    Weight[35] := 0.013128229566961572637;
    Weight[36] := 0.012151604671088319635;
    Weight[37] := 0.011162102099838498591;
    Weight[38] := 0.010160770535008415758;
    Weight[39] := 0.009148671230783386633;
    Weight[40] := 0.008126876925698759217;
    Weight[41] := 0.007096470791153865269;
    Weight[42] := 0.006058545504235961683;
    Weight[43] := 0.005014202742927517693;
    Weight[44] := 0.003964554338444686674;
    Weight[45] := 0.002910731817934946408;
    Weight[46] := 0.001853960788946921732;
    Weight[47] := 0.000796792065552012429;
end;
//------------------------------------------------------------------------------



// For TEST
procedure WriteMatrix(NomFic: string; MatA : t_vdd; am, an : Integer);
var
  i, j : Integer;
  fic : text;
begin
  assign(fic, NomFic);
  if FileExists(NomFic) then Append(fic)
  else Rewrite(fic);
  //
  writeln(fic, ' ', am, ' ', an);
  for i := 0 to am-1 do
  begin
    for j := 0 to an-1 do write(fic, ' ', MatA[i*an+j]:15);
    writeln(fic);
  end;
  writeln(fic);
  close(fic);

end;

end.
