(**
   Source
   http://delphiprogrammingdiary.blogspot.com/2019/04/ttaskdialog-custom-dialog-in-delphi.html
*)
unit UnitSimpleDialogs;

interface

// {$I Foundation.inc}

uses
  { Standard units }
  Vcl.ExtCtrls,
  Vcl.Forms,
  Vcl.Dialogs,

  System.Generics.Collections,
  System.Classes,

  { Component units }
  AdvToolBarStylers,
  AdvMenuStylers,

  { Linked units }
  TaskDialog
  ;

const
  VistaFont = 'Segoe UI';
  XPFont = 'Tahoma';

  // TaskDialog button definition
  BUTTON_RESULT_OK = 1;
  BUTTON_RESULT_YES = 2;
  BUTTON_RESULT_NO = 3;
  BUTTON_RESULT_CANCEL = 4;
  BUTTON_RESULT_RETRY = 5;
  BUTTON_RESULT_CLOSE = 6;
  BUTTON_RESULT_YESTOALL = 7;
  BUTTON_RESULT_NOTOALL = 8;
  BUTTON_RESULT_1 = 9;
  BUTTON_RESULT_2 = 10;
  BUTTON_RESULT_3 = 11;
  BUTTON_RESULT_4 = 12;
  BUTTON_RESULT_5 = 13;
  BUTTON_RESULT_6 = 14;
  BUTTON_RESULT_7 = 15;
  BUTTON_RESULT_8 = 16;
  BUTTON_RESULT_9 = 17;

  // TaskDialog style
  TD_STYLE_CLASSIC = 1;
  TD_STYLE_COMMAND = 2;

  // Icon options
  TD_ICON_QUESTION = tiQuestion;
  TD_ICON_WARNING = tiWarning;
  TD_ICON_ERROR = tiError;

  // Main form toolbar & menu
  MAINMENUTHEME = 6;
  TOOLBARTHEME = 8;

  procedure TaskSimpleMessage(
      const ACaption: String;
      const ATitle: String;
      const AText: String;
      const AMainIcon: Integer = tdiInformation);

  procedure TaskYesNoMessage();

  procedure TaskOKCloseMessage();

  procedure TaskCustomButtonsMessage();

implementation

uses
  { Standard units }
  Winapi.Windows,
  System.StrUtils,
  System.SysUtils,
  Vcl.Controls,
  Vcl.Graphics
  ;


procedure TaskSimpleMessage(
    const ACaption: String;
    const ATitle: String;
    const AText: String;
    const AMainIcon: Integer = tdiInformation);
var
  TaskDialog: TTaskDialog;
begin
  if Screen.ActiveForm <> nil then
  begin
    TaskDialog := TTaskDialog.Create(Screen.ActiveForm);
  end
  else
    TaskDialog := TTaskDialog.Create(Application.MainForm);

  try
    TaskDialog.Caption       := ACaption;    // 'Dialog Caption';
    TaskDialog.Title         := ATitle;      // 'Dialog Title';
    TaskDialog.Text          := AText;       // 'This is dialog text.';
    TaskDialog.MainIcon      := AMainIcon;
    TaskDialog.CommonButtons := [tcbClose];
    TaskDialog.Execute;
  finally
    TaskDialog.Free;
  end;
end;

procedure TaskYesNoMessage();
var
  TaskDialog: TTaskDialog;
begin
  if Screen.ActiveForm <> nil then
  begin
    TaskDialog := TTaskDialog.Create(Screen.ActiveForm);
  end
  else
    TaskDialog := TTaskDialog.Create(Application.MainForm);

  try
    TaskDialog.Caption := 'Error Dialog';
    TaskDialog.Title := 'Data Error';
    TaskDialog.Text := 'Invalid data. Do you want to continue ?';
    TaskDialog.MainIcon := tdiError;
    TaskDialog.CommonButtons := [tcbYes, tcbNo];
    if TaskDialog.Execute then
    begin
      if TaskDialog.ModalResult=mrYes then
        ShowMessage('Yes')
      else
        ShowMessage('No');
    end;
  finally
    TaskDialog.Free;
  end;
end;

procedure TaskOKCloseMessage();
var
  TaskDialog: TTaskDialog;
begin
  if Screen.ActiveForm <> nil then
  begin
    TaskDialog := TTaskDialog.Create(Screen.ActiveForm);
  end
  else
    TaskDialog := TTaskDialog.Create(Application.MainForm);

  try
    TaskDialog.Caption := 'Confirmation';
    TaskDialog.Title := 'Invalid Data';
    TaskDialog.Text := 'Do you want to continue ?';
    TaskDialog.MainIcon := tdiInformation;
    TaskDialog.CommonButtons := [tcbOk, tcbClose];
    TaskDialog.DefaultButton := tcbClose;
    if TaskDialog.Execute then
    begin
      if TaskDialog.ModalResult = mrOk then
        ShowMessage('Ok')
      else
        ShowMessage('Cancel');
    end;
  finally
    TaskDialog.Free;
  end;
end;

procedure TaskCustomButtonsMessage();
var
  TaskDialog: TTaskDialog;
begin
  if Screen.ActiveForm <> nil then
  begin
    TaskDialog := TTaskDialog.Create(Screen.ActiveForm);
  end
  else
    TaskDialog := TTaskDialog.Create(Application.MainForm);

  try
    TaskDialog.Title := 'Confirm removal';
    TaskDialog.Caption := 'Confirm';
    TaskDialog.Text := 'Remove selected item?';
    TaskDialog.CommonButtons := [];
    with TTaskDialogButtonItem(TaskDialog.Buttons.Add) do
    begin
      Caption := 'Demo';
      ModalResult := mrNone;
    end;
    with TTaskDialogButtonItem(TaskDialog.Buttons.Add) do
    begin
      Caption := 'Remove';
      ModalResult := mrYes;
    end;
    with TTaskDialogButtonItem(TaskDialog.Buttons.Add) do
    begin
      Caption := 'Keep';
      ModalResult := mrNo;
    end;
    // TaskDialog.OnButtonClicked := TaskDialogButtonClicked;
    TaskDialog.MainIcon := tdiInformation;
    if TaskDialog.Execute then
      if TaskDialog.ModalResult = mrYes then
        ShowMessage('Item removed');
  finally
    TaskDialog.Free;
  end
end;

end.
