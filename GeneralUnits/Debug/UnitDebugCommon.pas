{*------------------------------------------------------------------------------
  Unit for Debug & Test (Only for DAO Ngoc Ha)
  @author Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
  Create: 2013-03-26 12:00

  Copyright (c) 2001-2013 DrillScan.
  All rights reserved.
-------------------------------------------------------------------------------}
unit UnitDebugCommon;

interface

uses
  { Standard units }
  System.SysUtils,
  System.Classes,
  System.Generics.Collections,

  UnitModeleStructureBasic
  ;

  // HaDN TEST Casing Wear
function ColNumStr(var ColNumber : Integer) : String;

procedure WriteHeader(var FilMou : TextFile;
   const ALisStr : TArrayString;
   const NumSpace : Integer = -999);  // TStringList

function CountLineTextFile(const FileName : string) : Integer;

procedure FindDiffMax(const Val1, Val2 : Double; var Res : Double); Overload;
procedure FindDiffMax(const Val1, Val2 : Integer; var Res : Integer); Overload;
procedure FindDiffMax(const Val1, Val2 : Boolean; var Res : Boolean); Overload;

procedure WriteTime(elapsedTime: Integer;
    const FilePath: string;
    const Str : string = '');

function DateTimeToStrX(ADateTime: TDateTime) : String;
function RandomDoubleRange(const Val1, Val2 : Double) : Double;

procedure WriteNumber(ANumber: Integer; const FilePath: string);
procedure WriteNumber2(ANum1, ANum2: Integer; const FilePath: string);

//procedure WriteMatrix(
//    const n: Integer;
//    const Mat : t_vdd;
//    const FilePath: string);

procedure WriteArray(const FileName : string;
    const r : t_vdd);

procedure ReadArray(const FileName : string; var AArray : t_vdd);

implementation

uses
  { Standard units }
  System.IOUtils,
  System.Math,
  System.SyncObjs,
  System.DateUtils;

//----------------------------------------------------------------------------//
//----------------------------------------------------------------------------//

{-------------------------------------------------------------------------------
  Cette fonction permet d'ajouter facilement le numero de colonne quand ecrire
  un tableau a un fichier (les numeros sont automatiquement changes quand inserer
  une colonne).

  Le numero de colonne est utile quand on utilise GnuPlot pour dessiner les
  graphiques.

  @author : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
function ColNumStr(var ColNumber : Integer) : String;
begin
  Result := '['+IntToStr(ColNumber)+']';
  ColNumber := ColNumber+1;
end;

procedure WriteHeader(var FilMou : TextFile;
   const ALisStr : TArrayString;
   const NumSpace : Integer = -999);  // TStringList
var
  ColNo : Integer;
begin
  if Length(ALisStr) < 1 then
    Exit;
  // Header line
  for ColNo := 1 to Length(ALisStr) do
  begin
    if NumSpace < 0 then
      Write(FilMou, ' ', '[', IntToStr(ColNo), ']', ALisStr[ColNo-1])
    else
      Write(FilMou, ' ', '[' + IntToStr(ColNo) + ']' + ALisStr[ColNo-1]:NumSpace);
  end;
  Writeln(FilMou);
end;

function CountLineTextFile(const FileName : string) : Integer;
var
  FilMou : Textfile;
  nb : Integer;
begin
  if Not FileExists(FileName) then
    Exit(-999);

  AssignFile(FilMou, FileName);
  Reset(FilMou);

  nb := 0;
  while not Eof(FilMou) do
  begin
     ReadLn(FilMou);
     Inc(nb);
  end;
  Exit(nb);

  CloseFile(FilMou);
end;

procedure FindDiffMax(const Val1, Val2 : Double; var Res : Double);
begin
  if Abs(Val1-Val2) > Res then
    Res := Abs(Val1-Val2);
end;

procedure FindDiffMax(const Val1, Val2 : Integer; var Res : Integer);
begin
  if Abs(Val1-Val2) > Res then
    Res := Abs(Val1-Val2);
end;

procedure FindDiffMax(const Val1, Val2 : Boolean; var Res : Boolean);
begin
  if (Val1 <> Val2)  then
    Res := False;
end;

procedure WriteTime(elapsedTime: Integer;
    const FilePath: string;
    const Str : string = '');
var
  FilMou: Textfile;
begin
  AssignFile(FilMou, FilePath);
  if FileExists(FilePath) then
    Append(FilMou)
  else
    Rewrite(FilMou);
  writeln(FilMou,
             ' ', DateToStr(Now),
             ' ', TimeToStr(Now),
             ' ', elapsedTime/1000:15:3,
             ' ', elapsedTime/1000/60:15:3,
             ' ', elapsedTime:15,
             ' ', Str);
  CloseFile(FilMou);
end;

{-------------------------------------------------------------------------------
  @contact : Ngoc Ha DAO <ngoc-ha.dao@drillscan.com>
-------------------------------------------------------------------------------}
function DateTimeToStrX(ADateTime: TDateTime) : String;
var
  LFormatSettings: TFormatSettings;
begin
  LFormatSettings := FormatSettings;
  LFormatSettings.DateSeparator := '_';
  LFormatSettings.TimeSeparator := '_';
  LFormatSettings.ListSeparator := '_';
  Result := DateTimeToStr(ADateTime, LFormatSettings);
end;

function RandomDoubleRange(const Val1, Val2 : Double) : Double;
var
  LVal1, LVal2 : Double;
begin
  if Val1 < Val2 then
  begin
    LVal1 := Val1;
    LVal2 := Val2;
  end
  else
  begin
    LVal1 := Val2;
    LVal2 := Val1;
  end;
  Result := LVal1 + Random()*(LVal2-LVal1);
end;

procedure WriteNumber(ANumber: Integer; const FilePath: string);
var
  FilMou: Textfile;
begin
  AssignFile(FilMou, FilePath);
  if FileExists(FilePath) then
    Append(FilMou)
  else
    Rewrite(FilMou);
  Writeln(FilMou, ' ', ANumber:15);
  CloseFile(FilMou);
end;

procedure WriteNumber2(ANum1, ANum2: Integer; const FilePath: string);
var
  FilMou: Textfile;
begin
  AssignFile(FilMou, FilePath);
  if FileExists(FilePath) then
    Append(FilMou)
  else
    Rewrite(FilMou);
  Writeln(FilMou, ' ', ANum1:15,
                  ' ', ANum2:15);
  CloseFile(FilMou);
end;

//procedure WriteMatrix(
//    const n: Integer;
//    const Mat : t_vdd;
//    const FilePath: string);
//var
//  FilMou: Textfile;
//  i, j: Integer;
//begin
//  AssignFile(FilMou, FilePath);
//  Rewrite(FilMou);
//  Writeln(FilMou, ' ', n:10);
//  for i := 1 to n do
//  begin
//    for j := 1 to n do
//      Write(FilMou, ' ', Mat[(i-1)*n+(j-1)]:20);
//    Writeln(FilMou);
//  end;
//  CloseFile(FilMou);
//end;

procedure WriteArray(const FileName : string;
    const r : t_vdd);
var
  FilMou : Textfile;
  i : Integer;
begin
  AssignFile(FilMou, FileName);
  Rewrite(FilMou);
  Writeln(FilMou, ' # Array : ', Length(r));

  for i := 1 to Length(r) do
  begin
    Writeln(FilMou, ' ', r[i-1]:20);
  end;
  CloseFile(FilMou);
end;

procedure ReadArray(const FileName : string; var AArray : t_vdd);
var
  FilMou : Textfile;
  i, ii, nb : Integer;
  Val : Double;
begin
  AssignFile(FilMou, FileName);
  Reset(FilMou);
  SetLength(AArray, 0);

  Readln(FilMou, nb);
  SetLength(AArray, nb);
  Readln(FilMou);

  for i := 1 to nb do
  begin
    Readln(FilMou, ii, Val);
    AArray[i-1] := Val;
    if EOF(FilMou) then
      Break;
  end;
  CloseFile(FilMou);
end;

end.

